#!/bin/bash

# Building the app containers
docker-compose -f docker-compose-test.yml build

# Running the tests
docker-compose -f docker-compose-test.yml up --abort-on-container-exit
