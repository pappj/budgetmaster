import React from "react";

export default class InfoPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.close = this.close.bind(this);
  }

  close() {
    if(typeof this.props.onOk === 'function') {
      this.props.onOk();
    }

    this.props.hide();
  }

  render() {
    const errorClass = this.props.isError ? "error" : "";
    return <div key="panel" id="info-panel" className={errorClass}>
      <p>{this.props.message}</p>
      <button className="btn btn-text" onClick={this.close}>Ok</button>
    </div>
  }
}
