import { format } from "date-fns";
import Month from "./month.js";

export function convertNumberFields(object, fieldNames) {
  let ret = {...object};
  fieldNames.forEach(field => {
    if(object[field]) {
      ret[field] = parseFloat(object[field]);
    }
  });

  return ret;
}

export function convertAllObjects(objects, convertFun) {
  let ret = {};
  Object.keys(objects).forEach(id => {
    if(objects[id]) {
      ret[id] = convertFun(objects[id]);
    }
  });

  return ret;
}

export function getAvailableAmount(account) {
  switch(account.account_type) {
    case "credit":
      return account.current_balance;
    default:
      return account.current_balance + account.credit;
  }
}

export function availablePlanningAmount(accounts, expensePlans, goals) {
  const available = accounts.reduce(
    (sum, account) => sum + accountPlanningAmount(account),
    0
  );

  const allocatedForExpenses = expensePlans.reduce(
    (sum, plan) => sum + plan.amount, 0
  );

  const allocatedForGoals = goals.reduce(
    (sum, goal) => sum + goal.allocated_amount, 0
  );

  return available - (allocatedForExpenses + allocatedForGoals);
}

function accountPlanningAmount(account) {
  if(!account.in_plan) {
    return 0;
  }

  let amount;
  switch(account.account_type) {
    case "credit":
      // The spent amount decreases the planning pool
      amount = account.current_balance - account.credit;
      break;
    default:
      amount = account.current_balance;
      break;
  }

  return amount;
}

export function filterMonthPlans(plans, month) {
  const expensePlans = {};
  Object.values(plans).filter(plan => month.isEqualTo(new Month(plan.month)))
    .forEach((plan) => {
      expensePlans[plan.category_id] = plan;
    });

  return expensePlans;
}

export function maxAllocation(currentAllocation, availableAmount) {
  const available = availableAmount > 0 ? availableAmount : 0;

  return currentAllocation + available;
}

export function estimateGoalPlan(goal, monthlyAmount) {
  let fullyEstimated = false;
  let estimatedMonthlyAmount = monthlyAmount;
  let estimatedTargetAmount;
  let estimatedTargetDate;
  let monthlyAmountLabel = "Monthly amount";

  if(goal.target_amount && goal.target_date) {
    fullyEstimated = true;
    monthlyAmountLabel = "Monthly amount (estimated)";

    // TODO load this from user configuration
    const fractionalLength = 0;
    const months = monthDiff(new Date(), new Date(goal.target_date));
    if(months <= 0) {
      estimatedMonthlyAmount = goal.target_amount - goal.allocated_amount;
    } else {
      estimatedMonthlyAmount =
        Math.max((goal.target_amount - goal.allocated_amount), 0)/months;
    }

    estimatedMonthlyAmount =
      parseFloat(estimatedMonthlyAmount.toFixed(fractionalLength));
  }

  if(goal.target_amount && estimatedMonthlyAmount > 0) {
    if(estimatedMonthlyAmount > goal.target_amount - goal.allocated_amount) {
      estimatedTargetDate = "Monthly amount exceeds target";
    } else {
      const months = Math.max(Math.ceil(
        (goal.target_amount - goal.allocated_amount) / estimatedMonthlyAmount), 0);
      const today = new Date();
      const d = new Date(today.getFullYear(),
                         today.getMonth() + months,
                         today.getDate());
      estimatedTargetDate = `${d.getFullYear()}-` +
        `${(d.getMonth()+1).toString().padStart(2, "0")}-` +
      `${d.getDate().toString().padStart(2, "0")}`;
    }
  }

  if(goal.target_date) {
    const months = monthDiff(new Date(), new Date(goal.target_date));
    estimatedTargetAmount = goal.allocated_amount +
                            Math.max(months * estimatedMonthlyAmount, 0);
  }

  const amountLabelPostfix = goal.target_amount ? "" : " (estimated)";
  const dateLabelPostfix = goal.target_date ? "" : " (estimated)";

  return {
    monthlyAmount: estimatedMonthlyAmount,
    targetAmount: goal.target_amount || estimatedTargetAmount,
    targetDate: goal.target_date || estimatedTargetDate,
    labels: {
      monthlyAmount: monthlyAmountLabel,
      targetAmount: `Target amount${amountLabelPostfix}`,
      targetDate: `Target date${dateLabelPostfix}`
    },
    fullyEstimated
  };
}

function monthDiff(d1, d2) {
  return d2.getMonth()-d1.getMonth() + (d2.getFullYear()-d1.getFullYear())*12;
}

export function calculateOntrackAmount(monthlyAmount, targetAmount, targetDate) {
  const months = monthDiff(new Date(), new Date(targetDate));

  return Math.max(targetAmount - months * monthlyAmount, 0);
}

export function filterActiveGoals(goals) {
  return Object.values(goals).filter(g => !g.achieved);
}

export function convertToIdObject(array) {
  return array.reduce((acc, item) => {
    acc[item.id] = item;
    return acc;
  }, {});
}

export function selectWarnings(warningsState) {
  const ret = {...warningsState};
  delete ret["updateRequired"];

  return ret;
}

export function until(condition) {
  const check = resolve => {
    if(condition()) {
      return resolve();
    } else {
      return setTimeout(() => check(resolve), 500);
    }
  };

  return new Promise(check);
}

export function toDateOnlyString(date) {
  if(date instanceof Date) {
    return format(date, "Y-MM-dd");
  } else {
    return date;
  }
}

export function cutTime(date) {
  return new Date(toDateOnlyString(date));
}
