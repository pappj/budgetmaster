export default function Month(date = new Date(), minDate = new Date()) {
  this.date = initializeDate(date);
  this.minDate = initializeDate(minDate);

  this.nextMonth = function() {
    const d = new Date(this.date);
    d.setMonth(this.date.getMonth() + 1);

    return new Month(d, this.minDate);
  };

  this.previousMonth = function() {
    const d = new Date(this.date);
    if(this.minDate < this.date) {
      d.setMonth(this.date.getMonth() - 1);
    }

    return new Month(d, this.minDate);
  };

  this.stepBy = function(frequency) {
    let step;
    switch(frequency) {
      case "monthly":
        step = 1;
        break;
      case "quarterly":
        step = 3;
        break;
      case "yearly":
        step = 12;
        break;
    }

    const d = new Date(this.date);
    d.setMonth(d.getMonth() + step);

    return new Month(d, this.minDate);
  }

  this.isCurrentMonth = function() {
    return this.isEqualTo(new Month());
  }

  this.isEarlierThan = function(month) {
    return this.date.getFullYear() < month.date.getFullYear() ||
      (this.date.getFullYear() == month.date.getFullYear() &&
       this.date.getMonth() < month.date.getMonth());
  }

  this.isEqualTo = function(month) {
    return this.date.getFullYear() == month.date.getFullYear() &&
       this.date.getMonth() == month.date.getMonth();
  }

  this.reachedMinDate = function() {
    return !(this.minDate < this.date);
  };

  this.toString = function() {
    return `${this.date.getFullYear()} ${this.date.toLocaleString(
      'default', {month: 'long'})}`;
  }
}

function initializeDate(date) {
  const d = new Date(date);
  d.setDate(1);

  return d;
}
