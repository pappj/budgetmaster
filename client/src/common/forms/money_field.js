import React from "react";
import { connect } from "react-redux";
import { Money } from "../money.js";

class MoneyField extends Money {
  getMainClassName() {
    return "money-field";
  }

  renderAmount() {
    return <input type="text" id={this.props.name} name={this.props.name}
             value={this.props.value} onChange={this.props.onChange}
             disabled={this.props.disabled} autoComplete="off"
             className="amount" />
  }
}

const mapStateToProps = (state) => {
  const user = state.user || {};
  return {
    mainCurrency: state.currencies[user.currency_id],
    currencyString: user.currency_string,
    currencyPosition: user.currency_position,
    currencies: state.currencies
  };
};

export default connect(mapStateToProps)(MoneyField);
