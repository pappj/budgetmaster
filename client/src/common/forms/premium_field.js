import React from "react";
import { connect } from "react-redux";
import { entitledForBasic, entitledForPremium }
  from "../../utilities/subscriptions.js";

class PremiumField extends React.Component {
  renderPremiumField(level) {
    if(level == "basic" && entitledForBasic(this.props.user)) {
      return this.props.children;
    } else if(level == "premium" && entitledForPremium(this.props.user)) {
      return this.props.children;
    } else {
      return <p id={this.props.id}>Available in {level} subscriptions</p>;
    }
  }


  render() {
    const level = this.props.premiumLevel;
    if(level) {
      return this.renderPremiumField(level);
    } else {
      return this.props.children;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user || {}
  };
};

export default connect(mapStateToProps)(PremiumField);
