export function createDefaultModelInstance(fields) {
  let instance = {};

  fields.forEach((field) => {
    switch(field.type) {
      case "select":
        let options;
        if(field.dynamicOptions) {
          options = field.dynamicOptions(instance);
        } else {
          options = field.options;
        }

        instance[field.name] = defaultSelectValue(options, field.default);
        break;
      case "staticMoney":
        instance[field.name] = field.amount;
        break;
      case "static":
        instance[field.name] = field.value;
        break;
      default:
        instance[field.name] = field.default;
    }
  });

  return instance;
}

export function defaultSelectValue(options, defaultValue) {
  const keys = Object.keys(options);
  if(keys.length > 0) {
    return keys[0];
  } else {
    return defaultValue;
  }
}
