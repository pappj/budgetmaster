import React from "react";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";

class DateField extends React.Component {
  render() {
    return <DatePicker {...this.props} dateFormat={this.props.userDateFormat} />;
  }
}

const mapStateToProps = (state) => {
  const user = state.user || {};
  return {
    userDateFormat: (state.user || {}).date_format
  };
};

export default connect(mapStateToProps)(DateField);
