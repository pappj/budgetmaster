import React from "react";

import FormBody from "./form_body.jsx";
import { createDefaultModelInstance } from "./utilities.js";
import { validate } from "./validations.js";
import { processResponseErrors } from "../api_calls.js";

export default class Form extends React.Component {
  constructor(props) {
    super(props);

    let instance;
    if(props.instance == "new") {
      instance = createDefaultModelInstance(props.fields);
    } else {
      instance = props.instance;
    }

    this.state = {
      instance,
      errors: {}
    };

    this.updateInstance = this.updateInstance.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.renderApiError = this.renderApiError.bind(this);
  }

  updateInstance(newInstance) {
    this.setState({instance: newInstance});
  }

  onSubmit(evt) {
    evt.preventDefault();
    const errors = validate(this.props.fields, this.state.instance);
    if(Object.keys(errors).length === 0) {
      this.props.onSubmit(this.state.instance)
        .catch((error) => {
          const errObject = processResponseErrors(error.response);
          this.setState(errObject);
        });
    }

    this.setState({errors, apiError: null});
  }

  renderApiError() {
    let error;
    if(this.state.apiError) {
      error = <span id="generic-form-error" className="common-error">
        {this.state.apiError}
      </span>;
    }

    return error;
  }

  render() {
    return <form id={this.props.id} onSubmit={this.onSubmit}>
      {this.renderApiError()}
      <FormBody fields={this.props.fields} instance={this.state.instance}
        errors={this.state.errors} updateInstance={this.updateInstance}
        postUpdate={this.props.postUpdate}/>
      <button type="submit" className="btn btn-text">
        {this.props.submitLabel}
      </button>
    </form>;
  }
}
