import React from "react";

import CheckIcon from "../../icons/done.svg";

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if(this.props.checked) {
      return <button id={this.props.id} type="button" onClick={this.props.onClick}
               className="btn btn-icon-small btn-checked"
               disabled={this.props.disabled}>
               <CheckIcon />
             </button>;
    } else {
      return <button id={this.props.id} type="button" onClick={this.props.onClick}
               className="btn-unchecked" disabled={this.props.disabled}/>;
    }
  }
}
