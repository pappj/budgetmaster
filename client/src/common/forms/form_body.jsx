import React from "react";

import PremiumField from "./premium_field.js";
import Checkbox from "./checkbox.jsx";
import Money from "../money.js";
import MoneyField from "./money_field.js";
import UserDate from "../date.js";
import DateField from "./date_field.js";
import { defaultSelectValue } from "./utilities.js";

export default class FormBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleChange = this.handleChange.bind(this);
    this.changeDate = this.changeDate.bind(this);
    this.toggle = this.toggle.bind(this);
    this.renderField = this.renderField.bind(this);
    this.renderSelect = this.renderSelect.bind(this);
    this.renderDate = this.renderDate.bind(this);
    this.addError = this.addError.bind(this);
  }

  handleChange(evt) {
    let instance = {...this.props.instance, [evt.target.name]: evt.target.value};
    if(this.props.postUpdate) {
      instance = this.props.postUpdate(instance);
    }

    this.props.updateInstance(instance);
  }

  changeDate(newDate, fieldName) {
    let instance = {...this.props.instance, [fieldName]: newDate};
    if(this.props.postUpdate) {
      instance = this.props.postUpdate(instance);
    }

    this.props.updateInstance(instance);
  }

  toggle(evt, fieldName) {
    evt.preventDefault();
    this.props.updateInstance(
      {...this.props.instance,
       [fieldName]: !this.props.instance[fieldName]}
    );
  }

  renderField(field) {
    const instance = this.props.instance;
    const isDisabled = field.isDisabled && field.isDisabled(instance);
    const disabledClass = isDisabled ? "disabled" : "";
    const value = instance[field.name];

    let input;
    switch(field.type) {
      case "input":
        input = <input type={field.inputType} id={field.name} name={field.name}
          value={value} onChange={this.handleChange}
          disabled={isDisabled} placeholder={field.placeholder}
          className={field.className} />;
        break;
      case "text":
        input = <textarea type={field.inputType} id={field.name} name={field.name}
          value={value} onChange={this.handleChange}
          disabled={isDisabled} placeholder={field.placeholder} />;
        break;
      case "dynamicText":
        input = <p id={field.name} name={field.name}>{value}</p>;
        break;
      case "static":
        input = <p id={field.name} name={field.name}>{field.value}</p>;
        break;
      case "staticMoney":
        input = <Money id={field.name} amount={value || field.amount}
          currency={field.currency} />;
        break
      case "staticDate":
        input = <UserDate id={field.name} date={value || field.value} />;
        break
      case "money":
        input = this.renderMoneyField(field, value, isDisabled);
        break;
      case "select":
        input = this.renderSelect(field, isDisabled);
        break;
      case "date":
        input = this.renderDate(field, isDisabled);
        break;
      case "checkbox":
        input = <div className="checkbox">
          <Checkbox id={field.name} name={field.name}
            disabled={isDisabled} checked={value}
            onClick={(e) => this.toggle(e, field.name)} />
        </div>
        break;
    }

    const formField =
      <PremiumField key={field.name} premiumLevel={field.premiumLevel}
        id={field.name}>
        {this.addError(field.name, input)}
      </PremiumField>;

    if(this.props.fieldsOnly) {
      return formField;
    } else {
      return <div key={field.name} className="input-group">
        <label className={disabledClass} htmlFor={field.name}>{field.label}</label>
        {formField}
      </div>
    }
  }

  renderMoneyField(field, value, isDisabled) {
    let currency;
    if(field.dynamicCurrency) {
      currency = field.dynamicCurrency(this.props.instance);
    } else {
      currency = field.currency;
    }

    return <MoneyField name={field.name} value={value} currency={currency}
      onChange={this.handleChange} disabled={isDisabled} />;
  }

  renderSelect(field, isDisabled) {
    const instance = this.props.instance;
    let options = field.options;
    if(field.dynamicOptions) {
      options = field.dynamicOptions(instance);
      if(!Object.keys(options).includes(String(instance[field.name]))) {
        const value = defaultSelectValue(options, field.default);
        this.props.updateInstance({...instance, [field.name]: value});
      }
    }

    return <select id={field.name} name={field.name} value={instance[field.name]}
      onChange={this.handleChange} disabled={isDisabled}>
      {Object.keys(options).map((opt, idx) =>
        <option key={idx} value={opt}>{options[opt]}</option>
      )}
    </select>;
  }

  renderDate(field, isDisabled) {
    let minDate;
    let maxDate;

    if(field.dynamicMinDate) {
      minDate = field.dynamicMinDate(this.props.instance);
    }

    if(field.dynamicMaxDate) {
      maxDate = field.dynamicMaxDate(this.props.instance);
    }

    const position = field.position || "top-end";
    const currentDate = new Date(this.props.instance[field.name]);
    /* This is a bit of a hack. When a date is initialized from
     * a string, like "2021-04-16" then the time part is set to
     * "00:00". However when another date is selected in the
     * calendar, then differences to the time can be applied
     * (e.g. summer daytime savings), which could result in
     * a different date than what was selected. By setting the
     * hour to noon, hopefully this won't happen (at least it
     * has the least chance to happen) with this time
     */
    currentDate.setHours(12);

    return <div className="date-input-wrapper">
      <DateField selected={currentDate}
        onChange={(date) => this.changeDate(date, field.name)}
        minDate={minDate || field.minDate} maxDate={maxDate || field.maxDate}
        popperPlacement={position} id={field.name}
        disabled={isDisabled} fixedHeight={true}
        onFocus={(e) => e.target.readOnly=true} />
    </div>;
  }

  addError(fieldName, input) {
    const error = this.props.errors[fieldName];
    if(error) {
      return <div className="field-error">
        {input}
        <span className="error">{error}</span>
      </div>;
    } else {
      return input;
    }
  }

  render() {
    return this.props.fields.map(field => this.renderField(field));
  }
}
