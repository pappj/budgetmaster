import { getAvailableAmount } from "../utilities.js";

export const valid = {valid: true};

export function error(errString) {
  return {
    valid: false,
    error: errString
  };
}

export function validate(fields, instance) {
  let errors = {};
  fields.forEach((field) => {
    if(field.validate && (!field.isDisabled || !field.isDisabled(instance))) {
      const result = performValidations(field.validate, instance[field.name],
                                        instance);
      if(!result.valid) {
        errors[field.name] = result.error;
      }
    }
  });

  return errors;
}

function performValidations(validate, value, instance) {
  if(Array.isArray(validate)) {
    return validate.reduce((prevResult, currentValidationFun) => {
      if(prevResult.valid) {
        return currentValidationFun(value, instance);
      } else {
        return prevResult;
      }
    }, valid);
  } else {
    return validate(value, instance);
  }
}

export function nonEmpty(string) {
  if(string === "") {
    return error("can't be blank")
  } else {
    return valid;
  }
}

export function minLength(length) {
  return function(field) {
    if(field.trim().length < length) {
      return error("Too short (min " + length + " characters)");
    } else {
      return valid;
    }
  };
}

export function nonNegative(value) {
  if(value < 0) {
    return error("can't be negative");
  } else {
    return valid;
  }
}

export function positive(value) {
  if(value > 0) {
    return valid;
  } else {
    return error("must be positive");
  }
}

export function maximum(maxValue) {
  return function(value) {
    if(value > maxValue) {
      return error(`can't be more than ${maxValue}`);
    } else {
      return valid;
    }
  }
}

export function email(value) {
  const pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/;
  if(!pattern.test(value)) {
    return error("Invalid email address");
  } else {
    return valid;
  }
}

export function minDate(date = new Date()) {
  return function(value) {
    const dateStr = new Date(date.getTime() - (date.getTimezoneOffset()*60000)).toISOString().substr(0, 10);
    if(value < dateStr) {
      return error("can't be earlier than " + dateStr);
    } else {
      return valid;
    }
  }
}

export function accountChange(accounts, origStringAccount, origAmount) {
  return function(newAccount, instance) {
    const origAccount = parseInt(origStringAccount);

    if(!instance.updateAccount || origAccount == newAccount) {
      return valid;
    } else {
      const acc = accounts[origAccount];
      if(getAvailableAmount(acc) < origAmount) {
        return error(`can't be changed. Original amount (${origAmount}) ` +
                     `must be available on '${acc.name}' or don't update ` +
                     `the accounts`);
      } else {
        return valid;
      }
    }
  }
}

export function amountAdditionChange(accountField, accounts, origStringAccount,
                                     origAmount) {
  return function(newStringAmount, instance) {
    const newAmount = parseFloat(newStringAmount);

    if(!instance.update_account || newAmount == origAmount) {
      return valid;
    }

    const origAccount = parseInt(origStringAccount);

    if(instance[accountField] == origAccount) {
      const acc = accounts[origAccount];
      const available = getAvailableAmount(acc);
      if(available + newAmount < origAmount) {
        return error(`must be at least ${origAmount-available} or ` +
                     `'${acc.name}' must cover the change or don't ` +
                     `update the accounts`);
      } else {
        return valid;
      }
    } else {
      return valid;
    }
  }
}

export function amountSubtraction(accountField, accounts, origStringAccount = null,
                                  origAmount = 0) {
  return function(newStringAmount, instance) {
    if(!instance.update_account) {
      return valid;
    }

    const newAmount = parseFloat(newStringAmount);
    const origAccount = parseInt(origStringAccount);

    const accountId = instance[accountField];
    const acc = accounts[accountId];
    const available = getAvailableAmount(acc);
    let maxAmount = available;
    if(accountId == origAccount) {
      maxAmount += origAmount;
    }

    if(maxAmount < newAmount) {
      return error(`must be at most ${maxAmount} or '${acc.name}' ` +
                   `must cover it or don't update the accounts`);
    } else {
      return valid;
    }
  }
}
