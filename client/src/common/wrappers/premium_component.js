import { connect } from "react-redux";

import PremiumComponentView from "./premium_component_view.jsx";

const mapStateToProps = (state, ownProps) => {
  return {
    isLoaded: ownProps.isLoaded && state.user,
    user: state.user
  };
};

const PremiumComponent = connect(
  mapStateToProps,
)(PremiumComponentView);

export default PremiumComponent;
