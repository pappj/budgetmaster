import React from "react";

import { hasBasicSubscription, entitledForBasic, entitledForPremium }
  from "../../utilities/subscriptions.js";

export default class PremiumComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    switch(props.level) {
      case "onlyBasic":
        this.validate = hasBasicSubscription;
        this.subPackage = "basic or premium";
        break;
      case "basic":
        this.validate = entitledForBasic;
        this.subPackage = "basic or premium";
        break;
      case "premium":
        this.validate = entitledForPremium;
        this.subPackage = "premium";
        break
    }
  }

  renderNotice() {
    let notice;
    const title = <h2>{this.props.name}</h2>
    const text = <p className="upgrade-notice">
      {this.props.name} are available only in {this.subPackage} subscriptions.
    </p>;
    const button = <button className="btn btn-text">Upgrade</button>;

    switch(this.props.noticeType) {
      case "regular":
        notice = <React.Fragment>
          {title}
          {text}
        </React.Fragment>;
        break;
      case "regularWithButton":
        notice = <React.Fragment>
          {title}
          {text}
          {button}
        </React.Fragment>;
        break;
      case "fullPage":
        notice = <div className="panel">
          {title}
          {text}
          {button}
        </div>;
        break;
      case "empty":
        notice = null;
        break;
    }

    return notice;
  }

  render() {
    if(this.props.isLoaded && this.validate(this.props.user)) {
      return this.props.children;
    } else if(this.props.isLoaded) {
      return this.renderNotice();
    } else {
      return <React.Fragment>
        <h2>{this.props.name}</h2>
        <p>Loading...</p>
      </React.Fragment>;
    }
  }
}
