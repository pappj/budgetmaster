import React from "react";

import UpIcon from "../icons/up.svg";

export default class PanelToggleButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let btnClass = "btn btn-icon toggle-btn";
    let up;

    if(this.props.active) {
      btnClass = "btn-active btn-icon toggle-btn";
    }

    if(this.props.active && !this.props.hideMarker) {
      up = <UpIcon className="up" />;
    }

    return <div className="btn-group">
      <button id={this.props.id} className={btnClass} onClick={this.props.toggle}>
        {this.props.icon}
      </button>
      <span>{this.props.label}</span>
      {up}
    </div>;
  }
}
