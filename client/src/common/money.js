import React from "react";
import { connect } from "react-redux";

export class Money extends React.Component {
  getMainClassName() {
    const warning = this.props.warning ? " warning" : "";
    const danger = this.props.danger ? " danger" : "";

    return `money${warning}${danger}`;
  }

  getCurrency() {
    const ccy = this.props.currency;
    if(ccy == "user_main") {
      return this.props.mainCurrency;
    } else {
      return this.props.currencies[ccy];
    }
  }

  renderCurrency() {
    return <span key="curr" className="currency" onClick={this.props.onClick}>
      {this.getCurrency()[this.props.currencyString]}
    </span>
  }

  renderAmount() {
    return <span key="amnt" className="amount" onClick={this.props.onClick}>
      {this.props.amount}
    </span>
  }

  renderContent() {
    if(this.props.currencyPosition == "before") {
      return <React.Fragment>
        {this.renderCurrency()}
        {this.renderAmount()}
      </React.Fragment>;
    } else if(this.props.currencyPosition == "after" ) {
      return <React.Fragment>
        {this.renderAmount()}
        {this.renderCurrency()}
      </React.Fragment>
    }
  }

  render() {
    return <div className={this.getMainClassName()}>
      {this.renderContent()}
    </div>;
  }
}

const mapStateToProps = (state) => {
  const user = state.user || {};
  return {
    mainCurrency: state.currencies[user.currency_id],
    currencyString: user.currency_string,
    currencyPosition: user.currency_position,
    currencies: state.currencies
  };
};

export default connect(mapStateToProps)(Money);
