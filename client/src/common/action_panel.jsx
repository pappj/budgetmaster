import React from "react";

import CloseIcon from "../icons/close.svg";

export default class ActionPanel extends React.Component {
  render() {
    return <React.Fragment>
      <div key="panel" id="action-panel" className={this.props.className}>
        <button id="close-btn" onClick={this.props.hide}><CloseIcon /></button>
        {this.props.children}
      </div>
      <div key="shadow" id="shadow-box" />
    </React.Fragment>;
  }
}
