import axios from "axios";
import { removeUser, updateUserData } from "../actions/user_actions.js";

const api = (function() {
  function get(dispatch, url, params) {
    return axios.get(url, {params})
      .catch((error) => redirectOnError(error, dispatch));
  }

  function post(dispatch, url, data) {
    return axios.post(url, data)
      .catch((error) => redirectOnError(error, dispatch));
  }

  function patch(dispatch, url, data) {
    return axios.patch(url, data)
      .catch((error) => redirectOnError(error, dispatch));
  }

  function remove(dispatch, url, data = undefined) {
    return axios.delete(url, {data})
      .catch((error) => redirectOnError(error, dispatch));
  }

  function redirectOnError(error, dispatch) {
    const status = error.response.status;
    if(status == 401) {
      dispatch(removeUser());
      return undefined;
    } else if(status == 403 && error.response.data.subscriptionNotSufficient) {
      dispatch(updateUserData(error.response.data.user));
      throw error;
    } else {
      throw error;
    }
  }

  return {
    get,
    post,
    patch,
    delete: remove
  }
}());

export default api;

export const genericError = "An internal error happened. " +
  "Try again later or contact support.";

const networkError = "The server couldn't be reached. " +
  "Try again later or contact support.";

export function processResponseErrors(resp) {
  if(resp) {
    const errors = resp.data.errors;
    const apiError = resp.data.apiError;
    if(errors && errors.base) {
      return {apiError: errors.base[0]};
    } else if(errors) {
      return {errors};
    } else if(apiError) {
      return {apiError};
    } else {
      return {apiError: genericError};
    }
  } else {
    return {apiError: networkError};
  }
}
