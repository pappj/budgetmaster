import React from "react";
import { connect } from "react-redux";
import format from "date-fns/format";

class UserDate extends React.Component {
  render() {
    let date = "";
    try {
      date = format(new Date(this.props.date), this.props.userFormat);
    } catch(err) {
      date = this.props.date;
    }

    return <span id={this.props.id} className={this.props.className}
      onClick={this.props.onClick}>
      {date}
    </span>;
  }
}


const mapStateToProps = (state) => {
  const user = state.user || {};
  return {
    userFormat: state.user.date_format
  };
};

export default connect(mapStateToProps)(UserDate);
