import { connect } from "react-redux";

import SignUpFormView from "./sign_up_form_view.jsx";

import { updateUserData } from "../actions/user_actions.js";

const mapStateToProps = (state) => {
  return {
    currencies: state.currencies
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (user) => dispatch(updateUserData(user))
  };
};

const SignUpForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpFormView);

export default SignUpForm;
