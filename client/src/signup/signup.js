import React from "react";

import SignupView from "./signup_view.jsx";

import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    isLoaded: state.loaded.currencies,
    signedIn: state.user
  };
};

const Signup = connect(
  mapStateToProps
)(SignupView);

export default Signup;
