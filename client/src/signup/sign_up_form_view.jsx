import React from "react";
import axios from "axios";

import { signIn } from "../actions/user_actions.js";

import Form from "../common/forms/form.jsx";
import { email, minLength } from "../common/forms/validations.js";

export default class SignUpFormView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.signUp = this.signUp.bind(this);

    this.fields = [
      {
        name: "email",
        label: "Email",
        type: "input",
        inputType: "text",
        default: "",
        placeholder: "your@email.com",
        validate: email
      },
      {
        name: "password",
        label: "Password",
        type: "input",
        inputType: "password",
        default: "",
        validate: minLength(6)
      },
      {
        name: "password2",
        label: "Password again",
        type: "input",
        inputType: "password",
        default: "",
        validate: this.checkPasswordMatch
      },
      {
        name: "currency_id",
        label: "Main currency",
        type: "select",
        options: Object.values(props.currencies).reduce((acc, curr) =>
          ({...acc, [curr.id]: curr.short_code + " (" + curr.symbol + ")"}), {}),
        default: ""
      }
    ];
  }

  checkPasswordMatch(pwd, instance) {
    if(pwd !== instance.password) {
      return {
        valid: false,
        error: "Passwords don't match"
      };
    } else {
      return {valid: true};
    }
  }

  signUp(user) {
    return axios.post("/api/users",{user})
      .then((response) => {
        this.props.signIn(response.data);
      });
  };

  render() {
    return <section id="home-content">
      <Form id="sign-up" onSubmit={this.signUp} submitLabel="Sign up"
        fields={this.fields} instance="new" />
    </section>;
  }
}
