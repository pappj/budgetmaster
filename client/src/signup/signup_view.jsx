import React from "react";
import { Navigate, Link } from "react-router-dom"

import SignUpForm from "./sign_up_form.js";

export default class SignUpView extends React.Component {
  render() {
    if(this.props.signedIn) {
      return <Navigate to="/basics" />;
    } else {
      let form;
      if(this.props.isLoaded) {
        form = <SignUpForm />;
      }

      return <React.Fragment>
        <h1>Sign up</h1>
        {form}
        <Link to="/" className="nav-link">Back to the home page</Link>
      </React.Fragment>
    }
  }
}
