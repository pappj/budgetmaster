import { connect } from "react-redux";

import { createIncome, patchIncome } from "../actions/income_actions.js";

import IncomeFormPanelView from "./income_form_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    incomes: state.incomes,
    accounts: state.accounts
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createIncome: (income) => dispatch(createIncome(income)),
    patchIncome: (income) => dispatch(patchIncome(income))
  }
};

const IncomeFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(IncomeFormPanelView);

export default IncomeFormPanel;
