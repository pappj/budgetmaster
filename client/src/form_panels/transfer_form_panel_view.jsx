import React from "react";
import FormPanel from "./form_panel.jsx";
import { positive, accountChange, amountAdditionChange, amountSubtraction }
  from "../common/forms/validations.js";

export default class TransferFormPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.instance = props.instance == "new" ? "new" :
                    props.transfers[props.instance];

    this.getSourceCurrencyId = this.getSourceCurrencyId.bind(this);
    this.getTargetCurrencyId = this.getTargetCurrencyId.bind(this);
    this.getTargetAccountOptions = this.getTargetAccountOptions.bind(this);
    this.sameCurrency = this.sameCurrency.bind(this);
    this.postUpdate = this.postUpdate.bind(this);

    let sourceAmountValidations = [positive];
    let targetAccountValidation;
    let targetAmountValidations = [positive];
    if(props.instance == "new") {
      sourceAmountValidations = amountSubtraction("source_account_id",
        props.accounts);
    } else {
      sourceAmountValidations = [positive, amountSubtraction("source_account_id",
        props.accounts, this.instance.source_account_id,
        this.instance.source_amount)];
      targetAccountValidation = accountChange(props.accounts,
        this.instance.target_account_id, this.instance.target_amount);
      targetAmountValidations = [positive,
        amountAdditionChange("target_account_id", props.accounts,
          this.instance.target_account_id, this.instance.target_amount)];
    }



    this.fields = [
      {
        name: "source_account_id",
        label: "Source account",
        type: "select",
        options: Object.values(props.accounts).reduce((accum, account) =>
          ({...accum, [account.id]: account.name}), {}),
        default: ""
      },
      {
        name: "source_amount",
        label: "Source amount",
        type: "money",
        default: "0",
        dynamicCurrency: this.getSourceCurrencyId,
        validate: sourceAmountValidations
      },
      {
        name: "target_account_id",
        label: "Target account",
        type: "select",
        options: {},
        dynamicOptions: this.getTargetAccountOptions,
        default: "",
        validate: targetAccountValidation
      },
      {
        name: "target_amount",
        label: "Target amount",
        type: "money",
        default: "0",
        dynamicCurrency: this.getTargetCurrencyId,
        validate: targetAmountValidations,
        dynamicValue: this.getTargetAmount,
        isDisabled: this.sameCurrency
      },
      {
        name: "date",
        label: "Date",
        type: "date",
        default: new Date(),
        maxDate: new Date()
      },
      {
        name: "note",
        label: "Note",
        type: "text",
        default: "",
        placeholder: "e.g. Credit payment, cash exchange, etc."
      },
      {
        name: "update_account",
        label: "Update accounts balance",
        type: "checkbox",
        default: props.instance == "new"
      }
    ]
  }

  getSourceCurrencyId(instance) {
    const sourceAccount = this.props.accounts[instance.source_account_id];
    return sourceAccount.currency_id;
  }

  getTargetCurrencyId(instance) {
    const targetAccount = this.props.accounts[instance.target_account_id];
    return targetAccount.currency_id;
  }

  getTargetAccountOptions(instance) {
    return Object.keys(this.props.accounts).filter(
      id => id != instance.source_account_id
    ).reduce((accum, id) => {
      return {...accum, [id]: this.props.accounts[id].name}
    }, {});
  }

  isTargetAmountDisabled(instance) {
    return this.sameCurrency(instance);
  }

  sameCurrency(inst) {
    return this.getSourceCurrencyId(inst) == this.getTargetCurrencyId(inst);
  }

  postUpdate(instance) {
    if(this.sameCurrency(instance)) {
      return {...instance, target_amount: instance.source_amount};
    } else {
      return instance;
    }
  }

  render() {
    return <FormPanel hide={this.props.hide} instance={this.instance}
             create={this.props.createTransfer} patch={this.props.patchTransfer}
             fields={this.fields} postUpdate={this.postUpdate}
             postSubmit={this.props.postSubmit} model="Transfer" />;
  }
}
