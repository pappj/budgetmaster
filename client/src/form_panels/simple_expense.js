import { connect } from "react-redux";
import { createExpense, patchExpense } from "../actions/expense_actions.js";

import SimpleExpenseView from "./simple_expense_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts,
    categories: state.categories,
    expenses: state.expenses
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createExpense: (expense) => dispatch(createExpense(expense)),
    patchExpense: (expense) => dispatch(patchExpense(expense))
  };
};

const SimpleExpense = connect(
  mapStateToProps,
  mapDispatchToProps
)(SimpleExpenseView);

export default SimpleExpense;
