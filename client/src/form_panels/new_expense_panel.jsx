import React from "react";

import ActionPanel from "../common/action_panel.jsx";
import SimpleExpense from "./simple_expense.js";

const SIMPLE = "simple";
const COMPLEX = "complex";

export default class NewExpensePanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: SIMPLE
    };
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2>New expense</h2>
      <SimpleExpense hide={this.props.hide} instance="new" />
    </ActionPanel>;
  }
}
