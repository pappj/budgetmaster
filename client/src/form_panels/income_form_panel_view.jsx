import React from "react";

import { positive, accountChange, amountAdditionChange }
  from "../common/forms/validations.js";
import FormPanel from "./form_panel.jsx";

export default class IncomeFormPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.instance = props.instance == "new" ? "new" :
                      props.incomes[props.instance];

    this.getCurrencyId = this.getCurrencyId.bind(this);
    let accountValidation;
    let amountValidation = positive;
    if(props.instance != "new") {
      accountValidation = accountChange(props.accounts, this.instance.account_id,
        this.instance.amount);
      amountValidation = [positive, amountAdditionChange("account_id",
        props.accounts, this.instance.account_id, this.instance.amount)];
    }

    this.fields = [
      {
        name: "account_id",
        label: "Account",
        type: "select",
        options: Object.values(props.accounts).reduce((accum, account) =>
          ({...accum, [account.id]: account.name}), {}),
        default: "",
        validate: accountValidation
      },
      {
        name: "amount",
        label: "Amount",
        type: "money",
        default: "0",
        dynamicCurrency: this.getCurrencyId,
        validate: amountValidation
      },
      {
        name: "date",
        label: "Date",
        type: "date",
        default: new Date(),
        maxDate: new Date()
      },
      {
        name: "note",
        label: "Note",
        type: "text",
        default: "",
        placeholder: "e.g. Payment"
      },
      {
        name: "update_account",
        label: "Update account balance",
        type: "checkbox",
        default: props.instance == "new"
      }
    ];
  }

  getCurrencyId(instance) {
    return this.props.accounts[instance.account_id].currency_id;
  }

  render() {
    return <FormPanel hide={this.props.hide} instance={this.instance}
             create={this.props.createIncome} patch={this.props.patchIncome}
             postSubmit={this.props.postSubmit} fields={this.fields}
             model="Income" />;
  }
}
