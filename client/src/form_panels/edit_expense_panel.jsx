import React from "react";

import ActionPanel from "../common/action_panel.jsx";
import SimpleExpense from "./simple_expense.js";

export default class EditExpensePanel extends React.Component {
  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2>Edit expense</h2>
      <SimpleExpense hide={this.props.hide} instance={this.props.instance}
        postSubmit={this.props.postSubmit} />
    </ActionPanel>;
  }
}
