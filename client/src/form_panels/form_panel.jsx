import React from "react";
import ActionPanel from "../common/action_panel.jsx";
import Form from "../common/forms/form.jsx";

export default class FormPanel extends React.Component {
  constructor(props) {
    super(props);

    this.getTitle = this.getTitle.bind(this);
    this.getLabel = this.getLabel.bind(this);
    this.getSubmit = this.getSubmit.bind(this);
  }

  getTitle() {
    if(this.props.instance == "new") {
      return "New " + this.props.model;
    } else {
      return "Edit " + this.props.model;
    }
  }

  getLabel() {
    if(this.props.instance == "new") {
      return "Add";
    } else {
      return "Save";
    }
  }

  getSubmit() {
    let action;
    if(this.props.instance == "new") {
      action = this.props.create;
    } else {
      action = this.props.patch;
    }

    return (instance) => {
      return action(instance)
        .then((responseData) => {
          if(typeof this.props.postSubmit === 'function') {
            this.props.postSubmit(responseData);
          }
          this.props.hide();
        });
    };
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">{this.getTitle()}</h2>
      <Form key="form" instance={this.props.instance}
        fields={this.props.fields} postUpdate={this.props.postUpdate}
        submitLabel={this.getLabel()} onSubmit={this.getSubmit()} />
    </ActionPanel>;
  }
}
