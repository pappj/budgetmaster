import { connect } from "react-redux";
import { createTransfer, patchTransfer } from "../actions/transfer_actions.js";

import TransferFormPanelView from "./transfer_form_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts,
    transfers: state.transfers
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createTransfer: (transfer) => dispatch(createTransfer(transfer)),
    patchTransfer: (transfer) => dispatch(patchTransfer(transfer))
  };
};

const TransferFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(TransferFormPanelView);

export default TransferFormPanel;
