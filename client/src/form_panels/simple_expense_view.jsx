import React from "react";

import Form from "../common/forms/form.jsx";
import { positive, amountSubtraction } from "../common/forms/validations.js";

export default class SimpleExpenseView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.instance = props.instance == "new" ? "new" :
                      props.expenses[props.instance];

    this.getCurrencyId = this.getCurrencyId.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    let amountValidation;
    if(props.instance == "new") {
      amountValidation = amountSubtraction("account_id", props.accounts);
    } else {
      amountValidation = amountSubtraction("account_id", props.accounts,
        this.instance.account_id, this.instance.amount);
    }

    this.fields = [
      {
        name: "account_id",
        label: "Account",
        type: "select",
        options: Object.values(props.accounts).reduce((accum, account) =>
          ({...accum, [account.id]: account.name}), {}),
        default: ""
      },
      {
        name: "category_id",
        label: "Category",
        type: "select",
        options: Object.values(props.categories).reduce((accum, cat) =>
          ({...accum, [cat.id]: cat.name}), {}),
        default: ""
      },
      {
        name: "amount",
        label: "Amount",
        type: "money",
        default: "0",
        dynamicCurrency: this.getCurrencyId,
        validate: [positive, amountValidation]
      },
      {
        name: "date",
        label: "Date",
        type: "date",
        default: new Date(),
        maxDate: new Date()
      },
      {
        name: "note",
        label: "Note",
        type: "text",
        default: "",
        placeholder: "What did you buy?"
      },
      {
        name: "update_account",
        label: "Update account balance",
        type: "checkbox",
        default: true
      }
    ];
  }

  getLabel() {
    if(this.props.instance == "new") {
      return "Add";
    } else {
      return "Save";
    }
  }

  getCurrencyId(instance) {
    return this.props.accounts[instance.account_id].currency_id;
  }

  onSubmit(instance) {
    let action;
    if(this.props.instance == "new") {
      action = this.props.createExpense;
    } else {
      action = this.props.patchExpense;
    }

    return action({
      ...instance,
      currencyId: this.getCurrencyId(instance)
    })
      .then((responseData) => {
        if(typeof this.props.postSubmit === 'function') {
          this.props.postSubmit(responseData);
        }
        this.props.hide();
      });
  }

  render() {
    return <Form instance={this.instance} fields={this.fields}
      submitLabel={this.getLabel()} onSubmit={this.onSubmit} />;
  }
}
