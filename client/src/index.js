import React from "react";
import ReactDOM from "react-dom";
import {Route, Redirect, Routes, BrowserRouter as Router} from "react-router-dom";
import axios from "axios";

import ProtectedRoute from "./navbar/protected_route.js";

import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers/reducers.js";
import { signedIn } from "./actions/user_actions.js";

import NavBar from "./navbar/navbar.js";
import Home from "./home/home.js";
import Signup from "./signup/signup.js";
import Summary from "./summary/summary.jsx";
import Basics from "./basics/basics.jsx";
import Transactions from "./transactions/transactions.js";
import Planning from "./planning/planning.jsx";
import Profile from "./profile/profile.js";

import "./css/app.scss";

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(thunkMiddleware)));

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userChecked: false
    };
  }

  componentDidMount() {
    store.dispatch(signedIn())
      .then((response) => {
        this.setState({userChecked: true});
      });
  }

  render() {
    const protectedRoute =
      <ProtectedRoute userChecked={this.state.userChecked} />;
    return <Provider store={store}>
      <Router>
        <NavBar />

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/" element={protectedRoute}>
            <Route path="/summary" element={<Summary />} />
            <Route path="/basics" element={<Basics />} />
            <Route path="/transactions" element={<Transactions />} />
            <Route path="/planning" element={<Planning />} />
            <Route path="/profile" element={<Profile />} />
          </Route>
        </Routes>
      </Router>
    </Provider>;
  }
};

axios.defaults.headers.common = {
  "accept": "application/json",
  "responseType": "json",
  "X-Requested-With": "XMLHttpRequest"
};

axios.defaults.xsrfCookieName = "CSRF-PROTECTION"
axios.defaults.xsrfHeaderName = "X-CSRF-Token"
ReactDOM.render(<Index />, document.getElementById("react"));

window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});
