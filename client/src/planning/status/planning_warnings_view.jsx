import React from "react";

import NotificationPanel from "../../notifications/notification_panel.jsx";

export default class PlanningWarningsView extends React.Component {
  componentDidMount() {
    this.props.updateGoalWarnings();
    this.props.updateExpenseWarnings();
  }

  componentDidUpdate(prevProps) {
    if(this.props.goalUpdateRequired == true &&
       prevProps.goalUpdateRequired == false) {
      this.props.updateGoalWarnings();
    }
    if(this.props.expenseUpdateRequired == true &&
       prevProps.expenseUpdateRequired == false) {
      this.props.updateExpenseWarnings();
    }
  }

  addGoalWarnings(allWarnings) {
    const goalWarnings = this.props.goalWarnings;
    Object.keys(goalWarnings).forEach(id => {
      allWarnings[`goal_${id}`] = goalWarnings[id];
    });
  }

  addExpenseWarnings(allWarnings) {
    const expenseWarnings = this.props.expenseWarnings;
    Object.keys(expenseWarnings).forEach(id => {
      allWarnings[`exp_${id}`] = expenseWarnings[id];
    });
  }

  getAllWarnings() {
    const allWarnings = {};
    this.addGoalWarnings(allWarnings);
    this.addExpenseWarnings(allWarnings);
    return allWarnings;
  }

  render() {
    const warnings = this.getAllWarnings();
    if(Object.values(warnings).length > 0) {
      return <NotificationPanel items={warnings} />;
    } else {
      return null;
    }
  }
}
