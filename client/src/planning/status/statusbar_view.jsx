import React from "react";

import Money from "../../common/money.js";

export default class StatusbarView extends React.Component {
  componentDidMount() {
    this.props.updateAvailableAmount();
  }

  componentDidUpdate(prevProps) {
    if(this.props.updateRequired == true && prevProps.updateRequired == false) {
      this.props.updateAvailableAmount();
    }
  }

  renderContent() {
    if(this.props.accountsLength == 0) {
      return <span>No accounts included in your plan</span>;
    }

    const amount = this.props.availableAmount;
    if(amount == 0) {
      return <span className="success">All of your money is planned</span>;
    }

    if(amount > 0) {
      return <React.Fragment>
        <span>Amount to allocate:</span>
        <Money amount={amount} currency="user_main"/>
      </React.Fragment>
    }

    if(amount < 0) {
      return <React.Fragment>
        <span className="warning">Overallocated by</span>
        <Money amount={Math.abs(amount)} currency="user_main"
          warning={true}/>
      </React.Fragment>
    }
  }

  render() {
    if(this.props.isLoaded) {
      return <div key="status" id="status">
        {this.renderContent()}
      </div>;
    } else {
      return <p>Loading statusbar...</p>;
    }
  }
}
