import { connect } from "react-redux";

import { updateAvailableAmount } from "../../actions/planning_actions.js";

import StatusbarView from "./statusbar_view.jsx";

import { availablePlanningAmount, filterActiveGoals }
  from "../../common/utilities.js";

const mapStateToProps = (state) => {
  return {
    accountsLength: Object.values(state.accounts).filter(a => a.in_plan).length,
    availableAmount: state.planning.availableAmount.amount,
    updateRequired: state.planning.availableAmount.updateRequired,
    isLoaded: state.loaded.accounts && state.loaded.currencies && state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAvailableAmount: () => dispatch(updateAvailableAmount())
  };
};

const Statusbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusbarView);

export default Statusbar;
