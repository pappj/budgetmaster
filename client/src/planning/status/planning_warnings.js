import { connect } from "react-redux";

import { updateGoalWarnings, updateExpenseWarnings}
  from "../../actions/warning_actions.js";

import PlanningWarningsView from "./planning_warnings_view.jsx";

import { selectWarnings } from "../../common/utilities.js";

const mapStateToProps = (state) => {
  const warnings = state.planning.warnings;
  return {
    goalUpdateRequired: warnings.goals.updateRequired,
    goalWarnings: selectWarnings(warnings.goals),
    expenseUpdateRequired: warnings.expenses.updateRequired,
    expenseWarnings: selectWarnings(warnings.expenses)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateGoalWarnings: () => dispatch(updateGoalWarnings()),
    updateExpenseWarnings: () => dispatch(updateExpenseWarnings()),
  };
};

const PlanningWarnings = connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanningWarningsView);

export default PlanningWarnings;
