import { connect } from "react-redux";

import { fetchGoals, patchGoal } from "../../actions/goal_actions.js";
import { fetchAccounts } from "../../actions/account_actions.js";
import { fetchExpensePlans } from "../../actions/expense_plan_actions.js";

import GoalAllocationFormPanelView from "./goal_allocation_form_panel_view.jsx";

import { availablePlanningAmount, filterActiveGoals }
  from "../../common/utilities.js";

const mapStateToProps = (state, ownProps) => {
  const accounts = Object.values(state.accounts).filter(acc => acc.in_plan);
  const activeGoals = filterActiveGoals(state.goals);
  const expensePlans = Object.values(state.expensePlans);

  return {
    availableAmount: availablePlanningAmount(accounts, expensePlans, activeGoals),
    isLoaded: state.loaded.accounts && state.loaded.goals &&
              state.loaded.expensePlans && state.loaded.currencies && state.user,
    instance: state.goals[ownProps.goalId]
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAllocation: (id, amount) => dispatch(
      patchGoal({id, allocated_amount: amount})
    ),
    // for availableAmount
    fetchAccounts: () => dispatch(fetchAccounts()),
    fetchExpensePlans: () => dispatch(fetchExpensePlans()),
    fetchGoals: () => dispatch(fetchGoals())
  };
};

const GoalAllocationFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalAllocationFormPanelView);

export default GoalAllocationFormPanel;
