import { connect } from "react-redux";

import { createGoalPlan, patchGoalPlan } from "../../actions/goal_plan_actions.js";

import GoalPlanFormPanelView from "./goal_plan_form_panel_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const plan = Object.values(state.goalPlans).find(
    p => p.goal_id == ownProps.goalId
  );

  return {
    instance: plan || "new",
    goal: state.goals[ownProps.goalId]
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createGoalPlan: (inst) => dispatch(createGoalPlan(inst)),
    updateGoalPlan: (inst) => dispatch(patchGoalPlan(inst)),
  };
};

const GoalPlanFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalPlanFormPanelView);

export default GoalPlanFormPanel;
