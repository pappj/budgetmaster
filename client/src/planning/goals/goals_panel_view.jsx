import React from "react";

import Goal from "./goal.jsx";
import Money from "../../common/money.js";

export default class GoalsPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderGoalList = this.renderGoalList.bind(this);
  }

  componentDidMount() {
    this.props.fetchGoalPlans();
  }

  renderGoalList() {
    if(this.props.isLoaded) {
      return <ul>
        {this.props.goals.map(goal => {
          const plan = this.props.plans.find(p => p.goal_id == goal.id);
          return <Goal key={`goal_${goal.id}`} {...goal} plan={plan}
            editAllocation={this.props.editGoalAllocation}
            editPlan={this.props.editGoalPlan}
            showNotification={this.props.showNotification}
            warning={this.props.warnings[goal.id]}
          />
        })}
      </ul>;
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <div id="goals" className="panel">
      <div className="grid goals-planning-grid">
        <h2>Goals planning</h2>
        <div className="span-3" />
        {this.renderGoalList()}
      </div>
    </div>;
  }
}
