import React from "react";

import { WNoPlan, WObsoletePlan, WTargetReached, WBehindSchedule }
  from "../../notifications/goals.js";

import { calculateOntrackAmount } from "../../common/utilities.js";
import Month from "../../common/month.js";

import Money from "../../common/money.js";
import UserDate from "../../common/date.js";

import EditIcon from "../../icons/edit.svg";
import RightIcon from "../../icons/right.svg";
import DownIcon from "../../icons/down.svg";

export default class Goal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showDetails: false};

    this.toggleDetails = this.toggleDetails.bind(this);
    this.noTarget = this.noTarget.bind(this);
    this.targetInCurrentMonth = this.targetInCurrentMonth.bind(this);
    this.noPlan = this.noPlan.bind(this);
    this.obsoletePlan = this.obsoletePlan.bind(this);
    this.targetReached = this.targetReached.bind(this);
    this.behindSchedule = this.behindSchedule.bind(this);
    this.renderAllocatedAmount = this.renderAllocatedAmount.bind(this);
    this.renderOnTrackAmount = this.renderOnTrackAmount.bind(this);
    this.renderTarget = this.renderTarget.bind(this);
  }

  toggleDetails() {
    this.setState({showDetails: !this.state.showDetails});
  }

  noTarget() {
    return !this.props.target_amount && !this.props.target_date;
  }

  targetInCurrentMonth() {
      const td = this.props.target_date;
      return td && (new Month(td)).isEqualTo(new Month());
  }

  noPlan() {
    return this.props.warning && this.props.warning.type == WNoPlan;
  }

  obsoletePlan() {
    return this.props.warning && this.props.warning.type == WObsoletePlan;
  }

  targetReached() {
    return this.props.warning && this.props.warning.type == WTargetReached;
  }

  behindSchedule() {
    return this.props.warning && this.props.warning.type == WBehindSchedule;
  }

  renderAllocatedAmount() {
    const warning = this.behindSchedule();

    return <li>
      <span className={`goal-detail ${warning ? "warning" : ""}`}>Allocated</span>
      <Money amount={this.props.allocated_amount} currency="user_main"
        warning={warning} />
      <button title="edit-allocation" className="btn btn-icon-small"
        onClick={() => this.props.editAllocation({goalId: this.props.id})}>
        <EditIcon />
      </button>
    </li>;
  }

  renderOnTrackAmount() {
    if(this.noTarget()) {
      return null;
    } else {
      let onTrack = <span className="span-2">No plan</span>;

      if(this.noPlan()) {
        onTrack = <span className="span-2 warning">No plan</span>;
      } else if(this.obsoletePlan()) {
        onTrack =  <span className="span-2 warning">Plan is obsolete</span>;
      } else if(this.props.plan) {
        const amount = calculateOntrackAmount(this.props.plan.monthly_amount,
          this.props.target_amount || this.props.plan.target_amount,
          this.props.target_date || this.props.plan.target_date);
        onTrack = <Money amount={amount} currency="user_main" />;
      }

      let editAction;
      if(this.targetReached()) {
        editAction = () => this.props.showNotification({
          isError: true,
          message: `Can't edit plans of goals with past target dates.` +
                   `Extend your goal's target date.`
        });
      } else if(this.targetInCurrentMonth()) {
        editAction = () => this.props.showNotification({
          isError: false,
          message: `Your goal's target date is reached this month. ` +
                   `Instead of a plan just change the allocated amount ` +
                   `or extend its target date.`
        });
      } else {
        editAction = () => this.props.editPlan({goalId: this.props.id});
      }

      return <li>
        <span className="goal-detail">To be on track</span>
        {onTrack}
        <button title="edit-plan" className="btn btn-icon-small"
          onClick={editAction}>
          <EditIcon />
        </button>
      </li>
    }
  }

  getTargetAmount() {
    const amount = this.props.target_amount || (this.props.plan||{}).target_amount;
    if(amount) {
      return <Money key="amount" amount={amount} currency="user_main" />;
    } else {
      return <span key="amount" className="span-2">No target</span>;
    }
  }

  getTargetDate() {
    const targetDate = this.props.target_date ||
      (this.props.plan || {}).target_date;
    if(targetDate) {
      return <UserDate className="span-2" date={targetDate} />;
    } else {
      return <span className="span-2">No target</span>;
    }
  }

  renderTarget() {
    if(this.noTarget()) {
      return <li>
        <span className="goal-detail">Target</span>
        <span className="span-2">No target</span>
        <div className="empty-cell" />
      </li>
    }


    const amountInfo = this.props.target_amount ? "" : " (plan)";

    let dateInfo = "";
    let dateWarning = "";
    if(this.targetReached()) {
      dateInfo = " (reached)";
      dateWarning = "warning";
    } else if(this.props.plan && this.props.plan.target_date) {
      dateInfo = " (plan)";
    }

    return <React.Fragment>
      <li>
        <span key="amount_label" className="goal-detail">
          Target amount{amountInfo}
        </span>
        {this.getTargetAmount()}
        <div className="empty-cell" />
      </li>
      <li>
        <span key="date_label" className={`goal-detail ${dateWarning}`}>
          Target date{dateInfo}
        </span>
        {this.getTargetDate()}
        <div className="empty-cell" />
      </li>
    </React.Fragment>;
  }

  render() {
    let toggleIcon = <RightIcon />;
    let toggleTitle = "show-details";
    let details;
    if(this.state.showDetails) {
      toggleIcon = <DownIcon />;
      toggleTitle = "hide-details";

      details = <ul>
        {this.renderAllocatedAmount()}
        {this.renderOnTrackAmount()}
        {this.renderTarget()}
      </ul>
    }

    const titleWarning = this.props.warning ? "warning" : "";

    return <li id={`goal_${this.props.id}`}>
      <span className={titleWarning}>{this.props.title}</span>
      <div className="span-2" />
      <button title={toggleTitle} className="btn btn-icon-small"
        onClick={this.toggleDetails}>
        {toggleIcon}
      </button>
      {details}
    </li>;
  }
}
