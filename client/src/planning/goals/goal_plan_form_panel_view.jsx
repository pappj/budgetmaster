import React from "react";

import ActionPanel from "../../common/action_panel.jsx";
import Form from "../../common/forms/form.jsx";

import { estimateGoalPlan } from "../../common/utilities.js";
import { positive, maximum } from "../../common/forms/validations.js";

export default class GoalPlanFormPanelView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    const panelAction = this.props.instance == "new" ? "Create" : "Adjust";
    this.title = `${panelAction} Goal Plan`;
    this.submitLabel = `${panelAction} plan`;

    this.updateFields = this.updateFields.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    const instance = props.instance == "new" ? {} : Object.assign(
      {}, props.instance);

    instance.monthly_amount = instance.monthly_amount || 0;

    const estimation = estimateGoalPlan(props.goal, instance.monthly_amount);
    if(estimation.fullyEstimated) {
      instance.monthly_amount = estimation.monthlyAmount;
    }

    instance.target_amount = estimation.targetAmount;

    this.instance = instance;

    const monthlyValidation = [positive];
    if(props.goal.target_amount) {
      monthlyValidation.push(
        maximum(estimation.targetAmount - props.goal.allocated_amount));
    }

    this.fields =  [
      {
        name: "goal",
        label: "Goal",
        type: "static",
        value: props.goal.title
      },
      {
        name: "monthly_amount",
        label: estimation.labels.monthlyAmount,
        type: estimation.fullyEstimated ? "staticMoney" : "money",
        amount: estimation.monthlyAmount,
        currency: "user_main",
        validate: monthlyValidation
      },
      {
        name: "target_amount",
        label: estimation.labels.targetAmount,
        type: "staticMoney",
        amount: estimation.targetAmount,
        currency: "user_main"
      },
      {
        name: "target_date",
        label: estimation.labels.targetDate,
        type: "staticDate",
        value: estimation.targetDate
      }
    ];
  }

  updateFields(instance) {
    const estimation = estimateGoalPlan(this.props.goal, instance.monthly_amount)
    return {
      ...instance,
      target_amount: estimation.targetAmount,
      target_date: estimation.targetDate
    };
  }

  onSubmit(instance) {
    const action = this.props.instance == "new" ?
                    this.props.createGoalPlan : this.props.updateGoalPlan;
    const instanceToSend = {
      id: instance.id,
      goal_id: this.props.goal.id,
      monthly_amount: instance.monthly_amount,
      target_amount: !this.props.goal.target_amount && instance.target_amount ||
                     undefined,
      target_date: !this.props.goal.target_date && instance.target_date ||
                   undefined
    };

    return action(instanceToSend).then(
      (response) => {
        this.props.hide();
      });
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">{this.title} Goal Plan</h2>
      <Form key="form" instance={this.instance} fields={this.fields}
        postUpdate={this.updateFields} submitLabel="Save"
        onSubmit={this.onSubmit} />
    </ActionPanel>;
  }
}
