import React from "react";

import ActionPanel from "../../common/action_panel.jsx";
import Form from "../../common/forms/form.jsx";

import { maxAllocation } from "../../common/utilities.js";
import { nonNegative, maximum } from "../../common/forms/validations.js";

export default class GoalAllocationFormPanelView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.onSubmit = this.onSubmit.bind(this);
    this.renderForm = this.renderForm.bind(this);

    const currentAllocation = props.instance.allocated_amount
    const maxAllocationAmount = maxAllocation(currentAllocation,
                                              props.availableAmount);

    this.fields = [
      {
        name: "goal",
        label: "Goal",
        type: "static",
        value: props.instance.title
      },
      {
        name: "allocated_amount",
        label: "Allocated Amount",
        type: "money",
        default: 0,
        currency: "user_main",
        validate: [nonNegative, maximum(maxAllocationAmount)]
      },
      {
        name: "maxAllocation",
        label: "Max allocation",
        type: "staticMoney",
        amount: maxAllocationAmount,
        currency: "user_main"
      }
    ];
  }

  componentDidMount() {
    this.props.fetchAccounts();
    this.props.fetchExpensePlans();
    this.props.fetchGoals();
  }

  onSubmit(instance) {
    return this.props.updateAllocation(this.props.instance.id,
      instance.allocated_amount)
      .then((response) => {
        this.props.hide();
      });
  }

  renderForm() {
    if(this.props.isLoaded) {
      return <Form key="form" instance={this.props.instance} fields={this.fields}
               submitLabel="Save" onSubmit={this.onSubmit} />
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">Change Goal Allocation</h2>
      {this.renderForm()}
    </ActionPanel>;
  }
}
