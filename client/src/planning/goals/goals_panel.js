import { connect } from "react-redux";

import GoalsPanelView from "./goals_panel_view.jsx";

import { fetchGoalPlans } from "../../actions/goal_plan_actions.js";

import { filterActiveGoals } from "../../common/utilities.js";

const mapStateToProps = (state) => {
  return {
    goals: filterActiveGoals(state.goals),
    plans: Object.values(state.goalPlans),
    warnings: state.planning.warnings.goals,
    isLoaded: state.loaded.goals && state.loaded.goalPlans,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGoalPlans: () => dispatch(fetchGoalPlans()),
  };
};

const GoalsPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsPanelView);

export default GoalsPanel;
