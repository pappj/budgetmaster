import React from "react";

import MonthSelector from "./month_selector.jsx";
import Money from "../../common/money.js";
import Month from "../../common/month.js";

import { calculateDueAmount, getAmountForCurrency }
  from "../../utilities/expenses.js";
import EditIcon from "../../icons/edit.svg";

export default class ExpensesPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.getEditData = this.getEditData.bind(this);
    this.renderExpenseList = this.renderExpenseList.bind(this);
  }

  componentDidMount() {
    this.props.fetchExpensePlans();
    this.props.fetchMonthlyExpenses();
  }

  getEditData(category) {
    let spentAmount;
    if(this.props.month.isEqualTo(new Month())) {
      const categoryExpenses = this.props.monthlyExpenses[category.id];
      spentAmount = getAmountForCurrency(categoryExpenses,
                                         this.props.userCurrencyId);
    }

    const plan = this.props.expensePlans[category.id];
    const planId = plan ? plan.id : undefined;
    const recurringExps =
      category.recurring_expenses.map(id => this.props.recurringExpenses[id]);
    const dueAmount = calculateDueAmount(recurringExps, this.props.month);
    return {planId, categoryName: category.name, categoryId: category.id,
            dueAmount, spentAmount};
  }

  renderExpenseList() {
    if(this.props.isLoaded) {
      return <ul>
        {Object.values(this.props.categories).map(cat => {
          const plan = this.props.expensePlans[cat.id];
          const planAmount = plan ? plan.amount : 0;
          const warning = this.props.month.isCurrentMonth() &&
            this.props.warnings[cat.id] ? "warning" : "";

          return <li key={cat.id} className={`${warning}`}
            id={`cat_${cat.id}`}>
            <span key="name">{cat.name}</span>
            <Money key="planned" amount={planAmount} currency="user_main" />
            <button title="Edit" className="btn btn-icon-small"
              onClick={() => this.props.editExpensePlan(this.getEditData(cat))}>
              <EditIcon />
            </button>
          </li>;
        })}
      </ul>;
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <React.Fragment>
      <div id="expenses" className="panel">
        <div className="grid grid-4">
          <h2>Expenses</h2>
          <h2 className="span-2 panel-header">Allocated</h2>
          <div className="empty-cell" />
          {this.renderExpenseList()}
        </div>
      </div>
    </React.Fragment>;
  }
}
