import { connect } from "react-redux";

import { fetchExpensePlans, createExpensePlan, patchExpensePlan }
  from "../../actions/expense_plan_actions.js";
import { fetchAccounts } from "../../actions/account_actions.js";
import { fetchGoals } from "../../actions/goal_actions.js";

import ExpensePlanFormPanelView from "./expense_plan_form_panel_view.jsx";

import { availablePlanningAmount, filterActiveGoals }
  from "../../common/utilities.js";

const mapStateToProps = (state, ownProps) => {
  const accounts = Object.values(state.accounts).filter(acc => acc.in_plan);
  const activeGoals = filterActiveGoals(state.goals);
  const expensePlans = Object.values(state.expensePlans);

  return {
    availableAmount: availablePlanningAmount(accounts, expensePlans, activeGoals),
    isLoaded: state.loaded.accounts && state.loaded.goals &&
              state.loaded.expensePlans && state.loaded.currencies && state.user,
    instance: state.expensePlans[ownProps.planId] || "new"
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createExpensePlan: (inst) => dispatch(createExpensePlan(inst)),
    updateExpensePlan: (inst) => dispatch(patchExpensePlan(inst)),
    // for availableAmount
    fetchAccounts: () => dispatch(fetchAccounts()),
    fetchExpensePlans: () => dispatch(fetchExpensePlans()),
    fetchGoals: () => dispatch(fetchGoals())
  };
};

const ExpensePlanFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpensePlanFormPanelView);

export default ExpensePlanFormPanel;
