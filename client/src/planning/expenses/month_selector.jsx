import React from "react";

export default class MonthSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderPrevButton = this.renderPrevButton.bind(this);
    this.renderNextButton = this.renderNextButton.bind(this);
  }

  renderPrevButton() {
    let btnClass = "btn"
    const disabled = this.props.month.reachedMinDate();
    if(disabled) {
      btnClass = "btn-disabled";
    }

    return <button className={`${btnClass} btn-icon-medium`}
             onClick={this.props.prevMonth} disabled={disabled}>&lt;</button>;
  }

  renderNextButton() {
    return <button className="btn btn-icon-medium" onClick={this.props.nextMonth}>
      &gt;</button>;
  }

  render() {
    return <div id="month-selector">
      {this.renderPrevButton()}
      <h2>{this.props.month.toString()}</h2>
      {this.renderNextButton()}
    </div>;
  }
}
