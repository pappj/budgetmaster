import React from "react";

import ActionPanel from "../../common/action_panel.jsx";
import Form from "../../common/forms/form.jsx";

import { maxAllocation } from "../../common/utilities.js";
import { nonNegative, maximum } from "../../common/forms/validations.js";

export default class ExpensePlanFormPanelView extends React.Component {
  constructor(props) {
    super(props);

    const planId = this.props.planId;

    this.state = {};

    this.onSubmit = this.onSubmit.bind(this);
    this.renderForm = this.renderForm.bind(this);

    const currentAllocation = props.instance == "new" ? 0 : props.instance.amount;
    const maxAllocationAmount = maxAllocation(currentAllocation,
                                              props.availableAmount);

    this.fields = [
      {
        name: "category",
        label: "Category",
        type: "static",
        value: props.categoryName
      },
      {
        name: "month",
        label: "Month",
        type: "static",
        value: props.month.toString()
      },
      {
        name: "amount",
        label: "Allocated Amount",
        type: "money",
        default: 0,
        currency: "user_main",
        validate: [nonNegative, maximum(maxAllocationAmount)]
      },
      {
        name: "dueAmount",
        label: "Due this month",
        type: "staticMoney",
        amount: props.dueAmount,
        currency: "user_main"
      },
      ...(typeof(props.spentAmount) == 'number' ? [{
        name: "spentAmount",
        label: "Spent this month",
        type: "staticMoney",
        amount: props.spentAmount,
        currency: "user_main"
      }] : []),
      {
        name: "maxAllocation",
        label: "Max allocation",
        type: "staticMoney",
        amount: maxAllocationAmount,
        currency: "user_main"
      }
    ];
  }

  componentDidMount() {
    this.props.fetchAccounts();
    this.props.fetchExpensePlans();
    this.props.fetchGoals();
  }

  onSubmit(instance) {
    const expPlan = {
      id: instance.id,
      category_id: this.props.categoryId,
      amount: instance.amount,
      month: this.props.month.date
    }

    let action;
    if(this.props.instance == "new") {
      action = this.props.createExpensePlan;
    } else {
      action = this.props.updateExpensePlan;
    }

    return action(expPlan)
      .then((response) => {
        this.props.hide();
      });
  }

  renderForm() {
    if(this.props.isLoaded) {
      return <Form key="form" instance={this.props.instance} fields={this.fields}
               submitLabel="Save" onSubmit={this.onSubmit} />;
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">Change expense allocation</h2>
      {this.renderForm()}
    </ActionPanel>;
  }
}
