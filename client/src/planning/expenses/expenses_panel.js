import { connect } from "react-redux";

import { fetchExpensePlans } from "../../actions/expense_plan_actions.js";
import { fetchMonthlyExpenses } from "../../actions/monthly_expense_actions.js";

import ExpensesPanelView from "./expenses_panel_view.jsx";

import { filterMonthPlans } from "../../common/utilities.js";

const mapStateToProps = (state, ownProps) => {
  return {
    expensePlans: filterMonthPlans(state.expensePlans, ownProps.month),
    categories: state.categories,
    recurringExpenses: state.recurringExpenses,
    monthlyExpenses: state.monthlyExpenses,
    userCurrencyId: (state.user || {}).currency_id,
    warnings: state.planning.warnings.expenses,
    isLoaded: state.loaded.categories && state.loaded.recurringExpenses &&
              state.loaded.expensePlans && state.loaded.monthlyExpenses &&
              state.loaded.currencies && state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchExpensePlans: () => dispatch(fetchExpensePlans()),
    fetchMonthlyExpenses: () => dispatch(fetchMonthlyExpenses())
  };
};

const ExpensesPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpensesPanelView);

export default ExpensesPanel;
