import React from "react";

import PremiumComponent from "../common/wrappers/premium_component.js";
import Statusbar from "./status/statusbar.js";
import PlanningWarnings from "./status/planning_warnings.js";
import GoalsPanel from "./goals/goals_panel.js";
import GoalAllocationFormPanel from "./goals/goal_allocation_form_panel.js";
import GoalPlanFormPanel from "./goals/goal_plan_form_panel.js";
import MonthSelector from "./expenses/month_selector.jsx";
import ExpensesPanel from "./expenses/expenses_panel.js";
import ExpensePlanFormPanel from "./expenses/expense_plan_form_panel.js";

import InfoPanel from "../common/info_panel.jsx";
import Month from "../common/month.js";

export default class Planning extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      month: new Month(),
      actionPanel: undefined
    };

    this.prevMonth = this.prevMonth.bind(this);
    this.nextMonth = this.nextMonth.bind(this);
    this.hideActionPanel = this.hideActionPanel.bind(this);
    this.editGoalAllocation = this.editGoalAllocation.bind(this);
    this.editGoalPlan = this.editGoalPlan.bind(this);
    this.editExpensePlan = this.editExpensePlan.bind(this);
    this.showNotification = this.showNotification.bind(this);
    this.renderActionPanel = this.renderActionPanel.bind(this);
  }

  prevMonth() {
    this.setState({month: this.state.month.previousMonth()});
  }

  nextMonth() {
    this.setState({month: this.state.month.nextMonth()});
  }

  hideActionPanel() {
    this.setState({actionPanel: undefined});
  }

  editGoalAllocation(data) {
    this.setState({
      actionPanel: {type: "goalAllocation", data}
    });
  }

  editGoalPlan(data) {
    this.setState({
      actionPanel: {type: "goalPlan", data}
    });
  }

  editExpensePlan(data) {
    this.setState({
      actionPanel: {type: "expensePlan", data}
    });
  }

  showNotification(data) {
    this.setState({
      actionPanel: {type: "notification", data}
    });
  }

  renderActionPanel() {
    let panel;
    const ap = this.state.actionPanel;
    if(ap) {
      switch(ap.type) {
        case "expensePlan":
          panel = <ExpensePlanFormPanel hide={this.hideActionPanel}
                    {...ap.data} month={this.state.month} />;
          break;
        case "goalAllocation":
          panel = <GoalAllocationFormPanel hide={this.hideActionPanel}
                    goalId={ap.data.goalId} />;
          break;
        case "goalPlan":
          panel = <GoalPlanFormPanel hide={this.hideActionPanel}
                    goalId={ap.data.goalId} />;
          break;
        case "notification":
          panel = <InfoPanel hide={this.hideActionPanel} {...ap.data} />;
          break;
      }
    }

    return panel;
  }

  render() {
    return <PremiumComponent level="premium" name="Goal and Expense plans"
             noticeType="fullPage" isLoaded={true}>
      <Statusbar />
      <div key="panels" className="panels">
        <PlanningWarnings />
        <GoalsPanel
          editGoalAllocation={this.editGoalAllocation}
          editGoalPlan={this.editGoalPlan}
          showNotification={this.showNotification} />
        <MonthSelector key="selector" month={this.state.month}
          prevMonth={this.prevMonth} nextMonth={this.nextMonth} />
        <ExpensesPanel month={this.state.month}
          editExpensePlan={this.editExpensePlan} />
        {this.renderActionPanel()}
      </div>
    </PremiumComponent>;
  }
}
