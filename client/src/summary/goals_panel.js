import { connect } from "react-redux";
import { fetchGoals } from "../actions/goal_actions.js";

import GoalsPanelView from "./goals_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    isLoaded: state.loaded.goals && state.loaded.currencies && state.user,
    goals: Object.values(state.goals).filter(g => !g.achieved),
    user: state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGoals: () => dispatch(fetchGoals())
  }
};

const GoalsPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsPanelView);

export default GoalsPanel;
