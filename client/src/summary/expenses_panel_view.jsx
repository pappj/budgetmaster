import React from "react";

import Money from "../common/money.js";
import Month from "../common/month.js";

import { filterMonthPlans } from "../common/utilities.js";
import { getAmountForCurrency, calculateDueAmount }
  from "../utilities/expenses.js";

export default class ExpensesPanelView extends React.Component {
  componentDidMount() {
    this.props.fetchCategories();
    this.props.fetchMonthlyExpenses();
    this.props.fetchExpensePlans();
  }

  getExtraColumnHeader() {
    let header;
    switch(this.props.subPackage) {
      case "free":
        header = null;
        break;
      case "basic":
        header = <h2 className="span-2 panel-header">Due</h2>;
        break;
      case "premium":
        header = <h2 className="span-2 panel-header">Plan</h2>;
        break;
    }

    return header;
  }

  getExtraColumn(category, plan) {
    let column;
    switch(this.props.subPackage) {
      case "free":
        column = null;
        break;
      case "basic":
        const recurringExps =
          category.recurring_expenses.map(id => this.props.recurringExpenses[id]);
        const dueAmt = calculateDueAmount(recurringExps, new Month());
        column = <Money key="due" amount={dueAmt} currency="user_main" />;
        break;
      case "premium":
        const plannedAmt = plan.amount || 0;
        column = <Money key="planned" amount={plannedAmt} currency="user_main" />;
        break;
    }

    return column;
  }

  getCategoryExpenses(category, plan) {
    const userCurrencyId = this.props.userCurrencyId;
    const categoryExpenses = this.props.monthlyExpenses[category.id] || {};
    const spentAmount = getAmountForCurrency(categoryExpenses, userCurrencyId);

    const key = `${category.id}_${userCurrencyId}`;
    return <React.Fragment key={category.id}>
      <li key={category.id} id={`cat_${category.id}`}>
        <span key="name">{category.name}</span>
        <ul>
          <li key={key} id={`spent_${key}`}>
            <Money key="spent" danger={true} amount={spentAmount}
              currency="user_main" />
            {this.getExtraColumn(category, plan)}
          </li>
          {this.getOtherCurrencyExpenses(category.id, userCurrencyId)}
        </ul>
      </li>
    </React.Fragment>;
  }

  getOtherCurrencyExpenses(categoryId, userCurrencyId) {
    const categoryExpenses = this.props.monthlyExpenses[categoryId] || {};
    const expenseKeys =
      Object.keys(categoryExpenses).filter(id => id != userCurrencyId);
    let extraColumn = null;
    if(this.props.subPackage != "free") {
      extraColumn = <span className="span-2"></span>
    }

    return expenseKeys.map(id => {
      const key = `${categoryId}_${id}`;
      return <li key={key} id={`spent_${key}`}>
        <span></span>
        <Money key="spent" danger={true} amount={categoryExpenses[id]}
          currency={id} />
        {extraColumn}
      </li>;
    });
  }

  getExpenseList() {
    const currentPlans =
      Object.values(filterMonthPlans(this.props.expensePlans, new Month()));
    return <ul>
      {Object.values(this.props.categories).map(category => {
        const plan = currentPlans.find(p => p.category_id == category.id) || {};
        return this.getCategoryExpenses(category, plan);
      })}
    </ul>;
  }

  render() {
    let numberOfColumns;
    if(!this.props.isLoaded || this.props.subPackage == "free") {
      numberOfColumns = 3;
    } else {
      numberOfColumns = 5;
    }

    let content;
    if(this.props.isLoaded) {
      content = <React.Fragment>
        <h2 className="span-2 panel-header">Spent</h2>
        {this.getExtraColumnHeader()}
        {this.getExpenseList()}
      </React.Fragment>;
    } else {
      return <p>Loading...</p>;
    }

    return <div id="expenses" className="panel">
      <div className={`grid grid-${numberOfColumns}`}>
        <h2>Expenses</h2>
        {content}
      </div>
    </div>;
  }
}
