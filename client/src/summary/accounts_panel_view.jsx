import React from "react";

import {GenericAccount, CreditAccount} from "./account.jsx";

export default class AccountsPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.getAccountList = this.getAccountList.bind(this);
  }

  componentDidMount() {
    this.props.fetchAccounts();
  }

  shouldComponentUpdate(newProps) {
    return newProps.isLoaded != this.props.isLoaded ||
      newProps.accounts != this.props.accounts;
  }

  renderAccount(acc) {
    if(acc.account_type == "credit") {
      return <CreditAccount key={acc.id} {...acc} />;
    } else {
      return <GenericAccount key={acc.id} {...acc} />;
    }
  }

  getAccountList() {
    if(this.props.isLoaded) {
      return  <ul>
        {Object.values(this.props.accounts).map(acc =>
          this.renderAccount(acc)
        )}
      </ul>;
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <div id="accounts" className="panel">
      <div className="grid grid-3">
        <h2>Accounts</h2>
        <h2 className="span-2 panel-header">Balance</h2>
        {this.getAccountList()}
      </div>
    </div>;
  }
}
