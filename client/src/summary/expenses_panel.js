import { connect } from "react-redux";
import { fetchCategories } from "../actions/category_actions.js";
import { fetchExpensePlans } from "../actions/expense_plan_actions.js";
import { fetchMonthlyExpenses } from "../actions/monthly_expense_actions.js";

import { getSubscriptionPackage } from "../utilities/subscriptions.js";

import ExpensesPanelView from "./expenses_panel_view.jsx";

const mapStateToProps = (state) => {
  let currencyId;
  let subPackage;
  if(state.user) {
    currencyId = state.user.currency_id;
    subPackage = getSubscriptionPackage(state.user);
  }

  return {
    categories: state.categories,
    expensePlans: state.expensePlans,
    monthlyExpenses: state.monthlyExpenses,
    recurringExpenses: state.recurringExpenses,
    isLoaded: state.loaded.currencies && state.loaded.categories &&
              state.loaded.monthlyExpenses && state.loaded.expensePlans &&
              state.loaded.recurringExpenses && state.user,
    userCurrencyId: currencyId,
    subPackage: subPackage
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCategories: () => dispatch(fetchCategories()),
    fetchExpensePlans: () => dispatch(fetchExpensePlans()),
    fetchMonthlyExpenses: () => dispatch(fetchMonthlyExpenses())
  }
};

const ExpensesPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpensesPanelView);

export default ExpensesPanel;
