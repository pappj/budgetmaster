import React from "react";

import Money from "../common/money.js";

class Account extends React.Component {
  render() {
    const id = this.props.id;

    return <li key={id} id={`acc_${id}`}>
      <span key="acc-name">{this.props.name}</span>
      {this.props.children}
    </li>;
  }
}

export class GenericAccount extends React.Component {
  render() {
    return <Account {...this.props}>
      <Money amount={this.props.current_balance}
        currency={this.props.currency_id} />
    </Account>;
  }
}

export class CreditAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showBalance: true};

    this.toggleBalance = this.toggleBalance.bind(this);
    this.renderAmount = this.renderAmount.bind(this);
  }

  toggleBalance() {
    this.setState({
      showBalance: !this.state.showBalance
    });
  }

  renderAmount() {
    let amount;
    if(this.state.showBalance) {
      amount = this.props.current_balance;
    } else {
      amount = this.props.credit - this.props.current_balance;
    }

    return <Money amount={amount} currency={this.props.currency_id}
      onClick={this.toggleBalance} danger={!this.state.showBalance} />;
  }

  render() {
    return <Account {...this.props}>
      {this.renderAmount()}
    </Account>;
  }
}
