import React from "react";

import GoalsPanel from "./goals_panel.js";
import AccountsPanel from "./accounts_panel.js";
import ExpensesPanel from "./expenses_panel.js";
import IncomeFormPanel from "../form_panels/income_form_panel.js";
import NewExpensePanel from "../form_panels/new_expense_panel.jsx";
import TransferFormPanel from "../form_panels/transfer_form_panel.js";
import PanelToggleButton from "../common/panel_toggle_button.jsx";

import FavouritesIcon from "../icons/favourites.svg";
import IncomeIcon from "../icons/income.svg";
import ExpenseIcon from "../icons/expense.svg";
import TransferIcon from "../icons/transfer.svg";

export default class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      actionPanel: undefined
    };

    this.isPanelActive = this.isPanelActive.bind(this);
    this.togglePanel = this.togglePanel.bind(this);
    this.hidePanel = this.hidePanel.bind(this);
    this.getActionPanel = this.getActionPanel.bind(this);
  }

  isPanelActive(key) {
    return this.state.actionPanel == key;
  }

  togglePanel(key) {
    if(this.state.actionPanel == key) {
      this.setState({actionPanel: undefined});
    } else {
      this.setState({actionPanel: key});
    }
  }

  hidePanel() {
    this.setState({actionPanel: undefined});
  }

  getActionPanel() {
    switch(this.state.actionPanel) {
      case "income":
        return <IncomeFormPanel key="income" hide={this.hidePanel}
                 instance="new" />;
      case "expense":
        return <NewExpensePanel key="expense" hide={this.hidePanel} />;
      case "transfer":
        return <TransferFormPanel key="transfer" hide={this.hidePanel}
                 instance="new" />;
      default:
        return "";
    }
  }

  render() {
    return [
      <div key="status" id="status">
        <p className="success">Your budget is in good shape</p>
      </div>,
      <div key="panels" className="panels">
        <GoalsPanel />
        <AccountsPanel />
        <ExpensesPanel />
        {this.getActionPanel()}
      </div>,
      <div key="btns" className="btn-line">
        <PanelToggleButton key="2" icon={<IncomeIcon />} label="New Income"
          id="new-income" active={this.isPanelActive("income")}
          toggle={() => this.togglePanel("income")} />
        <PanelToggleButton key="3" icon={<ExpenseIcon />} label="New Expense"
          id="new-expense" active={this.isPanelActive("expense")}
          toggle={() => this.togglePanel("expense")} />
        <PanelToggleButton key="4" icon={<TransferIcon />} label="New Transfer"
          id="new-transfer" active={this.isPanelActive("transfer")}
          toggle={() => this.togglePanel("transfer")} />
      </div>
    ];
  }
}
