import React from "react";
import Money from "../common/money.js";
import UserDate from "../common/date.js";
import PremiumComponent from "../common/wrappers/premium_component.js";

import { entitledForBasic } from "../utilities/subscriptions.js";

export default class GoalsPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.getGoalList = this.getGoalList.bind(this);
  }

  componentDidMount() {
    this.props.fetchGoals();
  }

  shouldComponentUpdate(newProps) {
    return newProps.isLoaded != this.props.isLoaded ||
      newProps.goals != this.props.goals;
  }

  getTarget(goal) {
    if(goal.target_date) {
      return <UserDate className="span-2" date={goal.target_date} />;
    } else if(goal.target_amount) {
      return <Money amount={goal.target_amount} currency="user_main" />;
    } else {
      return <span className="span-2">No target</span>;
    }
  }

  getGoalList() {
    return <ul>
      {this.props.goals.map(goal =>
        <li key={goal.id} id={`goal_${goal.id}`}>
          <span>{goal.title}</span>
          {this.getTarget(goal)}
        </li>
      )}
    </ul>;
  }

  render() {
    return <div id="goals" className="panel">
      <PremiumComponent level="basic" name="Goals" noticeType="regularWithButton"
        isLoaded={this.props.isLoaded}>
        <div className="grid grid-3">
          <h2>Goals</h2>
          <h2 className="span-2 panel-header">Target</h2>
          {this.getGoalList()}
        </div>
      </PremiumComponent>
    </div>;
  }
}
