import { connect } from "react-redux";

import ProfileView from "./profile_view.jsx";

const mapStateToProps = (state) => {
  let mainCurrency = "";
  if(state.user && state.loaded.currencies) {
    const currency = state.currencies[state.user.currency_id];
    mainCurrency = `${currency.short_code} (${currency.symbol})`;
  }

  return {
    isLoaded: Boolean(state.user),
    ...state.user,
    mainCurrency
  };
};

const Profile = connect(
  mapStateToProps
)(ProfileView);

export default Profile;
