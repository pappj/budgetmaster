import React from "react";
import Form from "../common/forms/form.jsx";

import format from "date-fns/format";

export default class ProfileFormView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.instance = {
      currency_id: props.currency_id,
      currency_string: props.currency_string,
      currency_position: props.currency_position,
      currencyExample: this.getCurrencyExample(props.currency_id,
        props.currency_string, props.currency_position),
      date_format: props.date_format,
      dateExample: this.getDateExample(props.date_format)
    }

    this.fields = [
      {
        name: "currency_id",
        label: "Main currency",
        type: "select",
        options: Object.values(props.currencies).reduce((acc, curr) =>
          ({...acc, [curr.id]: curr.short_code + " (" + curr.symbol + ")"}), {})
      },
      {
        name: "currency_string",
        label: "Currency type",
        type: "select",
        options: {
          short_code: "Short code",
          symbol: "Symbol"
        }
      },
      {
        name: "currency_position",
        label: "Currency Position",
        type: "select",
        options: {
          before: "Before the amount",
          after: "After the amount"
        }
      },
      {
        name: "currencyExample",
        label: "Example",
        type: "dynamicText"
      },
      {
        name: "date_format",
        label: "Date format",
        type: "select",
        options: {
          "yyyy-MM-dd": "yyyy-MM-dd",
          "yyyy.MM.dd.": "yyyy.MM.dd.",
          "MM/dd/yyyy": "MM/dd/yyyy",
          "dd/MM/yyyy": "dd/MM/yyyy"
        }
      },
      {
        name: "dateExample",
        label: "Example",
        type: "dynamicText"
      }
    ];

    this.postUpdate = this.postUpdate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  getCurrencyExample(id, string, position) {
    const currency = this.props.currencies[id][string];
    let example;
    if(position == "before") {
      example = `${currency} 1500`;
    } else {
      example = `1500 ${currency}`;
    }

    return example;
  }

  getDateExample(formatStr) {
    return format(new Date(), formatStr);
  }

  postUpdate(instance) {
    const newInstance = {...instance};
    newInstance.currencyExample = this.getCurrencyExample(instance.currency_id,
      instance.currency_string, instance.currency_position);
    newInstance.dateExample = this.getDateExample(instance.date_format);

    return newInstance;
  }

  onSubmit(instance) {
    const instanceToSubmit = {id: this.props.userId, ...instance};
    delete instanceToSubmit["currencyExample"];
    delete instanceToSubmit["dateExample"];

    return this.props.patchProfile(instanceToSubmit).then(response => {
      this.props.hide();
    });
  }

  render() {
    return <Form instance={this.instance} fields={this.fields}
      onSubmit={this.onSubmit} postUpdate={this.postUpdate} submitLabel="Save" />;
  }
}
