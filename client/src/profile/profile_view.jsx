import React from "react";
import ProfileForm from "./profile_form.js";

import UserDate from "../common/date.js";

export default class ProfileView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false
    };

    this.toggleEditing = this.toggleEditing.bind(this);
  }

  toggleEditing() {
    this.setState({editing: !this.state.editing});
  }

  renderEditButton() {
    let label;
    if(this.state.editing) {
      label = "Cancel";
    } else {
      label = "Edit";
    }

    return <button id="edit-profile-btn" className="btn btn-text-small"
      onClick={this.toggleEditing}>{label}</button>;
  }

  renderUserInterfaceSection() {
    let section;
    if(this.state.editing) {
      section = <ProfileForm hide={this.toggleEditing} />;
    } else {
      section = <ul className="profile-value-list">
          <li className="profile-line">
            <span className="profile-label">Main currency</span>
            <span className="profile-value">{this.props.mainCurrency}</span>
          </li>
          <li className="profile-line">
            <span className="profile-label">Currency type</span>
            <span className="profile-value">{this.props.currency_string}</span>
          </li>
          <li className="profile-line">
            <span className="profile-label">Currency position</span>
            <span className="profile-value">
              {this.props.currency_position} the amount
            </span>
          </li>
          <li className="profile-line">
            <span className="profile-label">Date format</span>
            <span className="profile-value">{this.props.date_format}</span>
          </li>
        </ul>;
    }

    return section;
  }

  getSubscriptionType() {
    const subscriptions = {
      "trial": "Free trial period",
      "free": "Free (no subscription)",
      "basic": "Basic subscription",
      "premium": "Premium subscription"
    };

    return subscriptions[this.props.subscription_type];
  }

  render() {
    if(this.props.isLoaded) {
      return <div id="user-profile">
        <section className="user-section">
          <h1>Personal information</h1>
          <ul className="profile-value-list">
            <li className="profile-line">
              <span className="profile-label">E-mail</span>
              <span className="profile-value email-value">{this.props.email}</span>
            </li>
          </ul>
        </section>
        <section className="user-section">
          <h1>User interface{this.renderEditButton()}</h1>
          {this.renderUserInterfaceSection()}
        </section>
        <section className="user-section">
          <h1>Subscription</h1>
          <ul className="profile-value-list">
            <li className="profile-line">
              <span className="profile-label">Type</span>
              <span className="profile-value">{this.getSubscriptionType()}</span>
            </li>
            <li className="profile-line">
              <span className="profile-label">Valid until</span>
              <UserDate className="profile-value"
                date={this.props.subscription_expiry_date} />
            </li>
          </ul>
        </section>
      </div>;
    } else {
      return <p>Loading...</p>;
    }
  }
}
