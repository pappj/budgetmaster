import { connect } from "react-redux";
import { patchUserProfile } from "../actions/user_actions.js";

import ProfileFormView from "./profile_form_view.jsx";

const mapStateToProps = (state) => {
  return {
    userId: state.user.id,
    currency_id: state.user.currency_id,
    currency_string: state.user.currency_string,
    currency_position: state.user.currency_position,
    date_format: state.user.date_format,
    currencies: state.currencies
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    patchProfile: (profileData) => dispatch(patchUserProfile(profileData))
  };
};

const ProfileForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileFormView);

export default ProfileForm;
