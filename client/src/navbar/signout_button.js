import axios from "axios";
import React from "react";
import { connect } from "react-redux";

import { removeUser } from "../actions/user_actions.js";
import { clearStore } from "../actions/generic_actions.js";

class SignoutButtonView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.signOut = this.signOut.bind(this);
  }

  signOut(evt) {
    evt.preventDefault();
    axios.delete("/api/users/sign_out")
      .then((response) => {
        this.props.toggleMenu();
        this.props.removeUser();
        this.props.clearStore();
      });
  }

  render() {
   return <button className={this.props.className}
            onClick={this.signOut}>Sign out</button>;
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeUser: () => dispatch(removeUser()),
    clearStore: () => dispatch(clearStore())
  };
};

const SignoutButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignoutButtonView);

export default SignoutButton;
