import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

import HomeIcon from "../icons/home.svg";
import ProfileIcon from "../icons/profile.svg";
import NavMenu from "./nav_menu.js";

class NavBarView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState({showMenu: !this.state.showMenu});
  }

  render() {
    let home;
    let profile;
    let menu;

    if(this.props.signedIn) {
      home = <NavLink end to="/summary"><HomeIcon /></NavLink>;
      profile = <button id="toggle-menu" className="toggle-btn"
                  onClick={this.toggleMenu}>
                  <ProfileIcon />
                </button>;

      if(this.state.showMenu) {
        menu = <NavMenu toggleMenu={this.toggleMenu}
                 outsideClickIgnoreClass="toggle-btn"/>
      }
    }

    return <nav>
      {home}
      <div id="title">
        <h1>Budget Master</h1><span>Beta</span>
      </div>
      {profile}
      {menu}
    </nav>;
  }
}

const mapStateToProps = (state) => {
  return {
    signedIn: state.user
  };
};

const NavBar = connect(mapStateToProps)(NavBarView);

export default NavBar;
