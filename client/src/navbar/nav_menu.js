import React from "react";
import { connect } from "react-redux";
import onClickOutside from "react-onclickoutside";

import SignoutButton from "./signout_button.js";
import { NavLink } from "react-router-dom";

class NavMenuView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  handleClickOutside(evt) {
    evt.stopPropagation();
    this.props.toggleMenu();
  }

  render() {
    let signOut;
    if(this.props.signedIn) {
      signOut = <li key="4"><SignoutButton toggleMenu={this.props.toggleMenu}/></li>
    }

    return <ul className="top-visible menu">
      <li key="nav1">
        <NavLink end to="/basics" onClick={this.props.toggleMenu}>
        Basics</NavLink>
      </li>
      <li key="nav2">
        <NavLink end to="/transactions" onClick={this.props.toggleMenu}>
        Transactions</NavLink>
      </li>
      <li key="nav3">
        <NavLink end to="/planning" onClick={this.props.toggleMenu}>
        Planning</NavLink>
      </li>
      <li key="nav4">
        <NavLink end to="/profile" onClick={this.props.toggleMenu}>
        Profile</NavLink>
      </li>
      {signOut}
    </ul>;
  }
}

const mapStateToProps = (state) => {
  return {
    signedIn: state.user
  };
};

const NavMenu = connect(mapStateToProps)(onClickOutside(NavMenuView));

export default NavMenu;

