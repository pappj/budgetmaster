import React from "react";
import { connect } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";


class ProtectedRouteComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if(this.props.signedIn || !this.props.userChecked) {
      return <Outlet />;
    } else {
      return <Navigate to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    signedIn: state.user
  };
};

const ProtectedRoute = connect(
  mapStateToProps
)(ProtectedRouteComponent);

export default ProtectedRoute;
