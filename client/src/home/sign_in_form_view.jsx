import React from "react";
import axios from "axios";

import Form from "../common/forms/form.jsx";
import { email } from "../common/forms/validations.js";

export default class SignInFormView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}

    this.signIn = this.signIn.bind(this);

    this.fields = [
      {
        name: "email",
        label: "Email",
        type: "input",
        inputType: "text",
        default: "",
        placeholder: "your@email.com",
        validate: email
      },
      {
        name: "password",
        label: "Password",
        type: "input",
        inputType: "password",
        default: ""
      }
    ];
  }

  signIn(user) {
    return axios.post("/api/users/sign_in", {user})
      .then((response) => {
        this.props.updateUserData(response.data);
      });
  }

  render() {
    return <Form id="sign-in" onSubmit={this.signIn} submitLabel="Sign in"
             fields={this.fields} instance="new" />;
  }
}
