import React from "react";
import { NavLink, Navigate } from "react-router-dom";
import SignInForm from "../home/sign_in_form.js";

import FacebookIcon from "../icons/facebook.svg";
import InstaIcon from "../icons/instagram.svg";
import TwitterIcon from "../icons/twitter.svg";
import LinkedInIcon from "../icons/linkedin.svg"

export default class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if(this.props.userSignedIn) {
      return <Navigate to="/summary" />;
    } else {
      return [
        <section key="main" id="home-content">
          <h1>Already have an account?</h1>
          <SignInForm />
          <h1>Are you new here?</h1>
          <div className="btn-line">
            <div className="btn-group">
              <NavLink to="/signup" className="btn btn-text">Sign up</NavLink>
              <span>Start a free trial before subscribing</span>
            </div>
            <div className="btn-group">
              <button className="btn btn-text">Try it out</button>
              <span>Without any commitment</span>
            </div>
          </div>
        </section>,
        <footer key="footer">
          <h1 id="check-social">Check us out on social media</h1>
          <div className="btn-line">
            <a href="#"><FacebookIcon /></a>
            <a href="#"><InstaIcon /></a>
            <a href="#"><TwitterIcon /></a>
            <a href="#"><LinkedInIcon /></a>
          </div>
          <h1>#budgetmaster</h1>
        </footer>
      ];
    }
  };
}
