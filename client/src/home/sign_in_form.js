import React from "react";
import { connect } from "react-redux";

import SignInFormView from "./sign_in_form_view.jsx"

import { updateUserData } from "../actions/user_actions.js";

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserData: (user) => dispatch(updateUserData(user))
  }
};

const SignInForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInFormView);

export default SignInForm;
