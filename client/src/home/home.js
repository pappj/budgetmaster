import { connect } from "react-redux";

import HomeView from "./home_view.jsx";

const mapStateToProps = (state) => {
  return {
    userSignedIn: state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const Home = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeView);

export default Home;
