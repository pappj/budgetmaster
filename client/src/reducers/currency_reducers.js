import { REQUEST_CURRENCIES, CANCEL_REQUEST_CURRENCIES, RECEIVE_CURRENCIES }
  from "../actions/currency_actions.js";

export function currencies(state = {}, action) {
  switch(action.type) {
    case RECEIVE_CURRENCIES:
      return {
        ...state,
        ...action.currencies
      };
    default:
      return state;
  }
}

export function loadingCurrencies(state = false, action) {
  switch(action.type) {
    case REQUEST_CURRENCIES:
      return true;
    case CANCEL_REQUEST_CURRENCIES:
    case RECEIVE_CURRENCIES:
      return false;
    default:
      return state;
  }
}

export function currenciesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_CURRENCIES:
      return true;
    default:
      return state;
  }
}
