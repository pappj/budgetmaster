import { UPDATE_USER_DATA, DELETE_USER_DATA } from "../actions/user_actions.js";

export function user(state = null, action) {
  switch(action.type) {
    case UPDATE_USER_DATA:
      return {
        ...state,
        ...action.user
      };
    case DELETE_USER_DATA:
      return null;
    default:
      return state;
  }
}
