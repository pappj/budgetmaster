import { REQUEST_GOAL_PLANS, CANCEL_REQUEST_GOAL_PLANS, RECEIVE_GOAL_PLANS, ADD_GOAL_PLAN,
         UPDATE_GOAL_PLAN, DELETE_GOAL_PLAN }
  from "../actions/goal_plan_actions.js";
import { DELETE_GOAL } from "../actions/goal_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function goalPlans(state = {}, action) {
  switch(action.type) {
    case RECEIVE_GOAL_PLANS:
      return {
        ...state,
        ...action.goalPlans
      };
    case ADD_GOAL_PLAN:
    case UPDATE_GOAL_PLAN:
      return {
        ...state,
        [action.goalPlan.id]: action.goalPlan
      };
    case DELETE_GOAL_PLAN:
      let newState = {...state}
      delete newState[action.id];
      return newState;
    case DELETE_GOAL:
      // Delete the related plan, too
      const plansToDel = Object.values(state).filter(plan => plan.goal_id == action.id);
      if(plansToDel == []) {
        return state;
      } else {
        let newState = {...state};
        // There must be only 1 plan, but go for sure
        plansToDel.forEach(plan => {
          delete newState[plan.id];
        });

        return newState;
      }
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingGoalPlans(state = false, action) {
  switch(action.type) {
    case REQUEST_GOAL_PLANS:
      return true;
    case CANCEL_REQUEST_GOAL_PLANS:
    case RECEIVE_GOAL_PLANS:
      return false;
    default:
      return state;
  }
}

export function goalPlansLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_GOAL_PLANS:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
