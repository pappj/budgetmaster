import { combineReducers } from "redux";
import { goals, loadingGoals, goalsLoaded } from "./goal_reducers.js";
import { goalPlans, loadingGoalPlans, goalPlansLoaded }
  from "./goal_plan_reducers.js";
import { accounts, loadingAccounts, accountsLoaded } from "./account_reducers.js";
import { categories, loadingCategories, categoriesLoaded }
  from "./category_reducers.js";
import { recurringExpenses, recurringExpensesLoaded }
  from "./recurring_expense_reducers.js";
import { currencies, loadingCurrencies, currenciesLoaded }
  from "./currency_reducers.js";
import { incomes, loadingIncomes, incomesLoaded } from "./income_reducers.js";
import { expenses, loadingExpenses, expensesLoaded } from "./expense_reducers.js";
import { monthlyExpenses, loadingMonthlyExpenses, monthlyExpensesLoaded }
  from "./monthly_expense_reducers.js";
import { transfers, loadingTransfers, transfersLoaded }
  from "./transfer_reducers.js";
import { expensePlans, loadingExpensePlans, expensePlansLoaded }
  from "./expense_plan_reducers";
import { goalWarnings, expenseWarnings } from "./warning_reducers.js";
import { availableAmount } from "./planning_reducers.js";
import { user } from "./user_reducers.js";

const rootReducer = combineReducers({
  accounts,
  categories,
  currencies,
  expenses,
  expensePlans,
  goals,
  goalPlans,
  incomes,
  monthlyExpenses,
  recurringExpenses,
  transfers,
  loading: combineReducers({
    accounts: loadingAccounts,
    categories: loadingCategories,
    currencies: loadingCurrencies,
    expenses: loadingExpenses,
    expensePlans: loadingExpensePlans,
    goals: loadingGoals,
    goalPlans: loadingGoalPlans,
    incomes: loadingIncomes,
    monthlyExpenses: loadingMonthlyExpenses,
    transfers: loadingTransfers
  }),
  loaded: combineReducers({
    accounts: accountsLoaded,
    categories: categoriesLoaded,
    currencies: currenciesLoaded,
    expenses: expensesLoaded,
    expensePlans: expensePlansLoaded,
    goals: goalsLoaded,
    goalPlans: goalPlansLoaded,
    incomes: incomesLoaded,
    monthlyExpenses: monthlyExpensesLoaded,
    recurringExpenses: recurringExpensesLoaded,
    transfers: transfersLoaded
  }),
  planning: combineReducers({
    availableAmount,
    warnings: combineReducers({
      goals: goalWarnings,
      expenses: expenseWarnings
    })
  }),
  user
});

export default rootReducer;
