import { GOAL_WARNINGS_UPDATED, EXPENSE_WARNINGS_UPDATED }
  from "../actions/warning_actions.js";
import { ADD_GOAL, UPDATE_GOAL, DELETE_GOAL }
  from "../actions/goal_actions.js";
import { ADD_GOAL_PLAN, UPDATE_GOAL_PLAN, DELETE_GOAL_PLAN }
  from "../actions/goal_plan_actions.js";

import { ADD_RECURRING_EXPENSES, UPDATE_RECURRING_EXPENSES,
         DELETE_RECURRING_EXPENSES }
  from "../actions/recurring_expense_actions.js";
import { ADD_EXPENSE_PLAN, UPDATE_EXPENSE_PLAN }
  from "../actions/expense_plan_actions.js";
import { ADD_EXPENSE } from "../actions/expense_actions.js";

import { CLEAR_STORE } from "../actions/generic_actions.js";

export function goalWarnings(state = {updateRequired: true}, action) {
  switch(action.type) {
    case GOAL_WARNINGS_UPDATED:
      return {
        updateRequired: false,
        ...action.warnings
      };
    case ADD_GOAL:
    case UPDATE_GOAL:
    case DELETE_GOAL:
    case ADD_GOAL_PLAN:
    case UPDATE_GOAL_PLAN:
    case DELETE_GOAL_PLAN:
      return {
        ...state,
        updateRequired: true
      };
    case CLEAR_STORE:
      return {updateRequired: true};
    default:
      return state;
  }
}

export function expenseWarnings(state = {updateRequired: true}, action) {
  switch(action.type) {
    case EXPENSE_WARNINGS_UPDATED:
      return {
        updateRequired: false,
        ...action.warnings
      };
    case ADD_RECURRING_EXPENSES:
    case UPDATE_RECURRING_EXPENSES:
    case DELETE_RECURRING_EXPENSES:
    case ADD_EXPENSE_PLAN:
    case UPDATE_EXPENSE_PLAN:
    case ADD_EXPENSE:
      return {
        ...state,
        updateRequired: true
      };
    case CLEAR_STORE:
      return {updateRequired: true};
    default:
      return state;
  }
}
