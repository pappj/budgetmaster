import { RECEIVE_RECURRING_EXPENSES, ADD_RECURRING_EXPENSES,
         UPDATE_RECURRING_EXPENSES, DELETE_RECURRING_EXPENSES }
  from "../actions/recurring_expense_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function recurringExpenses(state = {}, action) {
  switch(action.type) {
    case RECEIVE_RECURRING_EXPENSES:
      return {
        ...state,
        ...action.recurringExpenses
      };
    case ADD_RECURRING_EXPENSES:
    case UPDATE_RECURRING_EXPENSES:
      return {
        ...state,
        ...action.recurringExpenses
      };
    case DELETE_RECURRING_EXPENSES:
      let newState = Object.assign({}, state);
      (action.ids || []).forEach(id => {
        delete newState[id];
      });
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function recurringExpensesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_RECURRING_EXPENSES:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
