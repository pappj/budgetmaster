import { REQUEST_EXPENSE_PLANS, CANCEL_REQUEST_EXPENSE_PLANS,
         RECEIVE_EXPENSE_PLANS, ADD_EXPENSE_PLAN, UPDATE_EXPENSE_PLAN }
  from "../actions/expense_plan_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function expensePlans(state = {}, action) {
  switch(action.type) {
    case RECEIVE_EXPENSE_PLANS:
      return {
        ...state,
        ...action.expensePlans
      };
    case ADD_EXPENSE_PLAN:
    case UPDATE_EXPENSE_PLAN:
      return {
        ...state,
        [action.expensePlan.id]: action.expensePlan
      };
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingExpensePlans(state = false, action) {
  switch(action.type) {
    case REQUEST_EXPENSE_PLANS:
      return true;
    case CANCEL_REQUEST_EXPENSE_PLANS:
    case RECEIVE_EXPENSE_PLANS:
      return false;
    default:
      return state;
  }
}

export function expensePlansLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_EXPENSE_PLANS:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
