import { UPDATE_AVAILABLE_AMOUNT } from "../actions/planning_actions.js";

import { ADD_ACCOUNT, UPDATE_ACCOUNT, DELETE_ACCOUNT }
  from "../actions/account_actions.js";
import { ADD_GOAL, UPDATE_GOAL, DELETE_GOAL }
  from "../actions/goal_actions.js";
import { ADD_EXPENSE_PLAN, UPDATE_EXPENSE_PLAN }
  from "../actions/expense_plan_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function availableAmount(state = {amount: null, updateRequired: true},
                                action) {
  switch(action.type) {
    case UPDATE_AVAILABLE_AMOUNT:
      return {
        amount: action.amount,
        updateRequired: false
      };
    case ADD_ACCOUNT:
    case UPDATE_ACCOUNT:
    case DELETE_ACCOUNT:
    case ADD_GOAL:
    case UPDATE_GOAL:
    case DELETE_GOAL:
    case ADD_EXPENSE_PLAN:
    case UPDATE_EXPENSE_PLAN:
      return {
        ...state,
        updateRequired: true
      };
    case CLEAR_STORE:
      return {
        amount: null,
        updateRequired: true
      };
    default:
      return state;
  }
}
