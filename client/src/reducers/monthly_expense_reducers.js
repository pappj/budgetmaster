import { REQUEST_MONTHLY_EXPENSES, CANCEL_REQUEST_MONTHLY_EXPENSES,
         RECEIVE_MONTHLY_EXPENSES } from "../actions/monthly_expense_actions.js";
import { ADD_EXPENSE } from "../actions/expense_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

import Month from "../common/month.js";

export function monthlyExpenses(state = {}, action) {
  switch(action.type) {
    case RECEIVE_MONTHLY_EXPENSES:
      return {
        ...action.monthlyExpenses
      };
    case ADD_EXPENSE:
      const thisMonth = new Month();
      const expenseMonth = new Month(action.expense.date);
      if(expenseMonth.isEqualTo(thisMonth)) {
        const catId = action.expense.category_id;
        const currencyId = action.expense.currencyId;
        return {
          ...state,
          [catId]: {...state[catId],
                    [currencyId]: ((state[catId] || {})[currencyId] || 0) +
                                  action.expense.amount}
        };
      } else {
        return state;
      }
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingMonthlyExpenses(state = false, action) {
  switch(action.type) {
    case REQUEST_MONTHLY_EXPENSES:
      return true;
    case CANCEL_REQUEST_MONTHLY_EXPENSES:
    case RECEIVE_MONTHLY_EXPENSES:
      return false;
    default:
      return state;
  }
}

export function monthlyExpensesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_MONTHLY_EXPENSES:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
