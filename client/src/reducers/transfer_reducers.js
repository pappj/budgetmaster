import { REQUEST_TRANSFERS, CANCEL_REQUEST_TRANSFERS, RECEIVE_TRANSFERS, ADD_TRANSFER,
         UPDATE_TRANSFER, DELETE_TRANSFER }
  from "../actions/transfer_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function transfers(state = {}, action) {
  switch(action.type) {
    case RECEIVE_TRANSFERS:
      return {
        ...state,
        ...action.transfers
      };
    case ADD_TRANSFER:
    case UPDATE_TRANSFER:
      return {
        ...state,
        [action.transfer.id]: action.transfer
      };
    case DELETE_TRANSFER:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingTransfers(state = false, action) {
  switch(action.type) {
    case REQUEST_TRANSFERS:
      return true;
    case CANCEL_REQUEST_TRANSFERS:
    case RECEIVE_TRANSFERS:
      return false;
    default:
      return state;
  }
}

export function transfersLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_TRANSFERS:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
