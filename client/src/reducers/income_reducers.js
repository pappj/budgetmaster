import { REQUEST_INCOMES, CANCEL_REQUEST_INCOMES,  RECEIVE_INCOMES, ADD_INCOME,
         UPDATE_INCOME, DELETE_INCOME }
  from "../actions/income_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function incomes(state = {}, action) {
  switch(action.type) {
    case RECEIVE_INCOMES:
      return {
        ...state,
        ...action.incomes
      };
    case ADD_INCOME:
    case UPDATE_INCOME:
      return {
        ...state,
        [action.income.id]: action.income
      };
    case DELETE_INCOME:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingIncomes(state = false, action) {
  switch(action.type) {
    case REQUEST_INCOMES:
      return true;
    case CANCEL_REQUEST_INCOMES:
    case RECEIVE_INCOMES:
      return false;
    default:
      return state;
  }
}

export function incomesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_INCOMES:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
