import { REQUEST_ACCOUNTS, CANCEL_REQUEST_ACCOUNTS,  RECEIVE_ACCOUNTS, ADD_ACCOUNT,
         UPDATE_ACCOUNT, DELETE_ACCOUNT }
  from "../actions/account_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function accounts(state = {}, action) {
  switch(action.type) {
    case RECEIVE_ACCOUNTS:
      return {
        ...state,
        ...action.accounts
      };
    case ADD_ACCOUNT:
    case UPDATE_ACCOUNT:
      return {
        ...state,
        [action.account.id]: action.account
      };
    case DELETE_ACCOUNT:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingAccounts(state = false, action) {
  switch(action.type) {
    case REQUEST_ACCOUNTS:
      return true;
    case CANCEL_REQUEST_ACCOUNTS:
    case RECEIVE_ACCOUNTS:
      return false;
    default:
      return state;
  }
}

export function accountsLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_ACCOUNTS:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
