import { REQUEST_GOALS, CANCEL_REQUEST_GOALS,  RECEIVE_GOALS, ADD_GOAL,
         UPDATE_GOAL, DELETE_GOAL }
  from "../actions/goal_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function goals(state = {}, action) {
  switch(action.type) {
    case RECEIVE_GOALS:
      return {
        ...state,
        ...action.goals
      };
    case ADD_GOAL:
    case UPDATE_GOAL:
      return {
        ...state,
        [action.goal.id]: action.goal
      };
    case DELETE_GOAL:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingGoals(state = false, action) {
  switch(action.type) {
    case REQUEST_GOALS:
      return true;
    case CANCEL_REQUEST_GOALS:
    case RECEIVE_GOALS:
      return false;
    default:
      return state;
  }
}

export function goalsLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_GOALS:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}
