import { REQUEST_CATEGORIES, CANCEL_REQUEST_CATEGORIES, RECEIVE_CATEGORIES,
         ADD_CATEGORY, UPDATE_CATEGORY, DELETE_CATEGORY }
  from "../actions/category_actions.js";
import { CLEAR_STORE } from "../actions/generic_actions.js";

export function categories(state = {}, action) {
  switch(action.type) {
    case RECEIVE_CATEGORIES:
      return {
        ...state,
        ...action.categories
      };
    case ADD_CATEGORY:
    case UPDATE_CATEGORY:
      return {
        ...state,
        [action.category.id]: action.category
      };
    case DELETE_CATEGORY:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    case CLEAR_STORE:
      return {};
    default:
      return state;
  }
}

export function loadingCategories(state = false, action) {
  switch(action.type) {
    case REQUEST_CATEGORIES:
      return true;
    case CANCEL_REQUEST_CATEGORIES:
    case RECEIVE_CATEGORIES:
      return false;
    default:
      return state;
  }
}

export function categoriesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_CATEGORIES:
      return true;
    case CLEAR_STORE:
      return false;
    default:
      return state;
  }
}

