import { connect } from "react-redux";

import { fetchIncomes } from "../actions/income_actions.js";
import { fetchExpenses } from "../actions/expense_actions.js";
import { fetchTransfers } from "../actions/transfer_actions.js";

import TransactionsView from "./transactions_view.jsx";

const mapStateToProps = (state) => {
  const loaded = state.loaded;

  return {
    incomes: state.incomes,
    expenses: state.expenses,
    transfers: state.transfers,
    isLoaded: state.user && loaded.incomes && loaded.expenses &&
              loaded.transfers && loaded.accounts && loaded.currencies &&
              loaded.categories
  };
};

export function fetchTransactions(dispatch, startDate, endDate) {
  dispatch(fetchIncomes(startDate, endDate));
  dispatch(fetchExpenses(startDate, endDate));
  dispatch(fetchTransfers(startDate, endDate));
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTransactions: (start, end) => fetchTransactions(dispatch, start, end)
  };
};

const Transactions = connect(
  mapStateToProps,
  mapDispatchToProps
)(TransactionsView);

export default Transactions;
