import React from "react";
import ActionPanel from "../common/action_panel.jsx";
import Checkbox from "../common/forms/checkbox.jsx";

export default class DeletePanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {updateAccount: false};

    this.toggleUpdateAccount = this.toggleUpdateAccount.bind(this);
    this.remove = this.remove.bind(this);
  }

  toggleUpdateAccount() {
    if(!this.props.disabled) {
      this.setState({updateAccount: !this.state.updateAccount});
    }
  }

  remove() {
    this.props.remove(this.props.id, this.state.updateAccount);
    this.props.postRemove();
    this.props.hide();
  }

  render() {
    const checkClass = this.props.disabled ? "disabled" : "";
    let note;
    if(this.props.note) {
      note = <span className="side-note">{this.props.note}</span>;
    }

    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">Remove {this.props.model}</h2>
      {this.props.children}
      <div className="check-line">
        <Checkbox checked={this.state.updateAccount}
          onClick={this.toggleUpdateAccount} disabled={this.props.disabled} />
        <span className={checkClass} onClick={this.toggleUpdateAccount}>
          Update {this.props.accountName} balance
        </span>
      </div>
      {note}
      <div key="buttons" className="btn-line">
        <button key="yes" onClick={this.remove} className="btn btn-text btn-dng">
          Yes
        </button>
        <button key="no" onClick={this.props.hide} className="btn btn-text">
          No
        </button>
      </div>
    </ActionPanel>;
  }
}
