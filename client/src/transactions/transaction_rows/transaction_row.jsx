import React from "react";

import UserDate from "../../common/date.js";

import EditIcon from "../../icons/edit.svg";
import DeleteIcon from "../../icons/delete.svg";
import RightIcon from "../../icons/right.svg";
import DownIcon from "../../icons/down.svg";

export class TransactionRow extends React.Component {
  constructor(props, name) {
    super(props);

    this.transactionName = name;

    this.renderSimpleView = this.renderSimpleView.bind(this);
    this.renderDetailedView = this.renderDetailedView.bind(this);
  }

  getExtraSimpleRow() {
    return null;
  }

  renderSimpleView() {
    return <React.Fragment>
      <Row date={this.props.date}>
        {this.getSimpleFields()}
        <button title="show-details" className="btn-transparent"
          onClick={this.props.toggleDetails}>
          <RightIcon />
        </button>
      </Row>
      {this.getExtraSimpleRow()}
    </React.Fragment>;
  }

  renderDetailedView() {
    let note;
    if(this.props.note) {
      note = <Row><span className="span-3">{this.props.note}</span></Row>;
    }

    return <React.Fragment>
      <Row date={this.props.date} onClick={this.props.toggleDetails}>
        <span onClick={this.props.toggleDetails}>{this.transactionName}</span>
        <span className="span-2">
          <button title="Edit" className="btn btn-icon-small"
            onClick={() => this.props.edit(this.props.id, this.transactionName,
              this.props.idx)}>
            <EditIcon />
          </button>
          <button title="Delete" className="btn btn-icon-small btn-dng"
            onClick={() => this.props.remove(this.props.id, this.transactionName,
              this.props.idx)}>
            <DeleteIcon />
          </button>
        </span>
        <button title="hide-details" className="btn-transparent"
          onClick={this.props.toggleDetails}>
          <DownIcon />
        </button>
      </Row>
      {this.getDetailedRows()}
      {note}
    </React.Fragment>;
  }

  render() {
    let content;
    let click;
    if(this.props.showDetails) {
      content = this.renderDetailedView();
    } else {
      content = this.renderSimpleView();
      click = this.props.toggleDetails;
    }

    return <li onClick={click}>{content}</li>;
  }
}

export class Row extends React.Component {
  render() {
    let leadingCells;
    let extraCell = null;
    if(this.props.date) {
      leadingCells = <UserDate date={this.props.date}
                       onClick={this.props.onClick} />;
    } else {
      leadingCells = <span />;
      extraCell = <span />;
    }

    return <React.Fragment>
      {leadingCells}
      {this.props.children}
      {extraCell}
    </React.Fragment>;
  }
}
