import React from "react";
import { TransactionRow, Row } from "./transaction_row.jsx";

import Money from "../../common/money.js";

export default class IncomeView extends TransactionRow {
  constructor(props) {
    super(props, "Income");
  }

  getSimpleFields() {
    const account = this.props.accounts[this.props.account_id]

    return <React.Fragment>
      <span>{account.name}</span>
      <Money amount={this.props.amount} currency={account.currency_id} />
    </React.Fragment>;
  }

  getDetailedRows() {
    const account = this.props.accounts[this.props.account_id]

    return <React.Fragment>
      <Row><span className="span-3">{account.name}</span></Row>
      <Row>
        <span>Received</span>
        <Money amount={this.props.amount} currency={account.currency_id} />
      </Row>
    </React.Fragment>;
  }
}
