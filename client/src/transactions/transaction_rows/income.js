import { connect } from "react-redux";

import IncomeView from "./income_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts
  };
};

const Income = connect(
  mapStateToProps
)(IncomeView);

export default Income;
