import { connect } from "react-redux";

import ExpenseView from "./expense_view.jsx";

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    accounts: state.accounts
  };
};

const Expense = connect(
  mapStateToProps
)(ExpenseView);

export default Expense;
