import React from "react";
import { TransactionRow, Row } from "./transaction_row.jsx";

import Money from "../../common/money.js";

export default class TransferView extends TransactionRow {
  constructor(props) {
    super(props, "Transfer");
  }

  getSimpleFields() {
    const sourceAccount = this.props.accounts[this.props.source_account_id]

    return <React.Fragment>
      <span>{sourceAccount.name}</span>
      <Money danger={true} amount={this.props.source_amount}
        currency={sourceAccount.currency_id} />
    </React.Fragment>;
  }

  getExtraSimpleRow() {
    const targetAccount = this.props.accounts[this.props.target_account_id]

    return <Row>
      <span>{targetAccount.name}</span>
      <Money amount={this.props.target_amount}
        currency={targetAccount.currency_id} />
    </Row>
  }

  getDetailedRows() {
    const sourceAccount = this.props.accounts[this.props.source_account_id]
    const targetAccount = this.props.accounts[this.props.target_account_id]

    return <React.Fragment>
      <Row>
        <span>{sourceAccount.name}</span>
        <Money danger={true} amount={this.props.source_amount}
          currency={sourceAccount.currency_id} />
      </Row>
      <Row>
        <span>{targetAccount.name}</span>
        <Money amount={this.props.target_amount}
          currency={targetAccount.currency_id} />
      </Row>
    </React.Fragment>;
  }
}
