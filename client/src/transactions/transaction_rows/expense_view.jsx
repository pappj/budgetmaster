import React from "react";
import { TransactionRow, Row } from "./transaction_row.jsx";

import Money from "../../common/money.js";
import UserDate from "../../common/date.js";

import RightIcon from "../../icons/right.svg";

export default class ExpenseView extends TransactionRow {
  constructor(props) {
    super(props, "Expense");
  }

  getSimpleFields() {
    const categoryName = this.props.categories[this.props.category_id].name;
    const currencyId = this.props.accounts[this.props.account_id].currency_id;

    return <React.Fragment>
      <span>{categoryName}</span>
      <Money danger={true} amount={this.props.amount} currency={currencyId} />
    </React.Fragment>;
  }

  getDetailedRows() {
    const categoryName = this.props.categories[this.props.category_id].name;
    const account = this.props.accounts[this.props.account_id];
    const currencyId = account.currency_id;

    return <React.Fragment>
      <Row><span className="span-3">{categoryName}</span></Row>
      <Row><span className="span-3">{account.name}</span></Row>
      <Row>
        <span className="danger">Spent</span>
        <Money danger={true} amount={this.props.amount} currency={currencyId} />
      </Row>
    </React.Fragment>;
  }
}
