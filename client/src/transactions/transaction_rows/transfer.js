import { connect } from "react-redux";

import TransferView from "./transfer_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts
  };
};

const Transfer = connect(
  mapStateToProps
)(TransferView);

export default Transfer;
