import React from "react";
import { addMonths } from "date-fns";

import ActionPanel from "../../common/action_panel.jsx";
import FormBody from "../../common/forms/form_body.jsx";

export default class ChangeDatesPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: props.startDate,
      endDate: props.endDate
    };

    this.getRangeEnd = this.getRangeEnd.bind(this);
    this.maxDate = this.maxDate.bind(this);
    this.adjustEndDate = this.adjustEndDate.bind(this);
    this.updateInstance = this.updateInstance.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.fields = [
      {
        name: "startDate",
        label: "Start date",
        type: "date",
        maxDate: new Date(),
        default: props.startDate,
        position: "top-start"
      },
      {
        name: "endDate",
        label: "End date",
        type: "date",
        dynamicMinDate: (state) => state.startDate,
        dynamicMaxDate: this.maxDate,
        default: props.startDate
      }
    ];
  }

  getRangeEnd(startDate) {
    const rangeEnd = addMonths(new Date(startDate), 6);
    const today = new Date();

    if(rangeEnd > today) {
      return today;
    } else {
      return rangeEnd;
    }
  }

  maxDate(state) {
    return this.getRangeEnd(state.startDate);
  }

  adjustEndDate(instance) {
    const rangeEnd = this.getRangeEnd(instance.startDate);

    if(instance.endDate > rangeEnd) {
      return {...instance, endDate: rangeEnd};
    } else if(instance.startDate > instance.endDate) {
      return {...instance, endDate: instance.startDate};
    } else {
      return instance;
    }
  }

  updateInstance(newInstance) {
    this.setState(newInstance);
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.props.fetchTransactions(this.state.startDate, this.state.endDate);
    this.props.updateDates(this.state.startDate, this.state.endDate);
    this.props.hide();
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2>Change dates</h2>
      <form onSubmit={this.onSubmit}>
        <div id="date-range-fields">
          <FormBody fieldsOnly={true} fields={this.fields} instance={this.state}
            updateInstance={this.updateInstance} errors={{}}
            postUpdate={this.adjustEndDate} />
        </div>
        <button type="submit" className="btn btn-text">Apply</button>
      </form>
    </ActionPanel>;
  }
}
