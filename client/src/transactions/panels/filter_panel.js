import { connect } from "react-redux";

import FilterPanelView from "./filter_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts,
    categories: state.categories
  };
};

const FilterPanel = connect(
  mapStateToProps
)(FilterPanelView);

export default FilterPanel;
