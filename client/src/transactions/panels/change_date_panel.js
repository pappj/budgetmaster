import { connect } from "react-redux";
import { fetchTransactions } from "../transactions.js";

import ChangeDatesPanelView from "./change_date_panel_view.jsx";

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTransactions: (start, end) => fetchTransactions(dispatch, start, end)
  };
};

const ChangeDatesPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangeDatesPanelView);

export default ChangeDatesPanel;
