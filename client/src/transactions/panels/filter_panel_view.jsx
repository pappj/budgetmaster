import React from "react";

import ActionPanel from "../../common/action_panel.jsx";
import IncomeFilter from "../filters/income_filter.jsx";
import ExpenseFilter from "../filters/expense_filter.jsx";
import TransferFilter from "../filters/transfer_filter.jsx";
import AmountFilter from "../filters/amount_filter.jsx";

import { Filter, RangeFilter } from "../../utilities/filters.js";

export default class FilterPanelView extends React.Component {
  constructor(props) {
    super(props);

    const accountIds = Object.values(props.accounts).map(acc => acc.id);
    const categoryIds = Object.values(props.categories).map(cat => cat.id);

    let filters = props.filters;
    if(props.filters == "noFilter") {
      filters = {
        incomeFilter: new Filter(accountIds),
        expenseFilter: new Filter(categoryIds),
        transferFilter: {
          source: new Filter(accountIds),
          target: new Filter(accountIds)
        },
        amountFilter: new RangeFilter()
      };
    }

    this.state = {
      ...filters
    };

    this.updateIncomeFilter = this.updateIncomeFilter.bind(this);
    this.updateExpenseFilter = this.updateExpenseFilter.bind(this);
    this.updateTransferFilters = this.updateTransferFilters.bind(this);
    this.updateAmountFilter = this.updateAmountFilter.bind(this);
    this.applyFilters = this.applyFilters.bind(this);
  }

  updateIncomeFilter(newFilter) {
    this.setState({incomeFilter: newFilter});
  }

  updateExpenseFilter(newFilter) {
    this.setState({expenseFilter: newFilter});
  }

  updateTransferFilters(sourceFilter, targetFilter) {
    this.setState({
      transferFilter: {
        source: sourceFilter,
        target: targetFilter
      }
    });
  }

  updateAmountFilter(newFilter) {
    this.setState({amountFilter: newFilter});
  }

  applyFilters() {
    this.props.updateFilters(this.state);
    this.props.hide();
  }

  render() {
    return <ActionPanel hide={this.props.hide} className="filter-panel">
      <h2>Filters</h2>
      <div id="filters">
        <IncomeFilter filter={this.state.incomeFilter}
          accounts={this.props.accounts} updateFilter={this.updateIncomeFilter} />
        <ExpenseFilter filter={this.state.expenseFilter}
          categories={this.props.categories}
          updateFilter={this.updateExpenseFilter} />
        <TransferFilter sources={this.state.transferFilter.source}
          targets={this.state.transferFilter.target} accounts={this.props.accounts} 
          updateFilter={this.updateTransferFilters} />
        <AmountFilter filter={this.state.amountFilter}
          updateFilter={this.updateAmountFilter} />
      </div>
      <button className="btn btn-text" onClick={this.applyFilters}>Apply</button>
    </ActionPanel>;
  }
}
