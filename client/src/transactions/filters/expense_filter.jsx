import React from "react";

import BaseFilter from "./base_filter.jsx";
import CheckBox from "../../common/forms/checkbox.jsx";

export default class ExpenseFilter extends BaseFilter {
  constructor(props) {
    super(props);
    this.name = "expense";
    this.label = "Expenses";
    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails() {
    let details = null;
    if(this.state.showDetails) {
      details = <ul className="filter-items">
        <li className="filter-head" key="head">Categories</li>
        {Object.values(this.props.categories).map(cat => {
          const cb = () => this.toggleItem(cat.id);
          return <li key={`cat_${cat.id}`} className="filter-row">
            <CheckBox checked={this.props.filter.itemIncluded(cat.id)}
              onClick={cb} />
            <span className="filter-item-name" onClick={cb}>{cat.name}</span>
          </li>
        })}
      </ul>;
    }

    return details;
  }
}
