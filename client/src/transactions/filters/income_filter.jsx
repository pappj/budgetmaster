import React from "react";

import BaseFilter from "./base_filter.jsx";
import CheckBox from "../../common/forms/checkbox.jsx";

export default class IncomeFilter extends BaseFilter {
  constructor(props) {
    super(props);
    this.name = "income";
    this.label = "Incomes";
    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails() {
    let details = null;
    if(this.state.showDetails) {
      details = <ul className="filter-items">
        <li className="filter-head" key="head">Accounts</li>
        {Object.values(this.props.accounts).map(acc => {
          const cb = () => this.toggleItem(acc.id);
          return <li key={`acc_${acc.id}`} className="filter-row">
            <CheckBox checked={this.props.filter.itemIncluded(acc.id)}
              onClick={cb} />
            <span className="filter-item-name" onClick={cb}>{acc.name}</span>
          </li>
        })}
      </ul>;
    }

    return details;
  }
}
