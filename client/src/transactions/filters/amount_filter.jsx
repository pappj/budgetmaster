import React from "react";

import CheckBox from "../../common/forms/checkbox.jsx";

export default class AmountFilter extends React.Component {
  toggleFilter = () => {
    this.props.updateFilter(this.props.filter.toggle());
  }

  handleChange = (evt) => {
    evt.preventDefault();

    const parsedValue = Number.parseFloat(evt.target.value);
    const newValue = Number.isNaN(parsedValue) ? 0 : Math.max(0, parsedValue);

    switch(evt.target.name) {
      case "from":
        this.props.updateFilter(this.props.filter.updateFrom(newValue));
        break;
      case "to":
        this.props.updateFilter(this.props.filter.updateTo(newValue));
        break;
    }
  }

  renderRange() {
    let range = null;
    if(!this.props.filter.all()) {
      range = <div className="filter-items">
        <label htmlFor="from">From</label>
        <input name="from" type="text" value={this.props.filter.from()}
          onChange={this.handleChange} />
        <label htmlFor="to">To</label>
        <input name="to" type="text" value={this.props.filter.to()}
          onChange={this.handleChange} />
      </div>
    }

    return range;
  }

  render() {
    return <div id="amount-filter">
      <div className="filter-row">
        <CheckBox checked={this.props.filter.all()} onClick={this.toggleFilter} />
        <span onClick={this.toggleFilter} className="filter-group-title">
          All amounts
        </span>
      </div>
      {this.renderRange()}
    </div>;
  }
}
