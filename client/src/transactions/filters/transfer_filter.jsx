import React from "react";

import BaseFilter from "./base_filter.jsx";
import CheckBox from "../../common/forms/checkbox.jsx";

export default class TransferFilter extends BaseFilter {
  constructor(props) {
    super(props);
    this.name = "transfer";
    this.label = "Transfers";

    this.toggleDetails = this.toggleDetails.bind(this);
    this.toggleGroup = this.toggleGroup.bind(this);
    this.toggleSource = this.toggleSource.bind(this);
    this.toggleTarget = this.toggleTarget.bind(this);
  }

  groupIncluded() {
    return this.props.sources.groupIncluded() &&
      this.props.targets.groupIncluded();
  }

  toggleDetails() {
    this.setState({showDetails: !this.state.showDetails});
  }

  toggleGroup() {
    if(this.groupIncluded()) {
      this.props.updateFilter(this.props.sources.toggleGroup(),
        this.props.targets.toggleGroup());
    } else {
      this.props.updateFilter(this.props.sources.ensureGroupIncluded(),
        this.props.targets.ensureGroupIncluded());
    }
  }

  toggleSource(accId) {
    this.props.updateFilter(this.props.sources.toggleItem(accId),
      this.props.targets);
  }

  toggleTarget(accId) {
    this.props.updateFilter(this.props.sources,
      this.props.targets.toggleItem(accId));
  }

  renderList(filter, toggleFun, head) {
    return <ul id={`${head.toLowerCase()}-filter`}>
      <li className="transfer-head" key="head">{head}</li>
      {Object.values(this.props.accounts).map(acc => {
        const cb = () => toggleFun(acc.id);
        return <li key={`acc_${acc.id}`} className="filter-row">
          <CheckBox checked={filter.itemIncluded(acc.id)}
            onClick={cb} />
          <span className="filter-item-name" onClick={cb}>{acc.name}</span>
        </li>
      })}
    </ul>;
  }

  renderSources() {
    return this.renderList(this.props.sources, this.toggleSource, "From");
  }

  renderTargets() {
    return this.renderList(this.props.targets, this.toggleTarget, "To");
  }

  renderDetails() {
    let details = null;
    if(this.state.showDetails) {
      details = <div id="transfer-items" className="filter-items">
        {this.renderSources()}
        {this.renderTargets()}
      </div>
    }

    return details;
  }
}
