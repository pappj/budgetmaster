import React from "react";

import CheckBox from "../../common/forms/checkbox.jsx";

import RightIcon from "../../icons/right.svg";
import DownIcon from "../../icons/down.svg";

export default class BaseFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDetails: false
    };

    this.toggleDetails = this.toggleDetails.bind(this);
    this.toggleGroup = this.toggleGroup.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
  }

  toggleDetails() {
    this.setState({showDetails: !this.state.showDetails});
  }

  toggleGroup() {
    this.props.updateFilter(this.props.filter.toggleGroup());
  }

  toggleItem(id) {
    this.props.updateFilter(this.props.filter.toggleItem(id));
  }

  groupIncluded() {
    return this.props.filter.groupIncluded();
  }

  renderDetails() {
    return null;
  }

  render() {
    const detailsTitle = this.state.showDetails ? "hide-details" : "show-details"
    const detailsIcon = this.state.showDetails ?  <DownIcon /> : <RightIcon />;
    const detailsButton =
      <button title={detailsTitle} className="btn-transparent"
        onClick={this.toggleDetails}>
        {detailsIcon}
      </button>;

    return <div id={`${this.name}-filter`}>
      <div className="filter-group filter-row">
        <CheckBox checked={this.groupIncluded()}
          onClick={this.toggleGroup} />
        <span onClick={this.toggleGroup} className="filter-group-title">
          {this.label}
        </span>
        {detailsButton}
      </div>
      {this.renderDetails()}
    </div>;
  }
}
