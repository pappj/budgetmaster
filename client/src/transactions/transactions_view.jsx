import React from "react";
import { format, addMonths } from "date-fns";

import { filterIncomes, filterExpenses, filterTransfers, isDateInRange }
  from "../utilities/filters.js";

import Income from "./transaction_rows/income.js";
import Expense from "./transaction_rows/expense.js";
import Transfer from "./transaction_rows/transfer.js";

import IncomeFormPanel from "../form_panels/income_form_panel.js";
import EditExpensePanel from "../form_panels/edit_expense_panel.jsx";
import TransferFormPanel from "../form_panels/transfer_form_panel.js";

import IncomeDeletePanel from "./delete_panels/income_delete_panel.js";
import ExpenseDeletePanel from "./delete_panels/expense_delete_panel.js";
import TransferDeletePanel from "./delete_panels/transfer_delete_panel.js";

import ChangeDatesPanel from "./panels/change_date_panel.js";
import FilterPanel from "./panels/filter_panel.js";

import UserDate from "../common/date.js";
import PanelToggleButton from "../common/panel_toggle_button.jsx";
import { cutTime } from "../common/utilities.js";

import FilterIcon from "../icons/filter.svg";
import CalendarIcon from "../icons/calendar.svg";
import DetailsIcon from "../icons/details.svg";
import GroupingIcon from "../icons/grouping.svg";

export default class TransactionsView extends React.Component {
  constructor(props) {
    super(props);

    // First convert the current date to date string, then initialize another
    // date. This will set the hours according to the user's timezone, like the
    // transactions' date are handled. Otherwise the transactions on one of the
    // edges would be excluded, if the hours are set to 00:00, while the
    // transaction date object has the UTC+X time.
    const today = cutTime(new Date());
    const oneYearEarlier = addMonths(today, -6)
    this.state = {
      actionPanel: undefined,
      transactions: [],
      filters: "noFilter",
      startDate: oneYearEarlier,
      endDate: today
    };

    this.refilterTransactions = this.refilterTransactions.bind(this);
    this.updateDates = this.updateDates.bind(this);
    this.updateFilters = this.updateFilters.bind(this);
    this.areAllDetailsShown = this.areAllDetailsShown.bind(this);
    this.toggleChangeDates = this.toggleChangeDates.bind(this);
    this.toggleFilters = this.toggleFilters.bind(this);
    this.toggleDetails = this.toggleDetails.bind(this);
    this.toggleAllDetails = this.toggleAllDetails.bind(this);
    this.hidePanel = this.hidePanel.bind(this);
    this.editTransaction = this.editTransaction.bind(this);
    this.updateTransaction = this.updateTransaction.bind(this);
    this.removeTransaction = this.removeTransaction.bind(this);
    this.removeTransactionFromPage = this.removeTransactionFromPage.bind(this);
  }

  componentDidMount() {
    if(this.props.isLoaded) {
      this.refilterTransactions();
    } else {
      this.props.fetchTransactions(this.state.startDate, this.state.endDate);
    }
  }

  componentDidUpdate(prevProps) {
    if(!prevProps.isLoaded && this.props.isLoaded) {
      this.refilterTransactions();
    }
  }

  refilterTransactions(stateUpdates = {}) {
    const transactions = this.filterAndSortTransactions({
      ...this.state,
      ...stateUpdates
    });

    this.setState({
      ...stateUpdates,
      transactions
    });
  }

  updateDates(startDate, endDate) {
    this.refilterTransactions({
      startDate: cutTime(startDate),
      endDate: cutTime(endDate)
    });
  }

  updateFilters(newFilters) {
    this.refilterTransactions({filters: newFilters});
  }

  areAllDetailsShown() {
    return this.state.transactions.length > 0 &&
      this.state.transactions.every(trn => trn.showDetails);
  }

  changeAllDetails(transactions, showDetails) {
    return transactions.map(trn => {
      return { ...trn, showDetails: showDetails };
    });
  }

  addTypeAndShowDetails(tag, transactions) {
    return transactions.map(trn => {
      return {
        type: tag,
        showDetails: false,
        ...trn
      };
    });
  }

  filterTransactions(state, filterFun, name) {
    let transactions = Object.values(this.props[`${name}s`]);
    const dateRange = {startDate: state.startDate,
                       endDate: state.endDate};
    if(state.filters != "noFilter") {
      transactions = filterFun(transactions, state.filters[`${name}Filter`],
        state.filters.amountFilter, dateRange);
    } else {
      transactions = transactions.filter(
        trn => isDateInRange(dateRange, trn.date)
      );
    }

    return this.addTypeAndShowDetails(name, transactions);
  }

  filterIncomes(state) {
    return this.filterTransactions(state, filterIncomes, "income");
  }

  filterExpenses(state) {
    return this.filterTransactions(state, filterExpenses, "expense");
  }

  filterTransfers(state) {
    return this.filterTransactions(state, filterTransfers, "transfer");
  }

  filterAndSortTransactions(state) {
    const filteredTransactions = [
      ...this.filterIncomes(state),
      ...this.filterExpenses(state),
      ...this.filterTransfers(state)
    ];

    return this.sortByDate(filteredTransactions);
  }

  sortByDate(transactions) {
    return transactions.sort((obj1, obj2) => {
      // Sort them in decreasing order
      if(obj1.date > obj2.date) {
        return -1;
      } else if(obj1.date == obj2.date) {
        return 0;
      } else {
        return 1;
      }
    });
  }

  isPanelActive(panel) {
    const ap = this.state.actionPanel;
    return ap && ap.type == panel;
  }

  togglePanel(panelName) {
    if(this.isPanelActive(panelName)) {
      this.setState({actionPanel: undefined});
    } else {
      this.setState({
        actionPanel: {type: panelName}
      });
    }
  }

  toggleChangeDates() {
    this.togglePanel("changeDates");
  }

  toggleFilters() {
    this.togglePanel("filters");
  }

  toggleDetails(idx) {
    const trn = this.state.transactions[idx];
    const newTransactions = [
      ...this.state.transactions.slice(0, idx),
      {...trn, showDetails: !trn.showDetails},
      ...this.state.transactions.slice(idx+1)
    ];

    this.setState({
      transactions: newTransactions
    });
  }

  toggleAllDetails() {
    const showDetails = this.areAllDetailsShown() ? false : true;
    const updatedTransactions = this.changeAllDetails(this.state.transactions,
      showDetails);
    this.setState({transactions: updatedTransactions});
  }

  hidePanel() {
    this.setState({actionPanel: undefined});
  }

  editTransaction(id, type, idx) {
    this.setState({
      actionPanel: {
        type: `edit${type}`,
        id,
        idx
      }
    });
  }

  updateTransaction(updatedInstance, idx) {
    let newTransactions = [
      ...this.state.transactions.slice(0, idx),
      ...this.state.transactions.slice(idx+1)
    ];
    const dateRange = {startDate: this.state.startDate,
                       endDate: this.state.endDate};
    if(isDateInRange(dateRange, updatedInstance.date)) {
      const oldTransaction = this.state.transactions[idx];
      newTransactions = this.sortByDate([
        {...updatedInstance, type: oldTransaction.type,
          showDetails: oldTransaction.showDetails},
        ...newTransactions
      ]);
    }

    this.setState({
      transactions: newTransactions
    });
  }

  removeTransaction(id, type, idx) {
    this.setState({
      actionPanel: {
        type: `remove${type}`,
        id,
        idx
      }
    });
  }

  removeTransactionFromPage(idx) {
    this.setState({
      transactions: [
        ...this.state.transactions.slice(0, idx),
        ...this.state.transactions.slice(idx+1)
      ]
    });
  }

  renderTransaction(trn, idx) {
    const details = trn.showDetails;
    switch(trn.type) {
      case "income":
        return <Income key={`inc_${trn.id}_${details}`} {...trn}
          edit={this.editTransaction} remove={this.removeTransaction}
          toggleDetails={() => this.toggleDetails(idx)} idx={idx} />;
      case "expense":
        return <Expense key={`exp_${trn.id}_${details}`} {...trn}
          edit={this.editTransaction} remove={this.removeTransaction}
          toggleDetails={() => this.toggleDetails(idx)} idx={idx} />;
      case "transfer":
        return <Transfer key={`trf_${trn.id}_${details}`} {...trn}
          edit={this.editTransaction} remove={this.removeTransaction}
          toggleDetails={() => this.toggleDetails(idx)} idx={idx} />;
    }
  }

  renderTransactions() {
    return <ul>
      {this.state.transactions.map((trn, i) => {
        return this.renderTransaction(trn, i);
      })}
    </ul>;
  }

  renderRange() {
    return <div id="transaction-range">
      <UserDate id="start-date" date={this.state.startDate} />
      <span id="date-separator">-</span>
      <UserDate id="end-date" date={this.state.endDate} />
    </div>;
  }

  renderActionPanel() {
    let panel;
    const ap = this.state.actionPanel;
    if(ap) {
      const updateTransaction =
        (updatedInstance) => this.updateTransaction(updatedInstance, ap.idx);
      switch(ap.type) {
        case "changeDates":
          panel = <ChangeDatesPanel startDate={this.state.startDate}
            endDate={this.state.endDate} updateDates={this.updateDates}
            hide={this.toggleChangeDates} />;
          break;
        case "filters":
          panel = <FilterPanel hide={this.toggleFilters}
            filters={this.state.filters} updateFilters={this.updateFilters} />;
          break;
        case "editIncome":
          panel = <IncomeFormPanel instance={ap.id} hide={this.hidePanel}
                    postSubmit={updateTransaction}/>;
          break;
        case "editExpense":
          panel = <EditExpensePanel instance={ap.id} hide={this.hidePanel}
                    postSubmit={updateTransaction}/>;
          break;
        case "editTransfer":
          panel = <TransferFormPanel instance={ap.id} hide={this.hidePanel}
                    postSubmit={updateTransaction}/>;
          break;
        case "removeIncome":
          panel = <IncomeDeletePanel id={ap.id} hide={this.hidePanel}
            postRemove={() => this.removeTransactionFromPage(ap.idx)} />;
          break;
        case "removeExpense":
          panel = <ExpenseDeletePanel id={ap.id} hide={this.hidePanel}
            postRemove={() => this.removeTransactionFromPage(ap.idx)} />;
          break;
        case "removeTransfer":
          panel = <TransferDeletePanel id={ap.id} hide={this.hidePanel}
            postRemove={() => this.removeTransactionFromPage(ap.idx)} />;
          break;
      }
    }

    return panel;
  }

  render() {
    if(this.props.isLoaded) {
      return <React.Fragment>
        <div id="transaction-container" className="panels">
          <div id="transactions" className="panel">
            <div id="transactions-grid" className="grid">
              {this.renderTransactions()}
            </div>
          </div>
          {this.renderRange()}
          {this.renderActionPanel()}
        </div>
        <div key="btns" className="btn-line">
          <PanelToggleButton key="1" icon={<FilterIcon />} label="Filter"
            id="filter" active={this.isPanelActive("filters")}
            toggle={this.toggleFilters} />
          <PanelToggleButton key="2" icon={<CalendarIcon />}
            label="Dates" id="dates" active={this.isPanelActive("changeDates")}
            toggle={this.toggleChangeDates} />
          <PanelToggleButton key="3" icon={<DetailsIcon />}
            label="All details" id="all-details" hideMarker={true}
            active={this.areAllDetailsShown()} toggle={this.toggleAllDetails} />
          <PanelToggleButton key="4" icon={<GroupingIcon />}
            label="Grouping" id="grouping"
            active={false} toggle={() => {}} />
        </div>
      </React.Fragment>
    } else {
      return <p>Loading...</p>;
    }
  }
}
