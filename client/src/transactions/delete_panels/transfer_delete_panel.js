import React from "react";
import { connect } from "react-redux";

import { removeTransfer } from "../../actions/transfer_actions.js";
import { getAvailableAmount } from "../../common/utilities.js";

import DeletePanel from "../delete_panel.jsx";
import Money from "../../common/money.js";

class TransferDeletePanel extends React.Component {
  isUpdateDisabled() {
    const available = getAvailableAmount(this.props.targetAccount);
    return available < this.props.transfer.target_amount;
  }

  getNote() {
    const account = this.props.targetAccount;

    if(this.isUpdateDisabled()) {
      return `${account.name} doesn't cover the removal. Increase its balance ` +
        "if you want to update it. Or remove the income without updating the " +
        "account.";
    } else {
      return null;
    }
  }

  render() {
    const sourceAmount = this.props.transfer.source_amount;
    const sourceAccName = this.props.sourceAccount.name;
    const targetAccName = this.props.targetAccount.name;

    return <DeletePanel hide={this.props.hide} remove={this.props.remove}
      postRemove={this.props.postRemove} id={this.props.id} model="Transfer"
      disabled={this.isUpdateDisabled()}
      accountName={`'${sourceAccName}' and '${targetAccName}'`}
      note={this.getNote()}>
      <div>Are you sure to remove '<Money amount={sourceAmount}
        currency={this.props.sourceAccount.currency_id} />' transfer
        from "{sourceAccName}"
        to "{targetAccName}"?
      </div>
    </DeletePanel>
  }
}

const mapStateToProps = (state, ownProps) => {
  const transfer = state.transfers[ownProps.id];
  return {
    transfer,
    sourceAccount: state.accounts[transfer.source_account_id],
    targetAccount: state.accounts[transfer.target_account_id]
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: (id, update_account) => dispatch(removeTransfer(id, update_account))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransferDeletePanel);
