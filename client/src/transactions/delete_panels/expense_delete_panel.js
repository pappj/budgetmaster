import React from "react";
import { connect } from "react-redux";

import { removeExpense } from "../../actions/expense_actions.js";

import DeletePanel from "../delete_panel.jsx";
import Money from "../../common/money.js";

class ExpenseDeletePanel extends React.Component {
  render() {
    return <DeletePanel hide={this.props.hide} remove={this.props.remove}
      postRemove={this.props.postRemove} id={this.props.id} model="Expense"
      accountName={`'${this.props.account.name}'`}>
      <div>Are you sure to remove '<Money amount={this.props.expense.amount}
        currency={this.props.account.currency_id} />' expense from category
        '{this.props.categoryName}' paid from "{this.props.account.name}"?
      </div>
    </DeletePanel>
  }
}

const mapStateToProps = (state, ownProps) => {
  const expense = state.expenses[ownProps.id];
  return {
    expense,
    account: state.accounts[expense.account_id],
    categoryName: state.categories[expense.category_id].name
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: (id, update_account) => dispatch(removeExpense(id, update_account))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpenseDeletePanel);
