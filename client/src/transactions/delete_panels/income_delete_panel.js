import React from "react";
import { connect } from "react-redux";

import { removeIncome } from "../../actions/income_actions.js";
import { getAvailableAmount } from "../../common/utilities.js";

import DeletePanel from "../delete_panel.jsx";
import Money from "../../common/money.js";

class IncomeDeletePanel extends React.Component {
  isUpdateDisabled() {
    return getAvailableAmount(this.props.account) < this.props.income.amount;
  }

  getNote() {
    const account = this.props.account;
    if(this.isUpdateDisabled()) {
      return `${account.name} doesn't cover the removal. Increase its balance ` +
        "if you want to update it. Or remove the income without updating the " +
        "account.";
    } else {
      return null;
    }
  }

  render() {
    return <DeletePanel hide={this.props.hide} remove={this.props.remove}
      postRemove={this.props.postRemove} id={this.props.id} model="Income"
      disabled={this.isUpdateDisabled()} note={this.getNote()}
      accountName={`'${this.props.account.name}'`}>
      <div>Are you sure to remove '<Money amount={this.props.income.amount}
        currency={this.props.account.currency_id} />' income received to
        "{this.props.account.name}"?
      </div>
    </DeletePanel>
  }
}

const mapStateToProps = (state, ownProps) => {
  const income = state.incomes[ownProps.id];
  return {
    income,
    account: state.accounts[income.account_id]
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: (id, update_account) => dispatch(removeIncome(id, update_account))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IncomeDeletePanel);
