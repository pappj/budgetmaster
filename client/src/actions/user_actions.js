import axios from "axios";
import { updateAccount } from "./account_actions.js";
import { fetchCurrencies } from "./currency_actions.js";

export const UPDATE_USER_DATA = "UPDATE_USER_DATA";
export const DELETE_USER_DATA = "DELETE_USER_DATA";

export function updateUserData(user) {
  return {
    type: UPDATE_USER_DATA,
    user
  };
}

export function removeUser() {
  return {
    type: DELETE_USER_DATA
  };
}

export function signedIn() {
  return function(dispatch) {
    dispatch(fetchCurrencies());

    return axios.get("/api/user/signed_in")
      .then((response) => {
        if(response.data.signed_in) {
          dispatch(updateUserData(response.data.user));
        } else {
          dispatch(removeUser());
        }
      });
  };
}

export function patchUserProfile(user_profile) {
  return function(dispatch) {
    return axios.patch("/api/user/profile", {user_profile})
      .then((response) => {
        dispatch(updateUserData(response.data.user));
        response.data.accounts.forEach(acc => dispatch(updateAccount(acc)));
      });
  }
}
