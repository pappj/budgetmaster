import { availablePlanningAmount, filterActiveGoals, until }
  from "../common/utilities.js";

import { fetchAccounts } from "./account_actions.js";
import { fetchExpensePlans } from "./expense_plan_actions.js";
import { fetchGoals } from "./goal_actions.js";

export const UPDATE_AVAILABLE_AMOUNT = "UPDATE_AVAILABLE_AMOUNT";

function availableAmountUpdated(amount) {
  return {
    type: UPDATE_AVAILABLE_AMOUNT,
    amount
  };
}

export function updateAvailableAmount() {
  return async function(dispatch, getState) {
    const loaded = getState().loaded;
    const loading = getState().loading;

    if(!loaded.accounts && !loading.accounts) {
      dispatch(fetchAccounts());
    }
    if(!loaded.expensePlans && !loading.expensePlans) {
      dispatch(fetchExpensePlans());
    }
    if(!loaded.goals && !loading.goals) {
      dispatch(fetchGoals());
    }

    await until(() => getState().loaded.accounts &&
                      getState().loaded.expensePlans &&
                      getState().loaded.goals);

    const newState = getState();

    const accounts = Object.values(newState.accounts).filter(acc => acc.in_plan);
    const activeGoals = filterActiveGoals(newState.goals);
    const expensePlans = Object.values(newState.expensePlans);

    const amount = availablePlanningAmount(accounts, expensePlans, activeGoals);
    dispatch(availableAmountUpdated(amount));
  };
}
