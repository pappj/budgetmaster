import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects, toDateOnlyString }
  from "../common/utilities.js";

import { receiveAccounts, fetchAccounts, accountSchema }
  from "./account_actions.js";

export const REQUEST_INCOMES = "REQUEST_INCOMES";
export const CANCEL_REQUEST_INCOMES = "CANCEL_REQUEST_INCOMES";
export const RECEIVE_INCOMES = "RECEIVE_INCOMES";
export const ADD_INCOME = "ADD_INCOME";
export const UPDATE_INCOME = "UPDATE_INCOME";
export const DELETE_INCOME = "DELETE_INCOME";

function requestIncomes() {
  return {
    type: REQUEST_INCOMES
  };
}

function cancelRequestIncomes() {
  return {
    type: CANCEL_REQUEST_INCOMES
  };
}

function receiveIncomes(incomes) {
  return {
    type: RECEIVE_INCOMES,
    incomes: convertAllObjects(incomes, convertIncome)
  };
}

function addIncome(income) {
  return {
    type: ADD_INCOME,
    income: convertIncome(income)
  };
}

function updateIncome(income) {
  return {
    type: UPDATE_INCOME,
    income: convertIncome(income)
  };
}

function deleteIncome(id) {
  return {
    type: DELETE_INCOME,
    id
  };
}

const incomeEntity = new schema.Entity("incomes");
const incomeSchema = [incomeEntity];

export function fetchIncomes(startDate, endDate) {
  return function(dispatch, getState) {
    dispatch(fetchAccounts());

    if(!getState().loaded.incomes && !getState().loading.incomes) {
      dispatch(requestIncomes());
      return api.get(dispatch, "/api/incomes",
                     {start: toDateOnlyString(startDate),
                      end: toDateOnlyString(endDate)})
        .then((response) => {
          const data = normalize(response.data.incomes, incomeSchema);
          dispatch(receiveIncomes(data.entities.incomes || {}));
        })
        .catch((error) => {
          dispatch(cancelRequestIncomes());
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createIncome(income) {
  return function(dispatch) {
    return api.post(dispatch, "/api/incomes", {income})
      .then((response) => {
        dispatch(addIncome(response.data.income));
        updateDependentModels(dispatch, response);
      });
  };
}

export function patchIncome(income) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/incomes/", {income})
      .then((response) => {
        dispatch(updateIncome(response.data.income));
        updateDependentModels(dispatch, response);

        return convertIncome(response.data.income);
      });
  };
}

export function removeIncome(id, update_account) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/incomes/" + id, {update_account})
      .then((response) => {
        dispatch(deleteIncome(id));
        updateDependentModels(dispatch, response);
      });
  };
}

function convertIncome(income) {
  return convertNumberFields(income, ["amount"]);
}

function updateDependentModels(dispatch, response) {
  if(response.data.accounts) {
    const data = normalize(response.data.accounts, accountSchema);
    dispatch(receiveAccounts(data.entities.accounts));
  }
}
