import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects } from "../common/utilities.js";

export const REQUEST_ACCOUNTS = "REQUEST_ACCOUNTS";
export const CANCEL_REQUEST_ACCOUNTS = "CANCEL_REQUEST_ACCOUNTS";
export const RECEIVE_ACCOUNTS = "RECEIVE_ACCOUNTS";
export const ADD_ACCOUNT = "ADD_ACCOUNT";
export const UPDATE_ACCOUNT = "UPDATE_ACCOUNT";
export const DELETE_ACCOUNT = "DELETE_ACCOUNT";

function requestAccounts() {
  return {
    type: REQUEST_ACCOUNTS
  };
}

function cancelRequestAccounts() {
  return {
    type: CANCEL_REQUEST_ACCOUNTS
  };
}

export function receiveAccounts(accounts) {
  return {
    type: RECEIVE_ACCOUNTS,
    accounts: convertAllObjects(accounts, convertAccount)
  };
}

function addAccount(account) {
  return {
    type: ADD_ACCOUNT,
    account: convertAccount(account)
  };
}

export function updateAccount(account) {
  return {
    type: UPDATE_ACCOUNT,
    account: convertAccount(account)
  };
}

function deleteAccount(id) {
  return {
    type: DELETE_ACCOUNT,
    id
  };
}

const accountEntity = new schema.Entity("accounts");
export const accountSchema = [accountEntity];

export function fetchAccounts() {
  return function(dispatch, getState) {
    if(!getState().loaded.accounts && !getState().loading.accounts) {
      dispatch(requestAccounts());
      return api.get(dispatch, "/api/accounts")
        .then((response) => {
          const data = normalize(response.data.accounts, accountSchema);
          dispatch(receiveAccounts(data.entities.accounts || {}));
        })
        .catch((error) => {
          dispatch(cancelRequestAccounts());
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createAccount(account) {
  return function(dispatch) {
    return api.post(dispatch, "/api/accounts", {account})
      .then((response) => {
        dispatch(addAccount(response.data));
      });
  };
}

export function patchAccount(account) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/accounts/", {account})
      .then((response) => {
        dispatch(updateAccount(account));
      });
  };
}

export function removeAccount(id) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/accounts/" + id)
      .then((response) => {
        dispatch(deleteAccount(id));
      });
  };
}

function convertAccount(account) {
  return convertNumberFields(account, ["current_balance", "credit"]);
}
