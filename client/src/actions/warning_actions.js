import regeneratorRuntime from "regenerator-runtime"; // required for async

import Month from "../common/month.js";
import { filterMonthPlans, filterActiveGoals, convertToIdObject, until }
  from "../common/utilities.js";
import { goalWarnings } from "../notifications/goals.js";
import { expenseWarnings } from "../notifications/expenses.js";

import { fetchGoals } from "./goal_actions.js";
import { fetchGoalPlans } from "./goal_plan_actions.js";
import { fetchCategories } from "./category_actions.js";
import { fetchExpensePlans } from "./expense_plan_actions.js";
import { fetchMonthlyExpenses } from "./monthly_expense_actions.js";


export const GOAL_WARNINGS_UPDATED = "GOAL_WARNINGS_UPDATED";
export const EXPENSE_WARNINGS_UPDATED = "EXPENSE_WARNINGS_UPDATED";

function goalWarningsUpdated(warnings) {
  return {
    type: GOAL_WARNINGS_UPDATED,
    warnings
  };
}

function expenseWarningsUpdated(warnings) {
  return {
    type: EXPENSE_WARNINGS_UPDATED,
    warnings
  };
}

export function updateGoalWarnings() {
  return async function(dispatch, getState) {
    const loaded = getState().loaded;
    const loading = getState().loading;

    // Start loading goals and plans if no other components
    // have done it so far.
    if(!loaded.goals && !loading.goals) {
      dispatch(fetchGoals());
    }
    if(!loaded.goalPlans && !loading.goalPlans) {
      dispatch(fetchGoalPlans());
    }

    await until(() => getState().loaded.goals && getState().loaded.goalPlans);

    const newState = getState();
    const goals = convertToIdObject(filterActiveGoals(newState.goals));
    const warnings = goalWarnings(goals, newState.goalPlans);
    dispatch(goalWarningsUpdated(warnings));
  };
}

export function updateExpenseWarnings() {
  return async function(dispatch, getState) {
    const loaded = getState().loaded;
    const loading = getState().loading;

    if(!loaded.categories && !loading.categories) {
      fetchCategories();
    }
    if(!loaded.expensePlans && !loading.expensePlans) {
      fetchExpensePlans();
    }
    if(!loaded.monthlyExpenses && !loading.monthlyExpenses) {
      fetchMonthlyExpenses();
    }

    await until(() => getState().loaded.categories &&
                      getState().loaded.expensePlans &&
                      getState().loaded.monthlyExpenses);

    const newState = getState();
    const currentExpensePlans = filterMonthPlans(newState.expensePlans,
                                                 new Month());
    const warnings = expenseWarnings(newState.categories,
                                     newState.recurringExpenses,
                                     currentExpensePlans,
                                     newState.monthlyExpenses,
                                     newState.user.currency_id);
    dispatch(expenseWarningsUpdated(warnings));
  };
}
