import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects, toDateOnlyString }
  from "../common/utilities.js";

import { receiveAccounts, fetchAccounts, accountSchema }
  from "./account_actions.js";
import { fetchCategories } from "./category_actions.js";
import { updateExpensePlan } from "./expense_plan_actions.js";
import { receiveMonthlyExpenses } from "./monthly_expense_actions.js";

export const REQUEST_EXPENSES = "REQUEST_EXPENSES";
export const CANCEL_REQUEST_EXPENSES = "CANCEL_REQUEST_EXPENSES";
export const RECEIVE_EXPENSES = "RECEIVE_EXPENSES";
export const ADD_EXPENSE = "ADD_EXPENSE";
export const UPDATE_EXPENSE = "UPDATE_EXPENSE";
export const DELETE_EXPENSE = "DELETE_EXPENSE";

function requestExpenses() {
  return {
    type: REQUEST_EXPENSES
  };
}

function cancelRequestExpenses() {
  return {
    type: CANCEL_REQUEST_EXPENSES
  };
}

function receiveExpenses(expenses) {
  return {
    type: RECEIVE_EXPENSES,
    expenses: convertAllObjects(expenses, convertExpense)
  };
}

function addExpense(expense) {
  return {
    type: ADD_EXPENSE,
    expense: convertExpense(expense)
  };
}

function updateExpense(expense) {
  return {
    type: UPDATE_EXPENSE,
    expense: convertExpense(expense)
  };
}

function deleteExpense(id) {
  return {
    type: DELETE_EXPENSE,
    id
  };
}

const expenseEntity = new schema.Entity("expenses");
const expenseSchema = [expenseEntity];

export function fetchExpenses(startDate, endDate) {
  return function(dispatch, getState) {
    dispatch(fetchAccounts());
    dispatch(fetchCategories());

    if(!getState().loaded.expenses && !getState().loading.expenses) {
      dispatch(requestExpenses());
      return api.get(dispatch, "/api/expenses",
                     {start: toDateOnlyString(startDate),
                      end: toDateOnlyString(endDate)})
        .then((response) => {
          const data = normalize(response.data.expenses, expenseSchema);
          dispatch(receiveExpenses(data.entities.expenses || {}));
        })
        .catch((error) => {
          dispatch(cancelRequestExpenses());
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createExpense(expense) {
  return function(dispatch) {
    return api.post(dispatch, "/api/expenses", prepareToPost(expense))
      .then((response) => {
        const retExpense = response.data.expense;
        dispatch(addExpense({...retExpense, currencyId: expense.currencyId}));
        updateDependentModels(dispatch, response);
      });
  };
}

export function patchExpense(expense) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/expenses/", prepareToPost(expense))
      .then((response) => {
        const retExpense = response.data.expense;
        dispatch(updateExpense({...retExpense, currencyId: expense.currencyId}));
        updateDependentModels(dispatch, response);

        return convertExpense(response.data.expense);
      });
  };
}

export function removeExpense(id, update_account) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/expenses/" + id, {update_account})
      .then((response) => {
        dispatch(deleteExpense(id));
        updateDependentModels(dispatch, response);
      });
  };
}

function convertExpense(expense) {
  return convertNumberFields(expense, ["amount"]);
}

function updateDependentModels(dispatch, response) {
  if(response.data.accounts) {
    const data = normalize(response.data.accounts, accountSchema);
    dispatch(receiveAccounts(data.entities.accounts));
  }

  const plan = response.data.plan;
  if(plan) {
    dispatch(updateExpensePlan(plan));
  }

  const monthlyExpenses = response.data.monthly_expenses;
  if(monthlyExpenses) {
    dispatch(receiveMonthlyExpenses(monthlyExpenses));
  }
}

function prepareToPost(exp) {
  const newExp = {...exp};
  delete newExp["currencyId"];
  return {expense: newExp};
}
