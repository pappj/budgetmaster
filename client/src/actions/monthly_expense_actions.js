import axios from "axios";

import api from "../common/api_calls.js";
import { convertAllObjects } from "../common/utilities.js";

export const REQUEST_MONTHLY_EXPENSES = "REQUEST_MONTHLY_EXPENSES";
export const CANCEL_REQUEST_MONTHLY_EXPENSES = "CANCEL_REQUEST_MONTHLY_EXPENSES";
export const RECEIVE_MONTHLY_EXPENSES = "RECEIVE_MONTHLY_EXPENSES";
export const ADD_MONTHLY_EXPENSE = "ADD_MONTHLY_EXPENSE";
export const UPDATE_MONTHLY_EXPENSE = "UPDATE_MONTHLY_EXPENSE";
export const DELETE_MONTHLY_EXPENSE = "DELETE_MONTHLY_EXPENSE";

function requestMonthlyExpenses() {
  return {
    type: REQUEST_MONTHLY_EXPENSES
  };
}

function cancelRequestMonthlyExpenses() {
  return {
    type: CANCEL_REQUEST_MONTHLY_EXPENSES
  };
}

export function receiveMonthlyExpenses(monthlyExpenses) {
  return {
    type: RECEIVE_MONTHLY_EXPENSES,
    monthlyExpenses: convertAllObjects(monthlyExpenses, convertMonthlyExpense)
  };
}

export function fetchMonthlyExpenses() {
  return function(dispatch, getState) {
    if(!getState().loaded.monthlyExpenses && !getState().loading.monthlyExpenses) {
      dispatch(requestMonthlyExpenses());
      return api.get(dispatch, "/api/monthly_expenses")
        .then((response) => {
          dispatch(receiveMonthlyExpenses(response.data));

          return response;
        })
        .catch((error) => {
          dispatch(cancelRequestMonthlyExpenses());

          return error;
        });
    } else {
      return Promise.resolve();
    }
  };
}

function convertMonthlyExpense(exp) {
  let parsedExpense = {};
  Object.keys(exp).forEach(key => {
    parsedExpense[key] = parseFloat(exp[key]);
  });
  return parsedExpense;
}
