import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects } from "../common/utilities.js";

import { fetchCategories } from "./category_actions.js";

export const REQUEST_EXPENSE_PLANS = "REQUEST_EXPENSE_PLANS";
export const CANCEL_REQUEST_EXPENSE_PLANS = "CANCEL_REQUEST_EXPENSE_PLANS";
export const RECEIVE_EXPENSE_PLANS = "RECEIVE_EXPENSE_PLANS";
export const ADD_EXPENSE_PLAN = "ADD_EXPENSE_PLAN";
export const UPDATE_EXPENSE_PLAN = "UPDATE_EXPENSE_PLAN";

function requestExpensePlans() {
  return {
    type: REQUEST_EXPENSE_PLANS
  };
}

function cancelRequestExpensePlans() {
  return {
    type: CANCEL_REQUEST_EXPENSE_PLANS
  };
}

function receiveExpensePlans(expensePlans) {
  return {
    type: RECEIVE_EXPENSE_PLANS,
    expensePlans: convertAllObjects(expensePlans, convertExpensePlan)
  };
}

function addExpensePlan(expensePlan) {
  return {
    type: ADD_EXPENSE_PLAN,
    expensePlan: convertExpensePlan(expensePlan)
  };
}

export function updateExpensePlan(expensePlan) {
  return {
    type: UPDATE_EXPENSE_PLAN,
    expensePlan: convertExpensePlan(expensePlan)
  };
}

const expensePlanEntity = new schema.Entity("expense_plans");
const expensePlanSchema = [expensePlanEntity];

export function fetchExpensePlans() {
  return function(dispatch, getState) {
    dispatch(fetchCategories());

    if(!getState().loaded.expensePlans && !getState().loading.expensePlans) {
      dispatch(requestExpensePlans());
      return api.get(dispatch, "/api/expense_plans")
        .then((response) => {
          const data = normalize(response.data.expense_plans, expensePlanSchema);
          dispatch(receiveExpensePlans(data.entities.expense_plans || {}));

          return response;
        })
        .catch((error) => {
          dispatch(cancelRequestExpensePlans());

          return error;
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createExpensePlan(expense_plan) {
  return function(dispatch) {
    return api.post(dispatch, "/api/expense_plans", {expense_plan})
      .then((response) => {
        dispatch(addExpensePlan(response.data));
      });
  };
}

export function patchExpensePlan(expense_plan) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/expense_plans/", {expense_plan})
      .then((response) => {
        dispatch(updateExpensePlan(response.data));
      });
  };
}

function convertExpensePlan(expensePlan) {
  return convertNumberFields(expensePlan, ["amount"]);
}
