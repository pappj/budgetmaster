import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";

import { receiveRecurringExpenses, addRecurringExpenses, updateRecurringExpenses,
         deleteRecurringExpenses }
  from "./recurring_expense_actions.js";

export const REQUEST_CATEGORIES = "REQUEST_CATEGORIES";
export const CANCEL_REQUEST_CATEGORIES = "CANCEL_REQUEST_CATEGORIES";
export const RECEIVE_CATEGORIES = "RECEIVE_CATEGORIES";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const UPDATE_CATEGORY = "UPDATE_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";

function requestCategories() {
  return {
    type: REQUEST_CATEGORIES
  };
}

function cancelRequestCategories() {
  return {
    type: CANCEL_REQUEST_CATEGORIES
  };
}

function receiveCategories(categories) {
  return {
    type: RECEIVE_CATEGORIES,
    categories
  };
}

function addCategory(category) {
  return {
    type: ADD_CATEGORY,
    category
  };
}

function updateCategory(category) {
  return {
    type: UPDATE_CATEGORY,
    category
  };
}

function deleteCategory(id) {
  return {
    type: DELETE_CATEGORY,
    id
  };
}

const recurringExpenseEntity = new schema.Entity("recurring_expenses");
const categoryEntity = new schema.Entity("categories", {
  recurring_expenses: [recurringExpenseEntity]
});
const categorySchema = [categoryEntity];

export function fetchCategories() {
  return function(dispatch, getState) {
    if(!getState().loaded.categories && !getState().loading.categories) {
      dispatch(requestCategories());
      return api.get(dispatch, "/api/categories")
        .then((response) => {
          const data = normalize(response.data.categories, categorySchema);
          dispatch(receiveCategories(data.entities.categories || {}));
          dispatch(receiveRecurringExpenses(data.entities.recurring_expenses||{}));

          return response;
        })
        .catch((error) => {
          dispatch(cancelRequestCategories());

          return error;
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createCategory(cat, recExps) {
  return function(dispatch) {
    return api.post(dispatch, "/api/categories", composeDataToSend(cat, recExps))
      .then((response) => {
        const data = normalize(response.data, categoryEntity);
        dispatch(addCategory(data.entities.categories[data.result]));
        dispatch(addRecurringExpenses(data.entities.recurring_expenses || {}));

        return response;
      });
  };
}

export function patchCategory(cat, recExps, deleteRecExps) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/categories/",
                     composeDataToSend(cat, recExps, deleteRecExps))
      .then((response) => {
        const data = normalize(response.data, categoryEntity);
        dispatch(updateCategory(data.entities.categories[data.result]));
        dispatch(updateRecurringExpenses(data.entities.recurring_expenses || {}));
        if(deleteRecExps.length > 0) {
          dispatch(deleteRecurringExpenses(deleteRecExps));
        }

        return response;
      });
  };
}

export function removeCategory(id) {
  return function(dispatch, getState) {
    return api.delete(dispatch, "/api/categories/" + id)
      .then((response) => {
        dispatch(
          deleteRecurringExpenses(getState().categories[id].recurring_expenses)
        );
        dispatch(deleteCategory(id));
      });
  };
}

function composeDataToSend(cat, recExps, deleteRecExps = []) {
  return {category: {
      id: cat.id,
      name: cat.name,
      recurring_expenses_attributes: [
        ...recExps,
        ...deleteRecExps.map(id => { return {id, _destroy: '1'} })
      ]
    }
  };
}
