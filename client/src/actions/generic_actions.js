export const CLEAR_STORE = "CLEAR_STORE";

export function clearStore() {
  return {
    type: CLEAR_STORE
  };
}
