import axios from "axios";
import { normalize, schema } from "normalizr";

export const REQUEST_CURRENCIES = "REQUEST_CURRENCIES";
export const CANCEL_REQUEST_CURRENCIES = "CANCEL_REQUEST_CURRENCIES";
export const RECEIVE_CURRENCIES = "RECEIVE_CURRENCIES";

function requestCurrencies() {
  return {
    type: REQUEST_CURRENCIES
  };
}

function cancelRequestCurrencies() {
  return {
    type: CANCEL_REQUEST_CURRENCIES
  };
}

function receiveCurrencies(currencies) {
  return {
    type: RECEIVE_CURRENCIES,
    currencies
  };
}

const currencyEntity = new schema.Entity("currencies");
const currencySchema = [currencyEntity];

export function fetchCurrencies() {
  return function(dispatch, getState) {
    if(!getState().loaded.currencies && !getState().loading.currencies) {
      dispatch(requestCurrencies());
      return axios.get("/api/currencies")
        .then((response) => {
          const data = normalize(response.data, currencySchema);
          dispatch(receiveCurrencies(data.entities.currencies));
        })
        .catch((error) => {
          dispatch(cancelRequestCurrencies());
        });
    } else {
      return Promise.resolve();
    }
  };
}
