import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects } from "../common/utilities.js";

export const RECEIVE_RECURRING_EXPENSES = "RECEIVE_RECURRING_EXPENSES";
export const ADD_RECURRING_EXPENSES = "ADD_RECURRING_EXPENSES";
export const UPDATE_RECURRING_EXPENSES = "UPDATE_RECURRING_EXPENSES";
export const DELETE_RECURRING_EXPENSES = "DELETE_RECURRING_EXPENSES";

export function receiveRecurringExpenses(recurringExpenses) {
  return {
    type: RECEIVE_RECURRING_EXPENSES,
    recurringExpenses: convertAllObjects(recurringExpenses, convertRecurringExpense)
  };
}

export function addRecurringExpenses(recurringExpenses) {
  return {
    type: ADD_RECURRING_EXPENSES,
    recurringExpenses: convertAllObjects(recurringExpenses, convertRecurringExpense)
  };
}

export function updateRecurringExpenses(recurringExpenses) {
  return {
    type: UPDATE_RECURRING_EXPENSES,
    recurringExpenses: convertAllObjects(recurringExpenses, convertRecurringExpense)
  };
}

export function deleteRecurringExpenses(ids) {
  return {
    type: DELETE_RECURRING_EXPENSES,
    ids
  };
}

function convertRecurringExpense(recurringExpense) {
  return convertNumberFields(recurringExpense, ["amount"]);
}
