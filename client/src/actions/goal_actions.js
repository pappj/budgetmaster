import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects } from "../common/utilities.js";
import { updateGoalPlan, deleteGoalPlan } from "./goal_plan_actions.js";

export const REQUEST_GOALS = "REQUEST_GOALS";
export const CANCEL_REQUEST_GOALS = "CANCEL_REQUEST_GOALS";
export const RECEIVE_GOALS = "RECEIVE_GOALS";
export const ADD_GOAL = "ADD_GOAL";
export const UPDATE_GOAL = "UPDATE_GOAL";
export const DELETE_GOAL = "DELETE_GOAL";

function requestGoals() {
  return {
    type: REQUEST_GOALS
  };
}

function cancelRequestGoals() {
  return {
    type: CANCEL_REQUEST_GOALS
  };
}

function receiveGoals(goals) {
  return {
    type: RECEIVE_GOALS,
    goals: convertAllObjects(goals, convertGoal)
  };
}

function addGoal(goal) {
  return {
    type: ADD_GOAL,
    goal: convertGoal(goal)
  };
}

function updateGoal(goal) {
  return {
    type: UPDATE_GOAL,
    goal: convertGoal(goal)
  };
}

function deleteGoal(id) {
  return {
    type: DELETE_GOAL,
    id
  };
}

const goalEntity = new schema.Entity("goals");
const goalSchema = [goalEntity];

export function fetchGoals() {
  return function(dispatch, getState) {
    if(!getState().loaded.goals && !getState().loading.goals) {
      dispatch(requestGoals());
      return api.get(dispatch, "/api/goals")
        .then((response) => {
          const data = normalize(response.data.goals, goalSchema);
          dispatch(receiveGoals(data.entities.goals || {}));

          return response;
        })
        .catch((error) => {
          dispatch(cancelRequestGoals());

          return error;
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createGoal(goal) {
  return function(dispatch) {
    return api.post(dispatch, "/api/goals", {goal})
      .then((response) => {
        dispatch(addGoal(response.data));

        return response.data;
      });
  };
}

export function patchGoal(goal) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/goals/", {goal})
      .then((response) => {
        dispatch(updateGoal(response.data.goal));
        const plan = response.data.plan;
        if(plan) {
          if(plan.removedId) {
            dispatch(deleteGoalPlan(plan.removedId));
          } else {
            dispatch(updateGoalPlan(plan));
          }
        }

        return response.data;
      });
  };
}

export function removeGoal(id) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/goals/" + id)
      .then((response) => {
        dispatch(deleteGoal(id));
      });
  };
}

function convertGoal(goal) {
  return convertNumberFields(goal, ["target_amount", "allocated_amount"]);
}
