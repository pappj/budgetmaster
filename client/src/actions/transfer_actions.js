import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertAllObjects, convertNumberFields, toDateOnlyString }
  from "../common/utilities.js";

import { receiveAccounts, fetchAccounts, accountSchema }
  from "./account_actions.js";

export const REQUEST_TRANSFERS = "REQUEST_TRANSFERS";
export const CANCEL_REQUEST_TRANSFERS = "CANCEL_REQUEST_TRANSFERS";
export const RECEIVE_TRANSFERS = "RECEIVE_TRANSFERS";
export const ADD_TRANSFER = "ADD_TRANSFER";
export const UPDATE_TRANSFER = "UPDATE_TRANSFER";
export const DELETE_TRANSFER = "DELETE_TRANSFER";

function requestTransfers() {
  return {
    type: REQUEST_TRANSFERS
  };
}

function cancelRequestTransfers() {
  return {
    type: CANCEL_REQUEST_TRANSFERS
  };
}

function receiveTransfers(transfers) {
  return {
    type: RECEIVE_TRANSFERS,
    transfers: convertAllObjects(transfers, convertTransfer)
  };
}

function addTransfer(transfer) {
  return {
    type: ADD_TRANSFER,
    transfer: convertTransfer(transfer)
  };
}

function updateTransfer(transfer) {
  return {
    type: UPDATE_TRANSFER,
    transfer: convertTransfer(transfer)
  };
}

function deleteTransfer(id) {
  return {
    type: DELETE_TRANSFER,
    id
  };
}

const transferEntity = new schema.Entity("transfers");
const transferSchema = [transferEntity];

export function fetchTransfers(startDate, endDate) {
  return function(dispatch, getState) {
    dispatch(fetchAccounts());

    if(!getState().loaded.transfers && !getState().loading.transfers) {
      dispatch(requestTransfers());
      return api.get(dispatch, "/api/transfers",
                     {start: toDateOnlyString(startDate),
                      end: toDateOnlyString(endDate)})
        .then((response) => {
          const data = normalize(response.data.transfers, transferSchema);
          dispatch(receiveTransfers(data.entities.transfers || {}));
        })
        .catch((error) => {
          dispatch(cancelRequestTransfers());
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createTransfer(transfer) {
  return function(dispatch) {
    return api.post(dispatch, "/api/transfers", {transfer})
      .then((response) => {
        dispatch(addTransfer(response.data.transfer));
        updateDependentModels(dispatch, response);
      });
  };
}

export function patchTransfer(transfer) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/transfers/", {transfer})
      .then((response) => {
        dispatch(updateTransfer(response.data.transfer));
        updateDependentModels(dispatch, response);

        return convertTransfer(response.data.transfer);
      });
  };
}

export function removeTransfer(id, update_account) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/transfers/" + id, {update_account})
      .then((response) => {
        dispatch(deleteTransfer(id));
        updateDependentModels(dispatch, response);
      });
  };
}

function convertTransfer(transfer) {
  return convertNumberFields(transfer, ["source_amount", "target_amount"]);
}

function updateDependentModels(dispatch, response) {
  if(response.data.accounts) {
    const data = normalize(response.data.accounts, accountSchema);
    dispatch(receiveAccounts(data.entities.accounts));
  }
}
