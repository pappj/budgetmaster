import axios from "axios";
import { normalize, schema } from "normalizr";
import api from "../common/api_calls.js";
import { convertNumberFields, convertAllObjects } from "../common/utilities.js";

import { fetchGoals } from "./goal_actions.js";

export const REQUEST_GOAL_PLANS = "REQUEST_GOAL_PLANS";
export const CANCEL_REQUEST_GOAL_PLANS = "CANCEL_REQUEST_GOAL_PLANS";
export const RECEIVE_GOAL_PLANS = "RECEIVE_GOAL_PLANS";
export const ADD_GOAL_PLAN = "ADD_GOAL_PLAN";
export const UPDATE_GOAL_PLAN = "UPDATE_GOAL_PLAN";
export const DELETE_GOAL_PLAN = "DELETE_GOAL_PLAN";

function requestGoalPlans() {
  return {
    type: REQUEST_GOAL_PLANS
  };
}

function cancelRequestGoalPlans() {
  return {
    type: CANCEL_REQUEST_GOAL_PLANS
  };
}

function receiveGoalPlans(goalPlans) {
  return {
    type: RECEIVE_GOAL_PLANS,
    goalPlans: convertAllObjects(goalPlans, convertGoalPlan)
  };
}

function addGoalPlan(goalPlan) {
  return {
    type: ADD_GOAL_PLAN,
    goalPlan: convertGoalPlan(goalPlan)
  };
}

export function updateGoalPlan(goalPlan) {
  return {
    type: UPDATE_GOAL_PLAN,
    goalPlan: convertGoalPlan(goalPlan)
  };
}

export function deleteGoalPlan(id) {
  return {
    type: DELETE_GOAL_PLAN,
    id
  };
}

const goalPlanEntity = new schema.Entity("goal_plans");
const goalPlanSchema = [goalPlanEntity];

export function fetchGoalPlans() {
  return function(dispatch, getState) {
    dispatch(fetchGoals());

    if(!getState().loaded.goalPlans && !getState().loading.goalPlans) {
      dispatch(requestGoalPlans());
      return api.get(dispatch, "/api/goal_plans")
        .then((response) => {
          const data = normalize(response.data.goal_plans, goalPlanSchema);
          dispatch(receiveGoalPlans(data.entities.goal_plans || {}));

          return response;
        })
        .catch((error) => {
          dispatch(cancelRequestGoalPlans());

          return error;
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createGoalPlan(goal_plan) {
  return function(dispatch) {
    return api.post(dispatch, "/api/goal_plans", {goal_plan})
      .then((response) => {
        dispatch(addGoalPlan(response.data));
      });
  };
}

export function patchGoalPlan(goal_plan) {
  return function(dispatch) {
    return api.patch(dispatch, "/api/goal_plans/", {goal_plan})
      .then((response) => {
        dispatch(updateGoalPlan(goal_plan));
      });
  };
}

export function removeGoalPlan(id) {
  return function(dispatch) {
    return api.delete(dispatch, "/api/goal_plans/" + id)
      .then((response) => {
        dispatch(deleteGoalPlan(id));
      });
  };
}

function convertGoalPlan(goalPlan) {
  return convertNumberFields(goalPlan, ["monthly_amount", "target_amount"]);
}
