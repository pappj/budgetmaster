import React from "react";

export default class NotificationPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };

    this.expand = this.expand.bind(this);
    this.collapse = this.collapse.bind(this);
  }

  expand() {
    this.setState({expanded: true});
  }

  collapse() {
    this.setState({expanded: false});
  }

  renderNotifications() {
    return Object.keys(this.props.items).map(k => {
      return <li key={k}>{this.props.items[k].message}</li>;
    });
  }

  renderExpandButton() {
    const cb = this.state.expanded ? this.collapse : this.expand;
    const label = this.state.expanded ? "Collapse" : "Expand";
    return <button className="btn-transparent" onClick={cb}>{label}</button>;
  }

  render() {
    const expandedClass = this.state.expanded ? "expanded" : "collapsed"
    if(this.state.expanded) {
      return <React.Fragment>
        <div key="notif" id="notif-panel" className="panel expanded">
          <ul>
            {this.renderNotifications()}
          </ul>
          {this.renderExpandButton()}
        </div>
        <div key="shadow" id="shadow-box" />
      </React.Fragment>;
    } else {
      return <div key="notif" id="notif-panel" className="panel collapsed">
        <ul>
          {this.renderNotifications()}
        </ul>
        {this.renderExpandButton()}
      </div>;
    }
  }
}
