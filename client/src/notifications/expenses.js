import React from 'react';

import Month from "../common/month.js";
import { calculateDueAmount } from "../utilities/expenses.js";

export const WNotCovered = "notCovered";

function notCoveredExpenses(categories,
                            dueAmounts,
                            plans,
                            expenses,
                            warnings,
                            userCurrencyId) {
  Object.keys(categories).forEach(catId => {
    const dueAmount = dueAmounts[catId];
    const spentAmount = (expenses[catId] || {})[userCurrencyId] || 0;
    const plannedAmount = (plans[catId] || {}).amount || 0;
    if(dueAmount > spentAmount + plannedAmount) {
      warnings[catId] = {
        type: WNotCovered,
        message: <p className="warning">This month's due amount of <em>
          {categories[catId].name}</em> is not covered yet. Increase
          the <em>allocated amount</em>.</p>
      };
    }
  });
}

export function expenseWarnings(categories,
                                recurringExpenses,
                                expensePlans,
                                monthlyExpenses,
                                userCurrencyId) {
  const warnings = {};
  const dueAmounts = {};
  Object.values(categories).forEach(cat => {
    const catRecurringExpenses =
      cat.recurring_expenses.map(id => recurringExpenses[id]);
    dueAmounts[cat.id] = calculateDueAmount(catRecurringExpenses, new Month());
  });
  notCoveredExpenses(categories,
                     dueAmounts,
                     expensePlans,
                     monthlyExpenses,
                     warnings,
                     userCurrencyId);

  return warnings;
}
