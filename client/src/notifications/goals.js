import React from 'react';

import Month from "../common/month.js";
import { calculateOntrackAmount } from "../common/utilities.js";

export const WNoPlan = "noPlan";
export const WObsoletePlan = "obsoletePlan";
export const WTargetReached = "targetReached";
export const WBehindSchedule = "behindSchedule";

function hasTarget(goal) {
  return Boolean(goal.target_amount || goal.target_date);
}

function hasNoPlan(goalId, plans) {
  return plans.filter(p => p.goal_id == goalId).length == 0;
}

function removePlans(goalId, plans) {
  Object.values(plans).forEach(p => {
    if(p.goal_id == goalId) {
      delete plans[p.id];
    }
  });
}

function goalsWithPastTarget(goals, plans, warnings) {
  Object.values(goals).forEach(g => {
    if(g.target_date) {
      if((new Month(g.target_date)).isEarlierThan(new Month())) {
        delete goals[g.id];
        removePlans(g.id, plans);
        warnings[g.id] = {
          type: WTargetReached,
          message: <p className="warning">
            <em>{g.title}</em> reached target date. Extend its target date or
            mark it 'achieved' on <em>Basics</em> page.
          </p>
        };
      } else if((new Month(g.target_date)).isEqualTo(new Month())) {
        delete goals[g.id];
        removePlans(g.id, plans);
      }
    }
  });
}

function obsoletePlans(goals, plans, warnings) {
  Object.values(plans).forEach(p => {
    const goal = goals[p.goal_id];
    if(p.obsolete && hasTarget(goal)) {
      delete plans[p.id];
      delete goals[goal.id];
      warnings[goal.id] = {
        type: WObsoletePlan,
        message: <p className="warning">
          <em>{goal.title}</em> has an obsolete plan. <em>Update it</em>.
        </p>
      };
    }
  });
}

function noPlan(goals, plans, warnings) {
  Object.values(goals).forEach(g => {
    if(hasTarget(g) && hasNoPlan(g.id, Object.values(plans))) {
      delete goals[g.id];
      warnings[g.id] = {
        type: WNoPlan,
        message: <p className="warning">
          <em>{g.title}</em> has no plan. <em>Create a plan for it</em>.
        </p>
      };
    };
  });
}

function behindSchedule(goals, plans, warnings) {
  Object.values(plans).forEach(plan => {
    const goal = goals[plan.goal_id];

    if(hasTarget(goal)) {
      const requiredAmount = calculateOntrackAmount(
        plan.monthly_amount,
        goal.target_amount || plan.target_amount,
        goal.target_date || plan.target_date
      );

      if(goal.allocated_amount < requiredAmount) {
        delete goals[goal.id];
        delete plans[plan.id];
        warnings[goal.id] = {
          type: WBehindSchedule,
          message: <p className="warning">
            <em>{goal.title}</em> is behind schedule. Increase its <em>allocated
            amount</em> or extend the <em>plan</em>.
          </p>
        };
      }
    }
  });
}

export function goalWarnings(goals, plans) {
  const goalsCopy = Object.assign({}, goals);
  const plansCopy = Object.assign({}, plans);
  const funs = [goalsWithPastTarget, obsoletePlans, noPlan, behindSchedule];
  const warnings = {};
  funs.forEach(f => f(goalsCopy, plansCopy, warnings));

  return warnings;
}
