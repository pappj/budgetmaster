import React from "react";

import Money from "../../common/money.js";

import EditIcon from "../../icons/edit.svg";
import DeleteIcon from "../../icons/delete.svg";

export default class AccountsPanelView extends React.Component {
  componentDidMount() {
    this.props.fetchAccounts();
  }

  shouldComponentUpdate(newProps) {
    return newProps.isLoaded != this.props.isLoaded ||
      newProps.accounts != this.props.accounts;
  }

  getBalance(acc) {
    return <Money amount={acc.current_balance} currency={acc.currency_id} />;
  }

  numberOfAccounts() {
    return Object.keys(this.props.accounts).length;
  }

  renderHeaders() {
    if(this.numberOfAccounts() > 0) {
      return <React.Fragment>
        <h2>Accounts</h2>
        <h2 className="span-2 panel-header">Balance</h2>
        <div className="empty-cell-2" />
      </React.Fragment>
    } else {
      return <React.Fragment>
        <h2>Accounts</h2>
        <div className="empty-cell-3" />
      </React.Fragment>
    }
  }

  renderContent() {
    if(this.props.isLoaded) {
      if(this.numberOfAccounts() > 0) {
        return <ul>
          {Object.values(this.props.accounts).map((acc) =>
          <li key={acc.id} id={`acc_${acc.id}`}>
            <span key="acc-name">{acc.name}</span>
            {this.getBalance(acc)}
            <button title="Edit" className="btn btn-icon-small"
              onClick={() => this.props.edit(acc.id)}>
              <EditIcon />
            </button>
            <button title="Delete" className="btn btn-icon-small btn-dng"
              onClick={() => this.props.remove(acc.id)}>
              <DeleteIcon />
            </button>
          </li>)}
        </ul>;
      } else {
        return <p>You have no accounts. Add them at the bottom.</p>;
      }
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <div className="panel" id="accounts">
      <div className="grid grid-5">
        {this.renderHeaders()}
        {this.renderContent()}
      </div>
    </div>;
  }
}
