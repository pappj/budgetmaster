import { connect } from "react-redux";
import { removeAccount } from "../../actions/account_actions.js";

import DeleteAccountPanelView from "./delete_account_panel_view.jsx";

const mapStateToProps = (state, ownProps) => {
  return {
    name: state.accounts[ownProps.id].name
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeAccount: (id) => dispatch(removeAccount(id))
  };
};

const DeleteAccountPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteAccountPanelView);

export default DeleteAccountPanel;
