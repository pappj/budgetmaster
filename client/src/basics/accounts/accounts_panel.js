import { connect } from "react-redux";
import { fetchAccounts } from "../../actions/account_actions.js";

import AccountsPanelView from "./accounts_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    isLoaded: state.loaded.accounts && state.loaded.currencies,
    accounts: state.accounts
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    fetchAccounts: () => dispatch(fetchAccounts())
  }
};

const AccountsPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountsPanelView);

export default AccountsPanel;
