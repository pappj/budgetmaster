import React from "react";
import DeletePanel from "../delete_panel.jsx";

export default class DeleteAccountPanelView extends React.Component {
  render() {
    return <DeletePanel hide={this.props.hide} remove={this.props.removeAccount}
             id={this.props.id} name={this.props.name} model="Account" />;
  }
}
