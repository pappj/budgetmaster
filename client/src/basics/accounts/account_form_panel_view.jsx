import React from "react";
import FormPanel from "../../form_panels/form_panel.jsx";
import { nonEmpty, nonNegative, valid, error } from "../../common/forms/validations.js";

export default class AccountFormPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.instance = props.instance == "new" ? "new" :
                      props.accounts[props.instance];

    this.getCurrencyId = this.getCurrencyId.bind(this);
    this.validateBalance = this.validateBalance.bind(this);
    this.isCash = this.isCash.bind(this);
    this.isNotMainCurrency = this.isNotMainCurrency.bind(this);

    this.fields = [
      {
        name: "name",
        label: "Name",
        type: "input",
        inputType: "text",
        default: "",
        validate: nonEmpty
      },
      {
        name: "account_type",
        label: "Type",
        type: "select",
        options: {
          bank: 'Bank account',
          credit: 'Credit card',
          cash: 'Cash',
          other: 'Other'
        },
        default: ""
      },
      {
        name: "currency_id",
        label: "Currency",
        type: "select",
        options: Object.values(props.currencies).reduce((acc, curr) =>
          ({...acc, [curr.id]: curr.short_code + " (" + curr.symbol + ")"}), {}),
        default: ""
      },
      {
        name: "current_balance",
        label: "Balance",
        type: "money",
        default: "0",
        dynamicCurrency: this.getCurrencyId,
        validate: [nonNegative, this.validateBalance]
      },
      {
        name: "credit",
        label: "Credit limit",
        type: "money",
        default: "0",
        dynamicCurrency: this.getCurrencyId,
        validate: nonNegative,
        isDisabled: this.isCash
      },
      {
        name: "in_plan",
        label: "Included in plan",
        type: "checkbox",
        default: false,
        isDisabled: this.isNotMainCurrency,
        premiumLevel: "premium"
      }
    ];
  }

  getCurrencyId(instance) {
    return instance.currency_id;
  }

  validateBalance(value, instance) {
    if(instance.account_type === 'credit' &&
       parseFloat(value) > parseFloat(instance.credit)) {
      return error("can't exceed the credit limit for credit cards");
    }

    return valid;
  }

  isCash(instance) {
    return instance.account_type === "cash";
  }

  isNotMainCurrency(instance) {
    return instance.currency_id != this.props.userCurrencyId;
  }

  postUpdate(instance) {
    if(instance.account_type == 'cash') {
      return {...instance, credit: 0};
    } else {
      return instance;
    }
  }

  render() {
    return <FormPanel hide={this.props.hide} instance={this.instance}
             create={this.props.createAccount} patch={this.props.patchAccount}
             fields={this.fields} postUpdate={this.postUpdate} model="Account" />;
  }
}
