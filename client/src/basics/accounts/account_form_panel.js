import { connect } from "react-redux";
import { createAccount, patchAccount }
  from "../../actions/account_actions.js";

import AccountFormPanelView from "./account_form_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts,
    currencies: state.currencies,
    userCurrencyId: (state.user || {}).currency_id
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createAccount: (acc) => dispatch(createAccount(acc)),
    patchAccount: (acc) => dispatch(patchAccount(acc))
  }
}

const AccountFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountFormPanelView);

export default AccountFormPanel;
