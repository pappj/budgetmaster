import React from "react";

import GoalsPanel from "./goals/goals_panel.js";
import GoalFormPanel from "./goals/goal_form_panel.js";
import DeleteGoalPanel from "./goals/delete_goal_panel.js";
import AccountsPanel from "./accounts/accounts_panel.js";
import AccountFormPanel from "./accounts/account_form_panel.js";
import DeleteAccountPanel from "./accounts/delete_account_panel.js";
import CategoriesPanel from "./categories/categories_panel.js";
import CategoryFormPanel from "./categories/category_form_panel.js";
import DeleteCategoryPanel from "./categories/delete_category_panel.js";
import PanelToggleButton from "../common/panel_toggle_button.jsx";
import InfoPanel from "../common/info_panel.jsx";

import GoalIcon from "../icons/goals.svg";
import AccountIcon from "../icons/accounts.svg";
import CategoryIcon from "../icons/categories.svg";
import EditIcon from "../icons/edit.svg";

export default class Basics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      actionPanel: undefined,
      infoPanel: undefined,
    };

    this.isFormPanelActive = this.isFormPanelActive.bind(this);
    this.toggleFormPanel = this.toggleFormPanel.bind(this);
    this.editModel = this.editModel.bind(this);
    this.removeModel = this.removeModel.bind(this);
    this.showInfo = this.showInfo.bind(this);
    this.actionPanel = this.actionPanel.bind(this);
    this.hideActionPanel = this.hideActionPanel.bind(this);
    this.hideInfoPanel = this.hideInfoPanel.bind(this);
  }

  isFormPanelActive(model) {
    const ap = this.state.actionPanel;
    return ap && ap.type == "form" && ap.descriptor.model == model;
  }

  toggleFormPanel(model) {
    if(this.isFormPanelActive(model)) {
      this.hideActionPanel();
    } else {
      this.setState({
        infoPanel: undefined,
        actionPanel: {
          type: "form",
          descriptor: {
            model,
            instance: "new"
          }
        }
      });
    }
  }

  editModel(model, id) {
    this.setState({
      infoPanel: undefined,
      actionPanel: {
        type: "form",
        descriptor: {
          model,
          instance: id
        }
      }
    });
  }

  removeModel(model, id) {
    this.setState({
      infoPanel: undefined,
      actionPanel: {
        type: "remove",
        descriptor: {
          model,
          id
        }
      }
    });
  }

  showInfo(message) {
    this.setState({
      actionPanel: undefined,
      infoPanel: message
    });
  }

  getForm(descriptor) {
    switch(descriptor.model) {
      case "goal":
        return <GoalFormPanel key="action" hide={this.hideActionPanel}
                 instance={descriptor.instance} showInfo={this.showInfo} />;
      case "account":
        return <AccountFormPanel key="action" hide={this.hideActionPanel}
                 instance={descriptor.instance} />;
      case "category":
        return <CategoryFormPanel key="action" hide={this.hideActionPanel}
                 instance={descriptor.instance} />;
    }
  }

  getDeleteConfirmation(descriptor) {
    switch(descriptor.model) {
      case "goal":
        return <DeleteGoalPanel key="action" hide={this.hideActionPanel}
                 id={descriptor.id} />;
      case "account":
        return <DeleteAccountPanel key="action" hide={this.hideActionPanel}
                 id={descriptor.id} />;
      case "category":
        return <DeleteCategoryPanel key="action" hide={this.hideActionPanel}
                 id={descriptor.id} />;
    }
  }

  actionPanel() {
    let panel;
    const ap = this.state.actionPanel;
    const ip = this.state.infoPanel;
    if(ap) {
      switch(ap.type) {
        case "form":
          panel = this.getForm(ap.descriptor);
          break;
        case "remove":
          panel = this.getDeleteConfirmation(ap.descriptor);
          break;
      }
    } else if(ip) {
      panel = <InfoPanel hide={this.hideInfoPanel} message={ip} />;
    }

    return panel;
  }

  hideActionPanel() {
    this.setState({actionPanel: undefined});
  }

  hideInfoPanel() {
    this.setState({infoPanel: undefined});
  }

  render() {
    return <React.Fragment>
      <div key="panels" className="panels">
        <GoalsPanel key="goals"
          edit={(id) => this.editModel("goal", id)}
          remove={(id) => this.removeModel("goal", id)} />
        <AccountsPanel key="accounts"
          edit={(id) => this.editModel("account", id)}
          remove={(id) => this.removeModel("account", id)} />
        <CategoriesPanel key="categories"
          edit={(id) => this.editModel("category", id)}
          remove={(id) => this.removeModel("category", id)} />
        {this.actionPanel()}
      </div>
      <div key="btns" className="btn-line">
        <PanelToggleButton key="1" icon={<GoalIcon />} label="New Goal"
          id="new-goal" active={this.isFormPanelActive("goal")}
          toggle={() => this.toggleFormPanel("goal")} />
        <PanelToggleButton key="2" icon={<AccountIcon />}
          label="New Account" id="new-account"
          active={this.isFormPanelActive("account")}
          toggle={() => this.toggleFormPanel("account")} />
        <PanelToggleButton key="3" icon={<CategoryIcon />}
          label="New Category" id="new-category"
          active={this.isFormPanelActive("category")}
          toggle={() => this.toggleFormPanel("category")} />
      </div>
    </React.Fragment>;
  }
}
