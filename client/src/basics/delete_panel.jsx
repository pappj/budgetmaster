import React from "react";
import ActionPanel from "../common/action_panel.jsx";

export default class DeletePanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.remove = this.remove.bind(this);
  }

  remove() {
    this.props.remove(this.props.id);
    this.props.hide();
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">Remove {this.props.model}</h2>
      <p key="question" className="center">
        Are you sure to remove "{this.props.name}"?
      </p>
      <div key="buttons" className="btn-line">
        <button key="yes" onClick={this.remove} className="btn btn-text btn-dng">
          Yes
        </button>
        <button key="no" onClick={this.props.hide} className="btn btn-text">
          No
        </button>
      </div>
    </ActionPanel>;
  }
}
