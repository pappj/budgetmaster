import { connect } from "react-redux";
import { createGoal, patchGoal }
  from "../../actions/goal_actions.js";

import GoalFormPanelView from "./goal_form_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    goals: state.goals,
    user: state.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createGoal: (goal) => dispatch(createGoal(goal)),
    patchGoal: (goal) => dispatch(patchGoal(goal))
  }
}

const GoalFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalFormPanelView);

export default GoalFormPanel;
