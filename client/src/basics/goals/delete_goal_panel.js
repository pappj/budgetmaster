import { connect } from "react-redux";
import { removeGoal } from "../../actions/goal_actions.js";

import DeleteGoalPanelView from "./delete_goal_panel_view.jsx";

const mapStateToProps = (state, ownProps) => {
  return {
    title: state.goals[ownProps.id].title
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeGoal: (id) => dispatch(removeGoal(id))
  }
};

const DeleteGoalPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteGoalPanelView);

export default DeleteGoalPanel;
