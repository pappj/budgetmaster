import React from "react";

import FormPanel from "../../form_panels/form_panel.jsx";
import Month from "../../common/month.js";
import { nonEmpty, positive, minDate, valid }
  from "../../common/forms/validations.js";
import { entitledForBasic } from "../../utilities/subscriptions.js";

export default class GoalFormPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    if(!entitledForBasic(props.user)) {
      props.showInfo("Goals are not available in your current subscription. " +
        "Upgrade it to basic or premium to create goals.");
    }

    let instance = props.instance;
    if(props.instance != "new") {
      const storedGoal = props.goals[props.instance];
      instance = {...storedGoal,
                  setTargetAmount: storedGoal.target_amount != null,
                  setTargetDate: storedGoal.target_date != null};

      if(!instance.target_date) {
        instance.target_date = new Date();
      }

      if(!instance.target_amount) {
        instance.target_amount = 0;
      }
    }

    this.instance = instance;

    this.create = this.create.bind(this);
    this.patch = this.patch.bind(this);
    this.showObsoletePlan = this.showObsoletePlan.bind(this);

    this.fields = [
      {
        name: "title",
        label: "Title",
        type: "input",
        inputType: "text",
        default: "",
        validate: nonEmpty
      },
      {
        name: "setTargetAmount",
        label: "Set target amount",
        type: "checkbox",
        default: true
      },
      {
        name: "target_amount",
        label: "Target amount",
        type: "money",
        default: 0,
        currency: "user_main",
        validate: positive,
        isDisabled: this.isTargetAmountDisabled
      },
      {
        name: "setTargetDate",
        label: "Set target date",
        type: "checkbox",
        default: true
      },
      {
        name: "target_date",
        label: "Target date",
        type: "date",
        minDate: new Date(),
        default: new Date(),
        isDisabled: this.isTargetDateDisabled,
        validate: this.validateTargetDate
      },
      {
        name: "achieved",
        label: "Achieved",
        type: "checkbox",
        default: false,
        isDisabled: () => props.instance == "new"
      }
    ];
  }

  isTargetAmountDisabled(instance) {
    return !instance.setTargetAmount;
  }

  isTargetDateDisabled(instance) {
    return !instance.setTargetDate;
  }

  validateTargetDate(value, instance) {
    if(!instance.achieved) {
      return minDate(new Date())(value);
    } else {
      return valid;
    }
  }

  create(instance) {
    return this.props.createGoal(this.prepareSubmit(instance));
  }

  patch(instance) {
    return this.props.patchGoal(this.prepareSubmit(instance));
  }

  prepareSubmit(instance) {
    return {
      id: instance.id,
      title: instance.title,
      target_amount: instance.setTargetAmount ? instance.target_amount : null,
      target_date: instance.setTargetDate ? instance.target_date : null,
      achieved: instance.achieved
    };
  }

  showObsoletePlan(respData) {
    const goal = respData.goal;
    const activeGoal = goal && !goal.achieved;
    const futureTargetDate = goal && goal.target_date &&
      (new Month()).isEarlierThan(new Month(respData.goal.target_date));
    const onlyTargetAmount = goal && goal.target_amount && !goal.target_date;
    const obsoletePlan = respData.plan && respData.plan.obsolete;

    if(activeGoal && (onlyTargetAmount || futureTargetDate) && obsoletePlan) {
      this.props.showInfo(`Your "${respData.goal.title}" goal has an obsolete ` +
        `plan (the goal targets have changed since the plan got created). Update` +
        ` your goal plan on the Planning page.`
      );
    }
  }

  render() {
    return <FormPanel hide={this.props.hide} instance={this.instance}
             create={this.create} patch={this.patch} fields={this.fields}
             postSubmit={this.showObsoletePlan} model="Goal" />;
  }
}
