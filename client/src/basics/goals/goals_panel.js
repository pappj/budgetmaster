import { connect } from "react-redux";
import { fetchGoals } from "../../actions/goal_actions.js";

import GoalsPanelView from "./goals_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    // Goals use main currency, so user must be also loaded
    isLoaded: state.loaded.goals && state.loaded.currencies && state.user,
    currentGoals: Object.values(state.goals).filter(g => !g.achieved),
    achievedGoals: Object.values(state.goals).filter(g => g.achieved)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGoals: () => dispatch(fetchGoals())
  }
};

const GoalsPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsPanelView);

export default GoalsPanel;
