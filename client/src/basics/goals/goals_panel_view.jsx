import React from "react";

import Money from "../../common/money.js";
import UserDate from "../../common/date.js";
import PremiumComponent from "../../common/wrappers/premium_component.js";

import EditIcon from "../../icons/edit.svg";
import DeleteIcon from "../../icons/delete.svg";
import DoneIcon from "../../icons/done.svg";

export default class GoalsPanelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderGoalList = this.renderGoalList.bind(this);
    this.renderGoal = this.renderGoal.bind(this);
    this.renderGoals = this.renderGoals.bind(this);
  }

  componentDidMount() {
    this.props.fetchGoals();
  }

  shouldComponentUpdate(newProps) {
    return newProps.isLoaded != this.props.isLoaded ||
      newProps.currentGoals != this.props.currentGoals ||
      newProps.achievedGoals != this.props.achievedGoals;
  }

  renderGoalList() {
    return  <ul className="grid grid-4">
      {Object.values(this.props.currentGoals).map(this.renderGoal)}
      {Object.values(this.props.achievedGoals).map(this.renderGoal)}
    </ul>;
  }

  renderGoal(goal) {
    let doneIcon;
    if(goal.achieved) {
      doneIcon = <DoneIcon className="text-icon" />;
    }

    return <li key={goal.id} id={`goal_${goal.id}`}>
      <span key="title">{doneIcon}{goal.title}</span>
      {this.getTarget(goal)}
      <button title="Edit" className="btn btn-icon-small"
        onClick={() => this.props.edit(goal.id)}>
        <EditIcon />
      </button>
      <button title="Delete" className="btn btn-icon-small btn-dng"
        onClick={() => this.props.remove(goal.id)}>
        <DeleteIcon />
      </button>
    </li>;
  }

  getTarget(goal) {
    if(goal.target_date) {
      return <UserDate className="span-2" date={goal.target_date} />;
    } else if(goal.target_amount) {
      return <Money amount={goal.target_amount} currency="user_main" />;
    } else {
      return <span className="span-2">No target</span>;
    }
  }

  numberOfGoals() {
    return Object.keys(this.props.currentGoals).length +
      Object.keys(this.props.achievedGoals).length;
  }

  renderHeaders() {
    if(this.numberOfGoals() > 0) {
      return <React.Fragment>
        <h2>Goals</h2>
        <h2 className="panel-header">Target</h2>
        <div className="empty-cell-2" />
      </React.Fragment>;
    } else {
      return <React.Fragment>
        <h2>Goals</h2>
        <div className="empty-cell-3" />
      </React.Fragment>;
    }
  }

  renderGoals() {
    if(this.numberOfGoals() > 0) {
      return this.renderGoalList();
    } else {
      return <p>You have no goals. Add them at the bottom.</p>;
    }
  }

  render() {
    return <div className="panel" id="goals">
      <PremiumComponent level="basic" name="Goals" noticeType="regularWithButton"
        isLoaded={this.props.isLoaded}>
        <div className="grid grid-5">
          {this.renderHeaders()}
          {this.renderGoals()}
        </div>
      </PremiumComponent>
    </div>;
  }
}
