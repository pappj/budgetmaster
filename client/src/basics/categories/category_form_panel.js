import { connect } from "react-redux";
import { createCategory, patchCategory }
  from "../../actions/category_actions.js";

import CategoryFormPanelView from "./category_form_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    userCurrency: state.currencies[state.user.currency_id].short_code,
    recurringExpenses: state.recurringExpenses
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createCategory: (cat, exp) => dispatch(createCategory(cat, exp)),
    patchCategory: (cat, exp, delExp) => dispatch(patchCategory(cat, exp, delExp))
  }
}

const CategoryFormPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryFormPanelView);

export default CategoryFormPanel;
