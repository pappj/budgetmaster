import React from "react";

import EditIcon from "../../icons/edit.svg";
import DeleteIcon from "../../icons/delete.svg";

export default class CategoriesPanelView extends React.Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  shouldComponentUpdate(newProps) {
    return newProps.isLoaded != this.props.isLoaded ||
      newProps.categories != this.props.categories;
  }

  renderContent() {
    if(this.props.isLoaded) {
      if(Object.keys(this.props.categories).length > 0) {
        return <ul>
          {Object.values(this.props.categories).map((cat) =>
            <li key={cat.id} id={`cat_${cat.id}`}>
              <span>{cat.name}</span>
              <button title="Edit" className="btn btn-icon-small"
                onClick={() => this.props.edit(cat.id)}>
                <EditIcon />
              </button>
              <button title="Delete" className="btn btn-icon-small btn-dng"
                onClick={() => this.props.remove(cat.id)}>
                <DeleteIcon />
              </button>
            </li>)}
        </ul>;
      } else {
        return <p>You have no expense categories. Add them at the bottom.</p>
      }
    } else {
      return <p>Loading...</p>;
    }
  }

  render() {
    return <div className="panel" id="expenses">
      <div className="grid grid-3">
        <h2>Expense Categories</h2>
        <div className="empty-cell-2" />
        {this.renderContent()}
      </div>
    </div>;
  }
}
