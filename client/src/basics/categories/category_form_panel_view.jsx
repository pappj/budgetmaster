import React from "react";

import ActionPanel from "../../common/action_panel.jsx";
import FormBody from "../../common/forms/form_body.jsx";
import PremiumComponent from "../../common/wrappers/premium_component.js";
import { createDefaultModelInstance } from "../../common/forms/utilities.js";
import { validate, nonEmpty, positive, minDate }
  from "../../common/forms/validations.js";
import { genericError } from "../../common/api_calls.js";

import DeleteIcon from "../../icons/delete.svg";

export default class CategoryFormPanelView extends React.Component {
  constructor(props) {
    super(props);

    this.fields = [
      {
        name: "name",
        label: "Name",
        type: "input",
        inputType: "text",
        default: "",
        validate: nonEmpty
      }
    ];

    this.expenseFields = [
      {
        name: "amount",
        label: "Amount",
        type: "input",
        inputType: "text",
        className: "amount",
        default: "0",
        validate: positive
      },
      {
        name: "frequency",
        label: "Frequency",
        type: "select",
        options: {
          monthly: "Monthly",
          quarterly: "Quarterly",
          yearly: "Yearly"
        },
        default: ""
      },
      {
        name: "due_date",
        label: "Next due",
        type: "date",
        minDate: new Date(),
        default: new Date(),
        validate: minDate(new Date())
      }
    ]

    let catInstance;
    let expenses = [];
    if(props.instance == "new") {
      this.title = "New Category";
      this.submitLabel = "Add";
      catInstance = createDefaultModelInstance(this.fields);
    } else {
      this.title = "Edit Category";
      this.submitLabel = "Save";
      catInstance = props.categories[props.instance];
      expenses = (catInstance.recurring_expenses || []).map(id =>
        props.recurringExpenses[id]);
    }

    this.state = {
      category: catInstance,
      recurringExpenses: expenses,
      deleteRecurringExpenses: [],
      errors: {category: {},
               recurringExpenses: {}}
    };

    this.updateCategory = this.updateCategory.bind(this);
    this.addRecurringExpense = this.addRecurringExpense.bind(this);
    this.updateRecurringExpense = this.updateRecurringExpense.bind(this);
    this.removeRecurringExpense = this.removeRecurringExpense.bind(this);
    this.renderRecurringExpenses = this.renderRecurringExpenses.bind(this);
    this.getRecurringExpenseErrors = this.getRecurringExpenseErrors.bind(this);
    this.validateRecurringExpenses = this.validateRecurringExpenses.bind(this);
    this.renderApiError = this.renderApiError.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.performSubmit = this.performSubmit.bind(this);
  }

  updateCategory(newInstance) {
    this.setState({category: newInstance});
  }

  addRecurringExpense(evt) {
    evt.preventDefault();
    const newExp = createDefaultModelInstance(this.expenseFields);
    this.setState({
      recurringExpenses: [...this.state.recurringExpenses, newExp]
    });
  }

  updateRecurringExpense(idx, newExp) {
    const currExp = this.state.recurringExpenses;
    this.setState({
      recurringExpenses: [...currExp.slice(0, idx),
                          newExp,
                          ...currExp.slice(idx+1)]
    });
  }

  removeRecurringExpense(evt, idx) {
    evt.preventDefault();
    const currExp = this.state.recurringExpenses;
    const deleteList = this.state.deleteRecurringExpenses;

    let newDeleteList = deleteList;
    const expToDelete = currExp[idx];
    if(expToDelete.id) {
      newDeleteList = [...deleteList, expToDelete.id];
    }

    const newExpList = [...currExp.slice(0, idx), ...currExp.slice(idx+1)];
    const newExpErrors = Object.assign({}, this.state.errors.recurringExpenses);
    Object.keys(newExpErrors).forEach(key => {
      const errors = newExpErrors[key];
      if(key >= idx) {
        delete newExpErrors[key];
      }

      if(key > idx) {
        newExpErrors[key-1] = errors;
      }
    });

    this.setState({
      recurringExpenses: newExpList,
      deleteRecurringExpenses: newDeleteList,
      errors: {...this.state.errors, recurringExpenses: newExpErrors}
    });
  }

  renderRecurringExpenses() {
    if(this.state.recurringExpenses.length == 0) {
      return <p>You haven't added recurring expenses yet.
        Click the button below to add a new one.</p>;
    } else {
      return <div className="grid top-items grid-4 compact">
        <span>Amount ({this.props.userCurrency})</span>
        <span>Frequency</span>
        <span>Next due</span>
        <div className="empty-cell"></div>
        <ul>
        {this.state.recurringExpenses.map((exp, idx) => {
          return <li id={"rec_exp_" + idx} key={"exp_" + idx}>
            <FormBody fields={this.expenseFields} instance={exp}
              updateInstance={(newExp) => this.updateRecurringExpense(idx, newExp)}
              errors={this.getRecurringExpenseErrors(idx)} fieldsOnly={true}/>
            <button name="delete" className="btn btn-icon-small"
              onClick={(evt) => this.removeRecurringExpense(evt, idx)}>
              <DeleteIcon />
            </button>
          </li>
        })}
        </ul>
      </div>
    }
  }

  getRecurringExpenseErrors(idx) {
    return this.state.errors.recurringExpenses[idx] || {};
  }

  validateRecurringExpenses(expenseList) {
    const errors = {};

    expenseList.forEach((exp, idx) => {
      const err = validate(this.expenseFields, exp);
      if(Object.keys(err).length > 0) {
        errors[idx] = err;
      }
    });

    return errors;
  }

  renderApiError() {
    let error;
    if(this.state.apiError) {
      error = <span className="common-error">{this.state.apiError}</span>;
    }

    return error;
  }

  onSubmit(evt) {
    evt.preventDefault();

    let action;
    if(this.props.instance == "new") {
      action = this.props.createCategory;
    } else {
      action = this.props.patchCategory;
    }

    const errors = validate(this.fields, this.state.category);
    const expenseErrors = this.validateRecurringExpenses(
      this.state.recurringExpenses
    );

    if(Object.keys(errors).length === 0 &&
       Object.keys(expenseErrors).length === 0) {
      this.performSubmit()
        .then((response) => {
          this.props.hide()
        })
        .catch((error) => {
          const data = error.response.data;
          const errObject = processApiErrors(data.errors, data.apiError);
          this.setState(errObject);
        });
    }

    this.setState({errors: {category: errors,
                            recurringExpenses: expenseErrors}});
  }

  performSubmit() {
    if(this.props.instance == "new") {
      return this.props.createCategory(this.state.category,
                                       this.state.recurringExpenses);
    } else {
      return this.props.patchCategory(this.state.category,
                                      this.state.recurringExpenses,
                                      this.state.deleteRecurringExpenses);
    }
  }

  render() {
    return <ActionPanel hide={this.props.hide}>
      <h2 key="title">{this.title}</h2>
      <form onSubmit={this.onSubmit} >
        {this.renderApiError()}
        <FormBody fields={this.fields} instance={this.state.category}
          errors={this.state.errors.category} updateInstance={this.updateCategory}
        />
        <PremiumComponent level="basic" name="Recurring Expenses" isLoaded={true}
          noticeType="regular">
          <h2 key="recurring-expenses">Recurring Expenses</h2>
          {this.renderRecurringExpenses()}
        </PremiumComponent>
        <div className="btn-line-wide flex-reverse">
          <button type="submit" className="btn btn-text">
            {this.submitLabel}
          </button>
          <PremiumComponent level="basic" isLoaded={true} noticeType="empty">
            <button className="btn btn-text" onClick={this.addRecurringExpense}>
              New Recurring Expense
            </button>
          </PremiumComponent>
        </div>
      </form>
    </ActionPanel>;
  }
}

function processApiErrors(errors, apiError) {
  if(errors) {
    const allErrors = {
      category: {},
      recurringExpenses: {}
    };
    const regex = /recurring_expenses\[(\d?)\]\.(.*)/

    Object.keys(errors).forEach(key => {
      const match = regex.exec(key);

      if(match) {
        const expErrors = allErrors.recurringExpenses[match[1]] || {};
        allErrors.recurringExpenses[match[1]] = {
          ...expErrors,
          [match[2]]: errors[key]
        };
      } else {
        allErrors.category[key] = errors[key];
      }
    });

    return {errors: allErrors};
  } else if(apiError) {
    return {apiError};
  } else {
    return {apiError: genericError};
  }
}
