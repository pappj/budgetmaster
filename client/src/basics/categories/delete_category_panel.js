import { connect } from "react-redux";
import { removeCategory } from "../../actions/category_actions.js";

import DeleteCategoryPanelView from "./delete_category_panel_view.jsx";

const mapStateToProps = (state, ownProps) => {
  return {
    name: state.categories[ownProps.id].name
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeCategory: (id) => dispatch(removeCategory(id))
  }
};

const DeleteCategoryPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteCategoryPanelView);

export default DeleteCategoryPanel;
