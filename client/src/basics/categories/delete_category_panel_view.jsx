import React from "react";
import DeletePanel from "../delete_panel.jsx";

export default class DeleteCategoryPanelView extends React.Component {
  render() {
    return <DeletePanel hide={this.props.hide} remove={this.props.removeCategory}
             id={this.props.id} name={this.props.name} model="Expense Category" />;
  }
}
