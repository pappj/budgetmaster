import { connect } from "react-redux";
import { fetchCategories } from "../../actions/category_actions.js";

import CategoriesPanelView from "./categories_panel_view.jsx";

const mapStateToProps = (state) => {
  return {
    isLoaded: state.loaded.categories && state.user,
    categories: state.categories
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCategories: () => dispatch(fetchCategories())
  };
};

const CategoriesPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoriesPanelView);

export default CategoriesPanel;
