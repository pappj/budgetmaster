import Month from "../common/month.js";

export function calculateDueAmount(recurringExpenses, month) {
  let sum = 0;
  recurringExpenses.forEach(exp => {
    if(isExpenseApplicable(exp.due_date, exp.frequency, month)) {
      sum += exp.amount;
    }
  });

  return sum;
}

function isExpenseApplicable(dateToRoll, frequency, month) {
  let monthToRoll = new Month(dateToRoll);
  while(monthToRoll.isEarlierThan(month)) {
    monthToRoll = monthToRoll.stepBy(frequency);
  }

  return monthToRoll.isEqualTo(month);
}

export function getAmountForCurrency(expensesByCurrency, currencyId) {
  return (expensesByCurrency || {})[currencyId] || 0;
}
