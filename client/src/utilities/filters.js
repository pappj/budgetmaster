export function Filter(allItems, filter = "all") {
  this.allItems = allItems;

  this.filter = filter;

  this.groupIncluded = function() {
    return this.filter == "all";
  }

  this.itemIncluded = function(item) {
    switch(this.filter) {
      case "all":
        return true;
      case "none":
        return false;
      default:
        return this.filter.includes(item);
    }
  }

  this.toggleGroup = function() {
    if(this.filter == "all") {
      return new Filter(this.allItems, "none");
    } else {
      return new Filter(this.allItems, "all");
    }
  }

  this.toggleItem = function(item) {
    let newFilter;
    if(this.filter == "all") {
      newFilter = removeItem(this.allItems, item);
    } else if(this.filter == "none") {
      newFilter = [item];
    } else if(this.filter.includes(item)) {
      newFilter = removeItem(this.filter, item);
    } else {
      newFilter = addItem(this.filter, item);
      if(newFilter.length == this.allItems.length) {
        newFilter = "all";
      }
    }

    return new Filter(this.allItems, newFilter);
  }

  this.ensureGroupIncluded = function() {
    return new Filter(this.allItems, "all");
  }
}

function removeItem(arr, item) {
  const idx = arr.indexOf(item);
  const copyArr = arr.slice();
  copyArr.splice(idx, 1);
  return copyArr;
}

function addItem(arr, item) {
  const copyArr = arr.slice();
  copyArr.push(item);
  return copyArr;
}

export function RangeFilter(filter = "all") {
  this.filter = filter;

  this.toggle = function() {
    if(this.filter == "all") {
      return new RangeFilter({from: 0, to: 0});
    } else {
      return new RangeFilter("all");
    }
  }

  this.all = function() {
    return this.filter == "all";
  }

  this.from = function() {
    if(this.filter == "all") {
      throw Error("RangeFilter allows anything, can't get from value");
    }

    return this.filter.from;
  }

  this.to = function() {
    if(this.filter == "all") {
      throw Error("RangeFilter allows anything, can't get to value");
    }

    return this.filter.to;
  }

  this.updateFrom = function(value) {
    if(this.filter == "all") {
      throw Error("RangeFilter allows anything, can't update from value");
    }

    const newTo = Math.max(this.filter.to, value);
    return new RangeFilter({from: value, to: newTo});
  }

  this.updateTo = function(value) {
    if(this.filter == "all") {
      throw Error("RangeFilter allows anything, can't update to value");
    }

    return new RangeFilter({from: this.filter.from, to: value});
  }

  this.inRange = function(value) {
    return this.filter == "all" ||
      (this.filter.from <= value && value <= this.filter.to);
  }
}

export function filterIncomes(incomes, baseFilter, amountFilter, dateRange) {
  return incomes.filter(incomeFilterFun(baseFilter, amountFilter, dateRange));
}

export function filterExpenses(expenses, baseFilter, amountFilter, dateRange) {
  return expenses.filter(expenseFilterFun(baseFilter, amountFilter, dateRange));
}

export function filterTransfers(transfers, baseFilter, amountFilter, dateRange) {
  return transfers.filter(transferFilterFun(baseFilter, amountFilter, dateRange));
}

export function isDateInRange(range, inDate) {
  const date = new Date(inDate);
  return range.startDate <= date && date <= range.endDate;
}

function incomeFilterFun(incomeFilter, amountFilter, dateRange) {
  return function(income) {
    return incomeFilter.itemIncluded(income.account_id) &&
      amountFilter.inRange(income.amount) && isDateInRange(dateRange, income.date);
  };
}

function expenseFilterFun(expenseFilter, amountFilter, dateRange) {
  return function(exp) {
    return expenseFilter.itemIncluded(exp.category_id) &&
      amountFilter.inRange(exp.amount) && isDateInRange(dateRange, exp.date);
  };
}

function transferFilterFun(transferFilter, amountFilter, dateRange) {
  return function(trf) {
    return transferFilter.source.itemIncluded(trf.source_account_id) &&
      transferFilter.target.itemIncluded(trf.target_account_id) &&
      amountFilter.inRange(trf.source_amount) &&
      isDateInRange(dateRange, trf.date);
  };
}
