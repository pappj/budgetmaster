export function hasBasicSubscription(user) {
  return user.subscription_type == "basic" &&
    isExpiryValid(user.subscription_expiry_date, false);
}

export function entitledForBasic(user) {
  const subType = user.subscription_type;
  const expDate = user.subscription_expiry_date;

  return (subType == "trial" && isExpiryValid(expDate, true)) ||
    ((subType == "basic" || subType == "premium")
      && isExpiryValid(expDate, false));
}

export function entitledForPremium(user) {
  const subType = user.subscription_type;
  const expDate = user.subscription_expiry_date;

  return (subType == "trial" && isExpiryValid(expDate, true)) ||
    (subType == "premium" && isExpiryValid(expDate, false));
}

function isExpiryValid(date, isStrict) {
  // TODO: extend expiry date if the user has an active subscription.
  // Don't allow the extra days, if they cancelled the subscription.
  if(isStrict) {
    return new Date(date) >= new Date();
  } else {
    // Allow the user 15 days to renew their subscription
    const expiry = new Date(date);
    const extendedExpiry =
      new Date(expiry.getFullYear(), expiry.getMonth(), expiry.getDate() + 15);
    return extendedExpiry >= new Date();
  }
}

export function getSubscriptionPackage(user) {
  if(entitledForPremium(user)) {
    return "premium";
  } else if(entitledForBasic(user)) {
    return "basic";
  } else {
    return "free";
  }
}
