const resolve = require("path").resolve;
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html"
});

module.exports = {
    entry: "./src/index.js",
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(s?)css/,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
              test: /\.svg$/,
              use: ["babel-loader", "@svgr/webpack"]
            }
        ]
    },
    devServer: {
      host: '0.0.0.0',
      port: 3000,
      hot: true,
      historyApiFallback: true,
    },
    output: {
      path: resolve("dist"),
      filename: "bundle.js",
      clean: true,
      publicPath: "/",
    },
    plugins: [htmlPlugin],
};

