#!/bin/bash

if [[ "$1" != "dev" ]] && [[ "$1" != "prod" ]]; then
    echo -e "Missing parameter!\nUsage:\n    $(basename $0) <dev | prod>"
fi

if [[ -z "$POSTGRES_PWD" ]]; then
    echo "ERROR: Env var \$POSTGRES_PWD is not set. It is used as the password"\
        "for the default db user (postgres) and must be set."
    exit 1
fi

if [[ "$1" == "dev" ]]; then
    CMD="docker-compose -f docker-compose-dev.yml run"
    $CMD api bash -c "bundle update && rails db:create && rails db:migrate && rails db:seed"
    $CMD client bash -c "npm install && npm audit fix --force"
elif [[ "$1" == "prod" ]]; then
    if [[ -z "$API_DATABASE_PWD" ]]; then
        echo "ERROR: Env var \$API_DATABASE_PWD is not set. It is used as the"\
            "password for the db user (api) and must be set."
        exit 1
    fi

    CMD="docker-compose"
    $CMD build

    # Start postgres before running psql
    PG_CONT=$($CMD run -d db)

    # Waiting for volume and container to be created
    sleep 5

    # Creating production db user
    docker exec -it $PG_CONT bash -c "psql -U postgres -c \"CREATE USER api WITH PASSWORD '$API_DATABASE_PWD' CREATEDB; \""

    sleep 1
    docker stop $PG_CONT

    $CMD run api bash -c "rails db:create && rails db:migrate && rails db:seed"
fi

# docker-compose doesn't stop dependent containers
docker stop budget_master_db_1
docker rm $(docker ps --filter name=budget_master* -qa)
