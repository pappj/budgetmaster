class IncomePolicy < ApplicationPolicy
  def create?
    acc = record.account
    not acc.nil? and acc.user == user
  end

  def update?
    record.account.user == user
  end

  def destroy?
    record.account.user == user
  end
end
