class GoalPlanPolicy < ApplicationPolicy
  def initialize(user, record)
    raise Exceptions::UserNotEntitled.new unless user.entitled_for_premium?
    super(user, record)
  end

  def create?
    goal = record.goal
    not goal.nil? and goal.user == user and plannable?(goal)
  end

  def update?
    record.goal.user == user and plannable?(record.goal)
  end

  def destroy?
    record.goal.user == user
  end

  private
    def plannable?(goal)
      raise Exceptions::NotPlannableGoal if goal.achieved or
        (goal.target_date and goal.target_date < Date.today)

      return true
    end
end
