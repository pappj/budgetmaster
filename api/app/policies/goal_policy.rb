class GoalPolicy < ApplicationPolicy
  def initialize(user, record)
    raise Exceptions::UserNotEntitled.new unless user.entitled_for_basic?
    super(user, record)
  end

  def create?
    true
  end

  def update?
    record.user == user
  end

  def destroy?
    record.user == user
  end
end
