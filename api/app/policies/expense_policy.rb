class ExpensePolicy < ApplicationPolicy
  def create?
    acc = record.account
    cat = record.category
    not acc.nil? and acc.user == user and not cat.nil? and cat.user == user
  end

  def update?
    record.account.user == user and record.category.user == user
  end

  def destroy?
    record.account.user == user and record.category.user == user
  end
end
