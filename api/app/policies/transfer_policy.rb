class TransferPolicy < ApplicationPolicy
  def create?
    s_acc = record.source_account
    t_acc = record.target_account
    not s_acc.nil? and s_acc.user == user and not t_acc.nil? and t_acc.user == user
  end

  def update?
    record.source_account.user == user and record.target_account.user == user
  end

  def destroy?
    record.source_account.user == user and record.target_account.user == user
  end
end
