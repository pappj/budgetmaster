class ExpensePlanPolicy < ApplicationPolicy
  def initialize(user, record)
    raise Exceptions::UserNotEntitled.new unless user.entitled_for_premium?
    super(user, record)
  end

  def create?
    category = record.category
    not category.nil? and category.user == user
  end

  def update?
    record.category.user == user
  end

  def destroy?
    record.category.user == user
  end
end
