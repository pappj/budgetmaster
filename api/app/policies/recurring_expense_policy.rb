class RecurringExpensePolicy < ApplicationPolicy
  def initialize(user, record)
    raise Exceptions::UserNotEntitled.new unless user.entitled_for_basic?
    super(user, record)
  end

  def create?
    record.category.user == user
  end

  def update?
    record.category.user == user
  end

  def destroy?
    record.category.user == user
  end
end
