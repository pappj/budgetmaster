module TransactionHelper
  def self.correctAmountAddition(transaction, amount_label, account_label)
    correctAmount(transaction, amount_label, account_label, :-, :+)
  end

  def self.correctAmountSubtraction(transaction, amount_label, account_label)
    correctAmount(transaction, amount_label, account_label, :+, :-)
  end

  def self.amountAdditionCovered?(transaction, changes, amount_label,
                                  account_label)
    return true unless changes[amount_label] or changes[account_label]

    if changes[account_label]
      account = Account.find(changes[account_label][0])
      amount =
        if changes[amount_label]
          changes[amount_label][0]
        else
          transaction[amount_label]
        end

      if account.available_amount < amount
        [false,
         "The amount available on the original account is not enough to cover "\
         "the change. Change the current balance of '#{account.name}' first, or "\
         "update the transaction only, without the related accounts."]
      else
        true
      end
    else
      account = Account.find(transaction[account_label])
      (prev_amount, new_amount) = changes[amount_label]
      if account.available_amount + new_amount < prev_amount
        [false,
         "The amount available on the account is not enough to cover the amount "\
         "change. Either increase the amount, change the current balance "\
         "of '#{account.name}', or update the transaction only, without the "\
         "related accounts."]
      else
        true
      end
    end
  end

  def self.amountSubtractionCovered?(transaction, changes, amount_label,
                                  account_label)
    return true unless changes[amount_label] or changes[account_label]

    if changes[account_label]
      account = Account.find(changes[account_label][1])
      amount =
        if changes[amount_label]
          changes[amount_label][1]
        else
          transaction[amount_label]
        end

      if account.available_amount < amount
        [false,
         "The amount available on the new account is not enough to cover "\
         "the change. Either decrease the amount, change the current balance "\
         "of '#{account.name}', or update the transaction only, without the "\
         "related accounts."]
      else
        true
      end
    else
      account = Account.find(transaction[account_label])
      (prev_amount, new_amount) = changes[amount_label]
      if account.available_amount + prev_amount < new_amount
        [false,
         "The amount available on the account is not enough to cover the amount "\
         "change. Either decrease the amount, change the current balance "\
         "of '#{account.name}', or update the transaction only, without the "\
         "related accounts."]
      else
        true
      end
    end
  end

  private
    def self.correctAmount(transaction, amount_label, account_label,
                           prev_op, new_op)
      changes = transaction.saved_changes
      return [] unless changes[amount_label] or changes[account_label]

      amount_changes = changes[amount_label]
      (prev_amount, new_amount) = amount_changes || [transaction[amount_label],
                                                     transaction[amount_label]]

      account_changes = changes[account_label]
      if account_changes
        (prev_account, new_account) = account_changes.map{|id| Account.find(id)}
        prev_account.current_balance =
          prev_account.current_balance.send(prev_op, prev_amount)
        prev_account.save

        new_account.current_balance =
          new_account.current_balance.send(new_op, new_amount)
        new_account.save

        [prev_account, new_account]
      else
        acc = Account.find(transaction[account_label])
        acc.current_balance = acc.current_balance.send(prev_op, prev_amount)
        acc.current_balance = acc.current_balance.send(new_op, new_amount)
        acc.save

        [acc]
      end
    end
end
