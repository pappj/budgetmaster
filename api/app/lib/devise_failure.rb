class DeviseFailure < Devise::FailureApp
  def http_auth_body
    if warden.message == :invalid
      msg = i18n_message
      {errors: {email: msg, password: msg}}.to_json
    else
      super
    end
  end
end

