class CurrenciesController < ApplicationController
  def index
    currencies = Currency.all.to_a
    render(json: currencies.map{|curr| curr.as_json })
  end
end
