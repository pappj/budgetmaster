class CategoriesController < CrudController
  def index
    categories =
      if(current_user.entitled_for_basic?)
        Category.includes(:recurring_expenses).where(user: current_user).order(:id)
      else
        Category.where(user: current_user).order(:id)
      end
    render(json: {categories: categories.map{|cat| show(cat) }})
  end

  def create_instance
    cat = Category.new(model_params)
    cat.user = current_user
    cat.recurring_expenses.each{|exp| authorize(exp)}

    return cat
  end

  def post_create(cat)
    show(cat)
  end

  def before_save(cat)
    cat.recurring_expenses.each do |exp|
      authorize exp if exp.changed? or exp.marked_for_destruction?
    end
  end

  def post_update(cat)
    show(cat)
  end

  private
    def model_params
      params.require(:category).permit(:name, recurring_expenses_attributes: [
        :id, :amount, :frequency, :due_date, :_destroy
      ])
    end

    def show(cat)
      cat.as_json(current_user.entitled_for_basic?)
    end
end
