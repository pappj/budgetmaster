class ExpensesController < CrudController
  def index
    expenses = Expense.joins(:category)
      .where("categories.user_id" => current_user,
             date: params[:start]..params[:end]).order(date: :desc)
    render(json: {expenses: expenses.map(&:as_json)})
  end

  def post_create(expense)
    resp = {expense: expense.as_json}

    if expense.update_account
      acc = expense.account
      acc.current_balance -= expense.amount
      acc.save

      resp.merge!({accounts: [show(acc)]})
    end

    if current_user.entitled_for_premium?
      plan = find_related_plan(expense)
      if(expense_plan_affected?(expense, plan))
        plan.amount = [0, plan.amount - expense.amount].max
        plan.save
      end

      resp.merge!({plan: plan.as_json})
    end

    resp
  end

  def post_update(expense)
    resp = {expense: expense.as_json,
            monthly_expenses: Expense.monthly_expenses(current_user)}

    if expense.update_account
      updated_accounts = TransactionHelper.correctAmountSubtraction(expense,
                                                                    :amount,
                                                                    :account_id)
      accs = updated_accounts.map{|acc| show(acc)}

      if not accs.empty?
        resp.merge!({accounts: accs})
      end
    end

    resp
  end

  def post_destroy(expense)
    resp = {monthly_expenses: Expense.monthly_expenses(current_user)}

    if params[:update_account]
      acc = expense.account
      acc.current_balance += expense.amount
      acc.save

      resp.merge!({accounts: [show(acc)]})
    end

    render(json: resp)
  end

  def monthly_expenses
    render(json: Expense.monthly_expenses(current_user))
  end

  private
    def model_params
      params.require(:expense).permit(:account_id, :category_id, :amount, :date,
                                      :note, :update_account)
    end

    def expense_plan_affected?(expense, plan)
      return false unless expense.account.in_plan
      return false unless plan

      exp_date = expense.date
      today = Date.today

      exp_date.year == today.year and exp_date.month == today.month
    end

    def find_related_plan(expense)
      current_month = Date.today.change(day: 1)
      return ExpensePlan.find_by(category: expense.category, month: current_month)
    end

    def show(acc)
      acc.as_json(current_user.entitled_for_premium?)
    end
end
