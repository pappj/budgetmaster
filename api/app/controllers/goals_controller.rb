class GoalsController < CrudController
  def index
    goals =
      if(current_user.entitled_for_basic?)
        Goal.where(user: current_user).order(:id)
      else
        []
      end

    render(json: {goals: goals.map{|goal| goal.as_json }})
  end

  def create_instance
    goal = Goal.new(model_params)
    goal.user = current_user

    return goal
  end

  def update
    head(:forbidden) and return if model_params.empty?

    goal = get_instance(params[param_key][:id])
    goal.assign_attributes(model_params)
    plan = goal.goal_plan
    if(plan and goal.achieved)
      plan.destroy
      plan = {removedId: plan.id}
    elsif(plan and (goal.target_amount_changed? or goal.target_date_changed?))
      plan.make_obsolete
      plan.save
    end
    success = goal.save

    if success
      return_data = {goal: goal.as_json}
      if(current_user.entitled_for_premium?)
        return_data.merge!({plan: plan.as_json})
      end

      render(json: return_data)
    else
      render(json: {errors: goal.errors}, status: :unprocessable_entity)
    end
  end

  private
    def model_params
      params.require(:goal).permit(:title, :target_amount, :target_date, :achieved,
                                   :allocated_amount)
    end
end
