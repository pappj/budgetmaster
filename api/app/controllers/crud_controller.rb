class CrudController < ApplicationController
  before_action :verify_user
  rescue_from Exceptions::UserNotEntitled, with: :wrong_subscription
  rescue_from Pundit::NotAuthorizedError, with: :other_user

  def send_object_on_update
    false
  end

  def create
    inst = create_instance
    authorize(inst)
    success = inst.save

    if success
      render(json: post_create(inst))
    else
      render(json: {errors: inst.errors}, status: :unprocessable_entity)
    end
  end

  def post_create(instance)
    # A hook for the child controllers
    instance
  end

  def update
    update_params = model_params_for_update
    head(:forbidden) and return if update_params.empty?

    inst = get_instance(params[param_key][:id])
    inst.assign_attributes(update_params)
    before_save(inst)
    success = inst.save

    if success
      render(json: post_update(inst))
    else
      render(json: {errors: inst.errors}, status: :unprocessable_entity)
    end
  end

  def post_update(instance)
    # A hook for the child controllers
    instance
  end

  def before_save(instance)
    # A hook for the child controllers
  end

  def destroy
    inst = get_instance(params[:id])
    before_destroy(inst)
    success = inst.destroy

    if success
      post_destroy(inst)
    else
      render(json: {errors: inst.errors}, status: :forbidden)
    end
  end

  def before_destroy(instance)
    # A hook for the child controllers
  end

  def post_destroy(instance)
    head(:no_content)
  end

  private
    def param_key
      self.controller_name.chomp("Controller").singularize.underscore.to_sym
    end

    def model_name
      self.controller_name.singularize.capitalize
    end

    def model_params_for_update
      model_params
    end

    def create_instance
      controller_name.classify.constantize.new(model_params)
    end

    def verify_user
      unless user_signed_in?
        render(json: {errors: ["Unauthorized. Sign in for this action."]},
               status: :unauthorized)
      end
    end

    def wrong_subscription
      user = current_user
      render(json:
             {apiError: "You tried to access a functionality that is not included"\
                        " in your subscription. You shouldn't see this message"\
                        " in normal circumstances. Please contact our support.",
              subscriptionNotSufficient: true,
              user: {subscription_type: user.subscription_type,
                     subscription_expiry_date: user.subscription_expiry_date}},
            status: :forbidden)
    end

    def other_user
      # The received object belongs to another user.
      # Don't let the client know if the object exists or not
      render(json: {errors: ["#{model_name} is not found"]}, status: :not_found)
    end
end
