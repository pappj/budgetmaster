class AccountsController < CrudController
  def index
    accounts = Account.where(user: current_user).order(:id)
    render(json: {accounts: accounts.map{|acc| show(acc)}})
  end

  def create_instance
    acc = Account.new(model_params)
    acc.user = current_user

    return acc
  end

  def post_create(acc)
    show(acc)
  end

  def post_update(acc)
    show(acc)
  end

  private
    def model_params
      params.require(:account).permit(*entitled_params)
    end

    def entitled_params
      param_list = [:name, :account_type, :currency_id, :current_balance, :credit]
      if(current_user.entitled_for_premium?)
        param_list.push(:in_plan)
      end

      param_list
    end

    def show(acc)
      acc.as_json(current_user.entitled_for_premium?)
    end
end
