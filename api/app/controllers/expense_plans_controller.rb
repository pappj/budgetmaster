class ExpensePlansController < CrudController
  def index
    exp_plans =
      if(current_user.entitled_for_premium?)
        ExpensePlan.includes(:category).where(categories: {user: current_user})
      else
        []
      end

    render(json: {expense_plans: exp_plans.map{|plan| plan.as_json}})
  end

  def destroy
    inst = get_instance(params[:id])
    authorize inst

    head(:forbidden)
  end

  private
    def model_params
      params.require(:expense_plan).permit(:category_id, :amount, :month)
    end

    def model_params_for_update
      params.require(:expense_plan).permit(:amount)
    end
end
