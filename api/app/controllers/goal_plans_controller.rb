class GoalPlansController < CrudController
  def index
    goal_plans =
      if(current_user.entitled_for_premium?)
        GoalPlan.includes(:goal).where(goals: {user: current_user})
      else
        []
      end

    render(json: {goal_plans: goal_plans.map{|plan| plan.as_json}})
  end

  def create
    if(GoalPlan.find_by(goal_id: params[:goal_plan][:goal_id]))
      render(json: {errors: ["There is already a plan for this goal"]},
             status: :forbidden)
    else
      super
    end
  rescue ::Exceptions::NotPlannableGoal
    render(json: {errors: ["Can't create plan for achieved or past goals"]},
           status: :forbidden)
  end

  def before_save(plan)
    plan.obsolete = false
  end

  def update
    super
  rescue ::Exceptions::NotPlannableGoal
    render(json: {errors: ["Can't update plan for achieved or past goals"]},
           status: :forbidden)
  end

  def destroy
    inst = get_instance(params[:id])
    authorize inst

    head(:forbidden)
  end

  private
    def model_params
      params.require(:goal_plan).permit(:goal_id, :monthly_amount, :target_amount,
                                        :target_date)
    end

    def model_params_for_update
      params.require(:goal_plan).permit(:monthly_amount, :target_amount,
                                        :target_date)
    end
end
