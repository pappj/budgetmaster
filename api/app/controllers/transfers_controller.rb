class TransfersController < CrudController
  def index
    transfers = Transfer.unscoped.joins(:source_account)
      .where("source_account.user_id" => current_user,
             date: params[:start]..params[:end]).order(date: :desc)
    render(json: {transfers: transfers.map(&:as_json)})
  end

  def post_create(transfer)
    resp = {transfer: transfer.as_json}
    if transfer.update_account
      source_acc = transfer.source_account
      source_acc.current_balance -= transfer.source_amount
      source_acc.save

      target_acc = transfer.target_account
      target_acc.current_balance += transfer.target_amount
      target_acc.save

      resp.merge!({accounts: [show(source_acc), show(target_acc)]})
    end

    resp
  end

  def post_update(transfer)
    resp = {transfer: transfer.as_json}

    if transfer.update_account
      source_accs =
        TransactionHelper.correctAmountSubtraction(transfer,
                                                   :source_amount,
                                                   :source_account_id)

      target_accs =
        TransactionHelper.correctAmountAddition(transfer,
                                                :target_amount,
                                                :target_account_id)

      accs = (source_accs + target_accs).map{|acc| show(acc)}

      if not accs.empty?
        resp.merge!({accounts: accs})
      end
    end

    resp
  end

  def before_destroy(transfer)
    transfer.update_account = params[:update_account]
  end

  def post_destroy(transfer)
    if params[:update_account]
      source_acc = transfer.source_account
      source_acc.current_balance += transfer.source_amount
      source_acc.save

      target_acc = transfer.target_account
      target_acc.current_balance -= transfer.target_amount
      target_acc.save

      render(json: {accounts: [show(source_acc), show(target_acc)]})
    else
      head(:no_content)
    end
  end

  private
    def model_params
      params.require(:transfer).permit(:source_account_id, :target_account_id,
                                       :source_amount, :target_amount, :date,
                                       :note, :update_account)
    end

    def show(acc)
      acc.as_json(current_user.entitled_for_premium?)
    end
end
