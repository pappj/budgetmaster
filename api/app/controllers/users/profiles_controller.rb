class Users::ProfilesController < ApplicationController
  before_action :verify_user

  def update
    user = current_user
    if(user.id == params[:user_profile][:id])
      user.assign_attributes(permitted_parameters)
      accounts = [];
      if(user.currency_id_changed?)
        affected_accounts = Account.where(user: user, in_plan: true)
        ids = affected_accounts.each{|acc| acc.id}
        affected_accounts.update_all(in_plan: false)
        accounts = Account.where(id: ids)
      end
      success = user.save

      if success
        render(json: {user: user, accounts: accounts.map{|acc| show(acc)}})
      else
        render(json: {errors: user.errors}, status: :unprocessable_entity)
      end
    else
      render(json: {errors: ["Id does not match with signed in user."]},
             status: :unauthorized)
    end
  end

  private
    def permitted_parameters
      params.require(:user_profile).permit(:currency_id, :currency_string,
                                           :currency_position, :date_format)
    end

    def verify_user
      unless user_signed_in?
        render(json: {errors: ["Unauthorized. Sign in for this action."]},
               status: :unauthorized)
      end
    end

    def show(acc)
      acc.as_json(current_user.entitled_for_premium?)
    end
end
