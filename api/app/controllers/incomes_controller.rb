class IncomesController < CrudController
  def index
    incomes = Income.unscoped.joins(:account)
      .where("accounts.user_id" => current_user,
             date: params[:start]..params[:end]).order(date: :desc)
    render(json: {incomes: incomes.map(&:as_json)})
  end

  def post_create(income)
    resp = {income: income.as_json}

    if income.update_account
      acc = income.account
      acc.current_balance += income.amount
      acc.save

      resp.merge!({accounts: [show(acc)]})
    end

    resp
  end

  def post_update(income)
    resp = {income: income.as_json}

    if income.update_account
      accs = TransactionHelper.correctAmountAddition(income, :amount, :account_id)
      accs.map{|acc| show(acc)}

      if not accs.empty?
        resp.merge!({accounts: accs})
      end
    end

    resp
  end

  def before_destroy(income)
    income.update_account = params[:update_account]
  end

  def post_destroy(income)
    if params[:update_account]
      acc = income.account
      acc.current_balance -= income.amount
      acc.save

      render(json: {accounts: [show(acc)]})
    else
      head(:no_content)
    end
  end

  private
    def model_params
      params.require(:income).permit(:account_id, :amount, :date, :note,
                                    :update_account)
    end

    def show(acc)
      acc.as_json(current_user.entitled_for_premium?)
    end
end
