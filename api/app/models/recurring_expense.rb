# == Schema Information
#
# Table name: recurring_expenses
#
#  id          :bigint           not null, primary key
#  category_id :bigint
#  amount      :decimal(, )
#  frequency   :enum
#  due_date    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class RecurringExpense < ApplicationRecord
  enum frequency: { yearly: "yearly", quarterly: "quarterly", monthly: "monthly" }

  belongs_to :category

  validates :category, :due_date, presence: true
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validate :date_is_in_the_future

  def date_is_in_the_future
    return unless due_date

    if due_date < Date.today
      errors.add(:due_date, "can't be in the past")
    end
  end
end
