class GoalPlan < ApplicationRecord
  belongs_to :goal

  validates :goal, presence: true
  validates_numericality_of :monthly_amount, greater_than_or_equal_to: 0
  validate :dont_allow_both_targets, :positive_target_amount,
   :future_target_date, :two_target_amounts, :two_target_dates,
   :each_targets_are_set

  def dont_allow_both_targets
    if target_amount && target_date
      errors.add(:target_amount, "only one estimated target can be set")
      errors.add(:target_date, "only one estimated target can be set")
    end
  end

  def positive_target_amount
    if target_amount && target_amount < 0
      errors.add(:target_amount, "must be positive")
    end
  end

  def future_target_date
    if target_date && target_date < Date.today
      errors.add(:target_date, "can't be in the past")
    end
  end

  def two_target_amounts
    return unless goal

    if target_amount && goal.target_amount
      errors.add(:target_amount, "The goal already has a target amount")
    end
  end

  def two_target_dates
    return unless goal

    if target_date && goal.target_date
      errors.add(:target_date, "The goal already has a target date")
    end
  end

  def each_targets_are_set
    return unless goal
    return if obsolete

    unless target_amount || goal.target_amount
      errors.add(:target_amount, "must be set")
    end

    unless target_date || goal.target_date
      errors.add(:target_date, "must be set")
    end
  end

  def make_obsolete
    self.monthly_amount = 0
    self.target_amount = nil
    self.target_date = nil
    self.obsolete = true
  end

  def as_json(options = {})
    super(only: [:id, :goal_id, :monthly_amount, :target_amount, :target_date,
                 :obsolete])
  end
end
