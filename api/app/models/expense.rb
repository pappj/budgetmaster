# == Schema Information
#
# Table name: expenses
#
#  id          :bigint           not null, primary key
#  account_id  :bigint
#  amount      :decimal(15, 2)
#  date        :date
#  category_id :bigint
#  note        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Expense < ApplicationRecord
  attr_accessor :update_account

  belongs_to :account
  belongs_to :category

  validates :account, presence: true
  validates :category, presence: true
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validate :same_user, :date_is_not_in_the_future
  validate :amount_covered, on: :create
  validate :changed_amount_covered, on: :update

  def same_user
    return unless account
    return unless category

    unless account.user == category.user
      errors.add(:account, "user is different than category's user")
      errors.add(:category, "user is different than account's user")
    end
  end

  def amount_covered
    return unless update_account && account

    if account.available_amount < amount
      errors.add(:amount, "is more than available")
    end
  end

  def changed_amount_covered
    return unless update_account && account

    (valid, msg) = TransactionHelper.amountSubtractionCovered?(self, changes,
                                                               :amount,
                                                               :account_id)
    unless valid
      errors.add(:base, msg)
    end
  end

  def date_is_not_in_the_future
    if date > Date.today
      errors.add(:date, "can't be in the future")
    end
  end

  def as_json(options = {})
    super(only: [:id, :category_id, :account_id, :amount, :date, :note])
  end

  def self.monthly_expenses(user)
    first_day = Date.today.beginning_of_month
    expenses = Expense.joins(:account)
      .select("SUM(expenses.amount) AS sum,
               category_id,
               accounts.currency_id AS currency_id")
      .where("accounts.user_id = ? AND date >= ?", user, first_day)
      .group(:category_id, :currency_id)

    grouped_expenses = {}
    # Collect sums by category_id then by currency_id
    expenses.each do |exp|
      if grouped_expenses[exp["category_id"]]
        grouped_expenses[exp["category_id"]][exp["currency_id"]] = exp["sum"]
      else
        grouped_expenses[exp["category_id"]] = {exp["currency_id"] => exp["sum"]}
      end
    end

    grouped_expenses.as_json
  end
end
