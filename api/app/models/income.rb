# == Schema Information
#
# Table name: incomes
#
#  id         :bigint           not null, primary key
#  amount     :decimal(15, 2)
#  date       :date
#  note       :text
#  account_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Income < ApplicationRecord
  attr_accessor :update_account

  belongs_to :account

  validates :account, presence: true
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validate :date_is_not_in_the_future
  validate :changed_amount_covered, on: :update
  before_destroy :can_be_removed

  def date_is_not_in_the_future
    if date > Date.today
      errors.add(:date, "can't be in the future")
    end
  end

  def changed_amount_covered
    return unless update_account

    (valid, msg) = TransactionHelper.amountAdditionCovered?(self, changes, :amount,
                                                            :account_id)
    unless valid
      errors.add(:base, msg)
    end
  end

  def can_be_removed
    return unless update_account

    if account.available_amount < amount
      errors.add(:base, "#{account.name} has not enough amount to cover the " +
                 "removal. Either increase the account's balance, or don't " +
                 "update the account.")
      throw(:abort)
    end
  end

  def as_json(options = {})
    super(only: [:id, :amount, :date, :note, :account_id])
  end
end
