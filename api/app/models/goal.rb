# == Schema Information
#
# Table name: goals
#
#  id            :bigint           not null, primary key
#  title         :string
#  target_amount :decimal(, )
#  target_date   :date
#  user_id       :bigint
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  achieved      :boolean          default(FALSE)
#
class Goal < ApplicationRecord
  belongs_to :user
  has_one :goal_plan, dependent: :destroy

  validates :user, :title, presence: true
  validates_numericality_of :target_amount, greater_than_or_equal_to: 0,
                            allow_blank: true
  validate :target_date_in_the_future
  validates_numericality_of :allocated_amount, greater_than_or_equal_to: 0

  def as_json(options = {})
    super(only: [:id, :title, :target_amount, :target_date, :achieved,
                 :allocated_amount])
  end

  private
    def target_date_in_the_future
      return if target_date.blank?

      if not achieved and target_date < Date.today
        errors.add(:target_date, "must be in the future")
      end
    end
end
