# == Schema Information
#
# Table name: currencies
#
#  id         :bigint           not null, primary key
#  short_code :string
#  symbol     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Currency < ApplicationRecord
  def as_json(options = {})
    super(only: [:id, :short_code, :symbol])
  end
end
