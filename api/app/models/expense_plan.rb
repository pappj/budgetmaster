# == Schema Information
#
# Table name: expense_plans
#
#  id          :bigint           not null, primary key
#  category_id :bigint
#  amount      :decimal(, )
#  month       :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class ExpensePlan < ApplicationRecord
  belongs_to :category

  validates :category, presence: true
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validate :month_not_in_the_past

  def month_not_in_the_past
    return unless month

    this_year = Date.today.year
    this_month = Date.today.month

    if month.year < this_year or
        (month.year == this_year and month.month < this_month)
      errors.add(:month, "can't be in the past")
    end
  end

  def as_json(options = {})
    super(only: [:id, :category_id, :amount, :month])
  end
end
