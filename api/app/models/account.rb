# == Schema Information
#
# Table name: accounts
#
#  id              :bigint           not null, primary key
#  name            :string
#  account_type    :string
#  currency_id     :bigint
#  current_balance :decimal(15, 2)
#  user_id         :bigint
#  in_plan         :boolean
#  credit          :decimal(10, 2)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Account < ApplicationRecord
  TYPES = {bank: 'Bank account', credit: 'Credit card', cash: 'Cash',
           other: 'Other'}

  belongs_to :currency
  belongs_to :user
  has_many :incomes, dependent: :destroy
  has_many :expenses

  validates :user, presence: true
  validates :name, presence: true
  validates :currency, presence: true
  validates :account_type, inclusion: {in: TYPES.keys.map(&:to_s),
    message: "%{value} is not valid"}
  validates :current_balance, numericality: true
  validates_numericality_of :credit, greater_than_or_equal_to: 0
  validate :zero_credit_for_cash, :credit_not_exceeds_limit,
           :in_plan_main_currency

  def zero_credit_for_cash
    if(account_type == "cash" && credit != 0)
      errors.add(:credit, "must be zero for cash")
    end
  end

  def credit_not_exceeds_limit
    if(account_type == "credit" && current_balance > credit)
      errors.add(:current_balance,
                 "can't exceed the credit limit for credit cards")
    end
  end

  def in_plan_main_currency
    if(in_plan && currency.id != user.currency_id)
      errors.add(:in_plan,
                 "can include accounts in planning only with your main currency")
    end
  end

  def available_amount
    if account_type == "credit"
      current_balance
    else
      current_balance + credit
    end
  end

  def as_json(include_plan, options = {})
    base_fields = [:id, :name, :account_type, :currency_id, :current_balance,
                   :credit]
    if(include_plan)
      super(only: base_fields.push(:in_plan))
    else
      super(only: base_fields)
    end
  end
end
