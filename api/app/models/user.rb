# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
class User < ApplicationRecord
  enum currency_string: { short_code: "short_code", symbol: "symbol" }
  enum currency_position: { before: "before", after: "after" }

  DATE_FORMATS = ["yyyy-MM-dd", "yyyy.MM.dd.", "MM/dd/yyyy", "dd/MM/yyyy"]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # For some reason setting the same default in the migration
  # did not work. The default value was yyyy-mm-dd, which is
  # invalid
  attribute :date_format, :string, default: "yyyy-MM-dd"

  # Set 1 month trial period. subscription_type = "trial" is set by the
  # database, but it can't set dynamic default values
  before_create do
    self.subscription_expiry_date = Date.today + 30.days
  end

  belongs_to :currency

  validates :currency, presence: true
  validate :date_format_is_supported

  def date_format_is_supported
    if(not DATE_FORMATS.include?(date_format))
      errors.add(:date_format, "Not supported date format: #{date_format}")
    end
  end

  def as_json(options = {})
    super(only: [:id, :email, :currency_id, :currency_string,
                 :currency_position, :date_format, :subscription_type,
                 :subscription_expiry_date])
  end

  def entitled_for_basic?
    has_trial? or has_basic? or has_premium?
  end

  def entitled_for_premium?
    has_trial? or has_premium?
  end

  private
    def has_trial?
      subscription_type == "trial" && subscription_expiry_date >= Date.today
    end

    def has_basic?
      subscription_type == "basic" &&
        subscription_expiry_date + 15.days >= Date.today
    end

    def has_premium?
      subscription_type == "premium" &&
        subscription_expiry_date + 15.days >= Date.today
    end
end
