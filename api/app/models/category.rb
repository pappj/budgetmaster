# == Schema Information
#
# Table name: categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  hidden     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
class Category < ApplicationRecord
  belongs_to :user
  has_many :expenses
  has_many :recurring_expenses, dependent: :destroy
  has_many :expense_plans, dependent: :destroy

  accepts_nested_attributes_for :recurring_expenses, allow_destroy: true

  validates :user, :name, presence: true

  def as_json(with_rec_exp, options = {})
    base_fields = {only: [:id, :name]}
    if(with_rec_exp)
      super(base_fields
        .merge({include: {recurring_expenses: {only: [:id, :amount, :frequency,
                                                      :due_date]}}}))
    else
      super(base_fields)
    end
  end
end
