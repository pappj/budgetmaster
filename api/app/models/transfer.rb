# == Schema Information
#
# Table name: transfers
#
#  id                :bigint           not null, primary key
#  source_account_id :bigint
#  target_account_id :bigint
#  source_amount     :decimal(15, 2)
#  target_amount     :decimal(15, 2)
#  date              :date
#  note              :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class Transfer < ApplicationRecord
  attr_accessor :update_account

  belongs_to :source_account, class_name: "Account"
  belongs_to :target_account, class_name: "Account"

  validates :source_account, presence: true
  validates :target_account, presence: true
  validate  :same_user, :currency_and_amount
  validate  :amount_covered, on: :create
  validate  :changed_amount_covered, on: :update
  validates_numericality_of :source_amount, greater_than_or_equal_to: 0
  validates_numericality_of :target_amount, greater_than_or_equal_to: 0
  validate  :date_is_not_in_the_future
  before_destroy :can_be_removed


  def same_user
    return unless source_account
    return unless target_account

    unless source_account.user == target_account.user
      errors.add(:source_account, "user is different than target account's user")
      errors.add(:target_account, "user is different than source account's user")
    end
  end

  def currency_and_amount
    return unless source_account
    return unless target_account

    if source_account.currency == target_account.currency
      unless source_amount == target_amount
        errors.add(:source_amount, "source and target amounts must be the same if
                                    currencies match")
        errors.add(:target_amount, "source and target amounts must be the same if
                                    currencies match")
      end
    end
  end

  def amount_covered
    return unless update_account && source_account

    if source_account.available_amount < source_amount
      errors.add(:source_amount, "is more than available")
    end
  end

  def changed_amount_covered
    return unless update_account

    (source_valid, source_msg) =
      TransactionHelper.amountSubtractionCovered?(self, changes, :source_amount,
                                                  :source_account_id)
    unless source_valid
      errors.add(:source_amount, source_msg)
    end

    (target_valid, target_msg) =
      TransactionHelper.amountAdditionCovered?(self, changes, :target_amount,
                                               :target_account_id)

    unless target_valid
      errors.add(:target_amount, target_msg)
    end
  end

  def date_is_not_in_the_future
    if date > Date.today
      errors.add(:date, "can't be in the future")
    end
  end

  def can_be_removed
    return unless update_account

    if target_account.available_amount < target_amount
      errors.add(:base, "#{target_account.name} has not enough amount to cover " +
                 " the removal. Either increase the account's balance, or don't " +
                 "update the accounts.")
      throw(:abort)
    end
  end

  def as_json(options = {})
    super(only: [:id, :source_account_id, :target_account_id, :source_amount,
                 :target_amount, :date, :note])
  end
end
