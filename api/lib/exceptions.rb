module Exceptions
  class NotPlannableGoal < RuntimeError; end
  class UserNotEntitled < RuntimeError; end
end
