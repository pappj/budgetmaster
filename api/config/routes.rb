Rails.application.routes.draw do
  devise_for :users, controllers: {registrations: "users/registrations",
                                   sessions: "users/sessions"}

  get "/accounts", to: "accounts#index"
  post "/accounts", to: "accounts#create"
  patch "/accounts", to: "accounts#update"
  delete "/accounts/:id", to: "accounts#destroy"

  get "/categories", to: "categories#index"
  post "/categories", to: "categories#create"
  patch "/categories", to: "categories#update"
  delete "/categories/:id", to: "categories#destroy"

  get "/goals", to: "goals#index"
  post "/goals", to: "goals#create"
  patch "/goals", to: "goals#update"
  delete "/goals/:id", to: "goals#destroy"

  get "/currencies", to: "currencies#index"

  get "/incomes", to: "incomes#index"
  post "/incomes", to: "incomes#create"
  patch "/incomes", to: "incomes#update"
  delete "/incomes/:id", to: "incomes#destroy"

  get "/expenses", to: "expenses#index"
  post "/expenses", to: "expenses#create"
  patch "/expenses", to: "expenses#update"
  delete "/expenses/:id", to: "expenses#destroy"

  get "/monthly_expenses", to: "expenses#monthly_expenses"

  get "/transfers", to: "transfers#index"
  post "/transfers", to: "transfers#create"
  patch "/transfers", to: "transfers#update"
  delete "/transfers/:id", to: "transfers#destroy"

  get "/expense_plans", to: "expense_plans#index"
  post "/expense_plans", to: "expense_plans#create"
  patch "/expense_plans", to: "expense_plans#update"
  # Users can't delete plans. They will be deleted by the system
  # at each month's end.
  delete "/expense_plans/:id", to: "expense_plans#destroy"

  get "/goal_plans", to: "goal_plans#index"
  post "/goal_plans", to: "goal_plans#create"
  patch "/goal_plans", to: "goal_plans#update"
  delete "/goal_plans/:id", to: "goal_plans#destroy"

  devise_scope :user do
    get "/user/signed_in", to: "users/sessions#signed_in"
    patch "/user/profile", to: "users/profiles#update"
  end
end
