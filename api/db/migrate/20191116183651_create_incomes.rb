class CreateIncomes < ActiveRecord::Migration[5.2]
  def change
    create_table :incomes do |t|
      t.decimal :amount, precision: 15, scale: 2
      t.date :date
      t.text :note
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
