class CreateExpensePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :expense_plans do |t|
      t.references :category, foreign_key: true
      t.decimal :amount, precision: 15, scale: 2
      t.date :month

      t.timestamps
    end
  end
end
