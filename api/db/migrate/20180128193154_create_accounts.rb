class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :type
      t.references :currency, foreign_key: true
      t.decimal :current_balance, precision: 15, scale: 2
      t.references :user, foreign_key: true
      t.boolean :in_plan, default: false
      t.decimal :credit, precision: 10, scale: 2

      t.timestamps
    end
  end
end
