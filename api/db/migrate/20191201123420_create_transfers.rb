class CreateTransfers < ActiveRecord::Migration[5.2]
  def change
    create_table :transfers do |t|
      t.references :source_account, foreign_key: {to_table: :accounts}
      t.references :target_account, foreign_key: {to_table: :accounts}
      t.decimal :source_amount, precision: 15, scale: 2
      t.decimal :target_amount, precision: 15, scale: 2
      t.date :date
      t.text :note

      t.timestamps
    end
  end
end
