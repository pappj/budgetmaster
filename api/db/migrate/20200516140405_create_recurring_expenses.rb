class CreateRecurringExpenses < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE TYPE date_frequency AS ENUM ('yearly', 'quarterly', 'monthly');
    SQL

    create_table :recurring_expenses do |t|
      t.references :category, foreign_key: true
      t.decimal :amount, precision: 15, scale: 2
      t.column :frequency, :date_frequency
      t.date :due_date

      t.timestamps
    end
  end

  def down
    drop_table :recurring_expenses
    execute <<-SQL
      DROP TYPE date_frequency;
    SQL
  end
end
