class AddUserProfileFields < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE TYPE currency_string AS ENUM ('symbol', 'short_code');
      CREATE TYPE currency_position AS ENUM ('before', 'after');
    SQL

    add_column :users, :currency_string, :currency_string, default: 'short_code'
    add_column :users, :currency_position, :currency_position, default: 'before'
    add_column :users, :date_format, :string, default: 'yyyy-MM-dd'
    add_reference :users, :currency, foreign_key: true
  end

  def down
    remove_reference :users, :currency
    remove_column :users, :date_format
    remove_column :users, :currency_position
    remove_column :users, :currency_string

    execute <<-SQL
      DROP TYPE currency_position;
      DROP TYPE currency_string;
    SQL
  end
end
