class AddSubscriptionToUser < ActiveRecord::Migration[6.1]
  def up
    execute <<-SQL
      CREATE TYPE subscription_type AS ENUM ('trial', 'free', 'basic', 'premium');
    SQL

    add_column :users, :subscription_type, :subscription_type, default: 'trial'
    add_column :users, :subscription_expiry_date, :date
  end

  def down
    remove_column :users, :subscription_expiry_date
    remove_column :users, :subscription_type

    execute <<-SQL
      DROP TYPE subscription_type
    SQL
  end
end
