class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.references :account, foreign_key: true
      t.decimal :amount, precision: 15, scale: 2
      t.date :date
      t.references :category, foreign_key: true
      t.text :note

      t.timestamps
    end
  end
end
