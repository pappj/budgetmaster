class CreateGoalPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :goal_plans do |t|
      t.references :goal, foreign_key: true
      t.decimal :monthly_amount, precision: 15, scale: 2
      t.decimal :target_amount, precision: 15, scale: 2
      t.date :target_date

      t.timestamps
    end
  end
end
