class AddAllocationToGoals < ActiveRecord::Migration[5.2]
  def change
    add_column :goals, :allocated_amount, :decimal, precision: 15, scale: 2,
      default: 0
  end
end
