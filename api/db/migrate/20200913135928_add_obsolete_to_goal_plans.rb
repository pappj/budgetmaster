class AddObsoleteToGoalPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :goal_plans, :obsolete, :boolean, default: false
  end
end
