class AddAchievedToGoals < ActiveRecord::Migration[5.2]
  def change
    add_column :goals, :achieved, :boolean, default: false
  end
end
