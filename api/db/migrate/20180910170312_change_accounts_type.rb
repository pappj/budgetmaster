class ChangeAccountsType < ActiveRecord::Migration[5.1]
  def change
    change_table :accounts do |t|
      t.rename :type, :account_type
    end
  end
end
