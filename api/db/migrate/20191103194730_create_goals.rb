class CreateGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :goals do |t|
      t.string :title
      t.decimal :target_amount
      t.date :target_date
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
