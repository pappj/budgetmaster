# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Currency.create(short_code: "EUR", symbol: "€")
Currency.create(short_code: "GBP", symbol: "£")
huf = Currency.create(short_code: "HUF", symbol: "Ft")
usd = Currency.create(short_code: "USD", symbol: "$")

if Rails.env.development?
  User.create(email: "valaki2@valaki.hu", password: "111111", currency: huf)
  user = User.first
  Account.create(name: "Erste szamla", account_type: "bank", currency: huf,
                 current_balance: 147890, credit: 0, in_plan: true,
                 user: user)
  Account.create(name: "SZEP kartya", account_type: "other", currency: huf,
                 current_balance: 2340, credit: 0, in_plan: false,
                 user: user)
  Account.create(name: "WizzAir kartya", account_type: "credit", currency: huf,
                 current_balance: 14300, credit: 100000, in_plan: false,
                 user: user)
  Account.create(name: "WU", account_type: "credit", currency: usd,
                 current_balance: 1570, credit: 0, in_plan: false,
                 user: user)
  Category.create(name: "Food", user: user)
  Category.create(name: "Clothing", user: user)
  Category.create(name: "Sport", user: user)
  Category.create(name: "Mortgage", user: user)
  Goal.create(title: "Vacation", target_amount: 450000, target_date: nil,
              user: user)
  Goal.create(title: "Car maintenance", target_amount: nil,
              target_date: Date.today + 1.year, user: user)
end

if Rails.env.test?
  User.create(email: "john@doe.com", password: "111111", currency: huf)
  User.create(email: "foo@bar.com", password: "111111", currency: usd,
              currency_string: "symbol", currency_position: "after",
              date_format: "MM/dd/yyyy")

  # Users with different subscriptions. The first user has trial by default.
  User.create(email: "free@user.com", password: "111111", currency: huf,
              subscription_type: "free", subscription_expiry_date: nil)
  User.create(email: "basic@user.com", password: "111111", currency: huf,
              subscription_type: "basic", subscription_expiry_date: Date.today)
  User.create(email: "premium@user.com", password: "111111", currency: huf,
              subscription_type: "premium", subscription_expiry_date: Date.today)
end
