# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_02_200432) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.string "account_type"
    t.bigint "currency_id"
    t.decimal "current_balance", precision: 15, scale: 2
    t.bigint "user_id"
    t.boolean "in_plan", default: false
    t.decimal "credit", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["currency_id"], name: "index_accounts_on_currency_id"
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.boolean "hidden", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_categories_on_user_id"
  end

  create_table "currencies", force: :cascade do |t|
    t.string "short_code"
    t.string "symbol"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "expense_plans", force: :cascade do |t|
    t.bigint "category_id"
    t.decimal "amount", precision: 15, scale: 2
    t.date "month"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_expense_plans_on_category_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.bigint "account_id"
    t.decimal "amount", precision: 15, scale: 2
    t.date "date"
    t.bigint "category_id"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_expenses_on_account_id"
    t.index ["category_id"], name: "index_expenses_on_category_id"
  end

  create_table "goal_plans", force: :cascade do |t|
    t.bigint "goal_id"
    t.decimal "monthly_amount", precision: 15, scale: 2
    t.decimal "target_amount", precision: 15, scale: 2
    t.date "target_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "obsolete", default: false
    t.index ["goal_id"], name: "index_goal_plans_on_goal_id"
  end

  create_table "goals", force: :cascade do |t|
    t.string "title"
    t.decimal "target_amount"
    t.date "target_date"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "achieved", default: false
    t.decimal "allocated_amount", precision: 15, scale: 2, default: "0.0"
    t.index ["user_id"], name: "index_goals_on_user_id"
  end

  create_table "incomes", force: :cascade do |t|
    t.decimal "amount", precision: 15, scale: 2
    t.date "date"
    t.text "note"
    t.bigint "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_incomes_on_account_id"
  end

# Could not dump table "recurring_expenses" because of following StandardError
#   Unknown type 'date_frequency' for column 'frequency'

  create_table "transfers", force: :cascade do |t|
    t.bigint "source_account_id"
    t.bigint "target_account_id"
    t.decimal "source_amount", precision: 15, scale: 2
    t.decimal "target_amount", precision: 15, scale: 2
    t.date "date"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["source_account_id"], name: "index_transfers_on_source_account_id"
    t.index ["target_account_id"], name: "index_transfers_on_target_account_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'currency_string' for column 'currency_string'

  add_foreign_key "accounts", "currencies"
  add_foreign_key "accounts", "users"
  add_foreign_key "categories", "users"
  add_foreign_key "expense_plans", "categories"
  add_foreign_key "expenses", "accounts"
  add_foreign_key "expenses", "categories"
  add_foreign_key "goal_plans", "goals"
  add_foreign_key "goals", "users"
  add_foreign_key "incomes", "accounts"
  add_foreign_key "recurring_expenses", "categories"
  add_foreign_key "transfers", "accounts", column: "source_account_id"
  add_foreign_key "transfers", "accounts", column: "target_account_id"
  add_foreign_key "users", "currencies"
end
