# == Schema Information
#
# Table name: transfers
#
#  id                :bigint           not null, primary key
#  source_account_id :bigint
#  target_account_id :bigint
#  source_amount     :decimal(15, 2)
#  target_amount     :decimal(15, 2)
#  date              :date
#  note              :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
require 'rails_helper'

RSpec.describe Transfer, type: :model do
  subject {
    u = User.new(email: "joe@sample.com", password: "supersecret")
    c = Currency.find_by(short_code: "HUF")
    a1 = Account.new(account_type: "bank", current_balance: 10000, credit: 5000,
                     user: u, currency: c)

    a2 = Account.new(account_type: "cash", current_balance: 20000, credit: 0,
                     user: u, currency: c)
    Transfer.new(source_account: a1, source_amount: 5000, target_account: a2,
                 target_amount: 5000, date: Date.today, update_account: true)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without source account" do
      subject.source_account = nil
      expect(subject).not_to be_valid
    end

    it "is not valid without target account" do
      subject.target_account = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with different user's accounts" do
      u2 = User.new(email: "bob@sample.com", password: "password")
      c = Currency.find_by(short_code: "HUF")
      a = Account.new(account_type: "bank", current_balance: 10000, credit: 0,
                      user: u2, currency: c)
      subject.target_account = a
      expect(subject).not_to be_valid
    end

    it "is not valid with different amounts for same the currency" do
      subject.target_amount = 6000
      expect(subject).not_to be_valid
    end

    it "is not valid with negative source amount" do
      subject.source_amount = -1000
      expect(subject).not_to be_valid
    end

    it "is not valid with negative target amount" do
      subject.target_amount = -1000
      expect(subject).not_to be_valid
    end

    it "is valid with source amount up to the credit limit" do
      subject.source_amount = 12000
      subject.target_amount = 12000
      expect(subject).to be_valid
    end

    it "is not valid with more source amount than available" do
      subject.source_amount = 23000
      subject.target_amount = 23000
      expect(subject).not_to be_valid
    end

    it "is not valid with date in the future" do
      subject.date = Date.today + 1.day
      expect(subject).not_to be_valid
    end
  end
end
