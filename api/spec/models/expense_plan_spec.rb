# == Schema Information
#
# Table name: expense_plans
#
#  id          :bigint           not null, primary key
#  category_id :bigint
#  amount      :decimal(, )
#  month       :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe ExpensePlan, type: :model do
  user = User.new(email: "joe@sample.com", password: "wontfindout")
  cat = Category.new(name: "Food", user: user)

  subject {
    ExpensePlan.new(category: cat, amount: 12000, month: Date.today)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is invalid without a category" do
      subject.category = nil
      expect(subject).not_to be_valid
    end

    it "is invalid with negative amount" do
      subject.amount = -650
      expect(subject).not_to be_valid
    end

    it "is invalid with date in a previous month" do
      subject.month = Date.today - 1.month
      expect(subject).not_to be_valid
    end

    it "is valid with a past date in the current month" do
      return if Date.today.day == 1

      subject.month = Date.today - 1.day
      expect(subject).to be_valid
    end
  end
end
