require 'rails_helper'

RSpec.describe User, type: :model do
  include_context "models"
  subject {
    User.new(email: "joe@sample.com", password: "wontfindout", currency: huf,
             subscription_type: "trial",
             subscription_expiry_date: Date.today + 30.days)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid with unknown currency_string" do
      expect{subject.currency_string='money'}
        .to raise_exception(ArgumentError, /money/)
    end

    it "is not valid with unknown currency_position" do
      expect{subject.currency_position='above'}
        .to raise_exception(ArgumentError, /above/)
    end

    it "is not valid with nil currency" do
      subject.currency = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with unsupported date string" do
      subject.date_format = "yyyy.dd.mm"
      expect(subject).not_to be_valid
    end
  end

  describe "Subscription" do
    it "free gives no access" do
      subject.subscription_type = "free"
      expect(subject.entitled_for_basic?).to be(false)
      expect(subject.entitled_for_premium?).to be(false)
    end

    it "basic gives only basic access" do
      subject.subscription_type = "basic"
      expect(subject.entitled_for_basic?).to be(true)
      expect(subject.entitled_for_premium?).to be(false)
    end

    it "trial gives basic and premium access" do
      subject.subscription_type = "trial"
      expect(subject.entitled_for_basic?).to be(true)
      expect(subject.entitled_for_premium?).to be(true)
    end

    it "premium gives basic and premium access" do
      subject.subscription_type = "premium"
      expect(subject.entitled_for_basic?).to be(true)
      expect(subject.entitled_for_premium?).to be(true)
    end

    it "trial expires on expiry date" do
      subject.subscription_type = "trial"
      subject.subscription_expiry_date = Date.today
      expect(subject.entitled_for_premium?).to be(true)
      subject.subscription_expiry_date = Date.today - 1.day
      expect(subject.entitled_for_premium?).to be(false)
    end

    it "basic is valid within 15 days of expiry" do
      subject.subscription_type = "basic"
      subject.subscription_expiry_date = Date.today - 15.days
      expect(subject.entitled_for_basic?).to be(true)
    end

    it "basic is not valid 15 days after expiry" do
      subject.subscription_type = "basic"
      subject.subscription_expiry_date = Date.today - 16.days
      expect(subject.entitled_for_basic?).to be(false)
    end

    it "premium is valid withing 15 days of expiry" do
      subject.subscription_type = "premium"
      subject.subscription_expiry_date = Date.today - 15.days
      expect(subject.entitled_for_premium?).to be(true)
    end

    it "premium is not valid 15 days after expiry" do
      subject.subscription_type = "premium"
      subject.subscription_expiry_date = Date.today - 16.days
      expect(subject.entitled_for_premium?).to be(false)
    end
  end
end
