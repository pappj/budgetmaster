# == Schema Information
#
# Table name: accounts
#
#  id              :bigint           not null, primary key
#  name            :string
#  account_type    :string
#  currency_id     :bigint
#  current_balance :decimal(15, 2)
#  user_id         :bigint
#  in_plan         :boolean
#  credit          :decimal(10, 2)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
require 'rails_helper'

RSpec.describe Account, type: :model do
  include_context "models"

  def u
    User.new(email: "joe@sample.com", password: "wontfindout", currency: huf)
  end

  subject {
    Account.new(name: "Main account", account_type: "bank", current_balance: 1000,
                credit: 500, in_plan: true, user: u, currency: huf)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid with empty name" do
      subject.name = ""
      expect(subject).not_to be_valid
    end

    it "is not valid with invalid type" do
      subject.account_type = "gimmethecash"
      expect(subject).not_to be_valid
    end

    it "is not valid with non-number balance" do
      subject.current_balance = "thousand"
      expect(subject).not_to be_valid
    end

    it "is not valid with non-number credit" do
      subject.credit = "hundred"
      expect(subject).not_to be_valid
    end

    it "is not valid with negative credit" do
      subject.credit = -500
      expect(subject).not_to be_valid
    end

    it "is not valid with positive credit for cash" do
      subject.account_type = "cash"
      subject.credit = 35000
      expect(subject).not_to be_valid
    end

    it "is not valid if credit balance exceeds limit" do
      subject.account_type = "credit"
      subject.credit = 1500
      subject.current_balance = 2000
      expect(subject).not_to be_valid
    end

    it "is not valid to include in plan with not the main currency" do
      subject.currency = usd
      subject.in_plan = true
      expect(subject).not_to be_valid
    end
  end

  describe "available_amount" do
    it "is correct for credit cards" do
      a = Account.new(account_type: "credit", current_balance: 5600, credit: 60000,
                      user: u, currency: huf)
      expect(a.available_amount).to eq(5600)
    end

    it "is correct for non credit cards" do
      a = Account.new(account_type: "bank", current_balance: 5600, credit: 60000,
                      user: u, currency: huf)
      expect(a.available_amount).to eq(65600)
    end
  end
end
