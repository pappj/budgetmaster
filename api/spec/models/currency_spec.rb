# == Schema Information
#
# Table name: currencies
#
#  id         :bigint           not null, primary key
#  short_code :string
#  symbol     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Currency, type: :model do
  subject { Currency.new(short_code: "USD", symbol: "$")}

  describe "Validations" do
    it "is valid with valid parameters" do
      expect(subject).to be_valid
    end
  end
end
