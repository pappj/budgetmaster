# == Schema Information
#
# Table name: recurring_expenses
#
#  id          :bigint           not null, primary key
#  category_id :bigint
#  amount      :decimal(, )
#  frequency   :enum
#  due_date    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe RecurringExpense, type: :model do
  subject {
    user = User.new(email: "joe@sample.com", password: "wontfindout")
    cat = Category.new(name: "Food", user: user)

    RecurringExpense.new(category: cat, amount: 5000, frequency: 'monthly',
                         due_date: Date.today + 10.days)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a category" do
      subject.category = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with negative amount" do
      subject.amount = -600
      expect(subject).not_to be_valid
    end

    it "is not valid with an unknown frequency" do
      expect{subject.frequency='often'}.to raise_exception(ArgumentError, /often/)
    end

    it "is not valid without due date" do
      subject.due_date = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with due date in the past" do
      subject.due_date = Date.today - 2.days
      expect(subject).not_to be_valid
    end
  end
end
