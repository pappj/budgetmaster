require 'rails_helper'

RSpec.describe GoalPlan, type: :model do
  def u
    User.new(email: "joe@sample.com", password: "wontfindout")
  end

  subject {
    g = Goal.new(user: u, title: "Vacation", target_amount: 240000,
                 target_date: Date.today + 2.years)
    GoalPlan.new(goal: g, monthly_amount: 10000) }

  describe "Validation" do
    it "is valid with valid with no estimated targets" do
      expect(subject).to be_valid
    end

    it "is valid with estimated amount" do
      subject.goal = Goal.new(user: u, title: "Vacation",
                              target_date: Date.today + 2.years)
      subject.target_amount = 240000
      expect(subject).to be_valid
    end

    it "is valid with estimated date" do
      subject.goal = Goal.new(user: u, title: "Vacation", target_amount: 240000)
      subject.target_date = Date.today + 2.years
      expect(subject).to be_valid
    end

    it "is invalid without a goal" do
      subject.goal = nil
      expect(subject).not_to be_valid
    end

    it "is invalid with negative monthly amount" do
      subject.monthly_amount = -20000
      expect(subject).not_to be_valid
    end

    it "is invalid with negative estimated amount" do
      subject.goal = Goal.new(user: u, title: "Vacation",
                              target_date: Date.today + 2.years)
      subject.target_amount = -80000
      expect(subject).not_to be_valid
    end

    it "is invalid with estimated date in the past" do
      subject.goal = Goal.new(user: u, title: "Vacation", target_amount: 240000)
      subject.target_date = Date.today - 2.months
      expect(subject).not_to be_valid
    end

    it "is invalid with two target amounts" do
      subject.target_amount = 300000
      expect(subject).not_to be_valid
    end

    it "is invalid with two target dates" do
      subject.target_date = Date.today + 1.year
      expect(subject).not_to be_valid
    end

    it "is invalid with no target amount" do
      subject.goal = Goal.new(user: u, title: "Vacation",
                              target_date: Date.today + 2.years)
      expect(subject).not_to be_valid
    end

    it "is invalid with no target date" do
      subject.goal = Goal.new(user: u, title: "Vacation", target_amount: 240000)
      expect(subject).not_to be_valid
    end

    it "is invalid with two estimated targets" do
      subject.goal = Goal.new(user: u, title: "Vacation")
      subject.monthly_amount = 240000
      subject.target_date = Date.today + 2.years
      expect(subject).not_to be_valid
    end
  end
end
