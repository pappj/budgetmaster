# == Schema Information
#
# Table name: incomes
#
#  id         :bigint           not null, primary key
#  amount     :decimal(15, 2)
#  date       :date
#  note       :text
#  account_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Income, type: :model do
  subject {
    u = User.new(email: "joe@sample.com", password: "wontfindout")
    c = Currency.find_by(short_code: "HUF")
    a = Account.new(account_type: "bank", current_balance: 1000, credit: 500,
                    user: u, currency: c)
    Income.new(account: a, amount: 4000, date: Date.today, note: "Paycheck") }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without an account" do
      subject.account = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with non-number amount" do
      subject.amount = "hundred bucks"
      expect(subject).not_to be_valid
    end

    it "is not valid with negative amount" do
      subject.amount = -4000
      expect(subject).not_to be_valid
    end

    it "is not valid with date in the future" do
      subject.date = Date.today + 1.month
      expect(subject).not_to be_valid
    end
  end
end
