# == Schema Information
#
# Table name: categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  hidden     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
require 'rails_helper'

RSpec.describe Category, type: :model do
  subject {
    u = User.new(email: "joe@sample.com", password: "wontfindout")
    Category.new(name: "Mortgage", hidden: false, user: u)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid with a short name" do
      subject.name = ""
      expect(subject).not_to be_valid
    end
  end
end
