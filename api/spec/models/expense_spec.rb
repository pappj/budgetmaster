# == Schema Information
#
# Table name: expenses
#
#  id          :bigint           not null, primary key
#  account_id  :bigint
#  amount      :decimal(15, 2)
#  date        :date
#  category_id :bigint
#  note        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe Expense, type: :model do
  subject {
    user = User.new(email: "joe@sample.com", password: "wontfindout")
    huf = Currency.find_by(short_code: "HUF")
    acc = Account.new(account_type: "bank", current_balance: 10000, credit: 500,
                    user: user, currency: huf)
    cat = Category.new(name: "Food", user: user)

    Expense.new(account: acc, category: cat, amount: 1600, date: Date.today,
                update_account: true)
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without an account" do
      subject.account = nil
      expect(subject).not_to be_valid
    end

    it "is not valid without a category" do
      subject.category = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with different user's account and category" do
      other_user = User.new(email: "other@user.com", password: "ohmygod")
      other_category = Category.new(name: "Car", user: other_user)
      subject.category = other_category
      expect(subject).not_to be_valid
    end

    it "is not valid with negative amount" do
      subject.amount = -250
      expect(subject).not_to be_valid
    end

    it "is valid with amount up to the credit limit" do
      subject.amount = 10400
      expect(subject).to be_valid
    end

    it "is not valid with amount more than available" do
      subject.amount = 12000
      expect(subject).not_to be_valid
    end

    it "is not valid with date in the future" do
      subject.date = Date.today + 1.month
      expect(subject).not_to be_valid
    end
  end
end
