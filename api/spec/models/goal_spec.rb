# == Schema Information
#
# Table name: goals
#
#  id            :bigint           not null, primary key
#  title         :string
#  target_amount :decimal(, )
#  target_date   :date
#  user_id       :bigint
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  achieved      :boolean          default(FALSE)
#
require 'rails_helper'

RSpec.describe Goal, type: :model do
  subject {
    u = User.new(email: "joe@sample.com", password: "wontfindout")
    Goal.new(user: u, title: "Vacation", target_amount: 250000,
             target_date: Date.today + 2.years) }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid with too short title" do
      subject.title = ""
      expect(subject).not_to be_valid
    end

    it "is not valid with negative target amount" do
      subject.target_amount = -500
      expect(subject).not_to be_valid
    end

    it "is not valid with non-number target amount" do
      subject.target_amount = "hundred"
      expect(subject).not_to be_valid
    end

    it "is valid with empty target amount" do
      subject.target_amount = nil
      expect(subject).to be_valid
    end

    it "is not valid with target date in the past" do
      subject.target_date = Date.today - 1.day
      expect(subject).not_to be_valid
    end

    it "is valid with target date in the past when achieved" do
      subject.target_date = Date.today - 3.day
      subject.achieved = true
      expect(subject).to be_valid
    end

    it "is valid with empty target date" do
      subject.target_date = nil
      expect(subject).to be_valid
    end

    it "is valid with empty targets" do
      subject.target_amount = nil
      subject.target_date = nil
      expect(subject).to be_valid
    end
  end
end
