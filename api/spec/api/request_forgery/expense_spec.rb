require 'rails_helper'

RSpec.describe "Expense request forging", type: :request do
  include_context "user login"
  include_context "models"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    Account.create(name: "Cash", account_type: "cash", currency: huf,
                   current_balance: 67300, credit: 0, in_plan: false, user: user)
    acc = Account.create(name: "Temp acc", account_type: "other",
                         currency: huf, current_balance: 7000,
                         credit: 0, in_plan: false, user: other_user)
    Category.create(name: "Entertainment", user: user)
    cat = Category.create(name: "Food", user: other_user)
    Expense.create(account: acc, category: cat, amount: 3240, date: Date.today,
                   note: "Groceries")
  end


  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Expense.find_by(note: "Groceries").id
  end

  def acc_id
    Account.find_by(name: "Cash").id
  end

  def other_acc_id
    Account.find_by(name: "Temp acc").id
  end

  def cat_id
    Category.find_by(name: "Entertainment").id
  end

  def other_cat_id
    Category.find_by(name: "Food").id
  end

  it "can't create expense for non-existent account" do
    post '/expenses', xhr: true, as: :json, params: {expense: {
      account_id: 76543, category_id: cat_id, amount: 3200, date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't create expense for non-existent category" do
    post '/expenses', xhr: true, as: :json, params: {expense: {
      account_id: acc_id, category_id: 9789, amount: 3200, date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't create expense for other user's account" do
    post '/expenses', xhr: true, as: :json, params: {expense: {
      account_id: other_acc_id, category_id: cat_id, amount: 3200,
      date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't create expense for other user's category" do
    post '/expenses', xhr: true, as: :json, params: {expense: {
      account_id: acc_id, category_id: other_cat_id, amount: 3200,
      date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't edit other's expense" do
    patch '/expenses', xhr: true, as: :json, params: {expense: {
      id: get_id, amount: 4500, note: "Wrong id"}}
    expect(response).to have_http_status(:not_found)

    exp = Expense.find_by(note: "Groceries")
    expect(exp).not_to be(nil)
    expect(exp.note).to eq("Groceries")
  end

  it "can't delete other's expense" do
    delete "/expenses/#{get_id}", xhr: true, as: :json
    expect(response).to have_http_status(:not_found)
    exp = Expense.find_by(note: "Groceries")
    expect(exp).not_to be(nil)
  end
end
