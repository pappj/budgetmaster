require 'rails_helper'

RSpec.describe "Income request forging", type: :request do
  include_context "user login"
  include_context "models"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    acc = Account.create(name: "Temp acc", account_type: "other",
                         currency: huf, current_balance: 7000,
                         credit: 0, in_plan: false, user: other_user)
    Income.create(account: acc, amount: 3000, date: Date.today,
                  note: "Paycheck")
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Income.find_by(note: "Paycheck").id
  end

  it "can't create income to non-existent account" do
    post '/incomes', xhr: true, as: :json, params: {income: {account_id: 43207,
      amount: 2500, date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't create income to someone else's account" do
    acc = Account.find_by(name: "Temp acc")
    post '/incomes', xhr: true, as: :json, params: {income: {account_id: acc.id,
      amount: 2500, date: Date.today}}
    expect(response).to have_http_status(:not_found)
  end

  it "can't edit others' income" do
    patch '/incomes', xhr: true, as: :json, params: {income: {id: get_id,
      amount: 2500, note: "Lottery"}}

    expect(response).to have_http_status(:not_found)
    income = Income.find_by(note: "Paycheck")
    expect(income).not_to be(nil)
    expect(income.amount).to eq(3000)
  end

  it "can't delete others' income" do
    delete "/incomes/#{get_id}", xhr: true, as: :json

    expect(response).to have_http_status(:not_found)
    income = Income.find_by(note: "Paycheck")
    expect(income).not_to be(nil)
  end

  it "can't delete if the account has low amount" do
    acc = Account.create(name: "Cash", account_type: "cash", currency: huf,
                         current_balance: 3400, credit: 0, user: user)
    inc = Income.create(account: acc, amount: 6000, date: Date.today)

    delete "/incomes/#{inc.id}", xhr: true, as: :json, params: {update_account: true}
    expect(response).to have_http_status(:forbidden)
    inc.destroy
    acc.destroy
  end
end
