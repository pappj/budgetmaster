require 'rails_helper'

RSpec.describe "Categories request forging", type: :request do
  include_context "user login"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    Category.create(name: "Temp cat", user: other_user)
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Category.find_by(name: "Temp cat").id
  end

  it "can't edit others' categories" do
    patch '/categories', xhr: true, as: :json, params: {category: {id: get_id, name: "Fancy watches"}}

    expect(response).to have_http_status(:not_found)
    cat = Category.find_by(name: "Fancy watches")
    expect(cat).to eq(nil)
    cat2 = Category.find_by(name: "Temp cat")
    cat2.destroy
  end

  it "can't delete others' categories" do
    delete "/categories/#{get_id}", xhr: true, as: :json

    expect(response).to have_http_status(:not_found)
    cat = Category.find_by(name: "Temp cat")
    expect(cat).not_to be(nil)
    cat.destroy
  end
end
