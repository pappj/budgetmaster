require 'rails_helper'

RSpec.describe "Goal plan request forging", type: :request do
  include_context "user login"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    g = Goal.create(title: "Car", target_amount: 1500000,
                    target_date: Date.today + 12.months, user: user)
    GoalPlan.create(goal: g, monthly_amount: 125000)

    g2 = Goal.create(title: "Christmas gifts", target_date: Date.today + 2.months,
                     user: other_user)
    GoalPlan.create(goal: g2, monthly_amount: 15000, target_amount: 30000)

    Goal.create(title: "Vacation", target_amount: 150000,
                target_date: Date.today + 3.months, user: user)
    Goal.create(title: "Renovation", target_amount: 150000, user: other_user)

    api_login_user
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def goal
    Goal.find_by(title: "Vacation")
  end

  def planned_goal
    Goal.find_by(title: "Car")
  end

  def plan
    planned_goal.goal_plan
  end

  def other_goal
    Goal.find_by(title: "Renovation")
  end

  def other_planned_goal
    Goal.find_by(title: "Christmas gifts")
  end

  def other_plan
    other_planned_goal.goal_plan
  end

  it "can't create a plan for achieved goals" do
    goal.update({achieved: true})
    post '/goal_plans', xhr: true, as: :json, params: {
      goal_id: goal.id, monthly_amount: 50000
    }

    expect(response).to have_http_status(:forbidden)
  end

  it "can't create a plan for past goals" do
    g = goal
    g.target_date = Date.today - 1.month
    g.save(validate: false)
    post '/goal_plans', xhr: true, as: :json, params: {
      goal_id: g.id, monthly_amount: 50000
    }

    expect(response).to have_http_status(:forbidden)
  end

  it "can't edit a plan for achieved goals" do
    planned_goal.update({achieved: true})
    patch '/goal_plans', xhr: true, as: :json, params: {
      id: plan.id, monthly_amount: 20000, target_amount: 40000
    }

    expect(response).to have_http_status(:forbidden)
  end

  it "can't edit a plan for past goals" do
    g = planned_goal
    g.target_date = Date.today - 1.month
    g.save(validate: false)
    patch '/goal_plans', xhr: true, as: :json, params: {
      id: plan.id, monthly_amount: 20000, target_amount: 40000
    }

    expect(response).to have_http_status(:forbidden)
  end

  it "can't create multiple plans for a goal" do
    Rails.logger.info("Test begins")
    post '/goal_plans', xhr: true, as: :json, params: {
      goal_id: planned_goal.id, monthly_amount: 120000
    }

    expect(response).to have_http_status(:forbidden)
  end

  it "can't create plan for other's goal" do
    post '/goal_plans', xhr: true, as: :json, params: {
      goal_id: other_goal.id, monthly_amount: 50000
    }

    expect(response).to have_http_status(:not_found)
  end

  it "can't edit other's goal plan" do
    patch '/goal_plans', xhr: true, as: :json, params: {
      id: other_plan.id, monthly_amount: 20000
    }

    expect(response).to have_http_status(:not_found)
  end

  it "deleting own goal allocation is forbidden" do
    delete "/goal_plans/#{plan.id}", xhr: true, as: :json
    expect(response).to have_http_status(:forbidden)
  end

  it "deleting others' goal allocation results in not_found" do
    delete "/goal_plans/#{other_plan.id}", xhr: true, as: :json
    expect(response).to have_http_status(:not_found)
  end

end
