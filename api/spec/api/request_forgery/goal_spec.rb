require 'rails_helper'

RSpec.describe "Goals request forging", type: :request do
  include_context "user login"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    Goal.create(user: other_user, title: "Vacation", target_amount: "2600",
                target_date: Date.today)
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Goal.find_by(title: "Vacation").id
    #ApplicationController::encode_id(goal.id)
  end

  it "can't edit others' goals" do
    patch '/goals', xhr: true, as: :json,
      params: {goal: {id: get_id, target_amount: 5000, target_date: nil}}

    expect(response).to have_http_status(:not_found)
    goal = Goal.find_by(title: "Vacation")
    expect(goal.target_amount).to eq(2600)
    expect(goal.target_date).to eq(Date.today)
    goal.destroy
  end

  it "can't delete others' goals" do
    delete "/goals/#{get_id}", xhr: true, as: :json

    expect(response).to have_http_status(:not_found)
    goal = Goal.find_by(title: "Vacation")
    expect(goal).not_to be(nil)
    goal.destroy
  end
end
