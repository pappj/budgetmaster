require 'rails_helper'

RSpec.describe "Recurring Expense request forging", type: :request do
  include_context "user login"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    cat = Category.create(name: "Temp cat", user: other_user)
    RecurringExpense.create(category: cat, amount: 6000, frequency: 'quarterly',
                            due_date: Date.today)
  end

  def get_id
    RecurringExpense.first.id
  end

  def get_category_id
    Category.find_by(name: "Temp cat")
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  it "can't create recurring expense for other's category" do
    patch '/categories', xhr: true, as: :json, params:
      {category: {id: get_category_id,
                  name: "Temp cat",
                  recurring_expenses_attributes: [{amount: 500,
                                                   frequency: "monthly",
                                                   due_date: Date.today}]}}
    expect(response).to have_http_status(:not_found)
    expect(RecurringExpense.count).to eq(1)
  end

  it "can't edit other's recurring expense" do
    patch '/categories', xhr: true, as: :json, params:
      {category: {id: get_category_id,
                  recurring_expenses_attributes: [{id: get_id, amount: 400}]}}

    expect(response).to have_http_status(:not_found)
    expect(RecurringExpense.first.amount).to eq(6000)
  end

  it "can't delete other's recurring expense" do
    patch '/categories/', xhr: true, as: :json, params:
      {category: {id: get_category_id,
                  recurring_expenses_attributes: [{id: get_id, _destroy: 1}]}}

    expect(response).to have_http_status(:not_found)
    expect(RecurringExpense.count).to eq(1)
  end
end
