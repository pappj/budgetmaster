require 'rails_helper'

RSpec.describe "Accounts request forging", type: :request do
  include_context "user login"
  include_context "models"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    acc = Account.create(name: "Temp acc", account_type: "other", currency: huf,
                         current_balance: 7000, credit: 0, in_plan: false,
                         user: other_user)
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Account.find_by(name: "Temp acc").id
  end

  it "can't edit others' accounts" do
    patch '/accounts', xhr: true, as: :json,
      params: {account: {id: get_id, current_balance: 12320, credit: 200}}

    expect(response).to have_http_status(:not_found)
    acc = Account.find_by(name: "Temp acc")
    expect(acc.current_balance).to eq(7000)
    expect(acc.credit).to eq(0)
    acc.destroy
  end

  it "can't delete others' accounts" do
    delete "/accounts/#{get_id}", xhr: true, as: :json

    expect(response).to have_http_status(:not_found)
    acc = Account.find_by(name: "Temp acc")
    expect(acc).not_to be(nil)
    acc.destroy
  end
end
