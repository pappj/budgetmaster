require 'rails_helper'
require 'api/request_forgery/subscriptions/generic'

RSpec.describe "Free subscription user", type: :request do
  include_context "user login"
  include_context "models"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  def test_user
    free_user
  end

  include_examples "accounts without planning"
  include_examples "forbidden recurring expenses"
  include_examples "forbidden goals"
  include_examples "forbidden goal plans"
  include_examples "forbidden expense plans"

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end
end
