RSpec.shared_examples "accounts without planning" do
  before(:each) do
    api_login(test_user)
  end

  it "can't fetch in_plan status" do
    Account.create(name: "Cash", account_type: "cash", current_balance: 1600,
                   credit: 0, currency: huf, user: test_user)
    get '/accounts', xhr: true, as: :json

    expect(response).to have_http_status(:ok)
    acc = JSON.parse(response.body)["accounts"][0]
    expect(acc["name"]).to eq("Cash")
    expect(acc).not_to have_key("in_plan")
  end

  it "can't set in_plan for new accounts" do
    post '/accounts', xhr: true, as: :json, params:
      {name: "Main account", account_type: "bank", current_balance: 5000,
       credit: 0, currency_id: huf.id, in_plan: true}

    expect(response).to have_http_status(:ok)
    expect(Account.find_by(name: "Main account").in_plan).to be(false)
  end

  it "can't edit in_plan status" do
    a = Account.create(name: "Cash", account_type: "cash", current_balance: 1600,
                       credit: 0, currency: huf, user: test_user, in_plan: true)
    patch '/accounts', xhr: true, as: :json, params:
      {id: a.id, current_balance: 2000, in_plan: false}

    expect(response).to have_http_status(:ok)
    acc = Account.find(a.id)
    expect(acc.current_balance).to eq(2000)
    expect(acc.in_plan).to be(true)
  end
end

RSpec.shared_examples "forbidden recurring expenses" do
  before(:each) do
    c = Category.create(name: "Sport", user: test_user)
    c.recurring_expenses.create(amount: 5000, frequency: "monthly",
                                due_date: Date.today)
    api_login(test_user)
  end

  it "can't fetch recurring expenses" do
    get '/categories', xhr: true, as: :json

    expect(response).to have_http_status(:ok)
    cat = JSON.parse(response.body)["categories"][0]
    expect(cat["name"]).to eq("Sport")
    expect(cat).not_to have_key("recurring_expenses")
  end

  it "can't create recurring expenses" do
    patch '/categories', xhr: true, as: :json, params:
      {category: {id: Category.first.id,
                  recurring_expenses_attributes:
                    [{amount: 600, frequency: "yearly", due_date: Date.today}]}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(RecurringExpense.count).to be(1)
  end

  it "can't edit recurring expenses" do
    patch '/categories', xhr: true, as: :json, params:
      {category: {id: Category.first.id,
                  recurring_expenses_attributes:
                    [{id: RecurringExpense.first.id, amount: 6000}]}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(RecurringExpense.first.amount).to eq(5000)
  end

  it "can't destroy recurring expenses" do
    patch '/categories', xhr: true, as: :json, params:
      {category: {id: Category.first.id,
                  recurring_expenses_attributes:
                    [{id: RecurringExpense.first.id, _destroy: 1}]}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(RecurringExpense.count).to be(1)
  end
end

RSpec.shared_examples "forbidden goals" do
  before(:each) do
    api_login(test_user)
  end

  it "can't fetch goals" do
    Goal.create(title: "Fetch goal", target_amount: 300000, user: test_user)

    get '/goals', xhr: true, as: :json
    expect(response).to have_http_status(:ok)
    expect(response.body).to eq({goals: []}.to_json)
  end

  it "can't create goals" do
    post '/goals', xhr: true, as: :json,
      params: {goal: {title: "Accept me", target_amount: 5000, target_date: nil}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(Goal.find_by(title: "Accept me")).to be(nil)
  end

  it "can't edit goals" do
    g = Goal.create(title: "Update goal", target_amount: 300000, user: test_user)
    patch '/goals', xhr: true, as: :json,
      params: {goal: {id: g.id, title: "Hacked title"}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(Goal.find_by(title: "Hacked title")).to be(nil)
  end

  it "can't delete goals" do
    g = Goal.create(title: "Remove goal", target_amount: 300000, user: test_user)
    delete "/goals/#{g.id}", xhr: true, as: :json

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(Goal.find_by(title: "Remove goal")).not_to be(nil)
  end
end

RSpec.shared_examples "forbidden goal plans" do
  before(:each) do
    api_login(test_user)
  end

  it "can't fetch goal plans" do
    g = Goal.create(title: "Birthday gifts", target_amount: 30000,
                    target_date: Date.today + 6.months, user: test_user)
    g.create_goal_plan(monthly_amount: 5000)

    get '/goal_plans', xhr: true, as: :json
    expect(response).to have_http_status(:ok)
    expect(response.body).to eq({goal_plans: []}.to_json)
  end

  it "can't create goal plans" do
    g = Goal.create(title: "Vacation", target_amount: 300000, user: test_user)
    post '/goal_plans', xhr: true, as: :json,
      params: {goal_plan: {goal_id: g.id, monthly_amount: 15000,
                           target_amount: 45000, target_date: nil}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(Goal.find_by(title: "Vacation").goal_plan).to be(nil)
  end

  it "can't edit goal plans" do
    g = Goal.create(title: "Birthday gifts", target_amount: 30000,
                    target_date: Date.today + 6.months, user: test_user)
    p = g.create_goal_plan(monthly_amount: 5000)
    patch '/goal_plans', xhr: true, as: :json,
      params: {goal_plan: {id: p.id, monthly_amount: 50000}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(GoalPlan.find(p.id).monthly_amount).to eq(5000)
  end

  it "can't delete goal plans" do
    g = Goal.create(title: "Birthday gifts", target_amount: 30000,
                    target_date: Date.today + 6.months, user: test_user)
    p = g.create_goal_plan(monthly_amount: 5000)
    delete "/goal_plans/#{p.id}", xhr: true, as: :json

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(GoalPlan.find(p.id)).not_to be(nil)
  end
end

RSpec.shared_examples "forbidden expense plans" do
  before(:each) do
    c = Category.create(name: "Food", user: test_user)
    c.expense_plans.create(month: Date.today, amount: 12000)
    api_login(test_user)
  end

  it "can't fetch expense plans" do
    get '/expense_plans', xhr: true, as: :json
    expect(response).to have_http_status(:ok)
    expect(response.body).to eq({expense_plans: []}.to_json)
  end

  it "can't create expense plans" do
    post '/expense_plans', xhr: true, as: :json,
      params: {expense_plan: {category_id: Category.find_by(name: "Food").id,
                              amount: 15000, month: Date.today + 1.month}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(ExpensePlan.count).to be(1)
  end

  it "can't edit expense plans" do
    patch '/expense_plans', xhr: true, as: :json,
      params: {expense_plan: {id: ExpensePlan.first.id, amount: 50000}}

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(ExpensePlan.first.amount).to eq(12000)
  end

  it "can't delete expense plans" do
    delete "/expense_plans/#{ExpensePlan.first.id}", xhr: true, as: :json

    expect(response).to have_http_status(:forbidden)
    expect(JSON.parse(response.body)["subscriptionNotSufficient"]).to be(true)
    expect(ExpensePlan.count).to be(1)
  end
end
