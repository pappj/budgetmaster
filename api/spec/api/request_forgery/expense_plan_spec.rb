require 'rails_helper'

RSpec.describe "Expense plan request foring", type: :request do
  include_context "user login"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    cat1 = Category.create(name: "Food", user: user)
    cat1.expense_plans.create(amount: 5500, month: Date.today)
    cat2 = Category.create(name: "Sports", user: other_user)
    cat2.expense_plans.create(amount: 7000, month: Date.today)

    api_login_user
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def category
    Category.find_by(name: "Food")
  end

  def other_category
    Category.find_by(name: "Sports")
  end

  def allocation
    category.expense_plans.first
  end

  def other_allocation
    other_category.expense_plans.first
  end

  it "can't change allocation category" do
    new_cat = Category.create(name: "Entertainment", user: user)
    patch '/expense_plans', xhr: true, as: :json, params: {
      id: allocation.id, category_id: new_cat.id
    }

    expect(response).to have_http_status(:forbidden)
    expect(allocation.category).to eq(category)
  end

  it "can't change allocation month" do
    patch '/expense_plans', xhr: true, as: :json, params: {
      id: allocation.id, month: Date.today + 2.months
    }

    expect(response).to have_http_status(:forbidden)
    expect(allocation.month).to eq(Date.today)
  end

  it "deleting own expense allocation is forbidden" do
    delete "/expense_plans/#{allocation.id}", xhr: true, as: :json
    expect(response).to have_http_status(:forbidden)
  end

  it "deleting others' expense allocation results in not_found" do
    delete "/expense_plans/#{other_allocation.id}", xhr: true, as: :json
    expect(response).to have_http_status(:not_found)
  end

  it "can't create expense allocation for other user's category" do
    post '/expense_plans', xhr: true, as: :json, params: {
      category_id: other_category.id, amount: 650,  month: Date.today + 2.months
    }

    expect(response).to have_http_status(:not_found)
  end

  it "can't edit other's expense allocation" do
    patch '/expense_plans', xhr: true, as: :json, params: {
      id: other_allocation.id, amount: 12000
    }

    expect(response).to have_http_status(:not_found)
    expect(other_allocation.amount).to eq(7000)
  end
end
