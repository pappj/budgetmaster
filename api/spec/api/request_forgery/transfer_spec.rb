require 'rails_helper'

RSpec.describe "Transfer request forging", type: :request do
  include_context "user login"
  include_context "models"

  before(:all) do
    ApplicationController.allow_forgery_protection = false
  end

  before(:each) do
    api_login_user

    acc1 = Account.create(name: "Acc1", account_type: "bank",
                          current_balance: 150000, credit: 0,
                          user: other_user, currency: huf)
    acc2 = Account.create(name: "Acc2", account_type: "cash",
                          current_balance: 5000, credit: 0,
                          user: other_user, currency: huf)
    Transfer.create(source_account: acc1, source_amount: 10000,
                    target_account: acc2, target_amount: 10000,
                    date: Date.today)
  end

  after(:all) do
    ApplicationController.allow_forgery_protection = true
  end

  def get_id
    Transfer.first.id
  end

  def get_acc_ids
    a1 = Account.find_by(name: "Acc1")
    a2 = Account.find_by(name: "Acc2")
    [a1, a2]
  end

  it "can't create transfer for other's account" do
    (a1, a2) = get_acc_ids
    post '/transfers', xhr: true, as: :json, params: {transfer: {
      source_account_id: a1, source_amount: 6000, target_account_id: a2,
      target_amount: 6000, date: Date.today}}

    expect(response).to have_http_status(:not_found)
  end

  it "can't transfer money from other's account" do
    (a1, a2) = get_acc_ids
    c = Currency.find_by(short_code: "HUF")
    my_acc = Account.create(name: "My acc", account_type: "bank",
                            current_balance: 500, credit: 0,
                            user: user, currency: c)
    post '/transfers', xhr: true, as: :json, params: {transfer: {
      source_account_id: a1, source_amount: 6000, target_account_id: my_acc.id,
      target_amount: 6000, date: Date.today}}

    expect(response).to have_http_status(:not_found)
    my_acc.destroy
  end

  it "can't edit other's transfers" do
    patch '/transfers', xhr: true, as: :json, params: {transfer: {
      id: get_id, source_amount: 8000, target_amount: 8000}}

    expect(response).to have_http_status(:not_found)
    transfer = Transfer.first
    expect(transfer.source_amount).to eq(10000)
    expect(transfer.target_amount).to eq(10000)
  end

  it "can't delete other's transfers" do
    delete "/transfers/#{get_id}", xhr: true, as: :json

    expect(response).to have_http_status(:not_found)
    expect(get_id).not_to be(nil)
  end

  it "can't delete if the target account has low amount" do
    acc1 = Account.create(name: "Credit", account_type: "credit", currency: huf,
                          current_balance: 13600, credit: 20000, user: user)
    acc2 = Account.create(name: "Cash", account_type: "cash", currency: huf,
                          current_balance: 3400, credit: 0, user: user)
    tr = Transfer.create(source_account: acc1, target_account: acc2,
                         source_amount: 6000, target_amount: 6000,
                         date: Date.today)

    delete "/transfers/#{tr.id}", xhr: true, as: :json, params: {update_account: true}
    expect(response).to have_http_status(:forbidden)
    tr.destroy
    acc1.destroy
    acc2.destroy
  end
end
