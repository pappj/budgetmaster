require 'rails_helper'

module Helpers
  def select_date(node, date_to_select)
    year = date_to_select.year
    month = date_to_select.month
    day = date_to_select.day

    if node.class.name == "String"
      find("##{node}").click
    else
      node.click
    end

    select_month(year, month)

    day_div = find(:xpath, "//div[not(contains(@class, 'outside-month')) and"\
                                  " text()=\"#{day}\"]")
    raise "Can't select date" if day_div[:class].include? "disabled"
    day_div.click
  end

  def select_month(year, month)
    current_month = find(".react-datepicker__current-month").text.split
    c_month = Date::MONTHNAMES.index(current_month[0])
    c_year = current_month[1].to_i
    if((c_year == year && c_month < month) || c_year < year)
      click_on("Next Month")
      select_month(year, month)
    elsif((c_year == year && c_month > month) || c_year > year)
      click_on("Previous Month")
      select_month(year, month)
    end
  rescue Capybara::ElementNotFound
    raise "Can't select date"
  end

  def panel_header(panel_name, column_num)
    page.find(:xpath, "//div[@id='#{panel_name}']//"\
              "h2[contains(@class, 'panel-header')][#{column_num}]").text
  end

  def wait_for_action_panel_to_close
    expect(page).to have_no_selector("#action-panel")
  end

  def wait_for_page_to_load
    expect(page).to have_no_content("Loading")
  end

  def validate_money(field)
    expect(field.currency).to eq('$')
    expect(field.currency_position).to eq('after')
  end

  def validate_date(field, date)
    expect(field).to eq(date.strftime("%m/%d/%Y"))
  end

  def validate_due_amount(category, amount)
    Page::Planning.find_category(category).edit do |plan|
      expect(plan.due_amount.amount).to eq(amount)
      plan.cancel
    end
  end

  def build_transaction_xpath(filter)
    query = ""
    if(filter.has_key?(:date))
      query += "span[1][text()='#{filter[:date].strftime("%Y-%m-%d")}']"
    end

    if(filter.has_key?(:account))
      add_query!(query, "span[2][text()='#{filter[:account]}']")
    end

    if(filter.has_key?(:category))
      add_query!(query, "span[2][text()='#{filter[:category]}']")
    end

    if(filter.has_key?(:amount))
      add_query!(query, "div[1]/span[@class='amount' and "\
                        "text()='#{filter[:amount]}']")
    end

    if(filter.has_key?(:amount2))
      add_query!(query, "div[2]/span[@class='amount' and "\
                        "text()='#{filter[:amount2]}']")
    end

    if(filter.has_key?(:account2))
      add_query!(query, "span[4][text()='#{filter[:account2]}']")
    end

    "//div[@id='transactions']//li[#{query}]"
  end

  def add_query!(base_query, addition)
    base_query << (base_query.empty? ? "" : " and ") + addition
  end
end
