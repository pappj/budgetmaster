module Page::Planning
  class Statusbar
    extend Capybara::DSL

    def initialize(node)
      @node = node
    end

    def message
      find_message.text
    end

    def message_success?
      find_message[:class].include?("success")
    end

    def message_warning?
      find_message[:class].include?("warning")
    end

    def money
      Page::Elements::Money.new(@node.find(:xpath,
        "./div[contains(@class, 'money')]"))
    end

    def self.find
      self.new(page.find(:xpath, "//div[@id='status']"))
    end

    private
      def find_message
        @node.find(:xpath, "./span")
      end
  end
end
