module Page::Planning
  class Category
    extend Capybara::DSL

    def initialize(node)
      @node = node
    end

    def warning?
      @node[:class].include?("warning")
    end

    def allocated
      Page::Elements::Money.new(@node.find(:xpath, "./div[1]"))
    end

    def edit
      @node.find(:xpath, ".//button[@title='Edit']").click
      yield Page::Forms::CategoryAllocation.new if block_given?
    end
  end
end
