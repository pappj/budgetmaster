module Page::Planning
  class Goal
    extend Capybara::DSL

    def initialize(node)
      @node = node
    end

    def title_warning?
      @node.find(:xpath, "./span")[:class].include?("warning")
    end

    def allocated_amount
      Page::Elements::Money.new(@node.find(:xpath,
        ".//span[text()='Allocated']/following-sibling::div[1][@class='money']"))
    end

    def allocated_amount_warning?
      @node.find(:xpath, ".//span[text()='Allocated']")[:class].include?("warning")
    end

    def on_track_amount_label
      @node.find(:xpath, ".//span[text()='To be on track']/"\
                 "following-sibling::span[1]").text
    end

    def on_track_warning?
      @node.find(:xpath, ".//span[text()='To be on track']/"\
                 "following-sibling::span[1]")[:class].include?("warning")
    end

    def on_track_amount
      Page::Elements::Money.new(@node.find(:xpath,
        ".//span[text()='To be on track']/"\
        "following-sibling::div[1][@class='money']"))
    end

    def target_amount
      Page::Elements::Money.new(@node.find(:xpath,
        ".//span[contains(text(),'Target amount')]/"\
        "following-sibling::div[1][@class='money']"))
    rescue Capybara::ElementNotFound
      @node.find(:xpath, ".//span[contains(text(),'Target amount')]/"\
                 "following-sibling::span[1]").text
    end

    def target_amount_label
      @node.find(:xpath, ".//span[contains(text(),'Target amount')]").text
    end

    def target_date
      @node.find(:xpath, ".//span[contains(text(),'Target date')]/"\
                 "following-sibling::span[1]").text
    end

    def target_date_label
      @node.find(:xpath, ".//span[contains(text(),'Target date')]").text
    end

    def target_date_warning?
      @node.find(:xpath, ".//span[contains(text(),'Target date')]")[:class]
        .include?("warning")
    end

    def edit_allocation
      @node.find(:xpath, ".//button[@title='edit-allocation']").click
      yield Page::Forms::GoalAllocation.new if block_given?
    end

    def edit_plan
      @node.find(:xpath, ".//button[@title='edit-plan']").click
      yield Page::Forms::GoalPlan.new if block_given?
    end
  end
end
