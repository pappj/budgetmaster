module Page
  module Profile
    extend Capybara::DSL
    extend Helpers
    extend RSpec::Matchers

    def self.visit
      page.visit "/profile"
      wait_for_page_to_load
    end

    def self.main_currency
      find(:xpath, "//span[text()='Main currency']/following-sibling::span[1]")
        .text
    end

    def self.currency_type
      find(:xpath, "//span[text()='Currency type']/following-sibling::span[1]")
        .text
    end

    def self.currency_position
      find(:xpath, "//span[text()='Currency position']/following-sibling"\
                   "::span[1]").text
    end

    def self.date_format
      find(:xpath, "//span[text()='Date format']/following-sibling::span[1]")
        .text
    end

    def self.subscription_type
      find(:xpath, "//span[text()='Type']/following-sibling::span[1]")
        .text
    end

    def self.subscription_expiry_date
      find(:xpath, "//span[text()='Valid until']/following-sibling::span[1]")
        .text
    end

    def self.edit
      find(:xpath, "//button[@id='edit-profile-btn']").click
      yield Page::Forms::Profile.new(find(:xpath, "//form"))
    end

    def self.cancel
      find(:xpath, "//button[@id='edit-profile-btn']").click
    end
  end
end
