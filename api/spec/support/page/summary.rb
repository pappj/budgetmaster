module Page
  module Summary
    extend Capybara::DSL
    extend Helpers
    extend RSpec::Matchers

    def self.visit
      page.visit "/summary"
      wait_for_page_to_load
    end

    def self.find_account(name)
      Page::Account.new(find(:xpath, "//div[@id='accounts']//li"\
        "/span[text()='#{name}']/.."))
    end

    def self.new_income
      click_button("new-income")
      yield Page::Forms::Income.new if block_given?
    end

    def self.new_expense
      find("#new-expense").click
      yield Page::Forms::Expense.new if block_given?
    end

    def self.new_transfer
      click_button("new-transfer")
      yield Page::Forms::Transfer.new if block_given?
    end
  end
end
