module Page::Transactions
  class DeletePanel
    def initialize
      @node = Page::Elements::ActionPanel.node
    end

    def update_account
      Page::Elements::Checkbox.new(@node.find(:xpath, "./div[@class='check-line']"\
        "/button"))
    end

    def yes
      click_button("Yes")
    end

    def no
      click_button("No")
    end

    def cancel
      Page::Elements::ActionPanel.close
    end

    private
      def click_button(label)
        @node.find(:xpath, ".//button[text()='#{label}']").click
      end
  end
end
