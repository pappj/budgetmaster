module Page::Transactions
  class Transaction
    def initialize(node)
      @node = node
    end

    def expanded?
      @node.find(:xpath, ".//button[@title='hide-details']")
      true
    rescue Capybara::ElementNotFound
      false
    end

    def simple
      @node.find(:xpath, ".//button[@title='hide-details']").click
      self
    rescue Capybara::ElementNotFound
      self
    end

    def detailed
      @node.find(:xpath, ".//button[@title='show-details']").click
      self
    rescue Capybara::ElementNotFound
      self
    end

    def type
      detailed
      @node.find(:xpath, "./span[2]").text
    end

    def edit
      detailed
      @node.find(:xpath, ".//button[@title='Edit']").click

      yield form if block_given?
    end

    def delete
      detailed
      @node.find(:xpath, ".//button[@title='Delete']").click

      yield Page::Transactions::DeletePanel.new if block_given?
    end

    def date
      @node.find(:xpath, "./span[1]").text
    end

    def amount
      Page::Elements::Money.new(@node.find(:xpath, "./div[1]"))
    end

    protected
      def form
        #For children to overwrite
      end
  end
end
