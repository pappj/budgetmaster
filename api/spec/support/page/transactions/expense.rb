require 'support/page/transactions/transaction.rb'

module Page::Transactions
  class Expense < Transaction
    def category
      if expanded?
        @node.find(:xpath, "./span[5]").text
      else
        @node.find(:xpath, "./span[2]").text
      end
    end

    def account
      detailed
      @node.find(:xpath, "./span[8]").text
    end

    def form
      return Page::Forms::Expense.new
    end
  end
end
