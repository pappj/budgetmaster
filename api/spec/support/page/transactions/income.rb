require 'support/page/transactions/transaction.rb'

module Page::Transactions
  class Income < Transaction
    def account
      if expanded?
        @node.find(:xpath, "./span[5]").text
      else
        @node.find(:xpath, "./span[2]").text
      end
    end

    def form
      return Page::Forms::Income.new
    end
  end
end
