require 'support/page/transactions/transaction.rb'

module Page::Transactions
  class Transfer < Transaction
    def source_account
      if expanded?
        @node.find(:xpath, "./span[5]").text
      else
        @node.find(:xpath, "./span[2]").text
      end
    end

    def target_account
      if expanded?
        @node.find(:xpath, "./span[8]").text
      else
        @node.find(:xpath, "./span[4]").text
      end
    end

    def source_amount
      amount
    end

    def target_amount
      Page::Elements::Money.new(@node.find(:xpath, "./div[2]"))
    end

    def form
      return Page::Forms::Transfer.new
    end
  end
end
