module Page::Transactions
  class DateRange
    extend Capybara::DSL

    def initialize(node)
      @node = node
    end

    def start_date
      @node.find(:xpath, ".//span[@id='start-date']")
    end

    def end_date
      @node.find(:xpath, ".//span[@id='end-date']")
    end

    def self.edit
      find(:xpath, "//button[@id='dates']").click
      yield Form.new(find(:xpath, "//div[@id='action-panel']")) if block_given?
    end

    class Form
      def initialize(node)
        @node = node
      end

      def start_date
        @node.find(:xpath, ".//input[@id='startDate']")
      end

      def end_date
        @node.find(:xpath, ".//input[@id='endDate']")
      end

      def apply
        @node.find(:xpath, ".//button[@type='submit']").click
      end
    end
  end
end
