class TransfersFilter
  def initialize(node)
    @node = node
  end

  def account(name)
    Page::Elements::Checkbox.new(@node.find(:xpath,
      ".//span[@class='filter-item-name' and text()='#{name}']/"\
      "preceding-sibling::button"))
  end
end
