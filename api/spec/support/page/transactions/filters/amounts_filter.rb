class AmountsFilter
  def initialize(node)
    @node = node
  end

  def from
    @node.find(:xpath, ".//input[@name='from']")
  end

  def to
    @node.find(:xpath, ".//input[@name='to']")
  end
end
