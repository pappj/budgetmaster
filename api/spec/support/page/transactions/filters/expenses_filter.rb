class ExpensesFilter
  def initialize(node)
    @node = node
  end

  def category(name)
    Page::Elements::Checkbox.new(@node.find(:xpath,
      ".//span[@class='filter-item-name' and text()='#{name}']/"\
      "preceding-sibling::button"))
  end
end
