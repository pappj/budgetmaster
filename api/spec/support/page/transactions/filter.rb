module Page::Transactions
  class Filter
    def initialize(node)
      @node = node
    end

    def incomes
      Page::Elements::Checkbox.new(@node.find(:xpath, ".//span[text()='Incomes']/"\
        "preceding-sibling::button"))
    end

    def incomes_filter
      yield IncomesFilter.new(@node.find(:xpath, ".//div[@id='income-filter']//ul")) if block_given?
    rescue Capybara::ElementNotFound
      @node.find(:xpath, ".//div[@id='income-filter']//"\
                         "button[@title='show-details']").click
      retry
    end

    def expenses
      Page::Elements::Checkbox.new(@node.find(:xpath, ".//span[text()='Expenses']/"\
        "preceding-sibling::button"))
    end

    def expenses_filter
      yield ExpensesFilter.new(@node.find(:xpath, ".//div[@id='expense-filter']//ul")) if block_given?
    rescue Capybara::ElementNotFound
      @node.find(:xpath, ".//div[@id='expense-filter']//"\
                         "button[@title='show-details']").click
      retry
    end

    def transfers
      Page::Elements::Checkbox.new(@node.find(:xpath, ".//span[text()='Transfers']/"\
        "preceding-sibling::button"))
    end

    def source_accounts_filter
      yield TransfersFilter.new(@node.find(:xpath, ".//ul[@id='from-filter']")) if block_given?
    rescue Capybara::ElementNotFound
      @node.find(:xpath, ".//div[@id='transfer-filter']//"\
                         "button[@title='show-details']").click
      retry
    end

    def target_accounts_filter
      yield TransfersFilter.new(@node.find(:xpath, ".//ul[@id='to-filter']")) if block_given?
    rescue Capybara::ElementNotFound
      @node.find(:xpath, ".//div[@id='transfer-filter']//"\
                         "button[@title='show-details']").click
      retry
    end

    def amounts
      Page::Elements::Checkbox.new(@node.find(:xpath,
        ".//span[text()='All amounts']/preceding-sibling::button"))
    end

    def amounts_filter
      yield AmountsFilter.new(@node.find(:xpath, ".//div[@id='amount-filter']/"\
        "div[@class='filter-items']")) if block_given?
    end

    def apply
      @node.find(:xpath, ".//button[text()='Apply']").click
    end
  end
end
