module Page
  module Planning
    extend Capybara::DSL
    extend Helpers
    extend RSpec::Matchers

    def self.visit
      page.visit('/planning')
      wait_for_page_to_load
    end

    def self.find_goal(title)
      path = "//div[@id='goals']//li/span[text() = '#{title}']/.."
      begin
        find(:xpath, "#{path}/button[@title='hide-details']")
      rescue Capybara::ElementNotFound
        find(:xpath, "#{path}/button[@title='show-details']").click
      end

      Page::Planning::Goal.new(find(:xpath, path))
    end

    def self.next_month
      find(:xpath, "//div[@id='month-selector']/button[2]").click
    end

    def self.previous_month_disabled?
      find(:xpath, "//div[@id='month-selector']/button[1]").disabled?
    end

    def self.find_category(name)
      Page::Planning::Category.new(find(:xpath,
        "//div[@id='expenses']//li/span[text()='#{name}']/.."))
    end
  end
end
