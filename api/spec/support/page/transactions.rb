module Page
  module Transactions
    extend Capybara::DSL
    extend Helpers
    extend RSpec::Matchers

    def self.visit
      page.visit('/transactions')
      wait_for_page_to_load
    end

    def self.filter
      page.find("#filter").click
      node = page.find("#action-panel")
      yield Filter.new(node) if block_given?
    end

    def self.date_range
      node = page.find("#transaction-range")
      DateRange.new(node)
    end

    def self.find_by(filter, klass = Page::Transactions::Transaction)
      raise "No parameters given" if filter.empty?
      path = build_transaction_xpath(filter)

      klass.new(page.find(:xpath, path))
    end

    def self.toggle_all_details
      page.find(:xpath, "//button[@id='all-details']").click
    end

    def self.all_details_active?
      page.find(:xpath, "//button[@id='all-details']")['class']
        .include?('btn-active')
    end

    def self.all
      nodes = page.find_all(:xpath, "//div[@id='transactions']//li")
      nodes.map{|n| Page::Transactions::Transaction.new(n)}
    end

    def self.count
      page.find_all(:xpath, ".//div[@id='transactions']//li").count
    end
  end
end
