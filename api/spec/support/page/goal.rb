module Page
  class Goal
    extend Capybara::DSL

    def initialize(node = nil)
      @node = node
    end

    def title
      @node.find(:xpath, "./span[1]").text
    end

    def target
      Page::Elements::Money.new(@node.find(:xpath, "./div[@class='money']"))
    rescue Capybara::ElementNotFound
      @node.find(:xpath, "./span[2]").text
    end

    def achieved?
      @node.find(:xpath, "./span[1]/*[@class='text-icon']")
      true
    rescue Capybara::ElementNotFound
      false
    end

    def self.count
      all(:xpath, "//div[@id='goals']//li").length
    end

    def self.first
      self.find_nth(1)
    end

    def self.find_nth(index)
      self.new(find(:xpath, "//div[@id='goals']//li[#{index}]"))
    end

    def self.find_by_title(title)
      self.new(find(:xpath,"//div[@id='goals']//li/span[text()='#{title}']/.."))
    end

    def self.get_goal_titles
      find_all(:xpath, "//div[@id='goals']/div/ul/li/span[1]").map(&:text)
    end

    def self.panel
      find("#goals.panel")
    end

    def self.new_goal
      click_button("new-goal")
      yield Form.new(find(:xpath, "//div[@id='action-panel']")) if block_given?
    end

    def edit
      @node.find(:xpath, "./button[@title='Edit']").click
      yield Form.new(self.class.find(:xpath, "//div[@id='action-panel']"))
    end

    def delete(btn_to_click = "Yes")
      @node.find(:xpath, "./button[@title='Delete']").click
      self.class.find(:xpath, "//div[@id='action-panel']//"\
                      "button[text()='#{btn_to_click}']").click
    end

    class Form
      def initialize(node = nil)
        @node = node
      end

      def title
        @node.find(:xpath, ".//input[@id='title']")
      end

      def setTargetAmount
        Page::Elements::Checkbox.new(@node.find(:xpath,
          ".//button[@id='setTargetAmount']"))
      end

      def target_amount
        Page::Elements::MoneyField.new(@node.find(:xpath,
          ".//input[@id='target_amount']/.."))
      end

      def setTargetDate
        Page::Elements::Checkbox.new(@node.find(:xpath,
          ".//button[@id='setTargetDate']"))
      end

      def target_date
        @node.find(:xpath, ".//input[@id='target_date']")
      end

      def achieved
        Page::Elements::Checkbox.new(@node.find(:xpath,
          ".//button[@id='achieved']"))
      end

      def submit
        @node.find(:xpath, ".//button[@type='submit']").click
      end

      def cancel
        Page::Elements::ActionPanel.close
      end
    end
  end
end
