module Page
  class Category
    extend Capybara::DSL

    def initialize(node)
      @node = node
    end

    def name
      @node.find(:xpath, "./span[1]").text
    end

    def spent(currency)
      Page::Elements::Money.new(@node.find( :xpath, "(.//span[text()='#{currency}'])[1]/.."))
    end

    def planned
      Page::Elements::Money.new(extra_column)
    end

    def due
      Page::Elements::Money.new(extra_column)
    end

    def extra_column(row = 1)
      @node.find(:xpath, ".//li[#{row}]/div[2]")
    end

    def self.count
      all(:xpath, "//div[@id='expenses']//li").length
    end

    def self.first
      self.find_nth(1)
    end

    def self.find_nth(index)
      self.new(find(:xpath, "//div[@id='expenses']//li[#{index}]"))
    end

    def self.find_by_name(name)
      self.new(find(:xpath,"//div[@id='expenses']//li/span[text()='#{name}']/.."))
    end

    def self.new_category
      click_button("new-category")
      yield Form.new(find(:xpath, "//div[@id='action-panel']"))
    end

    def edit
      @node.find(:xpath, "./button[@title='Edit']").click
      yield Form.new(self.class.find(
        :xpath, "//div[@id='action-panel']")) if block_given?
    end

    def delete(btn_to_click = "Yes")
      @node.find(:xpath, "./button[@title='Delete']").click
      self.class.find(:xpath, "//div[@id='action-panel']//"\
                      "button[text()='#{btn_to_click}']").click
    end

    class RecurringExpenseForm
      attr_reader :node

      def initialize(node)
        @node = node
      end

      def amount
        @node.find(:xpath, ".//input[contains(@name, 'amount')]")
      end

      def frequency
        @node.find(:xpath, ".//select[contains(@name, 'frequency')]")
      end

      def date
        @node.find(:xpath, ".//input[contains(@id, 'due_date')]")
      end

      def delete
        @node.find(:xpath, ".//button[@name='delete']").click
      end
    end

    class Form
      attr_reader :node

      def initialize(node)
        @node = node
      end

      def name
        @node.find(:xpath, ".//input[@id='name']")
      end

      def recurring_expenses
        @node.all(:xpath, ".//li[contains(@id, 'rec_exp')]")
          .map{|node| RecurringExpenseForm.new(node)}
      end

      def new_recurring_expense
        @node.find(:xpath, ".//button[text()='New Recurring Expense']").click
        yield recurring_expenses.last if block_given?
      end

      def submit
        @node.find(:xpath, ".//button[@type='submit']").click
      end

      def cancel
        Page::Elements::ActionPanel.close
      end
    end
  end
end
