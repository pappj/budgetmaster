module Page::Elements
  class NotifPanel
    extend Capybara::DSL

    def self.get_item(name)
      page.find(:xpath, "//div[@id='notif-panel']//"\
                        "em[contains(text(),'#{name}')]/..")
        .text
    end

    def self.empty?
      page.find("#notif-panel")
      false
    rescue Capybara::ElementNotFound
      true
    end
  end
end
