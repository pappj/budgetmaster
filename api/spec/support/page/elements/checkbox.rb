module Page::Elements
  class Checkbox
    def initialize(node)
      @node = node
    end

    def value
      @node[:class].include?("btn-checked")
    end

    def toggle
      @node.click
    end

    def disabled?
      @node.disabled?
    end
  end
end
