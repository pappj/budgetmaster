module Page::Elements
  class MoneyField < Money
    def set(value)
      amount_node.set(value)
    end

    def disabled?
      amount_node.disabled?
    end

    def amount
      amount_node.value
    end

    private
      def amount_node
        @node.find(:xpath, ".//input")
      end
  end
end
