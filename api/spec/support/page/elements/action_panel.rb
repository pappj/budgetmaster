module Page::Elements
  class ActionPanel
    extend Capybara::DSL
    extend RSpec::Matchers

    def self.node
      find(:xpath, "//div[@id='action-panel']")
    end

    def self.value(name, type)
      case type
      when :input
        find_input(name).value
      when :select
        find_select(name).value
      when :checkbox
        box = find(:xpath,
                   "//div[@id='action-panel']//div[contains(@class, checkbox)]/"\
                   "button[@id='#{name}']")
        box[:class].include?("btn-checked")
      end
    end

    def self.disabled?(name, type)
      case type
      when :input
        find_input(name).disabled?
      when :select
        find_select(name).disabled?
      when :checkbox
        find(:xpath, "//div[@id='action-panel']//div[contains(@class, checkbox)]/"\
             "button[@id='#{name}']").disabled?
      end
    end

    def self.money_field(name)
      Page::Elements::Money.new(find(:xpath, "//div[@id='action-panel']//"\
        "input[@name='#{name}']/.."))
    end

    def self.generic_error
      find(:xpath, "//div[@id='action-panel']//"\
         "span[@id='generic-form-error']").text
    end

    def self.error(name)
      find(:xpath, "//div[@id='action-panel']//div[@class='field-error' and"\
       " .//*[@id='#{name}']]/span[@class='error']").text
    end

    def self.label(name)
      find(:xpath, "//div[@id='action-panel']//label[@for='#{name}']").text
    end

    def self.close
      find(:xpath, "//div[@id='action-panel']//button[@id='close-btn']").click
    end

    private
      def self.find_input(name)
        find(:xpath, "//div[@id='action-panel']//input[@id='#{name}']")
      end

      def self.find_select(name)
        find(:xpath, "//div[@id='action-panel']//select[@id='#{name}']")
      end
  end
end
