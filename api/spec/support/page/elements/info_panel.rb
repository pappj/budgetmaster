module Page::Elements
  class InfoPanel
    extend Capybara::DSL

    def self.message
      find(:xpath, "//div[@id='info-panel']").text
    end
  end
end
