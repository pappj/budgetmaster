module Page::Elements
  class Money
    def initialize(node)
      @node = node
    end

    def currency
      currency_node.text
    end

    def currency_position
      if(@node.find(:xpath, "./*[1]")['class'].include?('currency'))
        return "before"
      else
        return "after"
      end
    end

    def amount
      amount_node.text
    end

    def currency_node
      @node.find(:xpath, "./span[@class='currency']")
    end

    def amount_node
      @node.find(:xpath, "./span[@class='amount']")
    end
  end
end
