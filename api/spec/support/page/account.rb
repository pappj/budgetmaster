module Page
  class Account
    extend Capybara::DSL

    def initialize(node = nil)
      @node = node
    end

    def name
      @node.find(:xpath, "./span[1]").text
    end

    def balance
      Page::Elements::Money.new(@node.find(:xpath,
        ".//div[contains(@class, 'money')]"))
    end

    def self.count
      all(:xpath, "//div[@id='accounts']//li").length
    end

    def self.first
      self.find_nth(1)
    end

    def self.find_nth(index)
      self.new(find(:xpath, "//div[@id='accounts']//li[#{index}]"))
    end

    def self.find_by_name(name)
      self.new(find(:xpath,"//div[@id='accounts']//li/span[text()='#{name}']/.."))
    end

    def self.new_account
      click_button("new-account")
      yield Form.new(find(:xpath, "//div[@id='action-panel']"))
    end

    def edit
      @node.find(:xpath, "./button[@title='Edit']").click
      yield Form.new(self.class.find(:xpath, "//div[@id='action-panel']"))
    end

    class Form
      def initialize(node)
        @node = node
      end

      def name
        @node.find(:xpath, ".//input[@id='name']")
      end

      def account_type
        @node.find(:xpath, ".//select[@id='account_type']")
      end

      def currency
        @node.find(:xpath, ".//select[@id='currency_id']")
      end

      def current_balance
        Page::Elements::MoneyField.new(@node.find(:xpath,
          ".//input[@id='current_balance']/.."))
      end

      def credit
        Page::Elements::MoneyField.new(@node.find(:xpath,
          ".//input[@id='credit']/.."))
      end

      def in_plan
        Page::Elements::Checkbox.new(@node.find(:xpath,
          ".//button[@id='in_plan']"))
      rescue Capybara::ElementNotFound
        @node.find(:xpath, ".//p[@id='in_plan']").text
      end

      def submit
        @node.find(:xpath, ".//button[@type='submit']").click
      end

      def cancel
        Page::Elements::ActionPanel.close
      end
    end
  end
end
