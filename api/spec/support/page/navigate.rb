module Page
  module Navigate
    extend Capybara::DSL

    def self.to_summary
      find(:xpath, "//a[@href='/summary']").click
    end

    def self.to_basics
      menu.find(:xpath, "//a[@href='/basics']").click
    end

    def self.to_transactions
      menu.find(:xpath, ".//a[@href='/transactions']").click
    end

    def self.to_planning
      menu.find(:xpath, "//a[@href='/planning']").click
    end

    private
      def self.menu
        find(:xpath, "//ul[contains(@class, 'menu')]")
      rescue Capybara::ElementNotFound
        find(:xpath, "//button[@id='toggle-menu']").click
        find(:xpath, "//ul[contains(@class, 'menu')]")
      end
  end
end
