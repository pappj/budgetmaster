module Page::Forms
  class Profile
    def initialize(node)
      @node = node
    end

    def main_currency
      @node.find(:xpath, ".//select[@id='currency_id']")
    end

    def currency_type
      @node.find(:xpath, ".//select[@id='currency_string']")
    end

    def currency_position
      @node.find(:xpath, ".//select[@id='currency_position']")
    end

    def currency_example
      @node.find(:xpath, ".//p[@id='currencyExample']").text
    end

    def date_format
      @node.find(:xpath, ".//select[@id='date_format']")
    end

    def date_example
      @node.find(:xpath, ".//p[@id='dateExample']").text
    end

    def submit
      @node.find(:xpath, "./button[@type='submit']").click
    end
  end
end
