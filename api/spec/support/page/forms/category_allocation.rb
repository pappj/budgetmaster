module Page::Forms
  class CategoryAllocation
    def initialize(node = Page::Elements::ActionPanel.node)
      @node = node
    end

    def allocation
      Page::Elements::MoneyField.new(@node.find(:xpath, ".//input[@id='amount']/.."))
    end

    def month
      @node.find(:xpath, ".//p[@name='month']").text
    end

    def due_amount
      Page::Elements::Money.new(@node.find(:xpath, ".//label[@for='dueAmount']/"\
                                         "following-sibling::div[1]"))
    end

    def spent_amount
      Page::Elements::Money.new(@node.find(:xpath, ".//label[@for='spentAmount']/"\
                                         "following-sibling::div[1]"))
    end

    def max_allocation
      Page::Elements::Money.new(@node.find(:xpath, ".//label[@for='maxAllocation']/"\
                                         "following-sibling::div[1]"))
    end

    def submit
      @node.find(:xpath, ".//button[@type='submit']").click
    end

    def cancel
      Page::Elements::ActionPanel.close
    end
  end
end
