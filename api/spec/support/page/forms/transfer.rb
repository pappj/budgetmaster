module Page::Forms
  class Transfer
    def initialize(node = Page::Elements::ActionPanel.node)
      @node = node
    end

    def source_account
      @node.find(:xpath, ".//select[@id='source_account_id']")
    end

    def source_amount
      Page::Elements::MoneyField.new(@node.find(:xpath,
        ".//input[@id='source_amount']/.."))
    end

    def target_account
      @node.find(:xpath, ".//select[@id='target_account_id']")
    end

    def target_amount
      Page::Elements::MoneyField.new(@node.find(:xpath,
        ".//input[@id='target_amount']/.."))
    end

    def date
      @node.find(:xpath, ".//input[@id='date']")
    end

    def note
      @node.find(:xpath, ".//textarea[@id='note']")
    end

    def update_account
      Page::Elements::Checkbox.new(@node.find(:xpath,
        ".//button[@id='update_account']"))
    end

    def submit
      @node.find(:xpath, ".//button[@type='submit']").click
    end

    def cancel
      Page::Elements::ActionPanel.close
    end
  end
end
