module Page::Forms
  class GoalPlan
    def initialize(node = Page::Elements::ActionPanel.node)
      @node = node
    end

    def monthly_amount
      begin
        Page::Elements::MoneyField.new(@node.find(:xpath,
          ".//input[@id='monthly_amount']/.."))
      rescue Capybara::ElementNotFound
        Page::Elements::Money.new(@node.find(:xpath,
          ".//label[@for='monthly_amount']/"\
          "following-sibling::div[1][@class='money']"))
      end
    end

    def target_amount
      Page::Elements::Money.new(@node.find(:xpath,
          ".//label[@for='target_amount']/"\
          "following-sibling::div[1][@class='money']"))
    end

    def target_date
      @node.find(:xpath, ".//div[@class='input-group'][4]/span").text
    end

    def submit
      @node.find(:xpath, ".//button[@type='submit']").click
    end
  end
end
