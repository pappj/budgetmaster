module Page::Forms
  class Income
    def initialize(node = Page::Elements::ActionPanel.node)
      @node = node
    end

    def account
      @node.find(:xpath, ".//select[@id='account_id']")
    end

    def amount
      Page::Elements::MoneyField.new(@node.find(:xpath,
        ".//input[@id='amount']/.."))
    end

    def date
      @node.find(:xpath, ".//input[@id='date']/..")
    end

    def note
      @node.find(:xpath, ".//textarea[@id='note']")
    end

    def update_account
      Page::Elements::Checkbox.new(@node.find(:xpath,
        ".//button[@id='update_account']"))
    end

    def submit
      @node.find(:xpath, ".//button[@type='submit']").click
    end

    def cancel
      @node.find(:xpath, ".//button[@id='close-btn']").click
    end
  end
end
