module Page::Forms
  class GoalAllocation
    def initialize(node = Page::Elements::ActionPanel.node)
      @node = node
    end

    def allocated_amount
      Page::Elements::MoneyField.new(@node.find(:xpath,
        ".//input[@id='allocated_amount']/.."))
    end

    def max_allocation
      Page::Elements::Money.new(@node.find(:xpath,
        ".//label[@for='maxAllocation']/"\
        "following-sibling::div[1][@class='money']"))
    end

    def submit
      @node.find(:xpath, ".//button[@type='submit']").click
    end
  end
end
