require 'rails_helper'

RSpec.shared_context "user login" do
  # User records
  def user
    User.find_by(email: "john@doe.com")
  end

  def other_user
    User.find_by(email: "foo@bar.com")
  end

  def trial_user
    user
  end

  def free_user
    User.find_by(email: "free@user.com")
  end

  def basic_user
    User.find_by(email: "basic@user.com")
  end

  def premium_user
    User.find_by(email: "premium@user.com")
  end

  # Login methods for the front end
  def login(user)
    visit "/"
    within("#sign-in") do
      fill_in 'Email', with: user.email
      fill_in 'Password', with: "111111"
      click_button 'Sign in'
    end
    find("#toggle-menu")
  end

  def login_user
    login(user)
  end

  def login_other_user
    login(other_user)
  end

  def logout_user
    click_button("toggle-menu")
    within(".menu") do
      click_button("Sign out")
    end
    find("#home-content")
  end

  # Login methods for the api
  def api_login_user
    api_login(user)
  end

  def api_login(user)
    post '/users/sign_in', xhr: true, as: :json, params:
      {user: {email: user.email, password: "111111"}}
  end
end
