require 'rails_helper'

RSpec.shared_context "models" do
  def huf
    Currency.find_by(short_code: "HUF")
  end

  def usd
    Currency.find_by(short_code: "USD")
  end

  def eur
    Currency.find_by(short_code: "EUR")
  end
end
