require 'rails_helper'

RSpec.describe "Apply user settings to goal plans", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Bank", account_type: "bank", current_balance: 42600,
                   currency: usd, in_plan: true, user: other_user)
    g = Goal.create(title: "Vacation", target_amount: 1650, allocated_amount: 340,
                    user: other_user)
    g.create_goal_plan(monthly_amount: 330, target_date: Date.today + 5.months)

    Goal.create(title: "Renovation", target_amount: 4600,
                target_date: Date.today + 6.months, user: other_user)

    login_other_user
    Page::Planning.visit
  end

  context "when editing allocation" do
    it "shows allocated amount properly" do
      Page::Planning.find_goal("Vacation").edit_allocation do |alloc|
        validate_money(alloc.allocated_amount)
      end
    end

    it "shows max allocation properly" do
      Page::Planning.find_goal("Vacation").edit_allocation do |alloc|
        validate_money(alloc.max_allocation)
      end
    end
  end

  context "when editing plan" do
    it "shows monthly amount form field properly" do
      Page::Planning.find_goal("Vacation").edit_plan do |plan|
        validate_money(plan.monthly_amount)
      end
    end

    it "shows estimated monthly amount field properly" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        validate_money(plan.monthly_amount)
      end
    end

    it "shows target amount properly" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        validate_money(plan.target_amount)
      end
    end

    it "shows target date properly" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        validate_date(plan.target_date, Date.today + 6.months)
      end
    end
  end
end
