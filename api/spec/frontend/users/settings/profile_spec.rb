require 'rails_helper'

RSpec.describe "User profile", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    login_other_user
    Page::Profile.visit
  end

  def wait_form_to_disappear
    expect(page).to have_no_selector("form")
  end

  it "shows the user's settings properly" do
    expect(Page::Profile.main_currency).to eq("USD ($)")
    expect(Page::Profile.currency_type).to eq("symbol")
    expect(Page::Profile.currency_position).to eq("after the amount")
    expect(Page::Profile.date_format).to eq("MM/dd/yyyy")
  end

  it "shows currency example properly on editing" do
    Page::Profile.edit do |prof|
      expect(prof.currency_example).to eq("1500 $")
      prof.currency_position.select("Before the amount")
      expect(prof.currency_example).to eq("$ 1500")
      prof.currency_type.select("Short code")
      expect(prof.currency_example).to eq("USD 1500")
    end
  end

  it "shows date example properly on editing" do
    Page::Profile.edit do |prof|
      expect(prof.date_example).to eq(Date.today.strftime("%m/%d/%Y"))
      prof.date_format.select("yyyy.MM.dd.")
      expect(prof.date_example).to eq(Date.today.strftime("%Y.%m.%d."))
    end
  end

  it "updates the user profile on save" do
    Page::Profile.edit do |prof|
      prof.currency_type.select("Short code")
      prof.currency_position.select("Before the amount")
      prof.date_format.select("yyyy.MM.dd.")
      prof.submit
    end

    wait_form_to_disappear

    user = other_user
    expect(user.currency_string).to eq("short_code")
    expect(user.currency_position).to eq("before")
    expect(user.date_format).to eq("yyyy.MM.dd.")
  end

  it "doesn't change the user profile on cancel" do
    Page::Profile.edit do |prof|
      prof.currency_type.select("Short code")
      prof.currency_position.select("Before the amount")
      prof.date_format.select("yyyy.MM.dd.")
    end

    Page::Profile.cancel
    wait_form_to_disappear

    user = other_user
    expect(user.currency_string).to eq("symbol")
    expect(user.currency_position).to eq("after")
    expect(user.date_format).to eq("MM/dd/yyyy")
  end

  context "when plans includes accounts" do
    before(:each) do
      Account.create(name: "Plan 1", account_type: "cash", current_balance: 5000,
                     credit: 0, currency: usd, user: other_user, in_plan: true)
      Account.create(name: "Plan 2", account_type: "cash", current_balance: 7000,
                     credit: 0, currency: usd, user: other_user, in_plan: true)
      Account.create(name: "Cash", account_type: "cash", current_balance: 860,
                     credit: 0, currency: usd, user: other_user, in_plan: false)
    end

    it "leaves accounts in plan, when main currency is not changed" do
      Page::Profile.edit do |prof|
        prof.currency_position.select("Before the amount")
        prof.submit
      end

      wait_form_to_disappear

      expect(other_user.currency_position).to eq("before")
      acc_count = Account.where(user: other_user, in_plan: true).count
      expect(acc_count).to be(2)
    end

    it "removes accounts from the plan, if main currency is changed" do
      Page::Profile.edit do |prof|
        prof.main_currency.select("HUF (Ft)")
        prof.submit
      end

      wait_form_to_disappear

      expect(other_user.currency_id).to be(huf.id)
      acc_count = Account.where(user: other_user, in_plan: true).count
      expect(acc_count).to be(0)
    end

    it "shows no accounts for planning, if main currency is changed" do
      Page::Profile.edit do |prof|
        prof.main_currency.select("HUF (Ft)")
        prof.submit
      end

      wait_form_to_disappear
      Page::Planning.visit

      expect(Page::Planning::Statusbar.find.message)
        .to eq("No accounts included in your plan")
    end

    it "keeps the accounts loaded after editing" do
      Page::Profile.edit do |prof|
        prof.main_currency.select("HUF (Ft)")
        prof.submit
      end

      Page::Summary.visit

      # This will fail with Capybara::ElementNotFound if
      # it does not work. so no explicit expect is needed
      Page::Account.find_by_name("Cash")
    end
  end
end
