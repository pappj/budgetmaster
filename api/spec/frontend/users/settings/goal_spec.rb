require 'rails_helper'

RSpec.describe "Apply user settings to goals", js: true do
  include_context "user login"

  before(:each) do
    g1 = Goal.create(title: "Vacation", target_amount: 1650, allocated_amount: 340,
                     user: other_user)
    g1.create_goal_plan(monthly_amount: 330, target_date: Date.today + 5.months)
    g2 = Goal.create(title: "Renovation", target_date: Date.today + 6.months,
                     user: other_user)
    g2.create_goal_plan(monthly_amount: 450, target_amount: 2700)

    login_other_user
  end

  context "when visiting summary page" do
    before(:each) do
      visit "/summary"
      wait_for_page_to_load
    end

    it "shows target amount properly" do
      validate_money(Page::Goal.find_by_title("Vacation").target)
    end

    it "shows target date properly" do
      validate_date(Page::Goal.find_by_title("Renovation").target,
                 Date.today + 6.months)
    end
  end

  context "when visiting basics page" do
    before(:each) do
      visit "/basics"
      wait_for_page_to_load
    end

    it "shows target amount properly" do
      validate_money(Page::Goal.find_by_title("Vacation").target)
    end

    it "shows target date properly" do
      validate_date(Page::Goal.find_by_title("Renovation").target,
                 Date.today + 6.months)
    end

    it "shows form target amount properly" do
      Page::Goal.new_goal do |g|
        validate_money(g.target_amount)
      end
    end

    it "shows form target date properly" do
      Page::Goal.new_goal do |g|
        validate_date(g.target_date.value, Date.today)
      end
    end
  end

  context "when visiting planning page" do
    before(:each) do
      Page::Planning.visit
    end

    it "shows allocated amount properly" do
      validate_money(Page::Planning.find_goal("Vacation").allocated_amount)
    end

    it "shows target amount properly" do
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.target_amount_label).not_to include("plan")
      validate_money(goal.target_amount)
    end

    it "shows estimated target amount properly" do
      goal = Page::Planning.find_goal("Renovation")
      expect(goal.target_amount_label).to include("plan")
      validate_money(goal.target_amount)
    end

    it "shows target date properly" do
      goal = Page::Planning.find_goal("Renovation")
      expect(goal.target_date_label).not_to include("plan")
      validate_date(goal.target_date, Date.today + 6.months)
    end

    it "shows estimated target date properly" do
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.target_date_label).to include("plan")
      validate_date(goal.target_date, Date.today + 5.months)
    end
  end
end
