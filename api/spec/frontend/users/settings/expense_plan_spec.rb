require 'rails_helper'

RSpec.describe "Apply user settings to expense plans", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    c = Category.create(name: "Food", user: other_user)
    c.recurring_expenses.create(amount: 160, frequency: 'monthly',
                                due_date: Date.today)
    c.expense_plans.create(amount: 200, month: Date.today)

    a = Account.create(name: "Bank", account_type: 'bank', current_balance: 4300,
                       credit: 0, in_plan: true, currency: usd, user: other_user)
    Expense.create(amount: 36, account: a, category: c, date: Date.today)

    login_other_user
  end

  context "when visiting planning page" do
    before(:each) do
      Page::Planning.visit
    end

    it "shows the planned amount properly" do
      validate_money(Page::Planning.find_category("Food").allocated)
    end

    it "shows the allocation form amount properly" do
      Page::Planning.find_category("Food").edit do |plan|
        validate_money(plan.allocation)
      end
    end

    it "shows the due amount properly" do
      Page::Planning.find_category("Food").edit do |plan|
        validate_money(plan.due_amount)
      end
    end

    it "shows the spent amount properly" do
      Page::Planning.find_category("Food").edit do |plan|
        validate_money(plan.spent_amount)
      end
    end

    it "shows the max allocation properly" do
      Page::Planning.find_category("Food").edit do |plan|
        validate_money(plan.max_allocation)
      end
    end
  end

  context "when visiting summary page" do
    before(:each) do
      visit "/summary"
      wait_for_page_to_load
    end

    it "shows the expense plan properly" do
      validate_money(Page::Category.find_by_name("Food").planned)
    end
  end
end
