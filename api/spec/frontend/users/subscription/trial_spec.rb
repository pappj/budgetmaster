require 'rails_helper'
require 'frontend/users/subscription/generic_allowed.rb'

RSpec.describe "Trial subscription", js: true do
  include_context "user login"

  def test_user
    user
  end

  before(:each) do
    login(test_user)
  end

  include_examples "subscription type", "Free trial period"
  include_examples "has access to goals"
  include_examples "has access to recurring expenses"
  include_examples "account with planning"
  include_examples "has access to planning"

  it "has the correct expiry date" do
    Page::Profile.visit

    expiry = Date.today + 30.days
    expect(Page::Profile.subscription_expiry_date)
      .to eq(expiry.strftime("%Y-%m-%d"))
  end
end
