require 'rails_helper'
require 'frontend/users/subscription/generic_allowed.rb'

RSpec.describe "Premium subscription", js: true do
  include_context "user login"

  def test_user
    premium_user
  end

  before(:each) do
    login(test_user)
  end

  include_examples "subscription type", "Premium subscription"
  include_examples "has access to goals"
  include_examples "has access to recurring expenses"
  include_examples "account with planning"
  include_examples "has access to planning"
end
