RSpec.shared_examples "subscription type" do |type_label|
  it "shows subscription type properly" do
    Page::Profile.visit

    expect(Page::Profile.subscription_type).to eq(type_label)
  end
end

RSpec.shared_examples "account with planning" do
  it "can enable planning at account creation" do
    visit "/basics"
    wait_for_page_to_load

    Page::Account.new_account do |acc|
      acc.currency.select("HUF")
      acc.in_plan.toggle
      expect(acc.in_plan.value).to be(true)
    end
  end

  it "shows in plan field checked" do
    Account.create(name: "Cash", account_type: "cash", current_balance: 8000,
                   credit: 0, currency: test_user.currency, user: test_user,
                   in_plan: true)
    visit "/basics"
    wait_for_page_to_load

    Page::Account.find_by_name("Cash").edit do |acc|
      expect(acc.in_plan.value).to be(true)
    end
  end
end

RSpec.shared_examples "has access to goals" do
  it "can create a new goal" do
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.new_goal do |goal|
      goal.title.set("Fishing equipment")
      goal.target_amount.set(50000)
      goal.setTargetDate.toggle
      goal.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Goal.find_by_title("Fishing equipment").target.amount)
      .to eq("50000")
  end

  it "can edit goals" do
    Goal.create(title: "Vacation", target_amount: "150000", user: test_user)
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.find_by_title("Vacation").edit do |goal|
      goal.target_amount.set(100000)
      goal.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Goal.find_by_title("Vacation").target.amount).to eq("100000")
  end

  it "shows the goals on the summary page" do
    Goal.create(title: "Vacation", target_amount: "150000", user: test_user)
    visit "/summary"
    wait_for_page_to_load

    expect(Page::Goal.find_by_title("Vacation").target.amount).to eq("150000")
  end
end

RSpec.shared_examples "has access to recurring expenses" do
  it "can add new category with recurring expenses" do
    visit "/basics"
    wait_for_page_to_load

    Page::Category.new_category do |cat|
      cat.name.set("Bills")
      cat.new_recurring_expense do |exp|
        exp.amount.set(18500)
      end
      cat.submit
    end
    wait_for_action_panel_to_close

    # Expect to find the category with the new name
    expect{Page::Category.find_by_name("Bills")}.not_to raise_error
    expect(RecurringExpense.first.amount).to eq(18500)
  end

  it "can edit recurring expense" do
    c = Category.create(name: "Hobby", user: test_user)
    c.recurring_expenses.create(amount: 30000, frequency: "quarterly",
                                due_date: Date.today)

    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Hobby").edit do |cat|
      cat.recurring_expenses[0].frequency.select("Yearly")
      cat.submit
    end
    wait_for_action_panel_to_close

    expect(RecurringExpense.first.frequency).to eq("yearly")
  end

  it "can delete recurring expense" do
    c = Category.create(name: "Hobby", user: test_user)
    c.recurring_expenses.create(amount: 30000, frequency: "quarterly",
                                due_date: Date.today)

    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Hobby").edit do |cat|
      cat.recurring_expenses[0].delete
      cat.submit
    end
    wait_for_action_panel_to_close

    expect(Category.count).to be(1)
    expect(RecurringExpense.count).to be(0)
  end
end

RSpec.shared_examples "has access to planning" do
  it "can modify goal allocation" do
    Account.create(name: "For plan", account_type: "bank", current_balance: 400000,
                   credit: 0, currency: test_user.currency, user: test_user,
                   in_plan: true)
    Goal.create(title: "Vacation", user: test_user)
    Page::Planning.visit

    Page::Planning.find_goal("Vacation").edit_allocation do |alloc|
      alloc.allocated_amount.set(100000)
      alloc.submit
    end
    wait_for_action_panel_to_close

    expect(Page::Planning.find_goal("Vacation").allocated_amount.amount)
      .to eq("100000")
  end

  it "can create goal plan" do
    Goal.create(title: "Christmas", target_date: Date.today + 2.months,
                user: test_user, allocated_amount: 40000)
    Page::Planning.visit

    Page::Planning.find_goal("Christmas").edit_plan do |plan|
      plan.monthly_amount.set(5000)
      plan.submit
    end
    wait_for_action_panel_to_close

    expect(Page::Planning.find_goal("Christmas").on_track_amount.amount)
      .to eq("40000")
    expect(Page::Planning.find_goal("Christmas").target_amount.amount)
      .to eq("50000")
  end

  it "can modify goal plan" do
    g = Goal.create(title: "Christmas", target_amount: 80000,
                    target_date: Date.today + 2.months, user: test_user,
                    allocated_amount: 40000)
    g.create_goal_plan(monthly_amount: 6000)
    Page::Planning.visit

    goal = Page::Planning.find_goal("Christmas")
    expect(goal.on_track_amount.amount).to eq("68000")
    goal.edit_plan do |plan|
      plan.submit
    end
    wait_for_action_panel_to_close

    expect(Page::Planning.find_goal("Christmas").on_track_amount.amount)
      .to eq("40000")
  end

  it "can create expense plan" do
    Account.create(name: "For plan", account_type: "bank", current_balance: 400000,
                   credit: 0, currency: test_user.currency, user: test_user,
                   in_plan: true)
    Category.create(name: "Mortgage", user: test_user)
    Page::Planning.visit

    Page::Planning.find_category("Mortgage").edit do |cat|
      cat.allocation.set(93000)
      cat.submit
    end
    wait_for_action_panel_to_close

    expect(Page::Planning.find_category("Mortgage").allocated.amount)
      .to eq("93000")
  end

  it "can modify expense plan" do
    Account.create(name: "For plan", account_type: "bank", current_balance: 400000,
                   credit: 0, currency: test_user.currency, user: test_user,
                   in_plan: true)
    c = Category.create(name: "Mortgage", user: test_user)
    c.expense_plans.create(amount: 50000, month: Date.today)

    Page::Planning.visit

    expect(Page::Planning.find_category("Mortgage").allocated.amount)
      .to eq("50000")
    Page::Planning.find_category("Mortgage").edit do |cat|
      cat.allocation.set(93000)
      cat.submit
    end
    wait_for_action_panel_to_close

    expect(Page::Planning.find_category("Mortgage").allocated.amount)
      .to eq("93000")
  end

  it "shows Plan column on summary page" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 7600, frequency: "monthly",
                                due_date: Date.today)
    c.expense_plans.create(amount: 9700, month: Date.today)
    visit "/summary"
    wait_for_page_to_load

    expect(panel_header("expenses", 2)).to eq("Plan")
    expect(Page::Category.find_by_name("Food").planned.amount).to eq("9700")
  end
end
