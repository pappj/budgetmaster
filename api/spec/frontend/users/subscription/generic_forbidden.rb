RSpec.shared_examples "account without planning" do
  it "shows notice for in plan field at account creation" do
    visit "/basics"
    wait_for_page_to_load

    Page::Account.new_account do |acc|
      expect(acc.in_plan).to eq("Available in premium subscriptions")
    end
  end

  it "shows notice for in plan field at account editing" do
    Account.create(name: "Cash", account_type: "cash", current_balance: 8000,
                   credit: 0, currency: test_user.currency, user: test_user,
                   in_plan: true)
    visit "/basics"
    wait_for_page_to_load

    Page::Account.find_by_name("Cash").edit do |acc|
      expect(acc.in_plan).to eq("Available in premium subscriptions")
    end
  end
end

RSpec.shared_examples "can't access goals" do
  it "shows the notice when creating new goals" do
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.new_goal
    expect(page).to have_no_css("#action-panel")
    expect(Page::Elements::InfoPanel.message).to include("Goals are not available")
  end

  it "shows the notice instead of goals on the summary page" do
    Goal.create(title: "Vacation", target_amount: "150000", user: test_user)
    visit "/summary"
    wait_for_page_to_load

    expect{Page::Goal.find_by_title("Vacation")}
      .to raise_exception(Capybara::ElementNotFound)
    expect(Page::Goal.panel.find(".upgrade-notice").text)
      .to eq("Goals are available only in basic or premium subscriptions.")
  end

  it "shows the notice instead of goals on the basics page" do
    Goal.create(title: "Vacation", target_amount: "150000", user: test_user)
    visit "/basics"
    wait_for_page_to_load

    expect{Page::Goal.find_by_title("Vacation")}
      .to raise_exception(Capybara::ElementNotFound)
    expect(Page::Goal.panel.find(".upgrade-notice").text)
      .to eq("Goals are available only in basic or premium subscriptions.")
  end
end

RSpec.shared_examples "can't access recurring expenses" do
  it "shows the notice for new category" do
    visit "/basics"
    wait_for_page_to_load

    Page::Category.new_category do |cat|
      expect(cat.node).to have_no_content("New Recurring Expense")
      expect(cat.node).to have_content("Recurring Expenses are available only "\
        "in basic or premium subscriptions")
    end
  end

  it "shows the notice for editing a category" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 5000, frequency: "monthly",
                                due_date: Date.today)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Food").edit do |cat|
      expect(cat.node).to have_no_content("New Recurring Expense")
      expect(cat.node).to have_content("Recurring Expenses are available only "\
        "in basic or premium subscriptions")
    end
  end

  it "can add a category without recurring expenses" do
    visit "/basics"
    wait_for_page_to_load

    Page::Category.new_category do |cat|
      cat.name.set("Luxury cars")
      cat.submit
    end
    wait_for_action_panel_to_close

    # Expect to find the category with the new name
    expect{Page::Category.find_by_name("Luxury cars")}.not_to raise_error
  end

  it "can edit category name when there are recurring expenses" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 5000, frequency: "monthly",
                                due_date: Date.today)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Food").edit do |cat|
      cat.name.set("Entertainment")
      cat.submit
    end

    wait_for_action_panel_to_close

    # Expect to find the category with the new name
    expect{Page::Category.find_by_name("Entertainment")}.not_to raise_error
  end

  it "can delete category when there are recurring expenses" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 5000, frequency: "monthly",
                                due_date: Date.today)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Food").delete
    wait_for_action_panel_to_close

    expect{Page::Category.find_by_name("Food")}
      .to raise_exception(Capybara::ElementNotFound)
    expect(Category.count).to be(0)
    expect(RecurringExpense.count).to be(0)
  end
end

RSpec.shared_examples "can't access planning" do
  it "shows notice on the planning page" do
    visit "/planning"
    wait_for_page_to_load

    expect(page).to have_content("Goal and Expense plans are available only "\
      "in premium subscriptions.")
    expect(page).to have_no_css("#status")
    expect(page).to have_no_css("#goals")
    expect(page).to have_no_css("#month-selector")
    expect(page).to have_no_css("#expenses")
  end

  it "can delete category with expense plans" do
    c = Category.create(name: "Food", user: test_user)
    c.expense_plans.create(amount: 5000, month: Date.today)

    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Food").delete
    wait_for_action_panel_to_close

    expect{Page::Category.find_by_name("Food")}
      .to raise_exception(Capybara::ElementNotFound)
    expect(Category.count).to be(0)
    expect(ExpensePlan.count).to be(0)
  end
end
