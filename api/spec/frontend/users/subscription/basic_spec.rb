require 'rails_helper'
require 'frontend/users/subscription/generic_allowed.rb'
require 'frontend/users/subscription/generic_forbidden.rb'

RSpec.describe "Basic subscription", js: true do
  include_context "user login"

  def test_user
    basic_user
  end

  before(:each) do
    login(test_user)
  end

  include_examples "subscription type", "Basic subscription"
  include_examples "has access to goals"
  include_examples "has access to recurring expenses"
  include_examples "account without planning"
  include_examples "can't access planning"

  it "can delete goals with plans" do
    g = Goal.create(title: "Road trip", target_amount: 60000, user: test_user,
                    target_date: Date.today + 2.months)
    g.create_goal_plan(monthly_amount: 30000)
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.find_by_title("Road trip").delete
    wait_for_action_panel_to_close

    expect{Page::Goal.find_by_title("Road trip")}
      .to raise_exception(Capybara::ElementNotFound)
    expect(Goal.count).to be(0)
    expect(GoalPlan.count).to be(0)
  end

  it "shows Due amount column on summary page" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 7600, frequency: "monthly",
                                due_date: Date.today)
    c.expense_plans.create(amount: 9700, month: Date.today)
    visit "/summary"
    wait_for_page_to_load

    expect(panel_header("expenses", 2)).to eq("Due")
    expect(Page::Category.find_by_name("Food").due.amount).to eq("7600")
  end
end
