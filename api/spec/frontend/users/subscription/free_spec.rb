require 'rails_helper'
require 'frontend/users/subscription/generic_allowed.rb'
require 'frontend/users/subscription/generic_forbidden.rb'

RSpec.describe "Free subscription", js: true do
  include_context "user login"

  def test_user
    free_user
  end

  before(:each) do
    login(test_user)
  end

  include_examples "subscription type", "Free (no subscription)"
  include_examples "can't access goals"
  include_examples "can't access recurring expenses"
  include_examples "account without planning"
  include_examples "can't access planning"

  it "has no extra column on Expense panel on summary page" do
    c = Category.create(name: "Food", user: test_user)
    c.recurring_expenses.create(amount: 7600, frequency: "monthly",
                                due_date: Date.today)
    c.expense_plans.create(amount: 9700, month: Date.today)
    visit "/summary"
    wait_for_page_to_load

    expect{panel_header("expenses", 2)}
      .to raise_exception(Capybara::ElementNotFound)
    expect{Page::Category.find_by_name("Food").due.amount}
      .to raise_exception(Capybara::ElementNotFound)
  end
end
