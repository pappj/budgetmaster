require 'rails_helper'

RSpec.describe "Transactions transfer filter", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a1 = Account.create(name: "Acc1", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    a2 = Account.create(name: "Acc2", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    a3 = Account.create(name: "Acc3", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    Transfer.create(source_account: a1, target_account: a2, source_amount: 4500,
                    target_amount: 4500, date: Date.today)
    Transfer.create(source_account: a2, target_account: a3, source_amount: 8500,
                    target_amount: 8500, date: Date.today)
    Transfer.create(source_account: a3, target_account: a1, source_amount: 14500,
                    target_amount: 14500, date: Date.today)

    login_user
    Page::Transactions.visit
  end

  it "is checked by default" do
    Page::Transactions.filter do |f|
      expect(f.transfers.value).to be(true)
    end
  end

  it "unchecks the group if a source account is unchecked" do
    Page::Transactions.filter do |f|
      f.source_accounts_filter do |sf|
        sf.account("Acc2").toggle
      end

      expect(f.transfers.value).to be(false)
    end
  end

  it "unchecks the group if a target account is unchecked" do
    Page::Transactions.filter do |f|
      f.target_accounts_filter do |tf|
        tf.account("Acc2").toggle
      end

      expect(f.transfers.value).to be(false)
    end
  end

  it "unchecks all source and target accounts when the group is unchecked" do
    Page::Transactions.filter do |f|
      f.transfers.toggle

      f.source_accounts_filter do |sf|
        expect(sf.account("Acc1").value).to be(false)
        expect(sf.account("Acc2").value).to be(false)
        expect(sf.account("Acc3").value).to be(false)
      end

      f.target_accounts_filter do |tf|
        expect(tf.account("Acc1").value).to be(false)
        expect(tf.account("Acc2").value).to be(false)
        expect(tf.account("Acc3").value).to be(false)
      end
    end
  end

  it "checks the group if all source and target accounts are checked" do
    Page::Transactions.filter do |f|
      f.transfers.toggle

      f.source_accounts_filter do |sf|
        sf.account("Acc1").toggle
        sf.account("Acc2").toggle
        sf.account("Acc3").toggle
      end

      f.target_accounts_filter do |tf|
        tf.account("Acc1").toggle
        tf.account("Acc2").toggle
        tf.account("Acc3").toggle
      end

      expect(f.transfers.value).to be(true)
    end
  end

  it "shows all transfers by default" do
    expect(Page::Transactions.count).to be(3)
  end

  it "hides all transfers if the group is unchecked" do
    Page::Transactions.filter do |f|
      f.transfers.toggle

      f.apply
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(0)
  end

  it "hides transfers of an unchecked source account" do
    Page::Transactions.filter do |f|
      f.source_accounts_filter do |sf|
        sf.account("Acc1").toggle
      end

      f.apply
    end

    wait_for_action_panel_to_close

    expect{Page::Transactions.find_by({account: "Acc1"})}
      .to raise_error(Capybara::ElementNotFound)

    expect{Page::Transactions.find_by({account2: "Acc1"})}
      .not_to raise_error
  end

  it "hides transfers of an unchecked target account" do
    Page::Transactions.filter do |f|
      f.target_accounts_filter do |tf|
        tf.account("Acc3").toggle
      end

      f.apply
    end

    wait_for_action_panel_to_close

    expect{Page::Transactions.find_by({account2: "Acc3"})}
      .to raise_error(Capybara::ElementNotFound)

    expect{Page::Transactions.find_by({account: "Acc3"})}
      .not_to raise_error
  end
end
