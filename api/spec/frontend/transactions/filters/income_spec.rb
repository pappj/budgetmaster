require 'rails_helper'

RSpec.describe "Transactions income filter", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a1 = Account.create(name: "Acc1", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    a2 = Account.create(name: "Acc2", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    a3 = Account.create(name: "Acc3", account_type: "other", currency: huf,
                        current_balance: 146300, credit: 0, in_plan: false,
                        user: user)
    Income.create(account: a1, date: Date.today, amount: 5000)
    Income.create(account: a2, date: Date.today, amount: 6000)
    Income.create(account: a3, date: Date.today, amount: 7000)
    login_user
    Page::Transactions.visit
  end

  it "is checked by default" do
    Page::Transactions.filter do |f|
      expect(f.incomes.value).to be(true)
    end
  end

  it "unchecks the group when an account is unchecked" do
    Page::Transactions.filter do |f|
      f.incomes_filter do |inf|
        inf.account("Acc2").toggle
        expect(inf.account("Acc2").value).to be(false)
      end
      expect(f.incomes.value).to be(false)
    end
  end

  it "unchecks all accounts when the group is unchecked" do
    Page::Transactions.filter do |f|
      f.incomes.toggle
      f.incomes_filter do |inf|
        expect(inf.account("Acc1").value).to be(false)
        expect(inf.account("Acc2").value).to be(false)
        expect(inf.account("Acc3").value).to be(false)
      end
    end
  end

  it "checks the group when all accounts are checked" do
    Page::Transactions.filter do |f|
      f.incomes.toggle
      f.incomes_filter do |inf|
        inf.account("Acc1").toggle
        inf.account("Acc2").toggle
        inf.account("Acc3").toggle
      end
      expect(f.incomes.value).to be(true)
    end
  end

  it "shows all incomes by default" do
    expect(Page::Transactions.count).to eq(3)
  end

  it "hides all incomes when the group is unchecked" do
    Page::Transactions.filter do |f|
      f.incomes.toggle
      f.apply
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to eq(0)
  end

  it "hides incomes of an unchecked account" do
    Page::Transactions.filter do |f|
      f.incomes_filter do |inf|
        inf.account("Acc2").toggle
      end
      f.apply
    end

    wait_for_action_panel_to_close

    expect{Page::Transactions.find_by({account: "Acc2"})}
      .to raise_error(Capybara::ElementNotFound)
  end
end
