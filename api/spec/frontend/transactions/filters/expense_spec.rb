require 'rails_helper'

RSpec.describe "Transactions expense filter", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    acc = Account.create(name: "Acc1", account_type: "other", currency: huf,
                         current_balance: 146300, credit: 0, in_plan: false,
                         user: user)

    c1 = Category.create(name: "Cat1", user: user)
    c2 = Category.create(name: "Cat2", user: user)
    c3 = Category.create(name: "Cat3", user: user)

    Expense.create(account: acc, category: c1, date: Date.today, amount: 5000)
    Expense.create(account: acc, category: c2, date: Date.today, amount: 6000)
    Expense.create(account: acc, category: c3, date: Date.today, amount: 7000)

    login_user
    Page::Transactions.visit
  end

  it "is checked by default" do
    Page::Transactions.filter do |f|
      expect(f.expenses.value).to be(true)
    end
  end

  it "unchecks the group when a category is unchecked" do
    Page::Transactions.filter do |f|
      f.expenses_filter do |ef|
        ef.category("Cat3").toggle
        expect(ef.category("Cat3").value).to be(false)
      end

      expect(f.expenses.value).to be(false)
    end
  end

  it "unchecks all categories when the group is unchecked" do
    Page::Transactions.filter do |f|
      f.expenses.toggle
      f.expenses_filter do |ef|
        expect(ef.category("Cat1").value).to be(false)
        expect(ef.category("Cat2").value).to be(false)
        expect(ef.category("Cat3").value).to be(false)
      end
    end
  end

  it "checks the group when all categories are checked" do
    Page::Transactions.filter do |f|
      f.expenses.toggle
      f.expenses_filter do |ef|
        ef.category("Cat1").toggle
        ef.category("Cat2").toggle
        ef.category("Cat3").toggle
      end

      expect(f.expenses.value).to be(true)
    end
  end

  it "shows all expenses by default" do
    expect(Page::Transactions.count).to eq(3)
  end

  it "hides all expenses when the group is unchecked" do
    Page::Transactions.filter do |f|
      f.expenses.toggle
      f.apply
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to eq(0)
  end

  it "hides expenses of an unchecked category" do
    Page::Transactions.filter do |f|
      f.expenses_filter do |ef|
        ef.category("Cat3").toggle
      end
      f.apply
    end

    wait_for_action_panel_to_close

    expect{Page::Transactions.find_by({category: "Cat3"})}
      .to raise_error(Capybara::ElementNotFound)
  end
end
