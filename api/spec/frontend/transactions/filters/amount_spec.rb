require 'rails_helper'

RSpec.describe "Transactions amount filter", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    acc = Account.create(name: "Acc1", account_type: "other", currency: huf,
                         current_balance: 146300, credit: 0, in_plan: false,
                         user: user)
    Income.create(account: acc, date: Date.today, amount: 2000)
    Income.create(account: acc, date: Date.today, amount: 6000)
    Income.create(account: acc, date: Date.today, amount: 14000)
    login_user
    Page::Transactions.visit
  end

  it "is checked by default" do
    Page::Transactions.filter do |f|
      expect(f.amounts.value).to be(true)
    end
  end

  it "does not show the range when all is selected" do
    Page::Transactions.filter do |f|
      expect{f.amounts_filter{}}.to raise_error(Capybara::ElementNotFound)
    end
  end

  it "updates the upper limit if lower limit gets bigger" do
    Page::Transactions.filter do |f|
      f.amounts.toggle
      f.amounts_filter do |af|
        af.to.set(5000)
        af.from.set(8600)

        expect(af.to.value).to eq("8600")
      end
    end
  end

  it "shows all transactions by default" do
    expect(Page::Transactions.count).to be(3)
  end

  it "filters out the amounts out of range" do
    Page::Transactions.filter do |f|
      f.amounts.toggle
      f.amounts_filter do |af|
        af.from.set(4700)
        af.to.set(6000)
      end

      f.apply
    end

    expect(Page::Transactions.count).to be(1)
    expect{Page::Transactions.find_by({amount: 2000})}
      .to raise_error(Capybara::ElementNotFound)
    expect{Page::Transactions.find_by({amount: 8000})}
      .to raise_error(Capybara::ElementNotFound)
    expect{Page::Transactions.find_by({amount: 6000})}
      .not_to raise_error
  end

  it "shows all transactions again, when all is selected" do
    Page::Transactions.filter do |f|
      f.amounts.toggle
      f.amounts_filter do |af|
        af.from.set(4700)
        af.to.set(6000)
      end

      f.apply
    end

    Page::Transactions.filter do |f|
      f.amounts.toggle
      f.apply
    end

    expect(Page::Transactions.count).to be(3)
  end
end
