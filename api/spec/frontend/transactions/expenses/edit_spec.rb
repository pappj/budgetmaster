require 'rails_helper'

RSpec.describe "Edit expense", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Category.create(name: "Sport", user: user)
    c = Category.create(name: "Food", user: user)
    Account.create(name: "Mastercard", account_type: "credit", currency: huf,
                       current_balance: 13500, credit: 20000, in_plan: false,
                       user: user)
    a = Account.create(name: "Main account", account_type: "bank", currency: huf,
                       current_balance: 1850, credit: 10000, in_plan: false,
                       user: user)
    Expense.create(category: c, account: a, amount: 20000, date: Date.today,
                   note: "Delivery")
    Expense.create(category: c, account: a, amount: 6500, date: 1.week.ago)

    login_user
    Page::Transactions.visit
  end

  def expense
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Expense)
  end

  def other_expense
    Page::Transactions.find_by({date: 1.week.ago}, Page::Transactions::Expense)
  end

  it "can't change the account, if the new account's balance is too low" do
    expense.edit do |exp|
      exp.account.select("Mastercard")
      exp.update_account.toggle
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error("amount"))
      .to include("'Mastercard' must cover it or don't update the accounts")
  end

  it "changes the account, if the account balance is not updated" do
    expense.edit do |exp|
      exp.account.select("Mastercard")
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(expense.account).to eq("Mastercard")
  end

  it "changes the account, and updates both of their balance" do
    other_expense.edit do |exp|
      exp.account.select("Mastercard")
      exp.update_account.toggle
      exp.submit
    end

    wait_for_action_panel_to_close
    expect(Account.find_by(name: "Mastercard").current_balance).to eq(7000)
    expect(Account.find_by(name: "Main account").current_balance).to eq(8350)

    Page::Navigate.to_summary
    expect(Page::Summary.find_account("Mastercard").balance.amount)
      .to eq("7000")
    expect(Page::Summary.find_account("Main account").balance.amount)
      .to eq("8350")
  end

  it "can't change the amount, if the account's balance is too low" do
    expense.edit do |exp|
      exp.amount.set(40000)
      exp.update_account.toggle
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error("amount"))
      .to include("must cover it or don't update the accounts")
  end

  it "changes the amount, if the account balance is not updated" do
    expense.edit do |exp|
      exp.amount.set(40000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(expense.amount.amount).to eq("40000")
  end

  it "can't change the amount to be negative" do
    expense.edit do |exp|
      exp.amount.set(-3000)
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error("amount")).to eq("must be positive")
  end

  it "increases the account balance" do
    expense.edit do |exp|
      exp.amount.set(11000)
      exp.update_account.toggle
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(expense.amount.amount).to eq("11000")
    expect(Account.find_by(name: "Main account").current_balance).to eq(10850)
  end

  it "lowers the account balance" do
    expense.edit do |exp|
      exp.amount.set(23850)
      exp.update_account.toggle
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(expense.amount.amount).to eq("23850")
    expect(Account.find_by(name: "Main account").current_balance).to eq(-2000)
  end

  it "changes the category" do
    expense.edit do |exp|
      exp.category.select("Sport")
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(expense.category).to eq("Sport")
  end

  it "moves the expense down when the date is changed" do
    expense.edit do |exp|
      select_date(exp.date, 1.month.ago)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.all[1].amount.amount).to eq("20000")
  end

  it "hides the expense if the date is changed out of range" do
    expect(Page::Transactions.count).to be(2)
    expense.edit do |exp|
      select_date(exp.date, 2.years.ago)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "changes the note" do
    expense.edit do |exp|
      exp.note.set("Dinner")
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Expense.find_by(date: Date.today).note).to eq("Dinner")
  end

  it "updates the monthly expenses with the new amount" do
    Page::Summary.visit
    Page::Navigate.to_transactions

    expense.edit do |exp|
      exp.amount.set(12000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expected_amount = if Date.today.month == 1.week.ago.month
      "18500"
    else
      "12000"
    end

    Page::Navigate.to_summary
    expect(Page::Category.find_by_name("Food").spent("HUF").amount)
      .to eq(expected_amount)
  end

  it "updates the monthly expenses when the date is set to older" do
    Page::Summary.visit
    Page::Navigate.to_transactions

    expense.edit do |exp|
      select_date(exp.date, 2.months.ago)
      exp.submit
    end

    wait_for_action_panel_to_close

    expected_amount = if Date.today.month == 1.week.ago.month
      "6500"
    else
      "0"
    end

    Page::Navigate.to_summary
    expect(Page::Category.find_by_name("Food").spent("HUF").amount)
      .to eq(expected_amount)
  end
end
