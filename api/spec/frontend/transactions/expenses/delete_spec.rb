require 'rails_helper'

RSpec.describe "Delete expense", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a = Account.create(name: "Cash", account_type: "cash", currency: huf,
                       current_balance: 2450, credit: 0, user: user)
    c = Category.create(name: "Food", user: user)
    Expense.create(account: a, category: c, amount: 12500, date: Date.today)

    login_user
    Page::Transactions.visit
  end

  def expense
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Expense)
  end

  it "deletes the expense and the account is not updated" do
    expense.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Expense.count).to be(0)
    expect(Account.first.current_balance).to eq(2450)
  end

  it "deletes the expense and updates the account's balance" do
    expense.delete do |del|
      del.update_account.toggle
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Expense.count).to be(0)
    expect(Account.first.current_balance).to eq(14950)

    Page::Navigate.to_summary
    expect(Page::Summary.find_account("Cash").balance.amount).to eq("14950")
  end

  it "removes the expense from the page" do
    expense.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(0)
  end

  it "doesn't delete the expense on cancel" do
    expense.delete do |del|
      del.cancel
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "doesn't delete the expense if clicking 'no'" do
    expense.delete do |del|
      del.no
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "updates the monthly expense after removal" do
    Page::Summary.visit
    Page::Navigate.to_transactions

    expense.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    Page::Navigate.to_summary
    expect(Page::Category.find_by_name("Food").spent("HUF").amount)
      .to eq("0")
  end
end
