require 'rails_helper'

RSpec.describe "Transactions date selector", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a1 = Account.create(name: "acc1", account_type: "bank", currency: huf,
                        current_balance: 12500, credit: 0, user: user)
    a2 = Account.create(name: "acc2", account_type: "bank", currency: huf,
                        current_balance: 15500, credit: 0, user: user)
    c1 = Category.create(name: "Sport", user: user)
    Income.create(account: a1, date: 3.months.ago - 1.day, amount: 10000)
    Income.create(account: a1, date: 3.months.ago, amount: 60000)
    Expense.create(account: a1, category: c1, date: 2.months.ago + 1.day,
                   amount: 5600)
    Expense.create(account: a2, category: c1, date: 2.months.ago, amount: 2900)
    Transfer.create(source_account: a1, source_amount: 5000, target_account: a2,
                    target_amount: 5000, date: 3.months.ago - 1.day)
    Transfer.create(source_account: a2, source_amount: 9000, target_account: a1,
                    target_amount: 9000, date: 2.months.ago - 18.day)

    login_user
    Page::Transactions.visit
  end

  it "shows the last 6 months by default" do
    dr = Page::Transactions.date_range
    expect(dr.start_date.text).to eq(6.months.ago.strftime("%Y-%m-%d"))
    expect(dr.end_date.text).to eq(Date.today.strftime("%Y-%m-%d"))
  end

  it "changes end date, when start date is set earlier" do
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, 9.months.ago)
      expect(dr.end_date.value).to eq(3.months.ago.strftime("%Y-%m-%d"))
    end
  end

  it "doesn't allow longer than 6 months period selection" do
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, 9.months.ago)
      end_date = 3.months.ago + 1.day
      expect{select_date(dr.end_date, end_date)}
        .to raise_exception("Can't select date")
    end
  end

  it "doesn't allow future start date" do
    Page::Transactions::DateRange.edit do |dr|
      expect{select_date(dr.start_date, Date.today + 1.day)}
        .to raise_exception("Can't select date")
    end
  end

  it "doesn't allow future end date" do
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, 1.month.ago)
      expect{select_date(dr.end_date, Date.today + 1.day)}
        .to raise_exception("Can't select date")
    end
  end

  it "allows to select less then 6 months period" do
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, 3.months.ago)
      select_date(dr.end_date, 1.month.ago)
    end
  end

  it "shows the selected date range" do
    start_date = 3.months.ago
    end_date = 1.month.ago
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, start_date)
      select_date(dr.end_date, end_date)
      dr.apply
    end

    wait_for_page_to_load

    expect(Page::Transactions.date_range.start_date.text)
      .to eq(start_date.strftime("%Y-%m-%d"))
    expect(Page::Transactions.date_range.end_date.text)
      .to eq(end_date.strftime("%Y-%m-%d"))
  end

  it "hides transactions outside of the selected period" do
    expect(Page::Transactions.count).to eq(6)
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.start_date, 3.months.ago)
      select_date(dr.end_date, 2.months.ago)
      dr.apply
    end

    wait_for_page_to_load

    expect(Page::Transactions.count).to eq(3)
  end
end
