require 'rails_helper'

RSpec.describe "Edit income", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Mastercard", account_type: "credit", currency: huf,
                       current_balance: 13500, credit: 20000, in_plan: false,
                       user: user)
    a = Account.create(name: "Main account", account_type: "bank", currency: huf,
                       current_balance: 1850, credit: 10000, in_plan: false,
                       user: user)
    Income.create(account: a, date: Date.today, amount: 38000, note: "Refund")
    Income.create(account: a, date: 2.days.ago, amount: 2000)

    login_user
    Page::Transactions.visit
  end

  def income
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Income)
  end

  def other_income
    Page::Transactions.find_by({date: 2.days.ago}, Page::Transactions::Income)
  end

  it "can't change the account, if the original account's balance is too low" do
    income.edit do |inc|
      inc.account.select("Mastercard")
      inc.update_account.toggle
      inc.submit
    end

    expect(Page::Elements::ActionPanel.generic_error)
      .to include("not enough to cover")
      .and include("Change the current balance of 'Main account'")
  end

  it "hides the generic error, once it becomes obsolete" do
    income.edit do |inc|
      inc.account.select("Mastercard")
      inc.update_account.toggle
      inc.submit

      expect(Page::Elements::ActionPanel.generic_error)
        .to include("not enough to cover")
        .and include("Change the current balance of 'Main account'")

      inc.account.select("Main account")
      inc.amount.set(10000)
      inc.submit

      expect{Page::Elements::ActionPanel.generic_error}
        .to raise_exception(Capybara::ElementNotFound)
      expect(Page::Elements::ActionPanel.error("amount"))
        .to include("must cover the change or don't update the accounts")
    end
  end

  it "changes the account, if the account balance is not updated" do
    income.edit do |inc|
      inc.account.select("Mastercard")
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(income.account).to eq("Mastercard")
  end

  it "changes the account, and updates both of their balance" do
    other_income.edit do |inc|
      inc.account.select("Mastercard")
      inc.update_account.toggle
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(other_income.account).to eq("Mastercard")
    expect(Account.find_by(name: "Mastercard").current_balance).to eq(15500)
    expect(Account.find_by(name: "Main account").current_balance).to eq(-150)

    Page::Navigate.to_summary
    expect(Page::Summary.find_account("Mastercard").balance.amount)
      .to eq("15500")
    expect(Page::Summary.find_account("Main account").balance.amount)
      .to eq("-150")
  end

  it "can't change the amount to be negative" do
    income.edit do |inc|
      inc.amount.set(-1000)
      inc.submit
    end

    expect(Page::Elements::ActionPanel.error("amount")).to eq("must be positive")
  end

  it "can't lower the amount, if the account balance is too low" do
    income.edit do |inc|
      inc.amount.set(10000)
      inc.update_account.toggle
      inc.submit
    end

    expect(Page::Elements::ActionPanel.error("amount"))
      .to include("must cover the change or don't update the accounts")
  end

  it "lowers the amount, if credit is used" do
    income.edit do |inc|
      inc.amount.set(28000)
      inc.update_account.toggle
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(income.amount.amount).to eq("28000")
    expect(Account.find_by(name: "Main account").current_balance).to eq(-8150)
  end

  it "lowers the amount, if the account balance is not updated" do
    income.edit do |inc|
      inc.amount.set(10000)
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(income.amount.amount).to eq("10000")
    expect(Account.find_by(name: "Main account").current_balance).to eq(1850)
  end

  it "increases the account balance" do
    income.edit do |inc|
      inc.amount.set(50000)
      inc.update_account.toggle
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(income.amount.amount).to eq("50000")
    expect(Account.find_by(name: "Main account").current_balance).to eq(13850)
  end

  it "moves the income down when the date is changed" do
    income.edit do |inc|
      select_date(inc.date, 2.weeks.ago)
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.all[1].amount.amount).to eq("38000")
  end

  it "hides the income if the date is changed out of range" do
    expect(Page::Transactions.count).to be(2)
    income.edit do |inc|
      select_date(inc.date, 1.year.ago)
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "changes the note" do
    income.edit do |inc|
      inc.note.set("Paycheck")
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(Income.find_by(date: Date.today).note).to eq("Paycheck")
  end
end
