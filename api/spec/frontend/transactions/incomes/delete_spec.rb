require 'rails_helper'

RSpec.describe "Delete income", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a = Account.create(name: "Cash", account_type: "cash", currency: huf,
                       current_balance: 2450, credit: 0, user: user)
    Income.create(account: a, amount: 12500, date: Date.today)
    Income.create(account: a, amount: 500, date: 2.days.ago)

    login_user
    Page::Transactions.visit
  end

  def income
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Income)
  end

  def other_income
    Page::Transactions.find_by({date: 2.days.ago}, Page::Transactions::Income)
  end

  it "can't delete the income if the account has low balance" do
    income.delete do |del|
      expect(del.update_account.disabled?).to be(true)
    end
  end

  it "deletes the income if the account is not updated" do
    income.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Income.count).to be(1)
  end

  it "deletes the income and updates the account's balance" do
    other_income.delete do |del|
      del.update_account.toggle
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Income.count).to be(1)
    expect(Account.first.current_balance).to eq(1950)

    Page::Navigate.to_summary
    expect(Page::Summary.find_account("Cash").balance.amount).to eq("1950")
  end

  it "removes the income from the page" do
    income.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "doesn't delete the income on cancel" do
    income.delete do |del|
      del.cancel
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(2)
  end

  it "doesn't delete the income if clicking 'no'" do
    income.delete do |del|
      del.no
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(2)
  end
end
