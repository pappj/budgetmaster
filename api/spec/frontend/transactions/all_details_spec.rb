require 'rails_helper'

RSpec.describe "Transactions all details", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    a1 = Account.create(name: "Acc", account_type: "bank", current_balance: 15000,
                        credit: 0, currency: huf, user: user)
    a2 = Account.create(name: "Acc2", account_type: "bank", current_balance: 15000,
                        credit: 0, currency: huf, user: user)
    Income.create(account: a1, amount: 2000, date: Date.today)
    c = Category.create(name: "Cat", user: user)
    Expense.create(category: c, account: a1, amount: 4500, date: Date.today)
    Transfer.create(source_account: a1, source_amount: 5000, target_account: a2,
                    target_amount: 5000, date: Date.today)

    login_user
    Page::Transactions.visit
  end

  it "hides all details by default" do
    expect(Page::Transactions.all.all?{ |t| not t.expanded? }).to be(true)
  end

  it "extends all transactions when clicked" do
    Page::Transactions.toggle_all_details
    expect(Page::Transactions.all.all?{ |t| t.expanded? }).to be(true)
  end

  it "hides all transactions when it's clicked while it's active" do
    Page::Transactions.toggle_all_details
    expect(Page::Transactions.all.all?{ |t| t.expanded? }).to be(true)

    Page::Transactions.toggle_all_details
    expect(Page::Transactions.all.all?{ |t| not t.expanded? }).to be(true)
  end

  it "gets inactive if a transactions is collapsed" do
    Page::Transactions.toggle_all_details
    Page::Transactions.all[0].simple
    expect(Page::Transactions.all_details_active?).to be(false)
  end

  it "gets active if all transactions are expanded manually" do
    Page::Transactions.all.each{|t| t.detailed}
    expect(Page::Transactions.all_details_active?).to be(true)
  end

  it "is not activated when there are no transactions" do
    Page::Transactions::DateRange.edit do |dr|
      select_date(dr.end_date, 1.month.ago)
      dr.apply
    end

    expect(Page::Transactions.count).to eq(0)
    expect(Page::Transactions.all_details_active?).to be(false)
  end

  it "shows transactions even when they are already loaded" do
    Page::Navigate.to_summary
    Page::Navigate.to_transactions
    expect(Page::Transactions.count).to eq(3)
  end
end
