require 'rails_helper'

RSpec.describe "Delete transfer", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    acc1 = Account.create(name: "Cash", account_type: "cash", currency: huf,
                          current_balance: 1860, credit: 0, user: user)
    acc2 = Account.create(name: "Credit", account_type: "credit", currency: huf,
                          current_balance: 12500, credit: 15000, user: user)
    Transfer.create(source_account: acc2, target_account: acc1,
                    source_amount: 5600, target_amount: 5600, date: Date.today)
    Transfer.create(source_account: acc1, target_account: acc2,
                    source_amount: 7400, target_amount: 7400, date: 3.days.ago)

    login_user
    Page::Transactions.visit
  end

  def transfer
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Transfer)
  end

  def other_transfer
    Page::Transactions.find_by({date: 3.days.ago}, Page::Transactions::Transfer)
  end

  it "can't delete the transfer if target account has low balance" do
    transfer.delete do |del|
      expect(del.update_account.disabled?).to be(true)
    end
  end

  it "deletes the transfer if the accounts are not updated" do
    transfer.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Transfer.count).to be(1)
  end

  it "deletes the transfer and updates the accounts' balance" do
    other_transfer.delete do |del|
      del.update_account.toggle
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Transfer.count).to be(1)
    expect(Account.find_by(name: "Cash").current_balance).to eq(9260)
    expect(Account.find_by(name: "Credit").current_balance).to eq(5100)

    Page::Navigate.to_summary
    expect(Page::Summary.find_account("Cash").balance.amount).to eq("9260")
    expect(Page::Summary.find_account("Credit").balance.amount).to eq("5100")
  end

  it "removes the transfer from the page" do
    transfer.delete do |del|
      del.yes
    end

    wait_for_action_panel_to_close

    expect(Page::Transactions.count).to be(1)
  end

  it "doesn't delete the transfer on cancel" do
    transfer.delete do |del|
      del.cancel
    end

    wait_for_action_panel_to_close

    expect(Transfer.count).to be(2)
  end

  it "doesn't delete the transfer if clicking 'no'" do
    transfer.delete do |del|
      del.no
    end

    wait_for_action_panel_to_close

    expect(Transfer.count).to be(2)
  end
end
