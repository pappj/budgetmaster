require 'rails_helper'

RSpec.describe "Edit transfer", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Daily spending", account_type: "cash", currency: huf,
                   current_balance: 2500, credit: 0, user: user)
    Account.create(name: "Savings", account_type: "cash", currency: huf,
                   current_balance: 168000, credit: 0, user: user)
    Account.create(name: "Emergency", account_type: "cash", currency: huf,
                   current_balance: 82700, credit: 0, user: user)
    Account.create(name: "Traveling", account_type: "cash", currency: eur,
                   current_balance: 320, credit: 0, user: user)
  end

  def transfer
    Page::Transactions.find_by({date: Date.today}, Page::Transactions::Transfer)
  end

  def other_transfer
    Page::Transactions.find_by({date: 4.days.ago}, Page::Transactions::Transfer)
  end

  context do
    before(:each) do
      low_acc = Account.find_by(name: "Daily spending")
      good_acc = Account.find_by(name: "Savings")
      good_acc_2 = Account.find_by(name: "Emergency")
      Transfer.create(source_account: good_acc, target_account: good_acc_2,
                      source_amount: 8500, target_amount: 8500, date: Date.today)
      Transfer.create(source_account: low_acc, target_account: good_acc,
                      source_amount: 15000, target_amount: 15000,
                      date: 4.days.ago)

      login_user
      Page::Transactions.visit
    end

    it "can't change the source account if the new account's balance is too low" do
      transfer.edit do |tr|
        tr.source_account.select("Daily spending")
        tr.update_account.toggle
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error("source_amount"))
        .to include("'Daily spending' must cover it or don't update the accounts")
    end

    it "changes the source account if the accounts are not updated" do
      transfer.edit do |tr|
        tr.source_account.select("Daily spending")
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.source_account).to eq("Daily spending")
    end

    it "changes the source account, and updates the balances" do
      other_transfer.edit do |tr|
        tr.source_account.select("Emergency")
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(other_transfer.source_account).to eq("Emergency")
      expect(Account.find_by(name: "Daily spending").current_balance)
        .to eq(17500)
      expect(Account.find_by(name: "Emergency").current_balance)
        .to eq(67700)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Daily spending").balance.amount)
        .to eq("17500")
      expect(Page::Summary.find_account("Emergency").balance.amount)
        .to eq("67700")
    end

    it "can't increase the source amount if the account's balance is too low" do
      other_transfer.edit do |tr|
        tr.source_amount.set(21200)
        tr.update_account.toggle
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error("source_amount"))
        .to include("must be at most 17500")
    end

    it "increases the source amount if the accounts are not updated" do
      other_transfer.edit do |tr|
        tr.source_amount.set(21200)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(other_transfer.source_amount.amount).to eq("21200")
      expect(other_transfer.target_amount.amount).to eq("21200")
    end

    it "increases the source amount, and updates the account's balance" do
      other_transfer.edit do |tr|
        tr.source_amount.set(16500)
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(other_transfer.source_amount.amount).to eq("16500")
      expect(Account.find_by(name: "Daily spending").current_balance)
        .to eq(1000)
      expect(Account.find_by(name: "Savings").current_balance)
        .to eq(169500)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Daily spending").balance.amount)
        .to eq("1000")
      expect(Page::Summary.find_account("Savings").balance.amount)
        .to eq("169500")
    end
  end

  context do
    before(:each) do
      low_acc = Account.find_by(name: "Daily spending")
      good_acc = Account.find_by(name: "Savings")
      good_acc_2 = Account.find_by(name: "Emergency")
      Transfer.create(source_account: good_acc, target_account: low_acc,
                      source_amount: 13000, target_amount: 13000,
                      date: Date.today)
      Transfer.create(source_account: good_acc, target_account: good_acc_2,
                      source_amount: 7200, target_amount: 7200, date: 4.days.ago)

      login_user
      Page::Transactions.visit
    end

    it "can't decrease the amounts if the target account's balance is too low" do
      transfer.edit do |tr|
        tr.source_amount.set(10000)
        tr.update_account.toggle
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error("target_amount"))
        .to include("The amount available on the account is not enough")
    end

    it "decreases the amounts if the accounts are not updated" do
      transfer.edit do |tr|
        tr.source_amount.set(10000)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.source_amount.amount).to eq("10000")
      expect(transfer.target_amount.amount).to eq("10000")
    end

    it "decreases the amounts, and updates the accounts" do
      transfer.edit do |tr|
        tr.source_amount.set(11000)
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.source_amount.amount).to eq("11000")
      expect(Account.find_by(name: "Savings").current_balance)
        .to eq(170000)
      expect(Account.find_by(name: "Daily spending").current_balance)
        .to eq(500)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Savings").balance.amount)
        .to eq("170000")
      expect(Page::Summary.find_account("Daily spending").balance.amount)
        .to eq("500")
    end

    it "can't change the target account if the old account's balance is too low" do
      transfer.edit do |tr|
        tr.target_account.select("Emergency")
        tr.update_account.toggle
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error("target_amount"))
        .to include("The amount available on the original account is not enough")
    end

    it "changes the target account if the accounts are not updated" do
      transfer.edit do |tr|
        tr.target_account.select("Emergency")
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.target_account).to eq("Emergency")
    end

    it "changes the target account, and updates the balances" do
      other_transfer.edit do |tr|
        tr.target_account.select("Daily spending")
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.target_account).to eq("Daily spending")
      expect(Account.find_by(name: "Emergency").current_balance)
        .to eq(75500)
      expect(Account.find_by(name: "Daily spending").current_balance)
        .to eq(9700)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Emergency").balance.amount)
        .to eq("75500")
      expect(Page::Summary.find_account("Daily spending").balance.amount)
        .to eq("9700")
    end
  end

  context do
    before(:each) do
      huf_acc = Account.find_by(name: "Savings")
      eur_acc = Account.find_by(name: "Traveling")

      Transfer.create(source_account: huf_acc, target_account: eur_acc,
                      source_amount: 12000, target_amount: 400,
                      date: Date.today)

      login_user
      Page::Transactions.visit
    end

    it "can't decrease the target amount if the account's balance is too low" do
      transfer.edit do |tr|
        tr.target_amount.set(20)
        tr.update_account.toggle
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error("target_amount"))
        .to include("'Traveling' must cover the change")
    end

    it "decreases the target amount if the accounts are not updated" do
      transfer.edit do |tr|
        tr.target_amount.set(20)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.target_amount.amount).to eq("20")
    end

    it "decreases the target amount, and updates the account's balance" do
      transfer.edit do |tr|
        tr.target_amount.set(300)
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.target_amount.amount).to eq("300")
      expect(Account.find_by(name: "Traveling").current_balance).to eq(220)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Traveling").balance.amount).to eq("220")
    end

    it "increases the target amount, and updates the account's balance" do
      transfer.edit do |tr|
        tr.target_amount.set(480)
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(transfer.target_amount.amount).to eq("480")
      expect(Account.find_by(name: "Traveling").current_balance).to eq(400)

      Page::Navigate.to_summary
      expect(Page::Summary.find_account("Traveling").balance.amount).to eq("400")
    end
  end

  context do
    before(:each) do
      acc1 = Account.find_by(name: "Savings")
      acc2 = Account.find_by(name: "Emergency")
      Transfer.create(source_account: acc1, target_account: acc2,
                      source_amount: 15000, target_amount: 15000,
                      date: Date.today, note: "Compensation")
      Transfer.create(source_account: acc2, target_account: acc1,
                      source_amount: 5000, target_amount: 5000,
                      date: 4.days.ago)

      login_user
      Page::Transactions.visit
    end

    it "can't change the target amount if the accounts' currency are the same" do
      transfer.edit do |tr|
        expect(tr.target_amount.disabled?).to be(true)
      end
    end

    it "moves the transfer down when the date is changed" do
      transfer.edit do |tr|
        select_date(tr.date, 2.weeks.ago)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Transactions.all[1].amount.amount).to eq("15000")
    end

    it "hides the transfer if the date is changed out of range" do
      expect(Page::Transactions.count).to be(2)
      transfer.edit do |tr|
        select_date(tr.date, 2.years.ago)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Transactions.count).to be(1)
    end

    it "changes the note" do
      transfer.edit do |tr|
        tr.note.set("Restructuring")
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Transfer.find_by(date: Date.today).note).to eq("Restructuring")
    end
  end
end
