require 'rails_helper'

RSpec.describe "Delete Recurring Expense", js: true do
  include_context "user login"

  before(:each) do
    cat = Category.create(name: "Sport", user: user)
    RecurringExpense.create(category: cat, amount: 6000, frequency: 'monthly',
                            due_date: Date.today)
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "removes the expense from the UI on save" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.recurring_expenses.first.delete
      cat.submit
    end

    wait_for_action_panel_to_close

    Page::Category.find_by_name("Sport").edit do |cat|
      expect(cat.node).to have_content("You haven't added recurring expenses yet.")
    end
  end

  it "removes the expense from the database on save" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.recurring_expenses.first.delete
      cat.submit
    end

    wait_for_action_panel_to_close
    expect(RecurringExpense.count).to eq(0)
  end

  it "doesn't remove the expense on cancel" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.recurring_expenses.first.delete
      cat.cancel
    end

    Page::Category.find_by_name("Sport").edit do |cat|
      expect(cat.recurring_expenses.length).to be(1)
    end

  end

  it "keeps the form errors correct when another expense is removed" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.new_recurring_expense do |exp|
        exp.amount.set(-500)
        exp.frequency.select("Quarterly")
      end
      cat.submit

      expect(cat.recurring_expenses[1].node).to have_content("must be positive")

      cat.recurring_expenses.first.delete

      # The new elem becomes the first one. Check if the
      # error is still shown for it.
      expect(cat.recurring_expenses.first.node).to have_content("must be positive")
    end
  end

  # This used to be the logic, but it has shitty user experience
  it "doesn't validate other expenses when removing one" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.new_recurring_expense do |exp|
        exp.amount.set(-500)
        exp.frequency.select("Quarterly")
      end

      cat.recurring_expenses.first.delete

      expect(cat.recurring_expenses.first.node)
        .to have_no_content("must be positive")
    end
  end
end
