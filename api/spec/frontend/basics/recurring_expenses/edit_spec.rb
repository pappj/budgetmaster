require 'rails_helper'

RSpec.describe "Edit RecurringExpense", js: true do
  include_context "user login"

  before(:each) do
    cat = Category.create(name: "Sport", user: user)
    RecurringExpense.create(category: cat, amount: 13000, frequency: 'quarterly',
                            due_date: Date.today)
    RecurringExpense.create(category: cat, amount: 1600, frequency: 'monthly',
                            due_date: Date.today)
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "updates the recurring expenses on save" do
    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses[1]
      exp.amount.set(50000)
      exp.frequency.select("Yearly")
      cat.submit
    end

    wait_for_action_panel_to_close

    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses[1]
      expect(exp.amount.value).to eq("50000")
      expect(exp.frequency.value).to eq("yearly")
    end
  end

  it "updates the recurring expenses in the database" do
    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses.first
      exp.amount.set(50000)
      exp.frequency.select("Yearly")
      cat.submit
    end

    wait_for_action_panel_to_close

    record = RecurringExpense.first
    expect(record.amount).to eq(50000)
    expect(record.frequency).to eq('yearly')
  end

  it "doesn't change recurring expenses on cancel" do
    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses.first
      exp.amount.set(50000)
      exp.frequency.select("Yearly")
      cat.cancel
    end

    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses.first
      expect(exp.amount.value).to eq("13000")
      expect(exp.frequency.value).to eq("quarterly")
    end
  end

  context "when expense is due in the past" do
    before(:each) do
      cat = Category.create(name: "Bills", user: user)
      rec_exp = RecurringExpense.new(category: cat, amount: 6000,
                                     frequency: 'quarterly',
                                     due_date: Date.today - 10.days)
      rec_exp.save(validate: false)
      visit "/basics"
    end

    it "shows error on saving with due date in the past" do
      Page::Category.find_by_name("Bills").edit do |cat|
        exp = cat.recurring_expenses.first
        exp.amount.set(4500)
        cat.submit
      end

      section = find("#rec_exp_0")
      date = Date.today.strftime('%Y-%m-%d')
      expect(section).to have_content("can't be earlier than #{date}")
    end

    it "doesn't change the database on saving with past due date" do
      Page::Category.find_by_name("Bills").edit do |cat|
        exp = cat.recurring_expenses.first
        exp.amount.set(4500)
        cat.submit
      end

      record = Category.find_by(name: "Bills").recurring_expenses.first
      expect(record.amount).to eq(6000)
    end

  end
end
