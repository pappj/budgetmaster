require 'rails_helper'

RSpec.describe "New Recurring Expense", js: true do
  include_context "user login"

  before(:each) do
    cat = Category.create(name: "Sport", user: user)
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "adds a new recurring expense to an existing category" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.new_recurring_expense do |exp|
        exp.amount.set(5000)
        exp.frequency.select("Quarterly")
      end
      cat.submit
    end

    wait_for_action_panel_to_close

    Page::Category.find_by_name("Sport").edit do |cat|
      exp = cat.recurring_expenses.first
      expect(exp.amount.value).to eq("5000")
      expect(exp.frequency.value).to eq("quarterly")
    end

    record = RecurringExpense.first
    expect(record.amount).to eq(5000)
    expect(record.frequency).to eq('quarterly')
  end

  it "creates recurring expenses alongside a new category" do
    Page::Category.new_category do |cat|
      cat.name.set("Entertainment")
      cat.new_recurring_expense do |exp|
        exp.amount.set(12000)
        exp.frequency.select("Yearly")
        select_date(exp.date, Date.today + 2.months)
      end
      cat.submit
    end

    wait_for_action_panel_to_close

    record = RecurringExpense.first
    expect(record).not_to be(nil)
    expect(record.amount).to eq(12000)
    expect(record.due_date).to eq(Date.today + 2.months)
    expect(record.category.name).to eq("Entertainment")
  end

  it "shows error on negative amount" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.new_recurring_expense do |exp|
        exp.amount.set(-800)
      end
      cat.submit
    end

    section = page.find("#rec_exp_0")
    expect(section).to have_content("must be positive")
    expect(RecurringExpense.count).to eq(0)
  end

  it "shows error on zero amount" do
    Page::Category.find_by_name("Sport").edit do |cat|
      cat.new_recurring_expense do |exp|
        exp.amount.set(0)
      end
      cat.submit
    end

    section = page.find("#rec_exp_0")
    expect(section).to have_content("must be positive")
    expect(RecurringExpense.count).to eq(0)
  end

  it "doesn't create the recurring expense on category name error" do
    Page::Category.new_category do |cat|
      cat.name.set("")
      cat.new_recurring_expense do |exp|
        exp.amount.set(12000)
        exp.frequency.select("Yearly")
      end
      cat.submit
    end

    expect(Page::Elements::ActionPanel.error("name")).to eq("can't be blank")
    expect(RecurringExpense.count).to eq(0)
  end
end
