require 'rails_helper'

RSpec.describe "Show goals", js: true do
  include_context "user login"

  before(:each) do
    Goal.create(user: user, title: "Vacation", target_amount: "300000")
    Goal.create(user: user, title: "Furniture", target_amount: "35000",
                achieved: true)
    Goal.create(user: user, title: "School", target_amount: "50000")
    Goal.create(user: user, title: "New laptop", target_amount: "300000")

    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "separates achieved goals" do
    goals = Page::Goal.get_goal_titles
    expect(goals).to eq(["Vacation", "School", "New laptop", "Furniture"])
  end

  it "updates goal order on removing achieved flag" do
    Page::Goal.find_by_title("Furniture").edit do |g|
      g.achieved.toggle
      g.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Goal.get_goal_titles)
      .to eq(["Vacation", "Furniture", "School", "New laptop"])
  end

  it "updates goal order on adding achieved flag" do
    Page::Goal.find_by_title("New laptop").edit do |g|
      g.achieved.toggle
      g.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Goal.get_goal_titles)
      .to eq(["Vacation", "School", "Furniture", "New laptop"])
  end
end
