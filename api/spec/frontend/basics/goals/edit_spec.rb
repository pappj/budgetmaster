require 'rails_helper'

RSpec.describe "Edit goals", js: true do
  include_context "user login"

  before(:each) do
    login_user
  end

  context "when both targets are set" do
    def get_date
      Date.today + 8.months
    end

    before(:each) do
      Goal.create(user: user, title: "Vacation", target_amount: 2600,
                  target_date: get_date)
      visit "/basics"
      expect(page).to have_content("Vacation")
    end

    it "edits the goal successfully" do
      new_date = Date.today + 2.years
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.title.set("New Car")
        g.target_amount.set(13000)
        select_date(g.target_date, new_date)
        g.submit
      end

      wait_for_action_panel_to_close

      goal = Page::Goal.first
      expect(goal.title).to eq("New Car")
      expect(goal.target).to eq(new_date.as_json)
      expect(Page::Goal.count).to eq(1)
    end

    it "clears target date" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Vacation").target.amount).to eq("2600")
    end

    it "shows error on invalid values" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.title.set("")
        g.target_amount.set(-500)
        g.submit
      end

      expect(Page::Elements::ActionPanel.error('title')).to eq("can't be blank")
      expect(Page::Elements::ActionPanel.error('target_amount'))
        .to eq("must be positive")
    end

    it "shows error on zero target amount" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.target_amount.set(0)
        g.submit
      end

      expect(Page::Elements::ActionPanel.error('target_amount'))
        .to eq("must be positive")
    end

    it "edits achieved flag" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.achieved.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      flag = Goal.first.achieved
      expect(flag).to eq(true)
    end

    it "adds tick to achieved goals" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.achieved.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Vacation").achieved?).to be(true)
    end
  end

  context "when only target amount is set" do
    before(:each) do
      Goal.create(user: user, title: "Renovation", target_amount: "2600",
                  target_date: nil)
      visit "/basics"
      expect(page).to have_content("Renovation")
    end

    it "shows setTargetDate checkbox unchecked" do
      Page::Goal.find_by_title("Renovation").edit do |g|
        expect(g.setTargetDate.value).to be(false)
        expect(g.target_date.disabled?).to be(true)
      end
    end

    it "sets target date" do
      new_date = Date.today + 6.months
      Page::Goal.find_by_title("Renovation").edit do |g|
        g.setTargetDate.toggle
        select_date(g.target_date, new_date)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Renovation").target).to eq(new_date.as_json)
    end

    it "enables target date also if the date is not selected" do
      Page::Goal.find_by_title("Renovation").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Renovation").target)
        .to eq(Date.today.as_json)
    end

    it "also clears target amount" do
      Page::Goal.find_by_title("Renovation").edit do |g|
        g.setTargetAmount.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Renovation").target).to eq("No target")
    end
  end

  context "when only target date is set" do
    def get_date
      Date.today + 8.months
    end

    before(:each) do
      Goal.create(user: user, title: "College", target_amount: nil,
                  target_date: get_date)
      visit "/basics"
      expect(page).to have_content("College")
    end

    it "shows setTargetAmount checkbox unchecked" do
      Page::Goal.find_by_title("College").edit do |g|
        expect(g.setTargetAmount.value).to be(false)
        expect(g.target_amount.disabled?).to be(true)
      end
    end

    it "sets target amount instead of date" do
      Page::Goal.find_by_title("College").edit do |g|
        g.setTargetDate.toggle
        g.setTargetAmount.toggle
        g.target_amount.set(150000)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("College").target.amount).to eq("150000")
    end

    it "also clears target date" do
      Page::Goal.find_by_title("College").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("College").target).to eq("No target")
    end
  end

  context "when no targets are set" do
    def get_date
      Date.today + 2.months
    end

    before(:each) do
      Goal.create(user: user, title: "Retirement", target_amount: nil,
                  target_date: nil)
      visit "/basics"
      expect(page).to have_content("Retirement")
    end

    it "shows target checkboxes unchecked" do
      Page::Goal.find_by_title("Retirement").edit do |g|
        expect(g.setTargetAmount.value).to be(false)
        expect(g.target_amount.disabled?).to be(true)
        expect(g.setTargetDate.value).to be(false)
        expect(g.target_date.disabled?).to be(true)
      end
    end

    it "sets target amount" do
      Page::Goal.find_by_title("Retirement").edit do |g|
        g.setTargetAmount.toggle
        g.target_amount.set(160000)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Retirement").target.amount).to eq("160000")
    end

    it "sets target date" do
      Page::Goal.find_by_title("Retirement").edit do |g|
        g.setTargetDate.toggle
        select_date(g.target_date, get_date)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Retirement").target)
        .to eq(get_date.as_json)
    end
  end

  context "when target date is in the past" do
    def get_date
      Date.today - 20.days
    end

    before(:each) do
      g = Goal.new(user: user, title: "Vacation", target_amount: nil,
                   target_date: get_date)
      g.save(validate: false)
      visit "/basics"
      expect(page).to have_content("Vacation")
    end

    it "shows error on saving with past target date" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.title.set("Birthday presents")
        g.submit
      end

      date = Date.today.strftime('%Y-%m-%d')
      expect(Page::Elements::ActionPanel.error('target_date'))
        .to eq("can't be earlier than #{date}")
    end

    it "saves the goal if it is achieved" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.achieved.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Vacation").achieved?).to be(true)
      goal = Goal.find_by(title: "Vacation")
      expect(goal.achieved).to be(true)
    end

    it "saves the goal if target date is disabled" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Goal.find_by_title("Vacation").target).to eq("No target")
      goal = Goal.find_by(title: "Vacation")
      expect(goal.target_date).to be(nil)
    end
  end

  context "when the goal has an active plan" do
    before(:each) do
      g = Goal.create(user: user, title: "Vacation", target_amount: 400000,
                      target_date: nil)
      g.create_goal_plan(monthly_amount: 50000, target_date: Date.today + 8.months)

      visit "/basics"
      expect(page).to have_content("Vacation")
    end

    it "makes plan obsolete on changing target amount" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.target_amount.set(350000)
        g.submit
      end

      wait_for_action_panel_to_close

      plan = Goal.find_by(title: "Vacation").goal_plan
      expect(plan.obsolete).to be(true)
    end

    it "makes plan obsolete on changing target date" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      plan = Goal.find_by(title: "Vacation").goal_plan
      expect(plan.obsolete).to be(true)
    end

    it "keeps plan active on changing title" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.title.set("Road trip")
        g.submit
      end

      wait_for_action_panel_to_close

      plan = Goal.find_by(title: "Road trip").goal_plan
      expect(plan.obsolete).to be(false)
    end

    it "removes plan when the goal gets achieved" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.achieved.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      plan = Goal.find_by(title: "Vacation").goal_plan
      expect(plan).to be(nil)
    end

    it "shows plan got obsolete if target date is in the future" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetDate.toggle
        select_date(g.target_date, Date.today + 2.months)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Elements::InfoPanel.message)
        .to include("goal has an obsolete plan")
    end

    it "shows plan got obsolete if only target amount is set" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.target_amount.set(420000)
        g.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Elements::InfoPanel.message)
        .to include("goal has an obsolete plan")
    end

    it "doesn't show plan got obsolete if target date is in the current month" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetDate.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect{Page::Elements::InfoPanel.message}
        .to raise_error(Capybara::ElementNotFound)
    end

    it "doesn't show plan got obsolete if goal is achieved" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.achieved.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect{Page::Elements::InfoPanel.message}
        .to raise_error(Capybara::ElementNotFound)
    end

    it "doesn't show plan got obsolete if there are no targets" do
      Page::Goal.find_by_title("Vacation").edit do |g|
        g.setTargetAmount.toggle
        g.submit
      end

      wait_for_action_panel_to_close

      expect{Page::Elements::InfoPanel.message}
        .to raise_error(Capybara::ElementNotFound)
    end
  end
end
