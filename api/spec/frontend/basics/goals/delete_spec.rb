require 'rails_helper'

RSpec.describe "Delete goal", js: true do
  include_context "user login"

  before(:each) do
    login_user
  end

  it "removes the goal" do
    Goal.create(user: user, title: "Vacation", target_amount: "2600",
                target_date: Date.today + 1.month)
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.find_by_title("Vacation").delete
    wait_for_action_panel_to_close

    expect(Page::Goal.count).to eq(0)

    goal = Goal.find_by(title: "Vacation")
    expect(goal).to be(nil)
  end

  it "removes the goal, if it has a plan" do
    g = Goal.create(user: user, title: "Vacation", target_amount: "2600",
                    target_date: Date.today + 1.month)
    g.create_goal_plan(monthly_amount: 2600)
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.find_by_title("Vacation").delete
    wait_for_action_panel_to_close

    expect(Page::Goal.count).to eq(0)

    goal = Goal.find_by(title: "Vacation")
    expect(goal).to be(nil)
  end

  it "doesn't remove the goal on cancel" do
    Goal.create(user: user, title: "Vacation", target_amount: "2600",
                target_date: Date.today + 1.month)
    visit "/basics"
    wait_for_page_to_load

    Page::Goal.find_by_title("Vacation").delete("No")
    wait_for_action_panel_to_close

    expect(Page::Goal.first.title).to eq("Vacation")
    goal = Goal.find_by(title: "Vacation")
    expect(goal).not_to be(nil)
  end
end
