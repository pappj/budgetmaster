require 'rails_helper'

RSpec.describe "New goal", js: true do
  include_context "user login"

  before(:each) do
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "adds new goal on success" do
    target_date = Date.today + 2.years
    Page::Goal.new_goal do |g|
      g.title.set("New car")
      g.target_amount.set(1850000)
      select_date(g.target_date, target_date)
      g.submit
    end

    wait_for_action_panel_to_close

    goal = Page::Goal.first
    expect(goal.title).to eq("New car")
    expect(goal.target).to eq(target_date.as_json)
  end

  it "adds new goal without target date" do
    Page::Goal.new_goal do |g|
      g.title.set("New car")
      g.target_amount.set(1850000)
      g.setTargetDate.toggle
      g.submit
    end

    wait_for_action_panel_to_close

    goal = Page::Goal.first
    expect(goal.title).to eq("New car")
    expect(goal.target.amount).to eq("1850000")
  end

  it "adds new goal without any target" do
    Page::Goal.new_goal do |g|
      g.title.set("New car")
      g.setTargetAmount.toggle
      g.setTargetDate.toggle
      g.submit
    end

    wait_for_action_panel_to_close

    goal = Page::Goal.first
    expect(goal.title).to eq("New car")
    expect(goal.target).to eq("No target")
  end

  it "shows errors on wrong values" do
    Page::Goal.new_goal do |g|
      g.title.set("")
      g.target_amount.set(-500)
      g.submit
    end

    expect(Page::Elements::ActionPanel.error('title')).to eq("can't be blank")
    expect(Page::Elements::ActionPanel.error('target_amount'))
      .to eq("must be positive")
    expect(Page::Goal.count).to eq(0)
  end

  it "shows errors on zero target amount" do
    Page::Goal.new_goal do |g|
      g.target_amount.set(0)
      g.submit
    end

    expect(Page::Elements::ActionPanel.error('target_amount'))
      .to eq("must be positive")
  end

  it "doesn't add goal on cancel" do
    Page::Goal.new_goal do |g|
      g.title.set("New car")
      g.target_amount.set(1850000)
      g.cancel
    end

    expect(page).not_to have_selector("#action-panel")

    expect(Page::Goal.count).to eq(0)
  end

  it "disables achieved field" do
    Page::Goal.new_goal do |g|
      expect(g.achieved.disabled?).to be(true)
    end
  end
end
