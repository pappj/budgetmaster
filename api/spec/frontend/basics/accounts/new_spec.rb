require 'rails_helper'

RSpec.describe "New account", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "adds new account on success" do
    Page::Account.new_account do |acc|
      acc.name.set("Erste account")
      acc.account_type.select("Cash")
      acc.currency.select("HUF")
      acc.current_balance.set(1680)
      acc.in_plan.toggle
      acc.submit
    end

    wait_for_action_panel_to_close

    acc = Page::Account.first
    expect(acc.name).to eq("Erste account")
    expect(acc.balance.currency).to eq("HUF")
    expect(acc.balance.amount).to eq("1680")
    expect(Page::Account.count).to eq(1)

    account = Account.find_by(name: "Erste account")
    expect(account).not_to be(nil)
    expect(account.user).to eq(user)
    expect(account.currency).to eq(huf)
    expect(account.account_type).to eq("cash")
  end

  it "shows error on blank name" do
    Page::Account.new_account do |acc|
      acc.name.set("")
      acc.submit
    end

    expect(page).to have_content("can't be blank")
  end

  it "sets credit to 0, when Cash is selected as type" do
    Page::Account.new_account do |acc|
      acc.credit.set(15000)
      acc.account_type.select("Cash")

      expect(acc.credit.amount).to eq("0")
    end
  end

  it "disables credit field for cash" do
    Page::Account.new_account do |acc|
      acc.account_type.select("Cash")

      expect(acc.credit.disabled?).to be(true)
    end
  end

  it "shows error on negative credit" do
    Page::Account.new_account do |acc|
      acc.name.set("Erste account")
      acc.account_type.select("Credit card")
      acc.currency.select("HUF")
      acc.current_balance.set(1680)
      acc.credit.set(-500)
      acc.in_plan.toggle
      acc.submit

      expect(Page::Elements::ActionPanel.error('credit'))
        .to eq("can't be negative")
      expect(acc.name.value).to eq("Erste account")
      expect(acc.account_type.value).to eq("credit")
      expect(acc.current_balance.amount).to eq("1680")
      expect(acc.credit.amount).to eq("-500")
      expect(acc.in_plan.value).to be(true)
    end
  end

  it "shows error if credit balance exceeds the limit" do
    Page::Account.new_account do |acc|
      acc.name.set("Main card")
      acc.account_type.select("Credit card")
      acc.currency.select("HUF")
      acc.current_balance.set(1680)
      acc.credit.set(1600)
      acc.in_plan.toggle
      acc.submit
    end

    expect(Page::Elements::ActionPanel.error('current_balance'))
      .to eq("can't exceed the credit limit for credit cards")
  end

  it "shows error on negative balance" do
    Page::Account.new_account do |acc|
      acc.name.set("Erste account")
      acc.account_type.select("Credit card")
      acc.currency.select("HUF")
      acc.current_balance.set(-1680)
      acc.credit.set(500)
      acc.in_plan.toggle
      acc.submit

      expect(Page::Elements::ActionPanel.error('current_balance'))
        .to eq("can't be negative")
      expect(acc.name.value).to eq("Erste account")
      expect(acc.account_type.value).to eq("credit")
      expect(acc.current_balance.amount).to eq("-1680")
      expect(acc.credit.amount).to eq("500")
      expect(acc.in_plan.value).to be(true)
    end
  end

  it "disables in_plan for non main currency" do
    Page::Account.new_account do |acc|
      acc.name.set("Erste account")
      acc.account_type.select("Credit card")
      acc.currency.select("EUR")

      expect(acc.in_plan.disabled?).to be(true)
    end
  end
end
