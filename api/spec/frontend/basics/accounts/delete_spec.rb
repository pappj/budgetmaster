require 'rails_helper'

RSpec.describe "Delete account", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Sample account", account_type: "bank", currency: huf,
                   current_balance: 146300, credit: 0, in_plan: false, user: user)
    login_user
    visit "/basics"
    expect(page).to have_content("Sample account")

    within("#accounts") do
      click_button('Delete')
    end
  end

  it "removes the account" do
    within("#action-panel") do
      click_button('Yes')
    end

    wait_for_action_panel_to_close

    expect(Page::Account.count).to eq(0)
    account = Account.find_by(name: "Sample account")
    expect(account).to be(nil)
  end

  it "doesn't remove the account on cancel" do
    within("#action-panel") do
      click_button('No')
    end

    wait_for_action_panel_to_close

    expect(Page::Account.first.name).to eq("Sample account")
    account = Account.find_by(name: "Sample account")
    expect(account).not_to be(nil)
  end
end
