require 'rails_helper'

RSpec.describe "Edit account", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Sample account", account_type: "other", currency: huf,
                   current_balance: 146300, credit: 0, in_plan: false, user: user)
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "edits the account successfully" do
    Page::Account.find_by_name("Sample account").edit do |acc|
      acc.name.set("Another name")
      acc.account_type.select("Cash")
      acc.currency.select("EUR")
      acc.current_balance.set(1680)
      acc.submit
    end

    wait_for_action_panel_to_close

    acc = Page::Account.first
    expect(acc.name).to eq("Another name")
    expect(acc.balance.currency).to eq("EUR")
    expect(acc.balance.amount).to eq("1680")
    expect(Page::Account.count).to eq(1)
  end

  it "resets the account on cancel" do
    Page::Account.find_by_name("Sample account").edit do |acc|
      acc.name.set("Another name")
      acc.account_type.select("Cash")
      acc.currency.select("EUR")
      acc.current_balance.set(1680)
      acc.cancel
    end

    wait_for_action_panel_to_close

    acc = Page::Account.first
    expect(acc.name).to eq("Sample account")
    expect(acc.balance.currency).to eq("HUF")
    expect(acc.balance.amount).to eq("146300")
    expect(Page::Account.count).to eq(1)
  end

  it "shows the correct account type" do
    Page::Account.find_by_name("Sample account").edit do |acc|
      expect(acc.account_type.value).to eq("other")
    end
  end
end
