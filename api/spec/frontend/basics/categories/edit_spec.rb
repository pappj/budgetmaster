require 'rails_helper'

RSpec.describe "Edit category", js: true do
  include_context "user login"

  before(:each) do
    Category.create(name: "Car maintenance", user: user)

    login_user

    visit "/basics"
    wait_for_page_to_load
  end

  it "edits the category successfully" do
    Page::Category.find_by_name("Car maintenance").edit do |cat|
      cat.name.set("Another name")
      cat.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.first.name).to eq("Another name")
    expect(Page::Category.count).to eq(1)
  end

  it "resets the category on cancel" do
    Page::Category.find_by_name("Car maintenance").edit do |cat|
      cat.name.set("Another name")
      cat.cancel
    end

    wait_for_action_panel_to_close

    expect(Page::Category.first.name).to eq("Car maintenance")
    expect(Page::Category.count).to eq(1)
  end
end
