require 'rails_helper'

RSpec.describe "Delete category", js: true do
  include_context "user login"

  before(:each) do
    login_user
  end

  it "removes the category" do
    Category.create(name: "Fast food", user: user)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Fast food").delete
    wait_for_action_panel_to_close

    expect(Page::Category.count).to eq(0)
    category = Category.find_by(name: "Fast food")
    expect(category).to be(nil)
  end

  it "removes the category with expense plan" do
    c = Category.create(name: "Fast food", user: user)
    c.expense_plans.create(amount: 12000, month: Date.today)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Fast food").delete
    wait_for_action_panel_to_close

    expect(Page::Category.count).to be(0)
    expect(Category.find_by(name: "Fast food")).to be(nil)
    expect(ExpensePlan.count).to be(0)
  end

  it "doesn't remove the category on cancel" do
    Category.create(name: "Fast food", user: user)
    visit "/basics"
    wait_for_page_to_load

    Page::Category.find_by_name("Fast food").delete("No")
    wait_for_action_panel_to_close

    expect(Page::Category.first.name).to eq("Fast food")
    category = Category.find_by(name: "Fast food")
    expect(category).not_to be(nil)
  end
end
