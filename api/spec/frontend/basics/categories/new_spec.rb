require 'rails_helper'

RSpec.describe "New category", js: true do
  include_context "user login"

  before(:each) do
    login_user
    visit "/basics"
    wait_for_page_to_load
  end

  it "adds new category on success" do
    Page::Category.new_category do |cat|
      cat.name.set("Clothes")
      cat.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.first.name).to eq("Clothes")

    category = Category.find_by(name: "Clothes")
    expect(category).not_to be(nil)
    expect(category.user).to eq(user)
  end

  it "shows error on blank name" do
    Page::Category.new_category do |cat|
      cat.name.set("")
      cat.submit
    end

    expect(Page::Elements::ActionPanel.error('name')).to eq("can't be blank")
  end
end
