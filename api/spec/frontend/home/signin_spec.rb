require 'rails_helper'

RSpec.describe "Sign in", js: true do
  include_context "models"

  before(:each) do
    visit "/"
  end

  it "shows an error if credentials are wrong" do
    within("#sign-in") do
      fill_in 'Email', with: 'test@user.com'
      fill_in 'Password', with: 'dummy-pwd'
      click_button 'Sign in'
    end

    expect(page).to have_content("Invalid Email or password")
  end

  context "when the user really exists" do
    before do
      User.create(email: "good@boy.com", password: "verysecured", currency: huf)
    end

    it "signs in successfully" do
      within("#sign-in") do
        fill_in 'Email', with: 'good@boy.com'
        fill_in 'Password', with: 'verysecured'
        click_button 'Sign in'
      end

      wait_for_page_to_load

      expect(page.current_path).to eq("/summary")
    end

    it "signs out successfully" do
      within("#sign-in") do
        fill_in 'Email', with: 'good@boy.com'
        fill_in 'Password', with: 'verysecured'
        click_button 'Sign in'
      end

      click_button("toggle-menu")
      within(".menu") do
        click_button("Sign out")
      end

      expect(page).to have_current_path("/")
      expect(page).not_to have_selector("#toggle-menu")
    end

    it "shows Summary as default page" do
      within("#sign-in") do
        fill_in 'Email', with: 'good@boy.com'
        fill_in 'Password', with: 'verysecured'
        click_button 'Sign in'
      end

      visit "/"
      wait_for_page_to_load

      expect(page.current_path).to eq("/summary")
    end
  end
end
