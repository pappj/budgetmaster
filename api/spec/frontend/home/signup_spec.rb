require 'rails_helper'

RSpec.describe "Sign up", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    visit "/signup"
  end

  it "redirects to /basics on success" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'totallysecure'
      click_button 'Sign up'
    end

    expect(page).to have_no_selector("#sign-up")
    expect(page.current_path).to eq("/basics")

    user = User.find_by(email: "user@test.com")
    expect(user).not_to be(nil)
  end

  it "shows error when passwords don't match" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'somethingelse'
      click_button 'Sign up'
    end

    expect(page).to have_content("Passwords don't match")
  end

  it "shows error when password is too short" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'ok'
      fill_in 'Password again', with: 'ok'
      click_button 'Sign up'
    end

    expect(page).to have_content("Too short")
  end

  it "shows error if the email is not correct" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'totallysecure'
      click_button 'Sign up'
    end

    expect(page).to have_content("Invalid email address")
  end

  it "User can go back to home page" do
    find(:xpath, "//a[text()='Back to the home page']").click

    expect(page.current_path).to  eq("/")
  end

  it "redirects if the user is signed in" do
    login(user)
    visit "/signup"
    wait_for_page_to_load

    expect(page.current_path).to  eq("/basics")
  end

  context "when the user already exists" do
    before do
      User.create(email: "user@test.com", password: "buzzword", currency: usd)
    end

    it "shows error for the same email address" do
      within("#sign-up") do
        fill_in 'Email', with: 'user@test.com'
        fill_in 'Password', with: 'totallysecure'
        fill_in 'Password again', with: 'totallysecure'
        click_button 'Sign up'
      end

      expect(page).to have_content("has already been taken")
    end
  end
end
