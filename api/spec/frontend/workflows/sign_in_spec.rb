require 'rails_helper'

RSpec.describe "Another user signs in", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Goal.create(user: user, title: "Vacation", target_amount: 190000,
                target_date: Date.today + 6.months)
    Account.create(name: "Sample account", account_type: "bank", currency: huf,
                   current_balance: 146300, credit: 0, in_plan: false, user: user)
    Category.create(user: user, name: "Food")
    Category.create(user: user, name: "Household")
    Category.create(user: user, name: "Movies")
    Category.create(user: user, name: "Car")

    Account.create(name: "Western union", account_type: "credit", currency: eur,
                   current_balance: 346, credit: 1500, in_plan: false,
                   user: other_user)
    Category.create(user: other_user, name: "Sport")
    Category.create(user: other_user, name: "Clothing")
    Category.create(user: other_user, name: "Watches")
    Category.create(user: other_user, name: "Hotels")
  end

  it "shows the right details of the respective user" do
    login_user
    visit "/basics"
    wait_for_page_to_load

    expect(page).to have_content("Vacation")
    expect(page).to have_content("146300")
    expect(page).to have_content("HUF")
    expect(page).to have_content("Food")
    expect(page).to have_content("Car")

    logout_user

    login_other_user
    visit "/basics"
    wait_for_page_to_load

    expect(page).to have_content("You have no goals")
    expect(page).to have_content("346")
    expect(page).to have_content("€")
    expect(page).to have_content("Watches")
    expect(page).to have_content("Hotels")
  end
end
