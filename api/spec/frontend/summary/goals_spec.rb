require 'rails_helper'

RSpec.describe "Summary goals", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Goal.create(user: user, title: "Vacation", target_amount: "2600",
                target_date: Date.today + 6.months, achieved: true)
    Goal.create(user: user, title: "Car maintenance",
                target_date: Date.today + 3.months)
    login_user
    Page::Summary.visit
  end

  it "doesn't show achieved goals" do
    expect(Page::Goal.first.title).to eq("Car maintenance")
    expect(Page::Goal.count).to eq(1)
  end
end
