require 'rails_helper'

RSpec.describe "Summary accounts", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Bank", account_type: "bank", current_balance: 48000,
                   credit: 100000, currency: huf, user: user)
    Account.create(name: "Credit", account_type: "credit", current_balance: 16000,
                   credit: 50000, currency: huf, user: user)
    Account.create(name: "Cash", account_type: "cash", current_balance: 23500,
                   credit: 0, currency: huf, user: user)
    Account.create(name: "Other", account_type: "other", current_balance: 15400,
                   credit: 10000, currency: huf, user: user)

    login_user
    Page::Summary.visit
  end

  it "shows credit balance by default" do
    expect(Page::Account.find_by_name("Credit").balance.amount)
      .to eq("16000")
  end

  it "shows credit debt on clicking credit card amount" do
    Page::Account.find_by_name("Credit").balance.amount_node.click

    expect(Page::Account.find_by_name("Credit").balance.amount)
      .to eq("34000")
  end

  it "shows credit debt on clicking credit card currency" do
    Page::Account.find_by_name("Credit").balance.currency_node.click

    expect(Page::Account.find_by_name("Credit").balance.amount)
      .to eq("34000")
  end

  it "shows credit card balance on clicking on debt" do
    Page::Account.find_by_name("Credit").balance.currency_node.click
    Page::Account.find_by_name("Credit").balance.currency_node.click

    expect(Page::Account.find_by_name("Credit").balance.amount)
      .to eq("16000")
  end

  it "doesn't change bank account balance on click" do
    Page::Account.find_by_name("Bank").balance.currency_node.click

    expect(Page::Account.find_by_name("Bank").balance.amount)
      .to eq("48000")
  end

  it "doesn't change cash balance on click" do
    Page::Account.find_by_name("Cash").balance.currency_node.click

    expect(Page::Account.find_by_name("Cash").balance.amount)
      .to eq("23500")
  end

  it "doesn't change other account balance on click" do
    Page::Account.find_by_name("Other").balance.currency_node.click

    expect(Page::Account.find_by_name("Other").balance.amount)
      .to eq("15400")
  end
end
