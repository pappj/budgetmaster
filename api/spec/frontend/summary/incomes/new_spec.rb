require 'rails_helper'

RSpec.describe "New income", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Sample account", account_type: "other",
                   currency: huf, current_balance: 146300, credit: 0,
                   in_plan: false, user: user)
    login_user
    Page::Summary.visit
  end

  it "adds the amount to the selected account" do
    Page::Summary.new_income do |inc|
      inc.account.select("Sample account")
      inc.amount.set(3700)
      select_date(inc.date, Date.today)
      inc.note.set("Paycheck")
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Account.find_by_name("Sample account").balance.amount)
      .to eq("150000")
  end

  it "shows error on invalid values" do
    Page::Summary.new_income do |inc|
      inc.account.select("Sample account")
      inc.amount.set(-3700)
      inc.submit
    end

    expect(Page::Elements::ActionPanel.error('amount')).to eq("must be positive")
  end

  it "shows error on zero amount" do
    Page::Summary.new_income do |inc|
      inc.account.select("Sample account")
      inc.amount.set(0)
      inc.submit
    end

    expect(Page::Elements::ActionPanel.error('amount')).to eq("must be positive")
  end

  it "doesn't change account balance on cancel" do
    Page::Summary.new_income do |inc|
      inc.account.select("Sample account")
      inc.amount.set(3700)
      inc.note.set("Paycheck")
      inc.cancel
    end

    wait_for_action_panel_to_close

    expect(Page::Account.find_by_name("Sample account").balance.amount)
      .to eq("146300")
  end

  it "can't be added for a future date" do
    Page::Summary.new_income do |inc|
      inc.amount.set(35000)
      expect{select_date(inc.date, Date.today + 5.days)}
        .to raise_error(RuntimeError, "Can't select date")
    end
  end

  it "doesn't update the account, when it's not checked" do
    Page::Summary.new_income do |inc|
      inc.account.select("Sample account")
      inc.amount.set(3700)
      select_date(inc.date, Date.today)
      inc.update_account.toggle
      inc.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Account.find_by_name("Sample account").balance.amount)
      .to eq("146300")
    expect(Income.first.amount).to eq(3700)
  end
end
