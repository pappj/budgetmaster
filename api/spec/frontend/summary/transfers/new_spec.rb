require 'rails_helper'

RSpec.describe "New transfer", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Huf1", account_type: "bank",
                   currency: huf, current_balance: 150000, credit: 0,
                   in_plan: false, user: user)
    Account.create(name: "Huf2", account_type: "cash",
                   currency: huf, current_balance: 150000, credit: 0,
                   in_plan: false, user: user)
    Account.create(name: "Huf3", account_type: "other",
                   currency: huf, current_balance: 150000, credit: 0,
                   in_plan: false, user: user)
    Account.create(name: "Usd1", account_type: "bank",
                   currency: usd, current_balance: 1500, credit: 0,
                   in_plan: false, user: user)
    login_user
    Page::Summary.visit
  end

  it "can't select a future date" do
    Page::Summary.new_transfer do |tr|
      expect{select_date(tr.date, Date.today + 5.days)}
        .to raise_error(RuntimeError, "Can't select date")
    end
  end

  it "can't select the same accounts" do
    Page::Summary.new_transfer do |tr|
      tr.target_account.select("Huf3")
      tr.source_account.select("Huf3")
      options = tr.target_account.all("option").map(&:text)
      expect(options).not_to include('Huf3')
    end
  end

  context "when transfering between the same currencies" do
    it "sets target amount automatically" do
      Page::Summary.new_transfer do |tr|
        tr.source_amount.set(5000)
        expect(tr.target_amount.amount).to eq("5000")
        expect(tr.target_amount.disabled?).to be(true)
      end
    end

    it "updates the accounts properly" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Huf2")
        tr.source_amount.set(20000)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Account.find_by_name('Huf1').balance.amount).to eq("130000")
      expect(Page::Account.find_by_name('Huf2').balance.amount).to eq("170000")
    end

    it "can't transfer more money than what is available" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Huf2")
        tr.source_amount.set(200000)
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error('source_amount'))
        .to eq("must be at most 150000 or 'Huf1' must cover it or don't update the accounts")
    end

    it "can transfer more money than available, if the accounts are not updated" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Huf2")
        tr.source_amount.set(200000)
        tr.update_account.toggle
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Account.find_by_name('Huf1').balance.amount).to eq("150000")
      expect(Page::Account.find_by_name('Huf2').balance.amount).to eq("150000")
      expect(Transfer.first.source_amount).to eq(200000)
    end

    it "doesn't change accounts on cancel" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Huf2")
        tr.source_amount.set(200000)
        tr.cancel
      end

      wait_for_action_panel_to_close

      expect(Page::Account.find_by_name('Huf1').balance.amount).to eq("150000")
      expect(Page::Account.find_by_name('Huf2').balance.amount).to eq("150000")
    end
  end

  context "when transfering between different currencies" do
    it "updates the accounts properly" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Usd1")
        tr.source_amount.set(20000)
        tr.target_amount.set(60)
        select_date(tr.date, Date.today - 3.weeks)
        tr.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Account.find_by_name('Huf1').balance.amount).to eq("130000")
      expect(Page::Account.find_by_name('Usd1').balance.amount).to eq("1560")
    end

    it "can't transfer more money than what is available" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Usd1")
        tr.source_amount.set(200000)
        tr.target_amount.set(600)
        tr.submit
      end

      expect(Page::Elements::ActionPanel.error('source_amount'))
        .to eq("must be at most 150000 or 'Huf1' must cover it or don't update the accounts")
    end

    it "doesn't change accounts on cancel" do
      Page::Summary.new_transfer do |tr|
        tr.source_account.select("Huf1")
        tr.target_account.select("Usd1")
        tr.source_amount.set(20000)
        tr.target_amount.set(60)
        tr.cancel
      end

      wait_for_action_panel_to_close

      expect(Page::Account.find_by_name('Huf1').balance.amount).to eq("150000")
      expect(Page::Account.find_by_name('Usd1').balance.amount).to eq("1500")
    end
  end
end
