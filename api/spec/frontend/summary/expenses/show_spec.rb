require 'rails_helper'

RSpec.describe "Show expenses", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    acc = Account.create(name: "Sample", account_type: "bank", currency: huf,
                         current_balance: 12000, credit: 0, in_plan: true,
                         user: user)
    cat = Category.create(name: "Food", user: user)
    cat2 = Category.create(name: "Entertainment", user: user)
    cat3 = Category.create(name: "Mortgage", user: user)

    Expense.create(account: acc, category: cat, amount: 500, date: Date.today)
    Expense.create(account: acc, category: cat, amount: 800, date: Date.today)
    Expense.create(account: acc, category: cat, amount: 500,
                   date: Date.today - 2.months)
    Expense.create(account: acc, category: cat2, amount: 200,
                   date: Date.today - 6.months)

    cat2.expense_plans.create(amount: 15500, month: Date.today)
    cat3.expense_plans.create(amount: 15500, month: Date.today + 1.month)

    login_user
    Page::Summary.visit
  end

  it "shows the amount spent this month" do
    expect(Page::Category.find_by_name("Food").spent("HUF").amount)
      .to eq("1300")
  end

  it "shows zero for categories with expenses only in older months" do
    expect(Page::Category.find_by_name("Entertainment").spent("HUF").amount)
      .to eq("0")
  end

  it "shows zero for categories without expenses" do
    expect(Page::Category.find_by_name("Mortgage").spent("HUF").amount)
      .to eq("0")
  end

  it "shows zero planned amount, if there is no plan" do
    expect(Page::Category.find_by_name("Food").planned.amount)
      .to eq("0")
  end

  it "shows the planned amount" do
    expect(Page::Category.find_by_name("Entertainment").planned.amount)
      .to eq("15500")
  end

  it "doesn't show future plans" do
    expect(Page::Category.find_by_name("Mortgage").planned.amount)
      .to eq("0")
  end
end
