require 'rails_helper'

RSpec.describe "New expense", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    acc = Account.create(name: "Sample 1", account_type: "bank", currency: huf,
                         current_balance: 12000, credit: 10000, in_plan: true,
                         user: user)
    Account.create(name: "Sample 2", account_type: "bank", currency: usd,
                   current_balance: 9000, credit: 0, in_plan: false, user: user)
    Account.create(name: "Sample 3", account_type: "credit", currency: usd,
                   current_balance: 900, credit: 1000, in_plan: false, user: user)

    cat = Category.create(name: "Food", user: user)
    cat2 = Category.create(name: "Entertainment", user: user)
    Category.create(name: "Mortgage", user: user)

    Expense.create(account: acc, category: cat, amount: 500, date: Date.today)
    Expense.create(account: acc, category: cat, amount: 800, date: Date.today)
    Expense.create(account: acc, category: cat, amount: 500,
                   date: Date.today - 2.months)
    Expense.create(account: acc, category: cat2, amount: 200,
                   date: Date.today - 6.months)

    login_user
    Page::Summary.visit
  end

  it "adds the first expense in the month to a category" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Mortgage")
      exp.amount.set(10000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Mortgage").spent("HUF").amount)
      .to eq("10000")
  end

  it "subtracts the amount from the account" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(1000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Account.find_by_name("Sample 1").balance.amount).to eq("11000")

    acc_model = Account.find_by(name: "Sample 1")
    expect(acc_model.current_balance).to eq(11000)
  end

  it "adds the amount to the spent amount" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(1000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Food").spent("HUF").amount).to eq("2300")
  end

  it "creates a new entry for a new currency" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 2")
      exp.category.select("Food")
      exp.amount.set(30)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Food").spent("HUF").amount).to eq("1300")
    expect(Page::Category.find_by_name("Food").spent("USD").amount).to eq("30")
  end

  it "doesn't change the spent amount if added to a different month" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(1000)
      select_date(exp.date, Date.today - 2.months)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Food").spent("HUF").amount).to eq("1300")
  end

  it "can't set negative amount" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(-100)
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error('amount')).to eq("must be positive")
  end

  it "can't set zero amount" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(0)
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error('amount')).to eq("must be positive")
  end

  it "can't spend more money than available" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(25000)
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error('amount'))
      .to eq("must be at most 22000 or 'Sample 1' must cover it or don't update the accounts")
  end

  it "can be more than available, if the account is not updated" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(25000)
      exp.update_account.toggle
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Food").spent("HUF").amount).to eq("26300")
    expect(Page::Account.find_by_name("Sample 1").balance.amount).to eq("12000")
  end

  it "can spend bank account's credit" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(15000)
      exp.submit
    end

    wait_for_action_panel_to_close

    expect(Page::Category.find_by_name("Food").spent("HUF").amount).to eq("16300")
  end

  it "can't spend more than the credit balance" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 3")
      exp.category.select("Food")
      exp.amount.set(950)
      exp.submit
    end

    expect(Page::Elements::ActionPanel.error('amount'))
      .to eq("must be at most 900 or 'Sample 3' must cover it or don't update the accounts")
  end

  it "can't be added for a future date" do
    Page::Summary.new_expense do |exp|
      exp.account.select("Sample 1")
      exp.category.select("Food")
      exp.amount.set(1000)
      expect{select_date(exp.date, Date.today + 5.days)}
        .to raise_error(RuntimeError, "Can't select date")
    end
  end

  context "when it has an active plan" do
    before(:each) do
      Category.find_by(name: "Food").expense_plans.create(amount: 6000,
        month: Date.today.change(day: 1))
      visit "/summary"
      wait_for_page_to_load
    end

    it "subtracts the amount from the plan" do
      Page::Summary.new_expense do |exp|
        exp.account.select("Sample 1")
        exp.category.select("Food")
        exp.amount.set("2300")
        exp.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Category.find_by_name("Food").planned.amount).to eq("3700")
    end

    it "doesn't change the plan, if account is not in the plan" do
      Page::Summary.new_expense do |exp|
        exp.account.select("Sample 2")
        exp.category.select("Food")
        exp.amount.set("2300")
        exp.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Category.find_by_name("Food").planned.amount).to eq("6000")
    end

    it "doesn't change the plan, if expense is added to an older month" do
      Page::Summary.new_expense do |exp|
        exp.account.select("Sample 1")
        exp.category.select("Food")
        exp.amount.set("2300")
        select_date(exp.date, Date.today - 1.month)
        exp.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Category.find_by_name("Food").planned.amount).to eq("6000")
    end

    it "sets plan to zero, if overspending" do
      Page::Summary.new_expense do |exp|
        exp.account.select("Sample 1")
        exp.category.select("Food")
        exp.amount.set("9300")
        exp.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Category.find_by_name("Food").planned.amount).to eq("0")
    end
  end
end
