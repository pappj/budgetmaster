require 'rails_helper'

RSpec.describe "Planning Statusbar", js: true do
  include_context "user login"
  include_context "models"

  def open_planning
    login_user
    Page::Planning.visit
  end

  it "shows if no accounts included" do
    open_planning
    expect(Page::Planning::Statusbar.find.message).to eq("No accounts included in your plan")
  end

  context "when plan is overallocated" do
    before(:each) do
      Account.create(user: user, name: "Test", account_type: "credit",
                     currency: huf, current_balance: 88000, credit: 100000,
                     in_plan: true)
      open_planning
    end

    it "shows the right message" do
      expect(Page::Planning::Statusbar.find.message).to eq("Overallocated by")
    end

    it "shows the right amount (subtracts spent credit from plan)" do
      expect(Page::Planning::Statusbar.find.money.amount).to eq("12000")
    end

    it "shows the message in red" do
      expect(Page::Planning::Statusbar.find.message_warning?).to be(true)
    end
  end

  context "when all money is spent" do
    before(:each) do
      Account.create(user: user, name: "Test", account_type: "cash", currency: huf,
                     current_balance: 0, credit: 0, in_plan: true)
      open_planning
    end

    it "shows the right message" do
      expect(Page::Planning::Statusbar.find.message).to eq("All of your money is planned")
    end

    it "shows the message in green" do
      expect(Page::Planning::Statusbar.find.message_success?).to be(true)
    end
  end

  context "when there is money to allocate" do
    before(:each) do
      Account.create(user: user, name: "Test", account_type: "bank", currency: huf,
                     current_balance: 87300, credit: 10000, in_plan: true)
      Account.create(user: user, name: "Test", account_type: "cash", currency: huf,
                     current_balance: 5980, credit: 0, in_plan: true)
      open_planning
    end

    it "shows the right message" do
      expect(Page::Planning::Statusbar.find.message).to eq("Amount to allocate:")
    end

    it "shows the right amount (adds multiple accounts)" do
      expect(Page::Planning::Statusbar.find.money.amount).to eq("93280")
    end
  end
end
