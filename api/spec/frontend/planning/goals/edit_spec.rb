require 'rails_helper'

RSpec.describe "Edit goals plan", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Main account", account_type: 'bank', currency: huf,
                   current_balance: 7320000, credit: 0, in_plan: true, user: user)

    login_user
  end

  context "when both targets are set and the goal is underallocated" do
    before(:each) do
      g = Goal.create(user: user, title: "Vacation", target_amount: 600000,
                      target_date: Date.today + 3.months, allocated_amount: 240000)
      g.create_goal_plan(monthly_amount: 100000)

      Page::Planning.visit
    end

    it "increases the monthly amount" do
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.on_track_amount.amount).to eq("300000")
      goal.edit_plan do |plan|
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Vacation").on_track_amount.amount)
        .to eq("240000")
      expect(GoalPlan.first.monthly_amount).to eq(120000)
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("Vacation").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .not_to include("estimated")
    end
  end

  context "when both targets are set and the goal is overallocated" do
    before(:each) do
      g = Goal.create(user: user, title: "Vacation", target_amount: 500000,
                      target_date: Date.today + 3.months, allocated_amount: 380000)
      g.create_goal_plan(monthly_amount: 100000)

      Page::Planning.visit
    end

    it "decreases the monthly amount" do
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.on_track_amount.amount).to eq("200000")
      goal.edit_plan do |plan|
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Vacation").on_track_amount.amount)
        .to eq("380000")
      expect(GoalPlan.first.monthly_amount).to eq(40000)
    end
  end

  context "when only target amount is set" do
    before(:each) do
      g = Goal.create(user: user, title: "Renovation", target_amount: 600000,
                      allocated_amount: 280000)
      g.create_goal_plan(monthly_amount: 40000, target_date: Date.today + 8.months)

      Page::Planning.visit
    end

    it "shows the monthly amount already set" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        expect(plan.monthly_amount.amount).to eq("40000")
      end
    end

    it "updates the estimated target on changing the monthly amount" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        plan.monthly_amount.set(80000)
        expect(plan.target_date).to eq((Date.today + 4.months).as_json)
      end
    end

    it "updates the estimated target date of the plan" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        plan.monthly_amount.set(80000)
        plan.submit
      end

      wait_for_action_panel_to_close

      d = Date.today + 4.months
      expect(Page::Planning.find_goal("Renovation").target_date)
        .to eq(d.as_json)
      expect(GoalPlan.first.target_date).to eq(d)
    end

    it "shows error on negative monthly amount" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        plan.monthly_amount.set(-30000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('monthly_amount'))
        .to eq("must be positive")
    end

    it "shows error on zero monthly amount" do
      Page::Planning.find_goal("Renovation").edit_plan do |plan|
        plan.monthly_amount.set(0)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('monthly_amount'))
        .to eq("must be positive")
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("Renovation").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .to include("estimated")
    end
  end

  context "when only target date is set" do
    before(:each) do
      g = Goal.create(user: user, title: "Donation", allocated_amount: 380000,
                      target_date: Date.today + 3.months)
      g.create_goal_plan(monthly_amount: 100000, target_amount: 680000)

      Page::Planning.visit
    end

    it "shows the monthly amount already set" do
      Page::Planning.find_goal("Donation").edit_plan do |plan|
        expect(plan.monthly_amount.amount).to eq("100000")
      end
    end

    it "updates the estimated target on changing the monthly amount" do
      Page::Planning.find_goal("Donation").edit_plan do |plan|
        plan.monthly_amount.set(130000)
        expect(plan.target_amount.amount).to eq("770000")
      end
    end

    it "updates the estimated target amount of the plan" do
      Page::Planning.find_goal("Donation").edit_plan do |plan|
        plan.monthly_amount.set(130000)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Donation").target_amount.amount)
        .to eq("770000")
      expect(GoalPlan.first.target_amount).to eq(770000)
    end

    it "shows error on negative monthly amount" do
      Page::Planning.find_goal("Donation").edit_plan do |plan|
        plan.monthly_amount.set(-30000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('monthly_amount'))
        .to eq("must be positive")
    end

    it "shows error on zero monthly amount" do
      Page::Planning.find_goal("Donation").edit_plan do |plan|
        plan.monthly_amount.set(0)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('monthly_amount'))
        .to eq("must be positive")
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("Donation").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .not_to include("estimated")
    end
  end

  it "can't edit plan if target date is in the current month" do
    g = Goal.create(title: "Relocation", allocated_amount: 800000,
                    target_date: Date.today, user: user)
    g.create_goal_plan(monthly_amount: 50000, target_amount: 800000)

    Page::Planning.visit

    Page::Planning.find_goal("Relocation").edit_plan
    expect(page).to have_no_selector("#action-panel")
    expect(page).to have_selector("#info-panel")
    expect(Page::Elements::InfoPanel.message)
      .to include("change the allocated amount or extend its target date")
  end
end
