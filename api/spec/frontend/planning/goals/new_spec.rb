require 'rails_helper'

RSpec.describe "New goals plan", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    Account.create(name: "Main account", account_type: 'bank', currency: huf,
                   current_balance: 7320000, credit: 0, in_plan: true, user: user)
    login_user
  end

  context "when both targets are set" do
    before(:each) do
      Goal.create(user: user, title: "Vacation", target_amount: 600000,
                  allocated_amount: 200000, target_date: Date.today + 5.months)

      Page::Planning.visit
    end

    it "calculates correctly the monthly amount" do
      Page::Planning.find_goal("Vacation").edit_plan do |plan|
        expect(plan.monthly_amount.amount).to eq("80000")
      end
    end

    it "creates a plan to reach the target" do
      Page::Planning.find_goal("Vacation").edit_plan do |plan|
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Vacation").on_track_amount.amount)
        .to eq("200000")
      expect(GoalPlan.first.monthly_amount).to eq(80000)
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("Vacation").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .not_to include("estimated")
    end
  end

  context "when only target amount is set" do
    before(:each) do
      Goal.create(title: "VR set", target_amount: 800000,
                  allocated_amount: 200000, user: user)

      Page::Planning.visit
    end

    it "updates the estimated target on changing the monthly amount" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(100000)
        expect(plan.target_date).to eq((Date.today + 6.months).as_json)
      end
    end

    it "creates a plan with estimated target date" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(100000)
        plan.submit
      end

      wait_for_action_panel_to_close

      estimated_date = Date.today + 6.months
      expect(Page::Planning.find_goal("VR set").target_date)
        .to eq(estimated_date.as_json)
      expect(GoalPlan.first.target_date).to eq(estimated_date)
    end

    it "shows error on negative monthly amount" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(-20000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error("monthly_amount")).to eq("must be positive")
    end

    it "shows error on zero monthly amount" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(0)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error("monthly_amount")).to eq("must be positive")
    end

    it "shows error when monthly amount exceeds the target" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(700000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error("monthly_amount"))
        .to eq("can't be more than 600000")
    end

    it "shows error at target date when monthly amount exceeds the target" do
      Page::Planning.find_goal("VR set").edit_plan do |plan|
        plan.monthly_amount.set(700000)
        expect(plan.target_date).to eq("Monthly amount exceeds target")
      end
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("VR set").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .to include("estimated")
    end
  end

  context "when only target date is set" do
    before (:each) do
      Goal.create(title: "Christmas gifts", allocated_amount: 100000,
                  target_date: Date.today + 7.months, user: user)
      past_goal = Goal.new(title: "Skiing", allocated_amount: 800000,
                           target_date: Date.today - 3.months, user: user)
      past_goal.save(validate: false)
      Goal.create(title: "Relocation", allocated_amount: 800000,
                  target_date: Date.today, user: user)

      Page::Planning.visit
    end

    it "updates the estimated target on changing the monthly amount" do
      Page::Planning.find_goal("Christmas gifts").edit_plan do |plan|
        plan.monthly_amount.set(12000)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Christmas gifts").target_amount.amount)
        .to eq("184000")
    end

    it "creates a plan with estimated target amount" do
      Page::Planning.find_goal("Christmas gifts").edit_plan do |plan|
        plan.monthly_amount.set(12000)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Christmas gifts").target_amount.amount)
        .to eq("184000")
      expect(GoalPlan.first.target_amount).to eq(184000)
    end

    it "shows error on negative monthly amount" do
      Page::Planning.find_goal("Christmas gifts").edit_plan do |plan|
        plan.monthly_amount.set(-20000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error("monthly_amount"))
        .to eq("must be positive")
    end

    it "shows error on zero monthly amount" do
      Page::Planning.find_goal("Christmas gifts").edit_plan do |plan|
        plan.monthly_amount.set(0)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error("monthly_amount"))
        .to eq("must be positive")
    end

    it "can't create plan if target date is in the past" do
      Page::Planning.find_goal("Skiing").edit_plan
      expect(page).to have_no_selector("#action-panel")
      expect(page).to have_selector("#info-panel")
      expect(Page::Elements::InfoPanel.message).to include("Can't edit plan")
    end

    it "can't create plan if target date is in the current month" do
      Page::Planning.find_goal("Relocation").edit_plan
      expect(page).to have_no_selector("#action-panel")
      expect(page).to have_selector("#info-panel")
      expect(Page::Elements::InfoPanel.message)
        .to include("change the allocated amount or extend its target date")
    end

    it "has the right estimation labels" do
      Page::Planning.find_goal("Christmas gifts").edit_plan
      expect(Page::Elements::ActionPanel.label("monthly_amount"))
        .not_to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_amount"))
        .to include("estimated")
      expect(Page::Elements::ActionPanel.label("target_date"))
        .not_to include("estimated")
    end
  end
end
