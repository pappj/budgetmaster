require 'rails_helper'

RSpec.describe "Show goal plans", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    login_user
  end

  it "shows if there is no plan" do
    Goal.create(title: "Kids school", target_amount: 400000, user: user)
    Page::Planning.visit

    on_track = Page::Planning.find_goal("Kids school").on_track_amount_label
    expect(on_track).to eq("No plan")
  end

  it "shows if there is no target amount" do
    Goal.create(title: "Kids school", target_date: 1.year.since, user: user)
    Page::Planning.visit

    expect(Page::Planning.find_goal("Kids school").target_amount)
      .to eq("No target")
  end

  it "shows if there is no target date" do
    Goal.create(title: "Kids school", target_amount: 480000, user: user)
    Page::Planning.visit

    expect(Page::Planning.find_goal("Kids school").target_date)
      .to eq("No target")
  end

  it "shows if the plan is obsolete" do
    g = Goal.create(title: "Kids school", target_amount: 400000, user: user)
    g.create_goal_plan(monthly_amount: 20000, target_date: 20.months.since,
                       obsolete: true)
    Page::Planning.visit

    on_track = Page::Planning.find_goal("Kids school").on_track_amount_label
    expect(on_track).to eq("Plan is obsolete")
  end

  it "doesn't show achieved goals" do
    Goal.create(title: "Roof repair", target_amount: 300000,
                allocated_amount: 300000, achieved: true, user: user)
    Page::Planning.visit

    expect{Page::Planning.find_goal("Roof repair")}
      .to raise_error(Capybara::ElementNotFound)
  end
end
