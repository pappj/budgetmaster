require 'rails_helper'

RSpec.describe "Goal allocation", js: true do
  include_context "user login"
  include_context "models"

  def visit_planning
    login_user
    Page::Planning.visit
  end

  before(:each) do
    Goal.create(title: "Roof repair", target_amount: 900000,
                allocated_amount: 300000, user: user)
    Goal.create(title: "Kids school", target_amount: 400000, user: user)
  end

  context "when the available amount is positive" do
    before(:each) do
      Account.create(name: "Raiffeisen", account_type: 'bank', currency: huf,
                     current_balance: 600000, credit: 0, in_plan: true, user: user)
      visit_planning
    end

    it "shows the right allocated amount" do
      expect(Page::Planning.find_goal("Roof repair").allocated_amount.amount)
        .to eq("300000")
    end

    it "shows zero if there is no allocation yet" do
      expect(Page::Planning.find_goal("Kids school").allocated_amount.amount)
        .to eq("0")
    end

    it "can create allocation" do
      Page::Planning.find_goal("Kids school").edit_allocation do |alloc|
        alloc.allocated_amount.set(120000)
        alloc.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Kids school").allocated_amount.amount)
        .to eq("120000")

      expect(Goal.find_by(title: "Kids school").allocated_amount).to eq(120000)
    end

    it "can modify current allocation" do
      Page::Planning.find_goal("Roof repair").edit_allocation do |alloc|
        alloc.allocated_amount.set(410000)
        alloc.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Roof repair").allocated_amount.amount)
        .to eq("410000")

      expect(Goal.find_by(title: "Roof repair").allocated_amount).to eq(410000)
    end

    it "updates the available planning amount" do
      expect(Page::Planning::Statusbar.find.money.amount).to eq("300000")
      Page::Planning.find_goal("Roof repair").edit_allocation do |alloc|
        alloc.allocated_amount.set(380000)
        alloc.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning::Statusbar.find.money.amount).to eq("220000")
    end

    it "can't allocate more than available" do
      Page::Planning.find_goal("Kids school").edit_allocation do |alloc|
        alloc.allocated_amount.set(420000)
        alloc.submit
      end

      expect(Page::Elements::ActionPanel.error('allocated_amount'))
        .to eq("can't be more than 300000")
    end

    it "shows error on negative allocation" do
      Page::Planning.find_goal("Kids school").edit_allocation do |alloc|
        alloc.allocated_amount.set(-7600)
        alloc.submit
      end

      expect(Page::Elements::ActionPanel.error('allocated_amount'))
        .to eq("can't be negative")
    end
  end

  context "when the available amount is negative" do
    before (:each) do
      Account.create(name: "Card", account_type: 'credit', currency: huf,
                     current_balance: 6000, credit: 400000, in_plan: true,
                     user: user)
      visit_planning
    end

    it "can't increase allocation" do
      Page::Planning.find_goal("Roof repair").edit_allocation do |alloc|
        alloc.allocated_amount.set(380000)
        alloc.submit
      end

      expect(Page::Elements::ActionPanel.error('allocated_amount'))
        .to eq("can't be more than 300000")
    end

    it "can decrease allocation" do
      Page::Planning.find_goal("Roof repair").edit_allocation do |alloc|
        alloc.allocated_amount.set(210000)
        alloc.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_goal("Roof repair").allocated_amount.amount)
        .to eq("210000")

      expect(Goal.find_by(title: "Roof repair").allocated_amount).to eq(210000)
    end

    it "shows the current allocation as maximum" do
      Page::Planning.find_goal("Roof repair").edit_allocation do |alloc|
        expect(alloc.max_allocation.amount).to eq('300000')
      end
    end

    it "shows zero maximum allocation if there is no allocation yet" do
      Page::Planning.find_goal("Kids school").edit_allocation do |alloc|
        expect(alloc.max_allocation.amount).to eq('0')
      end
    end
  end
end
