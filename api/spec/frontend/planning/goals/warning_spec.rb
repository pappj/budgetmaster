require 'rails_helper'

RSpec.describe "Goal planning warnings", js: true do
  include_context "user login"

  before(:each) do
    login_user
  end

  it "show if target is in the past" do
    g = Goal.new(user: user, title: "Birthday", target_amount: 40000,
                 target_date: Date.today - 1.month, allocated_amount: 0)
    g.save(validate: false)
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.get_item("Birthday"))
      .to include("reached target")
    goal = Page::Planning.find_goal("Birthday")
    expect(goal.title_warning?).to be(true)
    expect(goal.target_date_warning?).to be(true)
    expect(goal.target_date_label).to include("reached")
  end

  context "when there are targets" do
    before(:each) do
      Goal.create(user: user, title: "Vacation", target_amount: 400000,
                  allocated_amount: 100000)
    end

    it "show if there is no plan" do
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.get_item("Vacation"))
        .to include("no plan")
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.title_warning?).to be(true)
      expect(goal.on_track_warning?).to be(true)
      expect(goal.on_track_amount_label).to eq("No plan")
    end

    it "show if plan is obsolete" do
      Goal.find_by(title: "Vacation")
        .create_goal_plan(monthly_amount: 50000, target_date: 2.month.since,
                          obsolete: true)
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.get_item("Vacation"))
        .to include("obsolete plan")
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.title_warning?).to be(true)
      expect(goal.on_track_warning?).to be(true)
      expect(goal.on_track_amount_label).to eq("Plan is obsolete")
    end

    it "show if the goal is behind schedule" do
      Goal.find_by(title: "Vacation")
        .create_goal_plan(monthly_amount: 50000, target_date: 2.month.since)
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.get_item("Vacation"))
        .to include("behind schedule")
      goal = Page::Planning.find_goal("Vacation")
      expect(goal.title_warning?).to be(true)
      expect(goal.allocated_amount_warning?).to be(true)
    end

    it "show no warning, if goal is on schedule" do
      Goal.find_by(title: "Vacation")
        .create_goal_plan(monthly_amount: 150000,
                          target_date: 2.month.since)
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.empty?).to be(true)
      expect(Page::Planning.find_goal("Vacation").title_warning?).to be(false)
    end

    it "get updated, when a goal with a plan is deleted" do
      Goal.find_by(title: "Vacation")
        .create_goal_plan(monthly_amount: 150000,
                          target_date: 2.month.since)
      Page::Planning.visit
      Page::Navigate.to_basics

      Page::Goal.first.delete
      Page::Goal.new_goal do |goal|
        goal.title.set("Mortgage")
        goal.target_amount.set("8000000")
        select_date(goal.target_date, 14.year.since)
        goal.submit
      end

      wait_for_action_panel_to_close

      Page::Navigate.to_planning

      expect(Page::Elements::NotifPanel.get_item("Mortgage"))
        .to include("no plan")
    end
  end

  context "when there are no targets" do
    before(:each) do
      Goal.create(user: user, title: "Retirement", allocated_amount: 3420000)
    end

    it "show no warning, if there is no plan" do
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.empty?).to be(true)
      expect(Page::Planning.find_goal("Retirement").title_warning?)
        .to be(false)
    end

    it "show no warning, if plan is obsolete" do
      Goal.find_by(title: "Retirement").create_goal_plan(monthly_amount: 80000,
        target_amount: 15000000, obsolete: true)
      expect(GoalPlan.count).to be > 0
      Page::Planning.visit

      expect(Page::Elements::NotifPanel.empty?).to be(true)
      expect(Page::Planning.find_goal("Retirement").title_warning?)
        .to be(false)
    end
  end
end
