require 'rails_helper'

RSpec.describe "Expense planning warning", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    login_user

    cat = Category.create(name: "Food", user: user)
    cat.recurring_expenses.create(amount: 2300, frequency: 'monthly',
                                  due_date: Date.today)
    cat.recurring_expenses.create(amount: 4200, frequency: 'monthly',
                                  due_date: Date.today)
    cat.recurring_expenses.create(amount: 3100, frequency: 'monthly',
                                  due_date: Date.today)
    Account.create(name: "Sample", account_type: "bank", currency: huf,
                   current_balance: 145000, credit: 0, in_plan: true, user: user)
  end

  def acc
    Account.find_by(name: "Sample")
  end

  def cat
    Category.find_by(name: "Food")
  end

  it "shows if planned amount does not reach due amount" do
    ExpensePlan.create(amount: 5600, month: Date.today, category: cat)
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.get_item("Food")).to include("not covered")
    expect(Page::Planning.find_category("Food").warning?).to be(true)
  end

  it "shows if spent and planned amounts are less than due amount" do
    ExpensePlan.create(amount: 5600, month: Date.today, category: cat)
    Expense.create(account: acc, category: cat, amount: 1180, date: Date.today)
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.get_item("Food")).to include("not covered")
    expect(Page::Planning.find_category("Food").warning?).to be(true)
  end

  it "shows no warning if planned amount reaches due amount" do
    ExpensePlan.create(amount: 9600, month: Date.today, category: cat)
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.empty?).to be(true)
    expect(Page::Planning.find_category("Food").warning?).to be(false)
  end

  it "shows no warning if spent amount exceeds due amount" do
    Expense.create(account: acc, category: cat, amount: 3900, date: Date.today)
    Expense.create(account: acc, category: cat, amount: 7500, date: Date.today)
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.empty?).to be(true)
    expect(Page::Planning.find_category("Food").warning?).to be(false)
  end

  it "doesn't highlight future categories" do
    Page::Planning.visit

    expect(Page::Elements::NotifPanel.get_item("Food")).to include("not covered")
    expect(Page::Planning.find_category("Food").warning?).to be(true)

    Page::Planning.next_month
    expect(Page::Planning.find_category("Food").warning?).to be(false)
  end
end
