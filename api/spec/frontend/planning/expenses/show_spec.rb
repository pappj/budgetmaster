require 'rails_helper'

RSpec.describe "Show expense plans", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    cat = Category.create(user: user, name: "Food")
    cat.recurring_expenses.create(amount: 2500, frequency: 'monthly',
                                  due_date: Date.today)
    cat.recurring_expenses.create(amount: 3500, frequency: 'quarterly',
                                  due_date: Date.today)
    cat.expense_plans.create(amount: 4000, month: Date.today)
    cat.expense_plans.create(amount: 6000, month: Date.today + 1.month)
    cat2 = Category.create(user: user, name: "Sport")
    cat2.recurring_expenses.create(amount: 45000, frequency: 'yearly',
                                   due_date: Date.today + 2.months)

    acc = Account.create(user: user, name: "Sample", account_type: 'bank',
                         current_balance: 45000, credit: 0, in_plan: true,
                         currency: huf)
    cat.expenses.create(account: acc, amount: 4500, date: Date.today)
    cat.expenses.create(account: acc, amount: 2180, date: Date.today)
    login_user
    Page::Planning.visit
  end

  it "doesn't show expenses not due this month" do
    validate_due_amount("Sport", "0")
  end

  it "sums recurring_expenses due_amount in the current month" do
    validate_due_amount("Food", "6000")
  end

  it "shows quarterly expenses every 3 months" do
    2.times do
      Page::Planning.next_month
      validate_due_amount("Food", "2500")
    end

    Page::Planning.next_month
    validate_due_amount("Food", "6000")
  end

  it "shows yearly expenses every 12 months" do
    2.times{ Page::Planning.next_month }
    validate_due_amount("Sport", "45000")

    11.times do |i|
      Page::Planning.next_month
      validate_due_amount("Sport", "0")
    end

    Page::Planning.next_month
    validate_due_amount("Sport", "45000")
  end

  it "shows current month's allocation" do
    expect(Page::Planning.find_category("Food").allocated.amount).to eq("4000")
  end

  it "shows updates allocation on changing month" do
    Page::Planning.next_month
    expect(Page::Planning.find_category("Food").allocated.amount).to eq("6000")
  end

  it "shows zero for non-existing allocations" do
    expect(Page::Planning.find_category("Sport").allocated.amount).to eq("0")
  end

  it "can't go in the past" do
    expect(Page::Planning.previous_month_disabled?).to be(true)
  end

  it "shows how much is spent in the current month" do
    Page::Planning.find_category("Food").edit do |plan|
      expect(plan.spent_amount.amount).to eq("6680")
    end
  end

  it "shows zero spent amount if there are no expenses yet" do
    Page::Planning.find_category("Sport").edit do |plan|
      expect(plan.spent_amount.amount).to eq("0")
    end
  end

  it "doesn't show the spent amount in future months" do
    Page::Planning.next_month
    Page::Planning.find_category("Food").edit do |plan|
      expect{plan.spent_amount}.to raise_error(Capybara::ElementNotFound)
    end
  end

  it "shows the selected month" do
    date = Date.today + 1.month
    Page::Planning.next_month
    Page::Planning.find_category("Food").edit do |plan|
      expect(plan.month).to eq(date.strftime("%Y %B"))
    end
  end
end
