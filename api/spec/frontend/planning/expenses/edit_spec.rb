require 'rails_helper'

RSpec.describe "Edit expense plans", js: true do
  include_context "user login"
  include_context "models"

  before(:each) do
    cat = Category.create(name: "Entertainment", user: user)
    cat.recurring_expenses.create(amount: "7500", frequency: 'monthly',
                                  due_date: Date.today)
    cat.expense_plans.create(amount: 5000, month: Date.today)
    cat.expense_plans.create(amount: 10000, month: Date.today + 2.months)
    Category.create(name: "Bills", user: user)
  end

  context "when available amount is positive" do
    before(:each) do
      Account.create(name: "Cash", account_type: 'cash', currency: huf,
                     current_balance: 45000, credit: 0, in_plan: true, user: user)

      login_user
      Page::Planning.visit
    end

    it "creates allocation" do
      Page::Planning.find_category("Bills").edit do |plan|
        plan.allocation.set(12000)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_category("Bills").allocated.amount)
        .to eq("12000")
      plan = Category.find_by(name: "Bills").expense_plans.first
      expect(plan).not_to be(nil)
      expect(plan.amount).to eq(12000)
    end

    it "changes the allocated amount on save" do
      category = Page::Planning.find_category("Entertainment")
      expect(category.allocated.amount).to eq("5000")

      category.edit do |plan|
        plan.allocation.set(7500)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_category("Entertainment").allocated.amount)
        .to eq("7500")
    end

    it "shows error on negative amount" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        plan.allocation.set(-1000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('amount'))
        .to eq("can't be negative")
    end

    it "shows error on overallocation" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        plan.allocation.set(50000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('amount'))
        .to eq("can't be more than 35000")
    end

    it "updates the available amount on changing allocation" do
      Page::Planning.find_category("Bills").edit do |plan|
        plan.allocation.set(7500)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning::Statusbar.find.money.amount).to eq("22500")
    end

    it "shows the due amount for the given month" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        expect(plan.due_amount.amount).to eq("7500")
      end
    end

    it "shows the right maximum allocation amount" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        expect(plan.max_allocation.amount).to eq("35000")
      end
    end
  end

  context "when available amount is negative" do
    before(:each) do
      Account.create(name: "Card", account_type: 'credit', current_balance: 45000,
                     credit: 60000, currency: huf, in_plan: true, user: user)

      login_user
      Page::Planning.visit
    end

    it "shows zero as maximum allocation for non-existing plans" do
      Page::Planning.find_category("Bills").edit do |plan|
        expect(plan.max_allocation.amount).to eq("0")
      end
    end

    it "shows the current allocation as maximum" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        expect(plan.max_allocation.amount).to eq("5000")
      end
    end

    it "can't increase the current allocation" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        plan.allocation.set(6000)
        plan.submit
      end

      expect(Page::Elements::ActionPanel.error('amount'))
        .to eq("can't be more than 5000")
    end

    it "decreases the current allocation while the plan is still overallocated" do
      Page::Planning.find_category("Entertainment").edit do |plan|
        plan.allocation.set(4000)
        plan.submit
      end

      wait_for_action_panel_to_close

      expect(Page::Planning.find_category("Entertainment").allocated.amount)
        .to eq("4000")
    end
  end
end
